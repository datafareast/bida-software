export function isExternal(path) {
    return /^(https?:|mailto:|tel:)/.test(path);
}
export function parseIpToInt(ip) {
    var buf = ip.split('.');
    return ((parseInt(buf[0]) << 24) | (parseInt(buf[1]) << 16) | (parseInt(buf[2]) << 8) | parseInt(buf[3])) >>> 0;
}
