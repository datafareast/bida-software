import Cookies from './Cookies';
const manager={
    token: new Cookies('mall-admin'),
    language: new Cookies('mall-admin-language'),
    sidebar: new Cookies('mall-admin-sidebar'),
    size: new Cookies('mall-admin-size'),
};
export default manager;
