import axios from 'axios';
import { Message } from 'element-ui';
import store from '@/store';
import qs from 'qs';
import _ from "lodash";

const service = axios.create({
    baseURL: process.env.BASE_API,
    timeout: 500000
})

service.interceptors.request.use(
    config => {
        if (store.getters.token) {
            config.headers['Authorization'] = "Bearer " + store.getters.token;
        }
        //配置过滤空字串符参数
        config.paramsSerializer = function (params) {
            let params2 = _.cloneDeep(params);
            const removeEmpty = (obj) => {
                Object.keys(obj).forEach(k =>
                    (obj[k] && typeof obj[k] === 'object') && removeEmpty(obj[k]) ||
                    (!obj[k] && obj[k] !== undefined && obj[k] !== 0) && delete obj[k]
                );
                return obj;
            };
            removeEmpty(params2);
            return qs.stringify(params2, { allowDots: true })
        }
        return config;
    }, error => {
        Promise.reject(error);
    }
);

service.interceptors.response.use(
    response => {
        if (!response.data.flag) {
            if (response.data.code == 400) {
                store.dispatch("Logout");
            } else {
                Message({
                    message: response.data.msg,
                    type: 'error',
                    duration: 5 * 1000
                });
            }
        }

        return Promise.resolve(response);
    },
    error => {
        var msg = '';
        if (error.message.includes('timeout')) {   // 判断请求异常信息中是否含有超时timeout字符串
            msg = '网络超时';
        } else {
            msg = error.response.data.message || error.response.data.msg;
        }
        if (msg == 'SESSION_INVALID') {
            store.dispatch("Logout");
        }
        Message({
            message: msg,
            type: 'error',
            duration: 5 * 1000
        });
        return Promise.reject(error);

    }
)
export default service;