export function TreeDataTranslate(data,id='id',pid='parentId'){
    let res=[];
    let temp={};
    for(let i=0;i<data.length;i++){
        temp[data[i][id]]=data[i];
    }
    for(let k=0;k<data.length;k++){
        if(temp[data[k][pid]]&&data[k][id]!==data[k][pid]){
            if(!temp[data[k][pid]]['children']){
                temp[data[k][pid]]['children']=[];
            }
            if(!temp[data[k][pid]]['_level']){
                temp[data[k][pid]]['_level']=1;
            }
            data[k]['_level']=temp[data[k][pid]]._level+1;
            temp[data[k][pid]]['children'].push(data[k]);
        }else{
            res.push(data[k]);
        }
    }
    return res;
}


export function TreeDataTranslate2(data,level){
    if(data){
    for(let k=0;k<data.length;k++){
       data[k]["_level"]=level;
       if(data[k]["children"]&&data[k]["children"].length>0){
        TreeDataTranslate2(data[k]["children"],level+1);
       }
    }
}
}