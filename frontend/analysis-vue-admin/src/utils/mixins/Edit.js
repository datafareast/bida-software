import Validate from '@/utils/Validate';
import waves from "@/directive/waves";
import elDragDialog from '@/directive/el-dragDialog';
import {
    mapGetters
} from 'vuex';

export default {
    directives: { elDragDialog,waves },
    data(){
        return {
            isDisable: false,
            editFlag: false,
            dialogFormVisible: false,
            }
    },
    computed: {
        ...mapGetters([
            'auth'
        ]),

    },
    methods: {
        ...Validate,
        hasAuth(code) {
            return code != '';
            // return this.auth.some(a=>a===code);
        },
        cancel() {
            this.dialogFormVisible = false;
        },
        close(done) {
            this.resetFields();
            done();
        },
        resetFields(){
            this.$refs.dataForm.resetFields();
        },
        show(flag) {
            this.editFlag = flag;
            this.dialogFormVisible = true;
        },
        save() {
            this.isDisable = true
            setTimeout(() => {
                this.isDisable = false
            }, 1000);
            this.saveAction();
        },
        saveAction(){

        }
    }
}