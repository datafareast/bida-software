import waves from "@/directive/waves";
import formatDate from '@/utils/Date';
import {
    mapGetters
} from 'vuex';
export default {
    directives: {  waves },
    filters: {
        dateFormatter(time) {
            if(time==undefined){
                return "";
            }
            let date = new Date(time);
            return formatDate(date, 'yyyy-MM-dd HH:mm:ss');
        },
        shortDateFormatter(time) {
            if(time==undefined){
                return "";
            }
            let date = new Date(time);
            return formatDate(date, 'yyyy-MM-dd');
        },
        enabledFormatter(enabled) {
            if (enabled === 'T') {
                return "启用";
            } else {
                return "禁用";
            }
        }
    },
    data(){
        return {
            queryType:'simple',
            isDisable: false,
            list: [],
            selectionRows:[],
            total: 0,
            tableKey: 0,
            listLoading: true,
            dialogFormVisible: false,
            listQuery: {
                pn: 1,
                rn: 10,
                sort: "id",
                order: "desc",
            }
            }
    },
    computed: {
        ...mapGetters([
            'auth'
        ]),

    },
    methods: {
        changeSearch() {
            if (this.queryType == 'simple') {
                this.queryType = 'adv';
            } else {
                this.queryType = 'simple';
            }
        },
        hasAuth(code) {
            return code != '';
            // return this.auth.some(a=>a===code);
        },
        //新增
        add() {
            this.isDisable = true
            setTimeout(() => {
                this.isDisable = false
            }, 1000);
            this.showDialog();
        },
        //修改
        edit(row) {
            this.isDisable = true
            setTimeout(() => {
                this.isDisable = false
            }, 1000);
            this.showDialog(row.id);
        },
          //删除
        del(row) {
            this.isDisable = true
            setTimeout(() => {
                this.isDisable = false
            }, 1000);
            this.$confirm(this.$t("delPrompt"), this.$t("prompt"), {
                type: "warning"
            }).then(() => {
                const id = row.id;
                this.delAction(id);
            }).catch(() => {});
        },
        delAll() {
            if(this.selectionRows.length==0){
                this.$message.error(this.$t("mustSelectOne"));
                return;
            }
            this.isDisable = true
            setTimeout(() => {
                this.isDisable = false
            }, 1000);
            this.$confirm(this.$t("delAllPrompt"), this.$t("prompt"), {
                type: "warning"
            }).then(() => {
                let ids = this.selectionRows.map((item,index,arr)=>{
                    return item.id;
                });
                this.delAction(ids);
            }).catch(() => {});
        },
        //查
        query() {
            this.isDisable = true
            setTimeout(() => {
                this.isDisable = false
            }, 1000);
            //清除复杂查询
            if (this.queryType == 'simple') {
                //清理掉多余的
                this.clearQuery();
            }
            this.changeQuery();
            this.listQuery.pn = 1;
            this.getList();
        },
        showDialog(id) {
            this.dialogFormVisible = true;
            this.$nextTick(() => {
                this.$refs.edit.init(id);
            });
        },
        delAction(id){

        },
        clearQuery(){

        },
        changeQuery(){
            
        },
        getList(){

        },
        selectionChange(val) {
            this.selectionRows = val;
        },
         //排序
         sortChange(data) {
            const {
                prop,
                order
            } = data;
            this.listQuery.sort = prop;
            if (prop == null) {
                this.listQuery.order = null;
                this.listQuery.sort = null;
            } else {
                if (order === "ascending") {
                    this.listQuery.order = "asc";
                } else {
                    this.listQuery.order = "desc";
                }
            }
            this.query();
        }
    }
}