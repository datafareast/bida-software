/* eslint-disable no-useless-escape */
/* eslint-disable no-control-regex */

const Validate = {
    //加权因子
    validateIdNo(idNo) {
        const Wi = [7, 9, 10, 5, 8, 4, 2, 1, 6, 3, 7, 9, 10, 5, 8, 4, 2, 1];
        //身份证验证位值 10 代表X
        const ValideCode = [1, 0, 10, 9, 8, 7, 6, 5, 4, 3, 2];
        let sum = 0;
        let idNo1 = idNo;
        for (let i = 0; i < 17; i++) {
            //加权求和
            sum += Wi[i] * idNo1[i];
        }
        let position = sum % 11;
        return idNo1[17] === ValideCode[position] || (ValideCode[position] == 10 && idNo1[17].toLowerCase() == 'x');
    },
    //验证短日期
    isDate(str) {
        let value = str.match(/^(\d{1,4})(-|\/)?(\d{1,2})\2(\d{1,2})$/);
        if (value == null) {
            return false;
        } else {
            let date = new Date(value[1], value[3] - 1, value[4]);
            return date.getFullYear() == value[1] && date.getMonth() + 1 == value[3] && date.getDate() == value[4];
        }
    },
    isIdnoBirthday(idNo) {
        let datestr = idNo.substr(6, 8);
        return this.isDate(datestr);
    },
    isTime(str) {
        let value = str.match(/^(\d{1,2})(:)?(\d{1,2})\2(\d{1,2})$/);
        if (value == null) {
            return false;
        } else {
            //还要考虑负数，这个方法不是很完整
            if (value[1] > 23 || value[3] > 59 || value[4] > 59) {
                return false;
            } else {
                return true;
            }
        }
    },
    isFullDate(str) {
        let value = str.match(/^(?:19|20)[0-9][0-9]-(?:(?:0[1-9])|(?:1[0-2]))-(?:(?:[0-2][1-9])|(?:[1-3][0-1])) (?:(?:[0-2][0-3])|(?:[0-1][0-9])):[0-5][0-9]:[0-5][0-9]$/);
        if (value == null) {
            return false;
        } else {
            return true;
        }
    },
    //清除左空格
    ltrim(str) {
        return str.replace(/^s*/, '');
    },
    //清除右空格
    rtrim(str) {
        return str.replace(/\s*$/, '');
    },
    //清除两边空格
    trim(str) {
        return str.replace(/(^\s*)|(\s*$)/, '');
    },
    //是否邮箱
    isEmail(str) {
        let s = this.trim(str);
        let p = /^[_\.0-9a-z-]+@([0-9a-z][0-9a-z-]+\.){1,4}[a-z]{2,3}$/i;
        return p.test(s);
    },
    //是否颜色
    isColor(str) {
        let s = this.trim(str);
        if (s.length != 7) {
            return false;
        }
        return s.search(/#[a-fA-F0-9]{6}/) != -1;
    },
    //是否手机
    isMobile(str) {
        let s = this.trim(str);
        let p = /^1\d{10}$/;
        return p.test(s);
    },
    //是否手机前缀
    isMobilePrefix(str) {
        let s = this.trim(str);
        let p = /^1\d{6}$/;
        return p.test(s);
    },
    //是否url
    isUrl(str) {
        let s = this.trim(str).toLowerCase();
        let p = /^http:\/\/[A-Za-z0-9]+\.[A-Za-z0-9]+[\/=\?%\-&_~`@[\]\/':+!]*([^<>\"\"])*$/;
        return p.test(s);
    },
    //是否电话
    isTel(str) {
        let s = this.trim(str);
        let p = /^((\(\d{3}\))|(\d{3}\-))?(\(0\d{2,3}\)|\d{2,3}-)?[1-9]\d{6,7}$/;
        return p.test(s);
    },
    //是否邮编
    isZip(str) {
        let s = this.trim(str);
        let p = /^[1-9]\d{5}$/;
        return p.test(s);
    },
    //是否区号
    isAreaCode(str) {
        let s = this.trim(str);
        let p = /^\d{4}$/;
        return p.test(s);
    },
    isDecimal(str) {
        let s = this.trim(str);
        let p = /^[-\+]\d+$/;
        return p.test(s);
    },
    isInteger(str) {
        let s = this.trim(str);
        let p = /^[-\+]?\d$/;
        return p.test(s);
    },
    isMoney(str) {
        str = str + '';
        let s = this.trim(str);
        let p = /(^[1-9]([0-9]+)?(\.[0-9]{1,2})?$)|(^(0){1}$)|(^[0-9]\.[0-9]([0-9])?$)/;
        return p.test(s);
    },
    isEnglish(str) {
        let s = this.trim(str);
        let p = /[A-Za-z]+$/;
        return p.test(s);
    },
    isChinese(str) {
        let s = this.trim(str);
        let p = /^[\u391-\uFFE5]+$/;
        return p.test(s);
    },
    isChineseBlank(str) {
        let s = this.trim(str);
        let p = /^[\u391-\uFFE5\s]+$/;
        return p.test(s);
    },
    isEnglishDotNumber(str) {
        let s = this.trim(str);
        let p = /^([A-Za-z0-9]+(\.[A-Za-z0-9]+)?)+$/;
        return p.test(s);
    },
    isCode(str) {
        let s = this.trim(str);
        let p = /^[A-Za-z0-9_]+$/;
        return p.test(s);
    },
    isUpperCaseCode(str) {
        let s = this.trim(str);
        let p = /^[A-Z0-9_]+$/;
        return p.test(s);
    },

    isUnicode(str) {
        let s = this.trim(str);
        let p = /^[\x00-\xff]$/;
        return p.test(s);
    },
    isIdno(str) {
        let s = this.trim(str);
        let p = /^[1-9]\d{5}[1-9]\d{3}((0\d)|(1[0-2]))(([0|1|2]\d)|3[0-1])((\d{4})|\d{3}[xX])$/;
        if (p.test(s)) {
            return this.isIdnoBirthday(s) && this.validateIdNo(s);
        } else {
            return false;
        }
    },
    isIp(str) {
        let s = this.trim(str);
        let p = /^(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])\.(\d{1,2}|1\d\d|2[0-4]\d|25[0-5])$/;
        return p.test(s);
    },
};

const ValidateTools = {
    emailValidator(rule, value, callback) {
        if (value == '' || value == undefined) {
            callback();
        } else {
            if (Validate.isEmail(value)) {
                callback();
            } else {
                callback(new Error(this.$t('invalidate.email')));
            }
        }
    },
    urlValidator(rule, value, callback) {
        if (value == '' || value == undefined) {
            callback();
        } else {
            if (Validate.isUrl(value)) {
                callback();
            } else {
                callback(new Error(this.$t('invalidate.url')));
            }
        }
    },
    idNoValidator(rule, value, callback) {
        if (value == '' || value == undefined) {
            callback();
        } else {
            if (Validate.isIdno(value)) {
                callback();
            } else {
                callback(new Error(this.$t('invalidate.idno')));
            }
        }
    },
    moneyValidator(rule, value, callback) {
        if (value == '' || value == undefined) {
            callback();
        } else {
            if (Validate.isMoney(value)) {
                callback();
            } else {
                callback(new Error(this.$t('invalidate.money')));
            }
        }
    },
    telValidator(rule, value, callback) {
        if (value == '' || value == undefined) {
            callback();
        } else {
            if (Validate.isTel(value)) {
                callback();
            } else {
                callback(new Error(this.$t('invalidate.tel')));
            }
        }
    },
    mobilePrefixValidator(rule, value, callback) {
        if (value == '' || value == undefined) {
            callback();
        } else {
            if (Validate.isMobilePrefix(value)) {
                callback();
            } else {
                callback(new Error(this.$t('invalidate.mobilePrefix')));
            }
        }
    },
    areaCodeValidator(rule, value, callback) {
        if (value == '' || value == undefined) {
            callback();
        } else {
            if (Validate.isAreaCode(value)) {
                callback();
            } else {
                callback(new Error(this.$t('invalidate.areaCode')));
            }
        }
    },
    mobileValidator(rule, value, callback) {
        if (value == '' || value == undefined) {
            callback();
        } else {
            if (Validate.isMobile(value)) {
                callback();
            } else {
                callback(new Error(this.$t('invalidate.mobile')));
            }
        }
    },
    ipValidator(rule, value, callback) {
        if (value == '' || value == undefined) {
            callback();
        } else {
            if (Validate.isIp(value)) {
                callback();
            } else {
                callback(new Error(this.$t('invalidate.ip')));
            }
        }
    },
    strValidator(rule, value, callback) {
        if (value == '' || value == undefined) {
            callback();
        } else {
            if (rule.min && rule.max) {
                if (value.length < rule.min || value.length > rule.max) {
                    callback(
                        new Error(
                            this.$t('invalidate.str')
                                .replace('%min', rule.min + '')
                                .replace('%max', rule.max + '')
                        )
                    );
                } else {
                    callback();
                }
            } else if (rule.min) {
                if (value.length < rule.min) {
                    callback(new Error(this.$t('invalidate.strmin').replace('%min', rule.min + '')));
                } else {
                    callback();
                }
            } else if (rule.max) {
                if (value.length > rule.max) {
                    console.log(value.length);
                    callback(new Error(this.$t('invalidate.strmax').replace('%max', rule.max + '')));
                } else {
                    callback();
                }
            } else {
                callback();
            }
        }
    },
    propertiesValidator(rule, value, callback) {
        if (value == '' || value == undefined) {
            callback();
        } else {
            if (Validate.isEnglishDotNumber(value)) {
                this.strValidator(rule, value, callback);
            } else {
                callback(new Error(this.$t('invalidate.propertiesFormat')));
            }
        }
    },
    codeValidator(rule, value, callback) {
        if (value == '' || value == undefined) {
            callback();
        } else {
            if (Validate.isCode(value)) {
                this.strValidator(rule, value, callback);
            } else {
                callback(new Error(this.$t('invalidate.codeFormat')));
            }
        }
    },
    upperCaseCodeValidator(rule, value, callback) {
        if (value == '' || value == undefined) {
            callback();
        } else {
            if (Validate.isUpperCaseCode(value)) {
                this.strValidator(rule, value, callback);
            } else {
                callback(new Error(this.$t('invalidate.codeFormat')));
            }
        }
    },
};
export default ValidateTools;
