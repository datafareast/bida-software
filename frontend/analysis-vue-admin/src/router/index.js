import Vue from 'vue';
import Router from 'vue-router';
import Layout from '@/components/Layout';

Vue.use(Router);

export const routerMap = [
    {
        path: '/login',
        component: () => import('@/views/Login'),
        hidden: true,
    },
    {
        path: '',
        component: Layout,
        redirect: 'index',
        children: [
            {
                path: 'index',
                component: () => import('@/views/Index'),
                name: 'index',
                icon: 'icon-index',
                noCache: true,
            },
        ],
    },
];
export const asyncRouterMap = {
    layout: Layout,
    area: () => import('@/views/pages/Area'),
    auth: () => import('@/views/pages/Auth'),
    cms: () => import('@/views/pages/Cms'),
    dataTopo: () => import('@/views/pages/DataTopo'),
    dataIndustry: () => import('@/views/pages/DataIndustry'),
    dataType: () => import('@/views/pages/DataType'),
    file: () => import('@/views/pages/File'),
    folder: () => import('@/views/pages/Folder'),
    i18n: () => import('@/views/pages/I18n'),
    icon: () => import('@/views/pages/Icon'),
    ip: () => import('@/views/pages/Ip'),
    indexType: () => import('@/views/pages/IndexType'),
    industry: () => import('@/views/pages/Industry'),
    industryIndex: () => import('@/views/pages/IndustryIndex'),
    linkCat: () => import('@/views/pages/LinkCat'),
    linkItem: () => import('@/views/pages/LinkItem'),
    menu: () => import('@/views/pages/Menu'),
    role: () => import('@/views/pages/Role'),
    roleAuth: () => import('@/views/pages/RoleAuth'),
    roleMenu: () => import('@/views/pages/RoleMenu'),
    slide: () => import('@/views/pages/Slide'),
    slideItem: () => import('@/views/pages/SlideItem'),
    sysAuth: () => import('@/views/pages/SysAuth'),
    sysData: () => import('@/views/pages/SysData'),
    sysFile: () => import('@/views/pages/SysFile'),
    sysFolder: () => import('@/views/pages/SysFolder'),
    sysI18n: () => import('@/views/pages/SysI18n'),
    sysMenu: () => import('@/views/pages/SysMenu'),
    sysOption: () => import('@/views/pages/SysOption'),
    sysRole: () => import('@/views/pages/SysRole'),
    sysRoleAuth: () => import('@/views/pages/SysRoleAuth'),
    sysRoleMenu: () => import('@/views/pages/SysRoleMenu'),
    sysUser: () => import('@/views/pages/SysUser'),
    sysUserInfo: () => import('@/views/pages/SysUserInfo'),
    sysUserLog: () => import('@/views/pages/SysUserLog'),
    sysUserLoginLog: () => import('@/views/pages/SysUserLoginLog'),
    sysUserRole: () => import('@/views/pages/SysUserRole'),
    tableId: () => import('@/views/pages/TableId'),
    upgrade: () => import('@/views/pages/Upgrade'),
    user: () => import('@/views/pages/User'),
    userInfo: () => import('@/views/pages/UserInfo'),
    userLog: () => import('@/views/pages/UserLog'),
    userLoginLog: () => import('@/views/pages/UserLoginLog'),
    userRole: () => import('@/views/pages/UserRole'),
    dataExtract: () => import('@/views/pages/DataExtract'),
    dataSource: () => import('@/views/pages/DataSource'),
    imitatePosition: () => import('@/views/pages/ImitatePosition'),
    imitatePositionItem: () => import('@/views/pages/ImitatePositionItem'),
    researchReport: () => import('@/views/pages/ResearchReport'),
};
export default new Router({
    mode: 'history',
    scrollBehavior: () => ({ y: 0 }),
    routes: routerMap,
});
