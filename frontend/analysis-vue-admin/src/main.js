// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import Cookies from 'js-cookie';
import 'normalize.css/normalize.css';
import Element from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import '@/assets/scss/common/index.scss';
import '@/assets/iconfont/iconfont.css';
import App from '@/App';
import router from '@/router';
import '@/router/interceptor';
import store from '@/store';
import { i18n, messages } from '@/i18n';
import sysI18nApi from '@/api/SysI18nApi';

Vue.use(Element, {
    size: Cookies.get('size') || 'medium',
    i18n: (key, value) => i18n.t(key, value),
});
Vue.config.productionTip = false;
if (process.env.NODE_ENV == 'development') {
    Vue.config.devtools = true;
} else {
    Vue.config.devtools = false;
}
/* eslint-disable no-new */
sysI18nApi.findAll().then(({ data }) => {
    if (data.obj) {
        Object.assign(messages.zh, data.obj.zh);
        Object.assign(messages.en, data.obj.en);
    }
    new Vue({
        el: '#app',
        router,
        store,
        i18n,
        render: (h) => h(App),
    });
});
