import types from './mutation-type';
const mutations = {
    [types.MENU_CAT]: (state, menuCats) => {
        state.menuCats = menuCats;
    },
    [types.ICON_CAT]: (state, iconCats) => {
        state.iconCats = iconCats;
    },
    [types.OPTION_CAT]: (state, optionCats) => {
        state.optionCats = optionCats;
    },
    [types.INPUT_TYPE]: (state, inputTypes) => {
        state.inputTypes = inputTypes;
    },
    [types.ENABLED]: (state, enableds) => {
        state.enableds = enableds;
    },
    [types.SYS_ROLE]: (state, sysRoles) => {
        state.sysRoles = sysRoles;
    },
    [types.AREA_CAT]: (state, areaCats) => {
        state.areaCats = areaCats;
    },
    [types.VILLAGE_CODE]: (state, villageCodes) => {
        state.villageCodes = villageCodes;
    },
    [types.TOP_CAT]: (state, topCats) => {
        state.topCats = topCats;
    },
    [types.LEVEL]: (state, levels) => {
        state.levels = levels;
    },
    [types.IS_SHOW]: (state, isShows) => {
        state.isShows = isShows;
    },
    [types.DATA_INDUSTRY]: (state, dataIndustries) => {
        state.dataIndustries = dataIndustries;
    },
    [types.DB_TYPE]: (state, dbTypes) => {
        state.dbTypes = dbTypes;
    },
    [types.DATA_SOURCE]: (state, dataSources) => {
        state.dataSources = dataSources;
    },
};
export default mutations;
