import actions from './actions';
import getters from './getters';
import state from './state';
import mutations from './mutations';

const biz = {
    // namespace:true 如果用全局命名空间，嵌套模块太复杂了
    actions,
    getters,
    state,
    mutations,
};
export default biz;
