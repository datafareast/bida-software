import types from './mutation-type';
import sysDataApi from '@/api/SysDataApi';
import sysRoleApi from '@/api/SysRoleApi';
import dataIndustryApi from '@/api/DataIndustryApi';
import dataSourceApi from '@/api/DataSourceApi';

const actions = {
    menuCat({ dispatch, state }) {
        return new Promise((resolve, reject) => {
            if (state.menuCats.length == 0) {
                dispatch('reloadMenuCat').then((data) => {
                    resolve(data);
                });
            } else {
                resolve(state.menuCats);
            }
        });
    },
    reloadMenuCat({ commit }) {
        return new Promise((resolve, reject) => {
            sysDataApi.findMenuCat().then(({ data }) => {
                commit(types.MENU_CAT, data.obj);
                resolve(data.obj);
            });
        });
    },
    iconCat({ dispatch, state }) {
        return new Promise((resolve, reject) => {
            if (state.iconCats.length == 0) {
                dispatch('reloadIconCat').then((data) => {
                    resolve(data);
                });
            } else {
                resolve(state.iconCats);
            }
        });
    },
    reloadIconCat({ commit }) {
        return new Promise((resolve, reject) => {
            sysDataApi.findIconCat().then(({ data }) => {
                commit(types.ICON_CAT, data.obj);
                resolve(data.obj);
            });
        });
    },
    enabled({ dispatch, state }) {
        return new Promise((resolve, reject) => {
            if (state.enableds.length == 0) {
                dispatch('reloadEnabled').then((data) => {
                    resolve(data);
                });
            } else {
                resolve(state.enableds);
            }
        });
    },
    reloadEnabled({ commit }) {
        return new Promise((resolve, reject) => {
            sysDataApi.findEnabled().then(({ data }) => {
                commit(types.ENABLED, data.obj);
                resolve(data.obj);
            });
        });
    },
    dbType({ dispatch, state }) {
        return new Promise((resolve, reject) => {
            if (state.dbTypes.length == 0) {
                dispatch('reloadDbType').then((data) => {
                    resolve(data);
                });
            } else {
                resolve(state.dbTypes);
            }
        });
    },
    reloadDbType({ commit }) {
        return new Promise((resolve, reject) => {
            sysDataApi.findDbType().then(({ data }) => {
                commit(types.DB_TYPE, data.obj);
                resolve(data.obj);
            });
        });
    },
    optionCat({ dispatch, state }) {
        return new Promise((resolve, reject) => {
            if (state.optionCats.length == 0) {
                dispatch('reloadOptionCat').then((data) => {
                    resolve(data);
                });
            } else {
                resolve(state.optionCats);
            }
        });
    },
    reloadOptionCat({ commit }) {
        return new Promise((resolve, reject) => {
            sysDataApi.findOptionCat().then(({ data }) => {
                commit(types.OPTION_CAT, data.obj);
                resolve(data.obj);
            });
        });
    },
    inputType({ dispatch, state }) {
        return new Promise((resolve, reject) => {
            if (state.inputTypes.length == 0) {
                dispatch('reloadInputType').then((data) => {
                    resolve(data);
                });
            } else {
                resolve(state.inputTypes);
            }
        });
    },
    reloadInputType({ commit }) {
        return new Promise((resolve, reject) => {
            sysDataApi.findInputType().then(({ data }) => {
                commit(types.INPUT_TYPE, data.obj);
                resolve(data.obj);
            });
        });
    },
    sysRole({ dispatch, state }) {
        return new Promise((resolve, reject) => {
            if (state.sysRoles.length == 0) {
                dispatch('reloadSysRole').then((data) => {
                    resolve(data);
                });
            } else {
                resolve(state.sysRoles);
            }
        });
    },
    reloadSysRole({ commit }) {
        return new Promise((resolve, reject) => {
            sysRoleApi.findAll().then(({ data }) => {
                commit(types.SYS_ROLE, data.obj);
                resolve(data.obj);
            });
        });
    },
    areaCat({ dispatch, state }) {
        return new Promise((resolve, reject) => {
            if (state.areaCats.length == 0) {
                dispatch('reloadAreaCat').then((data) => {
                    resolve(data);
                });
            } else {
                resolve(state.areaCats);
            }
        });
    },
    reloadAreaCat({ commit }) {
        return new Promise((resolve, reject) => {
            sysDataApi.findAreaCat().then(({ data }) => {
                commit(types.AREA_CAT, data.obj);
                resolve(data.obj);
            });
        });
    },
    villageCode({ dispatch, state }) {
        return new Promise((resolve, reject) => {
            if (state.villageCodes.length == 0) {
                dispatch('reloadVillageCode').then((data) => {
                    resolve(data);
                });
            } else {
                resolve(state.villageCodes);
            }
        });
    },
    reloadVillageCode({ commit }) {
        return new Promise((resolve, reject) => {
            sysDataApi.findVillageCode().then(({ data }) => {
                commit(types.VILLAGE_CODE, data.obj);
                resolve(data.obj);
            });
        });
    },
    topCat({ dispatch, state }) {
        return new Promise((resolve, reject) => {
            if (state.topCats.length == 0) {
                dispatch('reloadTopCat').then((data) => {
                    resolve(data);
                });
            } else {
                resolve(state.topCats);
            }
        });
    },
    reloadTopCat({ commit }) {
        return new Promise((resolve, reject) => {
            sysDataApi.findTopCat().then(({ data }) => {
                commit(types.TOP_CAT, data.obj);
                resolve(data.obj);
            });
        });
    },
    level({ dispatch, state }) {
        return new Promise((resolve, reject) => {
            if (state.levels.length == 0) {
                dispatch('reloadLevel').then((data) => {
                    resolve(data);
                });
            } else {
                resolve(state.levels);
            }
        });
    },
    reloadLevel({ commit }) {
        return new Promise((resolve, reject) => {
            sysDataApi.findLevel().then(({ data }) => {
                commit(types.LEVEL, data.obj);
                resolve(data.obj);
            });
        });
    },
    isShow({ dispatch, state }) {
        return new Promise((resolve, reject) => {
            if (state.isShows.length == 0) {
                dispatch('reloadIsShow').then((data) => {
                    resolve(data);
                });
            } else {
                resolve(state.isShows);
            }
        });
    },
    reloadIsShow({ commit }) {
        return new Promise((resolve, reject) => {
            sysDataApi.findIsShow().then(({ data }) => {
                commit(types.IS_SHOW, data.obj);
                resolve(data.obj);
            });
        });
    },
    dataIndustry({ dispatch, state }) {
        return new Promise((resolve, reject) => {
            if (state.dataIndustries.length == 0) {
                dispatch('reloadDataIndustry').then((data) => {
                    resolve(data);
                });
            } else {
                resolve(state.dataIndustries);
            }
        });
    },
    reloadDataIndustry({ commit }) {
        return new Promise((resolve, reject) => {
            dataIndustryApi.findAll().then(({ data }) => {
                commit(types.DATA_INDUSTRY, data.obj);
                resolve(data.obj);
            });
        });
    },
    dataSource({ dispatch, state }) {
        return new Promise((resolve, reject) => {
            if (state.dataSources.length == 0) {
                dispatch('reloadDataSource').then((data) => {
                    resolve(data);
                });
            } else {
                resolve(state.dataSources);
            }
        });
    },
    reloadDataSource({ commit }) {
        return new Promise((resolve, reject) => {
            dataSourceApi.findAll().then(({ data }) => {
                commit(types.DATA_SOURCE, data.obj);
                resolve(data.obj);
            });
        });
    },
};
export default actions;
