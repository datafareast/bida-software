const getters = {
    menuCats: (state) => state.menuCats,
    iconCats: (state) => state.iconCats,
    enableds: (state) => state.enableds,
    sysRoles: (state) => state.sysRoles,
    optionCats: (state) => state.optionCats,
    inputTypes: (state) => state.inputTypes,
    areaCats: (state) => state.areaCats,
    villageCodes: (state) => state.villageCodes,
    topCats: (state) => state.topCats,
    levels: (state) => state.levels,
    isShows: (state) => state.isShows,
    dataIndustries: (state) => state.dataIndustries,
    dbTypes: (state) => state.dbTypes,
    dataSources: (state) => state.dataSources,
};
export default getters;
