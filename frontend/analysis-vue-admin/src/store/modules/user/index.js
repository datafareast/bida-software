import actions from './actions';
import getters from './getters';
import state from './state';
import mutations from './mutations';
const user = {
    //默认全局命名空间,如果用嵌套模块，太复杂了
    //namespace:true,
    actions,
    getters,
    state,
    mutations,
};
export default user;
