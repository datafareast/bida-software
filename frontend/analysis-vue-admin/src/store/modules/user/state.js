import cookie from '@/utils/CookieManager';

const state = {
    user: '',
    token: cookie.token.get(),
    //存放角色
    role: '',
    //存放头像
    headImg: '',
    //新增加的路由
    router: [],
    //完整路由，当菜单用
    fullRouter: [],
    //按钮权限
    auth: [],
};

export default state;
