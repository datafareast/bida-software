import types from './mutation-type';
import cookie from '@/utils/CookieManager';
const mutations = {
    //两种写法
    //1.匿名函数写法
    [types.TOGGLE_SIDEBAR]: (state) => {
        state.sidebar.opened = !state.sidebar.opened;
        state.sidebar.withoutAnimation = false;
        if (state.sidebar.opened) {
            cookie.sidebar.set(1);
        } else {
            cookie.sidebar.set(0);
        }
    },
    //直接函数写法
    [types.CLOSE_SIDEBAR](state, withoutAnimation) {
        cookie.sidebar.set(0);
        state.sidebar.opened = false;
        state.sidebar.withoutAnimation = withoutAnimation;
    },
    [types.TOGGLE_DEVICE]: (state, device) => {
        state.device = device;
    },
    [types.SET_LANGUAGE]: (state, language) => {
        state.language = language;
        cookie.language.set(language);
    },
    [types.SET_SIZE]: (state, size) => {
        state.size = size;
        cookie.size.set(size);
    },
};
export default mutations;
