const getters = {
    sidebar: (state) => state.sidebar,
    language: (state) => state.language,
    size: (state) => state.size,
    device: (state) => state.device,
};
export default getters;
