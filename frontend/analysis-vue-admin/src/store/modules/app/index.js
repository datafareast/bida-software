import actions from './actions';
import getters from './getters';
import state from './state';
import mutations from './mutations';
const app = {
    actions,
    getters,
    state,
    mutations,
};
export default app;
