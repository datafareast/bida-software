import types from './mutation-type';
const actions ={
    toggleSidebar({commit}){
        commit(types.TOGGLE_SIDEBAR);
    },
    closeSidebar({commit},{withoutAnimation}){
        commit(types.CLOSE_SIDEBAR,withoutAnimation);
    },
    toggleDevice({commit},device){
        commit(types.TOGGLE_DEVICE,device);
    },
    setLanguage({commit},language){
        commit(types.SET_LANGUAGE,language);
    },
    setSize({commit},size){
        commit(types.SET_SIZE,size);
    }

};

export default actions;