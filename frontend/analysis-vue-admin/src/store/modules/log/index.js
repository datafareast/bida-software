import actions from './actions';
import getters from './getters';
import state from './state';
import mutations from './mutations';
const log = {
    actions,
    getters,
    state,
    mutations,
};
export default log;
