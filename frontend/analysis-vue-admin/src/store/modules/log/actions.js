import types from './mutation-type';
const actions = {
    addErrorLog({ commit }, log) {
        commit(types.ADD_ERROR_LOG, log);
    },
};
export default actions;
