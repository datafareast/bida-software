import types from './mutation-type';
const actions = {
    addView({ dispatch }, view) {
        dispatch('addVisitedView', view);
        dispatch('addCachedView', view);
    },
    addVisitedView({ commit }, view) {
        commit(types.ADD_VISITED_VIEW, view);
    },
    addCachedView({ commit }, view) {
        commit(types.ADD_CACHED_VIEW, view);
    },
    delView({ dispatch, state }, view) {
        return new Promise((resolve) => {
            dispatch('delVisitedView', view);
            dispatch('delCachedView', view);
            resolve({
                visitedViews: [...state.visitedViews],
                cachedViews: [...state.cachedViews],
            });
        });
    },
    delVisitedView({ commit, state }, view) {
        return new Promise((resolve) => {
            commit(types.DEL_VISITED_VIEW, view);
            resolve([...state.visitedViews]);
        });
    },
    delCachedView({ commit, state }, view) {
        return new Promise((resolve) => {
            commit(types.DEL_CACHED_VIEW, view);
            resolve([...state.cachedViews]);
        });
    },
    delOthersViews({ dispatch, state }, view) {
        return new Promise((resolve) => {
            dispatch('delOthersVisitedViews', view);
            dispatch('delOthersCachedViews', view);
            resolve({
                visitedViews: [...state.visitedViews],
                cachedViews: [...state.cachedViews],
            });
        });
    },
    delOthersVisitedViews({ commit, state }, view) {
        return new Promise((resolve) => {
            commit(types.DEL_OTHERS_VISITED_VIEWS, view);
            resolve([...state.visitedViews]);
        });
    },
    delOthersCachedViews({ commit, state }, view) {
        return new Promise((resolve) => {
            commit(types.DEL_OTHERS_CACHED_VIEWS, view);
            resolve([...state.cachedViews]);
        });
    },
    delAllViews({ dispatch, state }, view) {
        return new Promise((resolve) => {
            dispatch('delAllVisitedViews', view);
            dispatch('delAllCachedViews', view);
            resolve({
                visitedViews: [...state.visitedViews],
                cachedViews: [...state.cachedViews],
            });
        });
    },
    delAllVisitedViews({ commit, state }) {
        return new Promise((resolve) => {
            commit(types.DEL_ALL_VISITED_VIEWS);
            resolve([...state.visitedViews]);
        });
    },
    delAllCachedViews({ commit, state }) {
        return new Promise((resolve) => {
            commit(types.DEL_ALL_CACHED_VIEWS);
            resolve([...state.cachedViews]);
        });
    },
    updateVisitedView({ commit }, view) {
        commit(types.UPDATE_VISITED_VIEW, view);
    },
};
export default actions;
