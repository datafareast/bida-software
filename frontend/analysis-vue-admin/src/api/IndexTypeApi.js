import BaseApi from './BaseApi';
class IndexTypeApi extends BaseApi {
    constructor(path) {
        super(path);
    }
}
export default new IndexTypeApi('/indexType');
