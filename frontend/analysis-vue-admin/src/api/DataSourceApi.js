import BaseApi from './BaseApi';
class DataSourceApi extends BaseApi {
    constructor(path) {
        super(path);
    }
}
export default new DataSourceApi('/dataSource');
