import BaseApi from './BaseApi';
class I18nApi extends BaseApi {
    constructor(path) {
        super(path);
    }
}
export default new I18nApi('/i18n');
