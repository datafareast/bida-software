import BaseApi from './BaseApi';
class UserRoleApi extends BaseApi {
    constructor(path) {
        super(path);
    }
}
export default new UserRoleApi('/userRole');
