import BaseApi from './BaseApi';
class UpgradeApi extends BaseApi {
    constructor(path) {
        super(path);
    }
}
export default new UpgradeApi('/upgrade');
