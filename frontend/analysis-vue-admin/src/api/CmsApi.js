import BaseApi from './BaseApi';
class CmsApi extends BaseApi {
    constructor(path) {
        super(path);
    }
}
export default new CmsApi('/cms');
