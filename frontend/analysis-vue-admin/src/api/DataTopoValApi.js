import BaseApi from './BaseApi';
class DataTopoValApi extends BaseApi {
    constructor(path) {
        super(path);
    }
}
export default new DataTopoValApi('/dataTopoVal');
