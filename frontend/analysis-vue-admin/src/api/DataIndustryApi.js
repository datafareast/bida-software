import BaseApi from './BaseApi';
class DataIndustryApi extends BaseApi {
    constructor(path) {
        super(path);
    }
}
export default new DataIndustryApi('/dataIndustry');
