import BaseApi from './BaseApi';
class AuthApi extends BaseApi {
    constructor(path) {
        super(path);
    }
}
export default new AuthApi('/auth');
