import BaseApi from './BaseApi';
class IndustryApi extends BaseApi {
    constructor(path) {
        super(path);
    }
}
export default new IndustryApi('/industry');
