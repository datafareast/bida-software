import BaseApi from './BaseApi';
class SysRoleAuthApi extends BaseApi {
    constructor(path) {
        super(path);
    }
}
export default new SysRoleAuthApi('/sysRoleAuth');
