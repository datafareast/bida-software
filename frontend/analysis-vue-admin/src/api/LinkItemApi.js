import BaseApi from './BaseApi';
class LinkItemApi extends BaseApi {
    constructor(path) {
        super(path);
    }
}
export default new LinkItemApi('/linkItem');
