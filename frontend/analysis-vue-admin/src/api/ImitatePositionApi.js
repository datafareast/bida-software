import BaseApi from './BaseApi';
class ImitatePositionApi extends BaseApi {
    constructor(path) {
        super(path);
    }
}
export default new ImitatePositionApi('/imitatePosition');
