import BaseApi from './BaseApi';
import request from '@/utils/Request';
class DataExtractApi extends BaseApi {
    constructor(path) {
        super(path);
    }
    preview(dataSourceId, query) {
        return request({
            url: this.path + '/preview',
            method: 'get',
            params: { dataSourceId, query },
        });
    }
}
export default new DataExtractApi('/dataExtract');
