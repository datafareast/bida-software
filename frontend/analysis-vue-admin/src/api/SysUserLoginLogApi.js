import BaseApi from './BaseApi';
class SysUserLoginLogApi extends BaseApi {
    constructor(path) {
        super(path);
    }
}
export default new SysUserLoginLogApi('/sysUserLoginLog');
