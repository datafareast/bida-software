import BaseApi from './BaseApi';
class FileApi extends BaseApi {
    constructor(path) {
        super(path);
    }
}
export default new FileApi('/file');
