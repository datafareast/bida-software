import BaseApi from './BaseApi';
import request from '@/utils/Request';
import qs from 'qs';
class DataTopoApi extends BaseApi {
    constructor(path) {
        super(path);
    }

    findShow(dataTypeId) {
        return request({
            url: this.path + '/findShow',
            method: 'get',
            params: { dataTypeId },
        });
    }
    findAllData(dataTypeId) {
        return request({
            url: this.path + '/findAllData',
            method: 'get',
            params: { dataTypeId },
        });
    }
    updateBound(data) {
        return request({
            url: this.path + '/updateBound',
            method: 'post',
            data: qs.stringify(data, { allowDots: true }),
        });
    }
}
export default new DataTopoApi('/dataTopo');
