import BaseApi from './BaseApi';
class UserLogApi extends BaseApi {
    constructor(path) {
        super(path);
    }
}
export default new UserLogApi('/userLog');
