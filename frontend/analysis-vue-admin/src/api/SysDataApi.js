import BaseApi from './BaseApi';
import request from '@/utils/Request';
class SysDataApi extends BaseApi {
    constructor(path) {
        super(path);
    }
    findParent() {
        return request({
            url: this.path + '/findParent',
            method: 'get',
        });
    }
    findMenuCat() {
        return request({
            url: this.path + '/findByParentCode',
            method: 'get',
            params: { parentCode: 'MENU_CAT' },
        });
    }
    findIconCat() {
        return request({
            url: this.path + '/findByParentCode',
            method: 'get',
            params: { parentCode: 'ICON_CAT' },
        });
    }
    findEnabled() {
        return request({
            url: this.path + '/findByParentCode',
            method: 'get',
            params: { parentCode: 'ENABLED' },
        });
    }
    findDbType() {
        return request({
            url: this.path + '/findByParentCode',
            method: 'get',
            params: { parentCode: 'DB_TYPE' },
        });
    }
    findOptionCat() {
        return request({
            url: this.path + '/findByParentCode',
            method: 'get',
            params: { parentCode: 'OPTION_CAT' },
        });
    }
    findAreaCat() {
        return request({
            url: this.path + '/findByParentCode',
            method: 'get',
            params: { parentCode: 'AREA_CAT' },
        });
    }
    findVillageCode() {
        return request({
            url: this.path + '/findByParentCode',
            method: 'get',
            params: { parentCode: 'VILLAGE_CODE' },
        });
    }
    findTopCat() {
        return request({
            url: this.path + '/findByParentCode',
            method: 'get',
            params: { parentCode: 'TOP_CAT' },
        });
    }
    findLevel() {
        return request({
            url: this.path + '/findByParentCode',
            method: 'get',
            params: { parentCode: 'LEVEL' },
        });
    }
    findIsShow() {
        return request({
            url: this.path + '/findByParentCode',
            method: 'get',
            params: { parentCode: 'IS_SHOW' },
        });
    }
}
export default new SysDataApi('/sysData');
