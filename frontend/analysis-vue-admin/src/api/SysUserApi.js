import BaseApi from './BaseApi';
class SysUserApi extends BaseApi {
    constructor(path) {
        super(path);
    }
}
export default new SysUserApi('/sysUser');
