import BaseApi from './BaseApi';
class SysI18nApi extends BaseApi {
    constructor(path) {
        super(path);
    }
}
export default new SysI18nApi('/sysI18n');
