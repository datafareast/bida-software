import BaseApi from './BaseApi';
class FolderApi extends BaseApi {
    constructor(path) {
        super(path);
    }
}
export default new FolderApi('/folder');
