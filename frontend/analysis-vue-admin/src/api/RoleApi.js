import BaseApi from './BaseApi';
class RoleApi extends BaseApi {
    constructor(path) {
        super(path);
    }
}
export default new RoleApi('/role');
