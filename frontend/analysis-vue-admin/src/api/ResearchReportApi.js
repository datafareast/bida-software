import BaseApi from './BaseApi';
class ResearchReportApi extends BaseApi {
    constructor(path) {
        super(path);
    }
}
export default new ResearchReportApi('/researchReport');
