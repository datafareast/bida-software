import BaseApi from './BaseApi';
class SysRoleApi extends BaseApi {
    constructor(path) {
        super(path);
    }
}
export default new SysRoleApi('/sysRole');
