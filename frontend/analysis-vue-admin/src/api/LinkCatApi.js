import BaseApi from './BaseApi';
class LinkCatApi extends BaseApi {
    constructor(path) {
        super(path);
    }
}
export default new LinkCatApi('/linkCat');
