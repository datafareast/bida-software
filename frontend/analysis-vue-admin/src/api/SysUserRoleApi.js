import BaseApi from './BaseApi';
class SysUserRoleApi extends BaseApi {
    constructor(path) {
        super(path);
    }
}
export default new SysUserRoleApi('/sysUserRole');
