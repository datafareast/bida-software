import BaseApi from './BaseApi';
class SysOptionApi extends BaseApi {
    constructor(path) {
        super(path);
    }
}
export default new SysOptionApi('/sysOption');
