import BaseApi from './BaseApi';
class SysUserLogApi extends BaseApi {
    constructor(path) {
        super(path);
    }
}
export default new SysUserLogApi('/sysUserLog');
