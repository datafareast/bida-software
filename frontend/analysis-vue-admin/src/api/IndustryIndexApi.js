import BaseApi from './BaseApi';
class IndustryIndexApi extends BaseApi {
    constructor(path) {
        super(path);
    }
}
export default new IndustryIndexApi('/industryIndex');
