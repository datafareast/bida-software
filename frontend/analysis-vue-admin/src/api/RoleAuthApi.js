import BaseApi from './BaseApi';
class RoleAuthApi extends BaseApi {
    constructor(path) {
        super(path);
    }
}
export default new RoleAuthApi('/roleAuth');
