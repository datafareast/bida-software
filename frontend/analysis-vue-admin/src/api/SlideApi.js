import BaseApi from './BaseApi';
class SlideApi extends BaseApi {
    constructor(path) {
        super(path);
    }
}
export default new SlideApi('/slide');
