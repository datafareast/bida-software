module.exports = {
    productionSourceMap: false,
    css: {
        //是否使用css分离插件 ExtractTextPlugin
        extract: true,
        //开启 CSS source maps?
        sourceMap: false,
        //css 预设器配置项
        loaderOptions: {
            css: {},
            postcss: {},
        },
    },
    //webpack-dev-server 相关配置
    devServer: {
        proxy: {
            // '/api': {
            //   target: 'http://localhost:8005',
            //   ws: true,
            //   changeOrigin: true,
            // },
            '/api': {
                target: 'http://localhost:8009',
                changeOrigin: true, //是否跨域
                pathRewrite: {
                    '^/api': '/', //重写接口
                },
            },
        },
    },
    //use thread-loader for babel & TS in production build
    // enabled by default if the machine has more than 1 cores
    parallel: require('os').cpus().length > 1,
    configureWebpack: {
        //配这里的，用CDN 发布时再弄
        externals: {
            vue: 'Vue',
            //   'vuex': 'Vuex',
            //   'vue-router': 'VueRouter',
            //   'Axios': 'axios',
            //   'moment': 'moment',
            jquery: 'jquery',
            'element-ui': 'ELEMENT',
        },
        //警告 webpack 的性能提示
        performance: {
            hints: 'warning',
            //入口起点的最大体积 整数类型（以字节为单位）
            maxEntrypointSize: 500000,
            //生成文件的最大体积 整数类型（以字节为单位 200k）
            maxAssetSize: 200000,
            //只给出 js 文件的性能提示
            assetFilter: function(assetFilename) {
                return assetFilename.endsWith('.js');
            },
        },
    },
    chainWebpack: (config) => {
        if (process.env.use_analyzer) {
            config.plugin('webpack-bundle-analyzer').use(require('webpack-bundle-analyzer').BundleAnalyzerPlugin);
        }
    },
};
