/**
 * @license Copyright (c) 2003-2019, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see https://ckeditor.com/legal/ckeditor-oss-license
 */

CKEDITOR.editorConfig = function (config) {
    // console.log(CKEDITOR.config) ,可打出默认项
    config.fontSize_defaultLabel = '12px';
    config.font_defaultLabel = '仿宋';
    config.font_names = '宋体/宋体;黑体/黑体;仿宋/仿宋_GB2312;楷体/楷体_GB2312;隶书/隶书;幼圆/幼圆;微软雅黑/微软雅黑;' + config.font_names;
    //因为网页最小支持的字体是12号
    config.fontSize_sizes = '12/12px;13/13px;14/14px;15/15px;16/16px;18/18px;20/20px;22/22px;24/24px;26/26px;28/28px;30/30px;36/36px;48/48px;72/72px';
    config.toolbarLocation = 'top';
    // config.toolbarStartupExpanded = true;
    // config.toolbarCanCollapse = true;
    // config.pasteFromWordRemoveStyles = false;
    config.toolbar_basic =
        [
            ['Maximize', '-', 'Bold', 'Italic', 'Underline', 'Strike', '-', 'Subscript', 'Superscript', '-', 'NumberedList', 'BulletedList', '-', 'TextColor', 'BGColor', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', '-', 'Link', 'Unlink' ,'Anchor','-', 'EmojiPanel', 'SpecialChar']
        ];
    config.toolbar_middle =
        [
            ['Maximize', '-', 'Source', '-', 'Preview', 'Templates', 'Print', '-', 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo', '-', 'Find', 'Replace', 'SelectAll', 'RemoveFormat', '-', 'SpellChecker', 'Scayt', '-',],
            ['Bold', 'Italic', 'Underline', 'Strike', '-', 'Subscript', 'Superscript', 'TextColor', 'BGColor', '-', 'NumberedList', 'BulletedList', 'Outdent', 'Indent', 'Blockquote', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', '-', 'Link', 'Unlink', 'Anchor'],
            ['Styles', 'Format', 'Font', 'FontSize', '-', 'EmojiPanel', 'SpecialChar', 'Mathjax', '-', 'Table', 'Image']
        ];
    config.toolbar_full =
        [
            ['Maximize', '-', 'Source', '-', 'Preview', 'Templates', 'Print', '-', 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo', '-', 'Find', 'Replace', 'SelectAll', 'RemoveFormat', '-', 'SpellChecker', 'Scayt', '-', 'Yaqr', 'Syntaxhighlight', 'cssanim']
            , 
            ['Bold', 'Italic', 'Underline', 'Strike', '-', 'Subscript', 'Superscript', 'TextColor', 'BGColor', '-', 'NumberedList', 'BulletedList', 'Outdent', 'Indent', 'Blockquote', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', '-', 'Link', 'Unlink', 'Anchor'],
            ['Styles', 'Format', 'Font', 'FontSize', '-', 'EmojiPanel', 'SpecialChar', 'Mathjax', '-', 'Table', 'Image', 'Html5audio', 'Html5video']
        ];
    config.mathJaxLib = '//cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.4/MathJax.js?config=TeX-AMS_HTML'
    config.extraPlugins = ['cssanim', 'emoji', 'ajax', 'autocomplete', 'textmatch','textwatcher', 'xml', 'mathjax', 'yaqr', 'html5audio', 'syntaxhighlight', 'html5video'];
    // config.filebrowserImageUploadUrl='s';
    // config.filebrowserUploadUrl='dd';
    config.filebrowserBrowseUrl='browser';
    // CKEDITOR.config.syntaxhighlight_hideGutter = [true|false];
    // CKEDITOR.config.syntaxhighlight_hideControls = [true|false];
    // CKEDITOR.config.syntaxhighlight_collapse = [true|false];
    // CKEDITOR.config.syntaxhighlight_codeTitle = any title; // default ''
    // CKEDITOR.config.syntaxhighlight_showColumns = [true|false];
    // CKEDITOR.config.syntaxhighlight_noWrap = [true|false];
    // CKEDITOR.config.syntaxhighlight_firstLine = any numeric value; // default 0
    // CKEDITOR.config.syntaxhighlight_highlight = i.e. [1,3,9]; // default null
    // CKEDITOR.config.syntaxhighlight_lang = 'applescript', 'actionscript3', 'as3', 'bash', 'shell', 'sh', 'coldfusion', 'cf', 'cpp', 'c', 'c#', 'c-sharp', 'csharp', 'css', 'delphi', 'pascal', 'pas', 'diff', 'patch', 'erl', 'erlang', 'groovy', 'haxe', 'hx', 'java', 'jfx', 'javafx', 'js', 'jscript', 'javascript', 'perl', 'Perl', 'pl', 'php', 'text', 'plain', 'powershell', 'ps', 'posh', 'py', 'python', 'ruby', 'rails', 'ror', 'rb', 'sass', 'scss', 'scala', 'sql', 'tap', 'Tap', 'TAP', 'ts', 'typescript', 'vb', 'vbnet', 'xml', 'xhtml', 'xslt', 'html'; // default null
    // CKEDITOR.config.syntaxhighlight_code = any source code;
};
