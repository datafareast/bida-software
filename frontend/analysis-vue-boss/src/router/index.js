import Vue from 'vue';
import Router from 'vue-router';
import Layout from '@/components/Layout';

Vue.use(Router);

export const routerMap = [
    {
        path: '/login',
        component: () => import('@/views/Login'),
        hidden: true,
    },
    {
        path: '/browser',
        component: () => import('@/views/pages/Album/Browser'),
        hidden: true,
    },
    {
        path: '',
        component: Layout,
        redirect: 'index',
        children: [
            {
                path: 'index',
                component: () => import('@/views/Index'),
                name: 'index',
                icon: 'icon-index',
                noCache: true,
            },
        ],
    },
];
export const asyncRouterMap = {
    layout: Layout,
    area: () => import('@/views/pages/Area'),
    auth: () => import('@/views/pages/Auth'),
    cms: () => import('@/views/pages/Cms'),
    dataTopo: () => import('@/views/pages/DataTopo'),
    dataType: () => import('@/views/pages/DataType'),
    file: () => import('@/views/pages/File'),
    folder: () => import('@/views/pages/Folder'),
    i18n: () => import('@/views/pages/I18n'),
    icon: () => import('@/views/pages/Icon'),
    ip: () => import('@/views/pages/Ip'),
    link: () => import('@/views/pages/Link'),
    linkItem: () => import('@/views/pages/LinkItem'),
    menu: () => import('@/views/pages/Menu'),
    role: () => import('@/views/pages/Role'),
    roleAuth: () => import('@/views/pages/RoleAuth'),
    roleMenu: () => import('@/views/pages/RoleMenu'),
    secAnalysis: () => import('@/views/pages/SecAnalysis'),
    analytics: () => import('@/views/pages/Analytics'),
    secAnalysisItem: () => import('@/views/pages/SecAnalysisItem'),
    secCat: () => import('@/views/pages/SecCat'),
    secExchange: () => import('@/views/pages/SecExchange'),
    secIndex: () => import('@/views/pages/SecIndex'),
    secPosition: () => import('@/views/pages/SecPosition'),
    slide: () => import('@/views/pages/Slide'),
    slideItem: () => import('@/views/pages/SlideItem'),
    sysAuth: () => import('@/views/pages/SysAuth'),
    sysData: () => import('@/views/pages/SysData'),
    sysFile: () => import('@/views/pages/SysFile'),
    sysFolder: () => import('@/views/pages/SysFolder'),
    sysI18n: () => import('@/views/pages/SysI18n'),
    sysMenu: () => import('@/views/pages/SysMenu'),
    sysOption: () => import('@/views/pages/SysOption'),
    sysRole: () => import('@/views/pages/SysRole'),
    sysRoleAuth: () => import('@/views/pages/SysRoleAuth'),
    sysRoleMenu: () => import('@/views/pages/SysRoleMenu'),
    sysUser: () => import('@/views/pages/SysUser'),
    sysUserInfo: () => import('@/views/pages/SysUserInfo'),
    sysUserLog: () => import('@/views/pages/SysUserLog'),
    sysUserLoginLog: () => import('@/views/pages/SysUserLoginLog'),
    sysUserRole: () => import('@/views/pages/SysUserRole'),
    tableId: () => import('@/views/pages/TableId'),
    upgrade: () => import('@/views/pages/Upgrade'),
    user: () => import('@/views/pages/User'),
    userInfo: () => import('@/views/pages/UserInfo'),
    userLog: () => import('@/views/pages/UserLog'),
    userLoginLog: () => import('@/views/pages/UserLoginLog'),
    userRole: () => import('@/views/pages/UserRole'),
    profitability1: () => import('@/views/pages/Profitability1'),
    profitability2: () => import('@/views/pages/Profitability2'),
    profitability3: () => import('@/views/pages/Profitability3'),
    insight1: () => import('@/views/pages/QuotedSpread'),
    insight2: () => import('@/views/pages/DailyTradingActivityIndustry/HotChart'),
    insight3: () => import('@/views/pages/ActivityIndex/KChart'),
    insight4: () => import('@/views/pages/IndexType/LineChart'),
    dataExtract: () => import('@/views/pages/DataExtract'),
    dataSource: () => import('@/views/pages/DataSource'),
    imitatePosition: () => import('@/views/pages/ImitatePosition'),
    addImitate: () => import('@/views/pages/ImitatePosition/Add'),
    researchReport: () => import('@/views/pages/ResearchReport'),
};

const originalPush = Router.prototype.push;
Router.prototype.push = function push(location, onResolve, onReject) {
    if (onResolve || onReject) return originalPush.call(this, location, onResolve, onReject);
    return originalPush.call(this, location).catch((err) => err);
};
export default new Router({
    mode: 'history',
    scrollBehavior: () => ({ y: 0 }),
    routes: routerMap,
});
