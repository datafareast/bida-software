import router from '@/router';
import store from '@/store';
import { Message } from 'element-ui';
import NProgress from 'nprogress';
import 'nprogress/nprogress.css';
import cookie from '@/utils/CookieManager';

NProgress.configure({ showSpinner: false });
//登录白名单
const whiteList = ['/login'];
router.beforeEach((to, from, next) => {
    //开始进度条
    NProgress.start();
    if (cookie.token.get()) {
        //如果已登录
        if (to.path === '/login') {
            //去默认的页面
            next({ path: '/' });
            //进度条关掉
        } else {
            //判断当前用户
            if (store.getters.role == '') {
                //获得用户信息
                store
                    .dispatch('UserInfo')
                    .then(() => {
                        //把路由加进来，目前系统源码设计有缺陷，只能动态加，不能动态移，所以
                        router.addRoutes(store.getters.router);
                        next({ ...to, replace: true });
                    })
                    .catch((err) => {
                        store.dispatch('Expiration').then(() => {
                            Message.error(err);
                            next({ path: '/' });
                        });
                    });
            } else {
                next();
            }
        }
    } else {
        //没有token ,在白名单里
        if (whiteList.indexOf(to.path) !== -1) {
            next();
        } else {
            next(`/login?redirect=${to.path}`);
            //重定向到登录页
        }
    }
});
//结束掉
router.afterEach(() => {
    NProgress.done();
});
