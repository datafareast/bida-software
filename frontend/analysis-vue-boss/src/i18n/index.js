import Vue from 'vue';
import VueI18n from 'vue-i18n';
import Cookies from 'js-cookie';
import elementEnLocale from 'element-ui/lib/locale/lang/en';
import elementZhLocale from 'element-ui/lib/locale/lang/zh-CN';
 

Vue.use(VueI18n);

 export let messages={
    en:{
        ...elementEnLocale
    },
    zh:{
        ...elementZhLocale
    }
};

export const i18n = new VueI18n({
    locale:Cookies.get('language') || 'zh',
    messages
});
 