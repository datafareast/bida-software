import Vue from 'vue';
import Vuex from 'vuex';
import app from '@/store/modules/app';
import user from '@/store/modules/user';
import log from '@/store/modules/log';
import biz from '@/store/modules/biz';
import tagsView from '@/store/modules/tagsView';
import getters from './getters';
import actions from './actions';

import mutations from './mutations';
import state from './state';

Vue.use(Vuex);
const store = new Vuex.Store({
    modules: {
        app,
        user,
        log,
        biz,
        tagsView,
    },
    getters,
    actions,
    mutations,
    state,
});

export default store;
