import types from './mutation-type';
import cookie from '@/utils/CookieManager';
import { TreeDataTranslate } from '@/utils/TreeTool';
import { routerMap, asyncRouterMap } from '@/router';
function fetchRouter(arr, level) {
    let ret = [];
    arr.forEach((menu) => {
        let item = {};
        item.name = menu.code;
        item.icon = menu.icon;
        item.hidden = menu.isShow != 'T';
        if (menu.cat == 0) {
            item.component = asyncRouterMap['layout'];
            item.redirect = 'noredirect';
            item.path = '/' + menu.code;
        } else {
            item.path = '/' + menu.code;
            item.component = asyncRouterMap[menu.code];
        }
        if (menu.children) {
            item.children = fetchRouter(menu.children, level + 1);
        }
        ret.push(item);
    });
    return ret;
}
const mutations = {
    [types.SET_TOKEN]: (state, token) => {
        state.token = token;
        cookie.token.set(token);
    },
    [types.SET_ROLE]: (state, role) => {
        state.role = role;
    },
    [types.ADD_HEAD_IMG]: (state, headImg) => {
        state.headImg = headImg;
    },
    [types.SET_ROUTER]: (state, router) => {
        //后台的JSON 组成前台的路由对象
        //用于路由添加
        if (router) {
            //先把菜单处理造出来
            var menus = TreeDataTranslate(router);
            state.router = fetchRouter(menus, 1);
            //用于画菜单
            state.fullRouter = routerMap.concat(state.router);
        } else {
            state.fullRouter = routerMap;
        }
    },
    //按钮权限组
    [types.SET_AUTH]: (state, auth) => {
        if (auth) {
            state.auth = auth;
        }
    },
};
export default mutations;
