import types from './mutation-type';
import commonApi from '@/api/CommonApi';
import cookie from '@/utils/CookieManager';
const actions = {
    //登录
    Logon({ commit }, userInfo) {
        return new Promise((resolve, reject) => {
            commonApi
                .logon(userInfo)
                .then((response) => {
                    const data = response.data;
                    commit(types.SET_TOKEN, data.obj.token);
                    resolve();
                })
                .catch((error) => {
                    //如果不加这一句，链传不下去
                    reject(error);
                });
        });
    },
    //获得用户信息
    UserInfo({ commit }) {
        return new Promise((resolve, reject) => {
            commonApi
                .info()
                .then(({ data }) => {
                    //1.存储角色
                    commit(types.SET_ROLE, data.obj.role);
                    //2.存储头像
                    commit(types.ADD_HEAD_IMG, data.obj.headImg);
                    //3.合并路由以及菜单
                    commit(types.SET_ROUTER, data.obj.menus);
                    //4.按钮控制权限
                    commit(types.SET_AUTH, data.obj.btns);
                    resolve(data);
                })
                .catch((error) => {
                    reject(error);
                });
        });
    },
    //退出
    Logout({ commit, state }) {
        return new Promise((resolve, reject) => {
            commit(types.SET_TOKEN, '');
            cookie.token.clear();
            resolve();
        });
    },
    //会话过期
    Expiration({ commit }) {
        return new Promise((resolve) => {
            commit(types.SET_TOKEN, '');
            cookie.token.clear();
            resolve();
        });
    },
};
export default actions;
