const Types = {
    SET_TOKEN: 'SET_TOKEN',
    SET_ROLE: 'SET_ROLE',
    ADD_HEAD_IMG: 'ADD_HEAD_IMG',
    SET_ROUTER: 'SET_ROUTER',
    SET_AUTH: 'SET_AUTH',
};
export default Types;
