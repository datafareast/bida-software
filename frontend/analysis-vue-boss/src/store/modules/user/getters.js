const getters = {
    token: (state) => state.token,
    role: (state) => state.role,
    headImg: (state) => state.headImg,
    router: (state) => state.router,
    fullRouter: (state) => state.fullRouter,
    auth: (state) => state.auth,
};
export default getters;
