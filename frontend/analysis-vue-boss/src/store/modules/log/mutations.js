import types from './mutation-type';
const mutations = {
    [types.ADD_ERROR_LOG]: (state, log) => {
        state.logs.push(log);
    },
};

export default mutations;
