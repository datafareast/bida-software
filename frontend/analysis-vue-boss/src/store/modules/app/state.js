import cookie from '@/utils/CookieManager';
const state={
    sidebar:{
        opened:cookie.sidebar.get()?!!+cookie.sidebar.get():true,
        withoutAnimation:false
    },
    device:'desktop',
    
    language:cookie.language.get()||'en',
    size:cookie.size.get()||'medium'

};
export default state;