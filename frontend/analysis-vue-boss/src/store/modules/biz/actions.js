import types from './mutation-type';
import sysDataApi from '@/api/SysDataApi';
import roleApi from '@/api/RoleApi';
import secExchangeApi from '@/api/SecExchangeApi';
import secCatApi from '@/api/SecCatApi';
import basicIndexApi from '@/api/BasicIndexApi';
import marketApi from '@/api/MarketApi';
import indexTypeApi from '@/api/IndexTypeApi';
import industryApi from '@/api/IndustryApi';
import industryIndexApi from '@/api/IndustryIndexApi';
import dataIndustryApi from '@/api/DataIndustryApi';
const actions = {
    menuCat({ dispatch, state }) {
        return new Promise((resolve, reject) => {
            if (state.menuCats.length == 0) {
                dispatch('reloadMenuCat').then((data) => {
                    resolve(data);
                });
            } else {
                resolve(state.menuCats);
            }
        });
    },
    reloadMenuCat({ commit }) {
        return new Promise((resolve, reject) => {
            sysDataApi.findMenuCat().then(({ data }) => {
                commit(types.MENU_CAT, data.obj);
                resolve(data.obj);
            });
        });
    },
    iconCat({ dispatch, state }) {
        return new Promise((resolve, reject) => {
            if (state.iconCats.length == 0) {
                dispatch('reloadIconCat').then((data) => {
                    resolve(data);
                });
            } else {
                resolve(state.iconCats);
            }
        });
    },
    reloadIconCat({ commit }) {
        return new Promise((resolve, reject) => {
            sysDataApi.findIconCat().then(({ data }) => {
                commit(types.ICON_CAT, data.obj);
                resolve(data.obj);
            });
        });
    },
    enabled({ dispatch, state }) {
        return new Promise((resolve, reject) => {
            if (state.enableds.length == 0) {
                dispatch('reloadEnabled').then((data) => {
                    resolve(data);
                });
            } else {
                resolve(state.enableds);
            }
        });
    },
    reloadEnabled({ commit }) {
        return new Promise((resolve, reject) => {
            sysDataApi.findEnabled().then(({ data }) => {
                commit(types.ENABLED, data.obj);
                resolve(data.obj);
            });
        });
    },
    imitateSta({ dispatch, state }) {
        return new Promise((resolve, reject) => {
            if (state.imitateStas.length == 0) {
                dispatch('reloadImitateSta').then((data) => {
                    resolve(data);
                });
            } else {
                resolve(state.imitateStas);
            }
        });
    },
    reloadImitateSta({ commit }) {
        return new Promise((resolve, reject) => {
            sysDataApi.findImitateSta().then(({ data }) => {
                commit(types.IMITATE_STA, data.obj);
                resolve(data.obj);
            });
        });
    },
    isMain({ dispatch, state }) {
        return new Promise((resolve, reject) => {
            if (state.isMains.length == 0) {
                dispatch('reloadIsMain').then((data) => {
                    resolve(data);
                });
            } else {
                resolve(state.isMains);
            }
        });
    },
    reloadIsMain({ commit }) {
        return new Promise((resolve, reject) => {
            sysDataApi.findIsMain().then(({ data }) => {
                commit(types.IS_MAIN, data.obj);
                resolve(data.obj);
            });
        });
    },
    optionCat({ dispatch, state }) {
        return new Promise((resolve, reject) => {
            if (state.optionCats.length == 0) {
                dispatch('reloadOptionCat').then((data) => {
                    resolve(data);
                });
            } else {
                resolve(state.optionCats);
            }
        });
    },
    reloadOptionCat({ commit }) {
        return new Promise((resolve, reject) => {
            sysDataApi.findOptionCat().then(({ data }) => {
                commit(types.OPTION_CAT, data.obj);
                resolve(data.obj);
            });
        });
    },
    inputType({ dispatch, state }) {
        return new Promise((resolve, reject) => {
            if (state.inputTypes.length == 0) {
                dispatch('reloadInputType').then((data) => {
                    resolve(data);
                });
            } else {
                resolve(state.inputTypes);
            }
        });
    },
    reloadInputType({ commit }) {
        return new Promise((resolve, reject) => {
            sysDataApi.findInputType().then(({ data }) => {
                commit(types.INPUT_TYPE, data.obj);
                resolve(data.obj);
            });
        });
    },
    role({ dispatch, state }) {
        return new Promise((resolve, reject) => {
            if (state.roles.length == 0) {
                dispatch('reloadRole').then((data) => {
                    resolve(data);
                });
            } else {
                resolve(state.roles);
            }
        });
    },
    reloadRole({ commit }) {
        return new Promise((resolve, reject) => {
            roleApi.findAll().then(({ data }) => {
                commit(types.ROLE, data.obj);
                resolve(data.obj);
            });
        });
    },
    areaCat({ dispatch, state }) {
        return new Promise((resolve, reject) => {
            if (state.areaCats.length == 0) {
                dispatch('reloadAreaCat').then((data) => {
                    resolve(data);
                });
            } else {
                resolve(state.areaCats);
            }
        });
    },
    reloadAreaCat({ commit }) {
        return new Promise((resolve, reject) => {
            sysDataApi.findAreaCat().then(({ data }) => {
                commit(types.AREA_CAT, data.obj);
                resolve(data.obj);
            });
        });
    },
    villageCode({ dispatch, state }) {
        return new Promise((resolve, reject) => {
            if (state.villageCodes.length == 0) {
                dispatch('reloadVillageCode').then((data) => {
                    resolve(data);
                });
            } else {
                resolve(state.villageCodes);
            }
        });
    },
    reloadVillageCode({ commit }) {
        return new Promise((resolve, reject) => {
            sysDataApi.findVillageCode().then(({ data }) => {
                commit(types.VILLAGE_CODE, data.obj);
                resolve(data.obj);
            });
        });
    },
    target({ dispatch, state }) {
        return new Promise((resolve, reject) => {
            if (state.targets.length == 0) {
                dispatch('reloadTarget').then((data) => {
                    resolve(data);
                });
            } else {
                resolve(state.enableds);
            }
        });
    },
    reloadTarget({ commit }) {
        return new Promise((resolve, reject) => {
            sysDataApi.findTarget().then(({ data }) => {
                commit(types.TARGET, data.obj);
                resolve(data.obj);
            });
        });
    },
    docType({ dispatch, state }) {
        return new Promise((resolve, reject) => {
            if (state.docTypes.length == 0) {
                dispatch('reloadDocType').then((data) => {
                    resolve(data);
                });
            } else {
                resolve(state.docTypes);
            }
        });
    },
    reloadDocType({ commit }) {
        return new Promise((resolve, reject) => {
            sysDataApi.findDocType().then(({ data }) => {
                commit(types.DOC_TYPE, data.obj);
                resolve(data.obj);
            });
        });
    },
    secExchange({ dispatch, state }) {
        return new Promise((resolve, reject) => {
            if (state.secExchanges.length == 0) {
                dispatch('reloadSecExchange').then((data) => {
                    resolve(data);
                });
            } else {
                resolve(state.secExchanges);
            }
        });
    },
    reloadSecExchange({ commit }) {
        return new Promise((resolve, reject) => {
            secExchangeApi.findAll().then(({ data }) => {
                commit(types.SEC_EXCHANGE, data.obj);
                resolve(data.obj);
            });
        });
    },
    secCat({ dispatch, state }) {
        return new Promise((resolve, reject) => {
            if (state.secCats.length == 0) {
                dispatch('reloadSecCat').then((data) => {
                    resolve(data);
                });
            } else {
                resolve(state.secCats);
            }
        });
    },
    reloadSecCat({ commit }) {
        return new Promise((resolve, reject) => {
            secCatApi.findAll().then(({ data }) => {
                commit(types.SEC_CAT, data.obj);
                resolve(data.obj);
            });
        });
    },

    basicIndex({ dispatch, state }) {
        return new Promise((resolve, reject) => {
            if (state.basicIndexes.length == 0) {
                dispatch('reloadBasicIndex').then((data) => {
                    resolve(data);
                });
            } else {
                resolve(state.basicIndexes);
            }
        });
    },
    reloadBasicIndex({ commit }) {
        return new Promise((resolve, reject) => {
            basicIndexApi.findEnabled().then(({ data }) => {
                commit(types.BASIC_INDEX, data.obj);
                resolve(data.obj);
            });
        });
    },
    market({ dispatch, state }) {
        return new Promise((resolve, reject) => {
            if (state.markets.length == 0) {
                dispatch('reloadMarket').then((data) => {
                    resolve(data);
                });
            } else {
                resolve(state.markets);
            }
        });
    },
    reloadMarket({ commit }) {
        return new Promise((resolve, reject) => {
            marketApi.findAll().then(({ data }) => {
                commit(types.MARKET, data.obj);
                resolve(data.obj);
            });
        });
    },
    indexType({ dispatch, state }) {
        return new Promise((resolve, reject) => {
            if (state.indexTypes.length == 0) {
                dispatch('reloadIndexType').then((data) => {
                    resolve(data);
                });
            } else {
                resolve(state.indexTypes);
            }
        });
    },
    reloadIndexType({ commit }) {
        return new Promise((resolve, reject) => {
            indexTypeApi.findEnabled().then(({ data }) => {
                commit(types.INDEX_TYPE, data.obj);
                resolve(data.obj);
            });
        });
    },
    industry({ dispatch, state }) {
        return new Promise((resolve, reject) => {
            if (state.industries.length == 0) {
                dispatch('reloadIndustry').then((data) => {
                    resolve(data);
                });
            } else {
                resolve(state.industries);
            }
        });
    },
    reloadIndustry({ commit }) {
        return new Promise((resolve, reject) => {
            industryApi.findAll().then(({ data }) => {
                commit(types.INDUSTRY, data.obj);
                resolve(data.obj);
            });
        });
    },
    industryIndex({ dispatch, state }) {
        return new Promise((resolve, reject) => {
            if (state.industryIndexes.length == 0) {
                dispatch('reloadIndustryIndex').then((data) => {
                    resolve(data);
                });
            } else {
                resolve(state.industryIndexes);
            }
        });
    },
    reloadIndustryIndex({ commit }) {
        return new Promise((resolve, reject) => {
            industryIndexApi.findAll().then(({ data }) => {
                commit(types.INDUSTRY_INDEX, data.obj);
                resolve(data.obj);
            });
        });
    },
    dataIndustry({ dispatch, state }) {
        return new Promise((resolve, reject) => {
            if (state.dataIndustries.length == 0) {
                dispatch('reloadDataIndustry').then((data) => {
                    resolve(data);
                });
            } else {
                resolve(state.dataIndustries);
            }
        });
    },
    reloadDataIndustry({ commit }) {
        return new Promise((resolve, reject) => {
            dataIndustryApi.findAll().then(({ data }) => {
                commit(types.DATA_INDUSTRY, data.obj);
                resolve(data.obj);
            });
        });
    },
};
export default actions;
