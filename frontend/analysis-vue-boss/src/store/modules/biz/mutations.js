import types from './mutation-type';
const mutations = {
    [types.MENU_CAT]: (state, menuCats) => {
        state.menuCats = menuCats;
    },
    [types.ICON_CAT]: (state, iconCats) => {
        state.iconCats = iconCats;
    },
    [types.OPTION_CAT]: (state, optionCats) => {
        state.optionCats = optionCats;
    },
    [types.INPUT_TYPE]: (state, inputTypes) => {
        state.inputTypes = inputTypes;
    },
    [types.ENABLED]: (state, enableds) => {
        state.enableds = enableds;
    },
    [types.IS_MAIN]: (state, isMains) => {
        state.isMains = isMains;
    },
    [types.IMITATE_STA]: (state, imitateStas) => {
        state.imitateStas = imitateStas;
    },
    [types.ROLE]: (state, roles) => {
        state.roles = roles;
    },
    [types.AREA_CAT]: (state, areaCats) => {
        state.areaCats = areaCats;
    },
    [types.VILLAGE_CODE]: (state, villageCodes) => {
        state.villageCodes = villageCodes;
    },
    [types.TARGET]: (state, targets) => {
        state.targets = targets;
    },
    [types.DOC_TYPE]: (state, docTypes) => {
        state.docTypes = docTypes;
    },
    [types.SEC_EXCHANGE]: (state, secExchanges) => {
        state.secExchanges = secExchanges;
    },
    [types.SEC_CAT]: (state, secCats) => {
        state.secCats = secCats;
    },
    [types.BASIC_INDEX]: (state, basicIndexes) => {
        state.basicIndexes = basicIndexes;
    },
    [types.MARKET]: (state, markets) => {
        state.markets = markets;
    },
    [types.INDEX_TYPE]: (state, indexTypes) => {
        state.indexTypes = indexTypes;
    },
    [types.INDUSTRY]: (state, industries) => {
        state.industries = industries;
    },
    [types.INDUSTRY_INDEX]: (state, industryIndexes) => {
        state.industryIndexes = industryIndexes;
    },
    [types.DATA_INDUSTRY]: (state, dataIndustries) => {
        state.dataIndustries = dataIndustries;
    },
};
export default mutations;
