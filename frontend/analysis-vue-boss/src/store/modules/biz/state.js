const state = {
    menuCats: [],
    iconCats: [],
    enableds: [],
    roles: [],
    optionCats: [],
    inputTypes: [],
    areaCats: [],
    villageCodes: [],
    targets: [],
    docTypes: [],
    secExchanges: [],
    secCats: [],
    basicIndexes: [],
    markets: [],
    indexTypes: [],
    industries: [],
    industryIndexes: [],
    dataIndustries: [],
    isMains: [],
    imitateStas: [],
};
export default state;
