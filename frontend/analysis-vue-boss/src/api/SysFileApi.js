import BaseApi from './BaseApi';
class SysFileApi extends BaseApi {
    constructor(path) {
        super(path);
    }
}
export default new SysFileApi('/sysFile');
