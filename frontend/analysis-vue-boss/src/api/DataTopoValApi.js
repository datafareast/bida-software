import BaseApi from './BaseApi';
import request from '@/utils/Request';
class DataTopoValApi extends BaseApi {
    constructor(path) {
        super(path);
    }
    findData(id) {
        return request({
            url: this.path + '/findData',
            method: 'get',
            params: { id },
        });
    }
}
export default new DataTopoValApi('/dataTopoVal');
