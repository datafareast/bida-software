import BaseApi from './BaseApi';
class SysAuthApi extends BaseApi {
    constructor(path) {
        super(path);
    }
}
export default new SysAuthApi('/sysAuth');
