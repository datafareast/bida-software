import BaseApi from './BaseApi';
class AutocorrelationMarketApi extends BaseApi {
    constructor(path) {
        super(path);
    }
}
export default new AutocorrelationMarketApi('/autocorrelationMarket');
