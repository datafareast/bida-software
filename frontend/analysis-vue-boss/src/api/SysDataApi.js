import BaseApi from './BaseApi';
import request from '@/utils/Request';
class SysDataApi extends BaseApi {
    constructor(path) {
        super(path);
    }
    findParent() {
        return request({
            url: this.path + '/findParent',
            method: 'get',
        });
    }
    findMenuCat() {
        return request({
            url: this.path + '/findByParentCode',
            method: 'get',
            params: { parentCode: 'MENU_CAT' },
        });
    }
    findIconCat() {
        return request({
            url: this.path + '/findByParentCode',
            method: 'get',
            params: { parentCode: 'ICON_CAT' },
        });
    }
    findEnabled() {
        return request({
            url: this.path + '/findByParentCode',
            method: 'get',
            params: { parentCode: 'ENABLED' },
        });
    }
    findImitateSta() {
        return request({
            url: this.path + '/findByParentCode',
            method: 'get',
            params: { parentCode: 'IMITATE_STA' },
        });
    }
    findIsMain() {
        return request({
            url: this.path + '/findByParentCode',
            method: 'get',
            params: { parentCode: 'IS_MAIN' },
        });
    }
    findOptionCat() {
        return request({
            url: this.path + '/findByParentCode',
            method: 'get',
            params: { parentCode: 'OPTION_CAT' },
        });
    }
    findAreaCat() {
        return request({
            url: this.path + '/findByParentCode',
            method: 'get',
            params: { parentCode: 'AREA_CAT' },
        });
    }
    findVillageCode() {
        return request({
            url: this.path + '/findByParentCode',
            method: 'get',
            params: { parentCode: 'VILLAGE_CODE' },
        });
    }
    findTarget() {
        return request({
            url: this.path + '/findByParentCode',
            method: 'get',
            params: { parentCode: 'TARGET' },
        });
    }
    findDocType() {
        return request({
            url: this.path + '/findByParentCode',
            method: 'get',
            params: { parentCode: 'DOC_TYPE' },
        });
    }
    findInputType() {
        return request({
            url: this.path + '/findByParentCode',
            method: 'get',
            params: { parentCode: 'INPUT_TYPE' },
        });
    }
}
export default new SysDataApi('/sysData');
