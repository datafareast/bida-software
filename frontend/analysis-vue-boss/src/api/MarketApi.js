import BaseApi from './BaseApi';
class MarketApi extends BaseApi {
    constructor(path) {
        super(path);
    }
}
export default new MarketApi('/market');
