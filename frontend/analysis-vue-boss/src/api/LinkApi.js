import BaseApi from './BaseApi';
class LinkApi extends BaseApi {
    constructor(path) {
        super(path);
    }
}
export default new LinkApi('/link');
