import BaseApi from './BaseApi';
class SysRoleMenuApi extends BaseApi {
    constructor(path) {
        super(path);
    }
}
export default new SysRoleMenuApi('/sysRoleMenu');
