import request from '@/utils/Request';
import BaseApi from './BaseApi';
class DailyTradingActivityIndustryApi extends BaseApi {
    constructor(path) {
        super(path);
    }
    findData(data) {
        return request({
            url: this.path + '/findData',
            method: 'get',
            params: data,
        });
    }
}
export default new DailyTradingActivityIndustryApi('/dailyTradingActivityIndustry');
