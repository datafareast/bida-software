import BaseApi from './BaseApi';
class SecExchangeApi extends BaseApi {
    constructor(path) {
        super(path);
    }
}
export default new SecExchangeApi('/secExchange');
