import request from '@/utils/Request';
import context from '@/utils/Context';
import qs from 'qs';

class CommonApi {
    constructor(path) {
        this.path = context.path + path;
    }
    logon(data) {
        return request({
            url: this.path + '/logon',
            method: 'post',
            data: qs.stringify(data, { allowDots: true }),
        });
    }
    info() {
        return request({
            url: this.path + '/userInfo',
            method: 'get',
        });
    }
    modifyPassword(data) {
        return request({
            url: this.path + '/modifyPassword',
            method: 'post',
            data: qs.stringify(data, { allowDots: true }),
        });
    }
}
export default new CommonApi('');
