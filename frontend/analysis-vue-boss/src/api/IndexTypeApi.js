import request from '@/utils/Request';
import BaseApi from './BaseApi';
class IndexTypeApi extends BaseApi {
    constructor(path) {
        super(path);
    }
    findEnabled() {
        return request({
            url: this.path + '/findEnabled',
            method: 'get',
        });
    }
    findData(data) {
        return request({
            url: this.path + '/findData',
            method: 'get',
            params: data,
        });
    }
}
export default new IndexTypeApi('/indexType');
