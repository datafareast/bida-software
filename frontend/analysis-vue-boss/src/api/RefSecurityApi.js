import BaseApi from './BaseApi';
class RefSecurityApi extends BaseApi {
    constructor(path) {
        super(path);
    }
}
export default new RefSecurityApi('/refSecurity');
