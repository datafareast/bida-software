import BaseApi from './BaseApi';
class SecAnalysisApi extends BaseApi {
    constructor(path) {
        super(path);
    }
}
export default new SecAnalysisApi('/secAnalysis');
