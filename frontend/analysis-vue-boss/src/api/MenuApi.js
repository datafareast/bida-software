import BaseApi from './BaseApi';
class MenuApi extends BaseApi {
    constructor(path) {
        super(path);
    }
}
export default new MenuApi('/menu');
