import BaseApi from './BaseApi';
import request from '@/utils/Request';
class DataTopoApi extends BaseApi {
    constructor(path) {
        super(path);
    }
    findDto(id) {
        return request({
            url: this.path + '/findDto',
            method: 'get',
            params: { id },
        });
    }
    findShow(dataTypeId) {
        return request({
            url: this.path + '/findShow',
            method: 'get',
            params: { dataTypeId },
        });
    }
}
export default new DataTopoApi('/dataTopo');
