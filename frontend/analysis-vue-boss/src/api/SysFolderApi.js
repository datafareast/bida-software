import BaseApi from './BaseApi';
class SysFolderApi extends BaseApi {
    constructor(path) {
        super(path);
    }
}
export default new SysFolderApi('/sysFolder');
