import BaseApi from './BaseApi';
import request from '@/utils/Request';
class ResearchReportApi extends BaseApi {
    constructor(path) {
        super(path);
    }
    findNav() {
        return request({
            url: this.path + '/findNav',
            method: 'get',
        });
    }
}
export default new ResearchReportApi('/researchReport');
