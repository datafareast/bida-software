import BaseApi from './BaseApi';
class DataTypeApi extends BaseApi {
    constructor(path) {
        super(path);
    }
}
export default new DataTypeApi('/dataType');
