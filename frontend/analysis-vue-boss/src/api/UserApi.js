import BaseApi from './BaseApi';
class UserApi extends BaseApi {
    constructor(path) {
        super(path);
    }
}
export default new UserApi('/user');
