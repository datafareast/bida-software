import BaseApi from './BaseApi';
class AreaApi extends BaseApi {
    constructor(path) {
        super(path);
    }
}
export default new AreaApi('/area');
