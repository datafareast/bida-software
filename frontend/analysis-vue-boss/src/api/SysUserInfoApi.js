import BaseApi from './BaseApi';
class SysUserInfoApi extends BaseApi {
    constructor(path) {
        super(path);
    }
}
export default new SysUserInfoApi('/sysUserInfo');
