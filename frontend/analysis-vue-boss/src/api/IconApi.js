import BaseApi from './BaseApi';
class IconApi extends BaseApi {
    constructor(path) {
        super(path);
    }
}
export default new IconApi('/icon');
