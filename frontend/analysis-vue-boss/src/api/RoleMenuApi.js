import BaseApi from './BaseApi';
class RoleMenuApi extends BaseApi {
    constructor(path) {
        super(path);
    }
}
export default new RoleMenuApi('/roleMenu');
