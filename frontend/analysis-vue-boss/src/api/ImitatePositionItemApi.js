import BaseApi from './BaseApi';
class ImitatePositionItemApi extends BaseApi {
    constructor(path) {
        super(path);
    }
}
export default new ImitatePositionItemApi('/imitatePositionItem');
