import BaseApi from './BaseApi';
class IpApi extends BaseApi {
    constructor(path) {
        super(path);
    }
}
export default new IpApi('/ip');
