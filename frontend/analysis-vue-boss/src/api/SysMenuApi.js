import BaseApi from './BaseApi';
class SysMenuApi extends BaseApi {
    constructor(path) {
        super(path);
    }
}
export default new SysMenuApi('/sysMenu');
