import BaseApi from './BaseApi';
class SlideItemApi extends BaseApi {
    constructor(path) {
        super(path);
    }
}
export default new SlideItemApi('/slideItem');
