import BaseApi from './BaseApi';
class SecIndexApi extends BaseApi {
    constructor(path) {
        super(path);
    }
}
export default new SecIndexApi('/secIndex');
