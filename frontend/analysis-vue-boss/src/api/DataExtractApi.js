import BaseApi from './BaseApi';
class DataExtractApi extends BaseApi {
    constructor(path) {
        super(path);
    }
}
export default new DataExtractApi('/dataExtract');
