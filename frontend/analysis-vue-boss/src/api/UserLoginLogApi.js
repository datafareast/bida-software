import BaseApi from './BaseApi';
class UserLoginLogApi extends BaseApi {
    constructor(path) {
        super(path);
    }
}
export default new UserLoginLogApi('/userLoginLog');
