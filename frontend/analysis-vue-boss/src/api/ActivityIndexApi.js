import BaseApi from './BaseApi';
import request from '@/utils/Request';
class ActivityIndexApi extends BaseApi {
    constructor(path) {
        super(path);
    }
    findData(data) {
        return request({
            url: this.path + '/findData',
            method: 'get',
            params: data,
        });
    }
}
export default new ActivityIndexApi('/activityIndex');
