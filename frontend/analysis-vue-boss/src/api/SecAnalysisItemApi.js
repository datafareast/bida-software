import BaseApi from './BaseApi';
class SecAnalysisItemApi extends BaseApi {
    constructor(path) {
        super(path);
    }
}
export default new SecAnalysisItemApi('/secAnalysisItem');
