import BaseApi from './BaseApi';
import request from '@/utils/Request';
class SecPositionApi extends BaseApi {
    constructor(path) {
        super(path);
    }
    findPosition(occDate) {
        return request({
            url: this.path + '/findPosition',
            method: 'get',
            params: { occDate },
        });
    }
}
export default new SecPositionApi('/secPosition');
