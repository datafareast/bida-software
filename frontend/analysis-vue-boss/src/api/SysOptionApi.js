import BaseApi from './BaseApi';
import request from '@/utils/Request';
class SysOptionApi extends BaseApi {
    constructor(path) {
        super(path);
    }

    findKConfig() {
        return request({
            url: this.path + '/findKConfig',
            method: 'get',
        });
    }
    findHotmapConfig() {
        return request({
            url: this.path + '/findHotmapConfig',
            method: 'get',
        });
    }
    findLineConfig() {
        return request({
            url: this.path + '/findLineConfig',
            method: 'get',
        });
    }
    findMxServer() {
        return request({
            url: this.path + '/findMxServer',
            method: 'get',
        });
    }
}
export default new SysOptionApi('/sysOption');
