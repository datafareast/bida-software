import BaseApi from './BaseApi';
class AutocorrelationSecurityApi extends BaseApi {
    constructor(path) {
        super(path);
    }
}
export default new AutocorrelationSecurityApi('/autocorrelationSecurity');
