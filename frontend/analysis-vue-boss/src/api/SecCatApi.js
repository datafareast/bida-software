import BaseApi from './BaseApi';
class SecCatApi extends BaseApi {
    constructor(path) {
        super(path);
    }
}
export default new SecCatApi('/secCat');
