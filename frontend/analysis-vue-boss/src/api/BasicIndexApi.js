import BaseApi from './BaseApi';
import request from '@/utils/Request';
class BasicIndexApi extends BaseApi {
    constructor(path) {
        super(path);
    }
    findEnabled() {
        return request({
            url: this.path + '/findEnabled',
            method: 'get',
        });
    }
}
export default new BasicIndexApi('/basicIndex');
