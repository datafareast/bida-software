import BaseApi from './BaseApi';
class UserInfoApi extends BaseApi {
    constructor(path) {
        super(path);
    }
}
export default new UserInfoApi('/userInfo');
