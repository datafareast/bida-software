import BaseApi from './BaseApi';
class TableIdApi extends BaseApi {
    constructor(path) {
        super(path);
    }
}
export default new TableIdApi('/tableId');
