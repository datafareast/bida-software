import request from '@/utils/Request';
import qs from 'qs';
class McapSecurityApi{
	constructor(path){
        this.path = path; 
    } 
	 list(data){
	        return request({
	            url:this.path+'/list',
	            method:'get',
	            params: data
	        });
    }
    find(id){
        return request({
            url:this.path+'/find',
            method:'get',
            params: {id}
        });
    }
    findAll(){
        return request({
            url:this.path+'/findAll',
            method:'get',
        });
    }
    del(id){
        const data = {id};
        return request({
            url:this.path+'/del',
            method:'post',
            data: qs.stringify(data)
        });
    }
    save(data){
        return request({
            url:this.path+'/save',
            method:'post',
            data: qs.stringify(data,{allowDots:true})
        });
    }
    update(data){
        return request({
            url:this.path+'/update',
            method:'post',
            data: qs.stringify(data,{allowDots:true})
        });
    }
}
export default new McapSecurityApi("/analysis-api/mcapSecurity");