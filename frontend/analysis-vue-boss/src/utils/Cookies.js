import cookies from 'js-cookie';
class Cookies{
    constructor(key){
        this.key = key; 
    } 
    get(){
        return cookies.get(this.key);
    }
    set(value){
        cookies.set(this.key,value);
    }
    clear(){
        cookies.remove(this.key);
    }
}
export default Cookies;