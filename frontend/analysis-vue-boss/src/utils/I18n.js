export function generateTitle(title) {
    const hasKey = this.$te('route.' + title);
    if (hasKey) {
        const traslatedTitle = this.$t('route.' + title);
        return traslatedTitle;
    }
    return title;
}
