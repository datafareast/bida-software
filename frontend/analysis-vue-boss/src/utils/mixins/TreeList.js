import waves from "@/directive/waves";
import formatDate from '@/utils/Date';
import elDragDialog from '@/directive/el-dragDialog';
import {
    mapGetters
} from 'vuex';
export default {
    directives: { elDragDialog, waves },
    filters: {
        dateFormatter(time) {
            let date = new Date(time);
            return formatDate(date, 'yyyy-MM-dd HH:mm:ss');
        },
        shortDateFormatter(time) {
            let date = new Date(time);
            return formatDate(date, 'yyyy-MM-dd');
        },
        enabledFormatter(enabled) {
            if (enabled === 'T') {
                return "启用";
            } else {
                return "禁用";
            }
        }
    },
    data() {
        return {
            isDisable: false,
            list: [],
            tableKey: 0,
            listLoading: true,
            dialogFormVisible: false,
        }
    },
    computed: {
        ...mapGetters([
            'auth'
        ]),

    },
    methods: {
        hasAuth(code) {
            return code != '';
            // return this.auth.some(a=>a===code);
        },
        //新增
        add() {
            this.isDisable = true
            setTimeout(() => {
                this.isDisable = false
            }, 1000);
            this.showDialog();
        },
        //修改
        edit(row) {
            this.isDisable = true
            setTimeout(() => {
                this.isDisable = false
            }, 1000);
            this.showDialog(row.id);
        },
        //删除
        del(row) {
            this.isDisable = true
            setTimeout(() => {
                this.isDisable = false
            }, 1000);
            this.$confirm(this.$t("delPrompt"), this.$t("prompt"), {
                type: "warning"
            }).then(() => {
                const id = row.id;
                this.delAction(id);
            }).catch(() => { });
        },
        //查
        query() {
            this.isDisable = true
            setTimeout(() => {
                this.isDisable = false
            }, 1000);
            this.getList();
        },
        showDialog(id) {
            this.dialogFormVisible = true;
            this.$nextTick(() => {
                this.$refs.edit.init(id);
            });
        },
        delAction(id) {

        },
        getList() {

        },
    }
}