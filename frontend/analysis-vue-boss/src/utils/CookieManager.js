import Cookies from './Cookies';
const manager = {
    token: new Cookies('analysis-boss'),
    language: new Cookies('analysis-boss-language'),
    sidebar: new Cookies('analysis-boss-sidebar'),
    size: new Cookies('analysis-boss-size'),
};
export default manager;
