package com.hzizs.analysis.action;


import javax.annotation.Resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hzizs.analysis.entity.SysRoleMenu;
import com.hzizs.analysis.query.SysRoleMenuQuery;
import com.hzizs.analysis.service.SysRoleMenuService;
import com.hzizs.pojo.ErrorInfo;
import com.hzizs.vo.Container;
/**
 * 系统角色菜单控制器
 * @author crazy_cabbage
 *
 */
@RestController
@RequestMapping("/sysRoleMenu")
public class SysRoleMenuAction {
	@Resource
	private SysRoleMenuService sysRoleMenuService;

	@GetMapping("/list")
	public ErrorInfo list(SysRoleMenuQuery query ) {
		Container<SysRoleMenu> container = sysRoleMenuService.findContainer(query);
		return ErrorInfo.ok(container);
	}

	@GetMapping("/find")
	public ErrorInfo find(Long id) {
		SysRoleMenu sysRoleMenu=sysRoleMenuService.findById(id);
		return ErrorInfo.ok(sysRoleMenu);
	}

 

	@PostMapping("/del")
	public ErrorInfo del(Long id) {
		sysRoleMenuService.deleteById(id);
		return ErrorInfo.ok();
	}

	@PostMapping("/save")
	public ErrorInfo save(SysRoleMenu sysRoleMenu) {
		sysRoleMenuService.save(sysRoleMenu);
		return ErrorInfo.ok();
	}

	@PostMapping("/update")
	public ErrorInfo update(SysRoleMenu sysRoleMenu) {
		sysRoleMenuService.update(sysRoleMenu);
		return ErrorInfo.ok();
	}
}
