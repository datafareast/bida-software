package com.hzizs.analysis.action;


import javax.annotation.Resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hzizs.analysis.entity.SysUserRole;
import com.hzizs.analysis.query.SysUserRoleQuery;
import com.hzizs.analysis.service.SysUserRoleService;
import com.hzizs.pojo.ErrorInfo;
import com.hzizs.vo.Container;
/**
 * 系统用户角色控制器
 * @author crazy_cabbage
 *
 */
@RestController
@RequestMapping("/sysUserRole")
public class SysUserRoleAction {
	@Resource
	private SysUserRoleService sysUserRoleService;

	@GetMapping("/list")
	public ErrorInfo list(SysUserRoleQuery query ) {
		Container<SysUserRole> container = sysUserRoleService.findContainer(query);
		return ErrorInfo.ok(container);
	}

	@GetMapping("/find")
	public ErrorInfo find(Long id) {
		SysUserRole sysUserRole=sysUserRoleService.findById(id);
		return ErrorInfo.ok(sysUserRole);
	}

 

	@PostMapping("/del")
	public ErrorInfo del(Long id) {
		sysUserRoleService.deleteById(id);
		return ErrorInfo.ok();
	}

	@PostMapping("/save")
	public ErrorInfo save(SysUserRole sysUserRole) {
		sysUserRoleService.save(sysUserRole);
		return ErrorInfo.ok();
	}

	@PostMapping("/update")
	public ErrorInfo update(SysUserRole sysUserRole) {
		sysUserRoleService.update(sysUserRole);
		return ErrorInfo.ok();
	}
}
