package com.hzizs.analysis.action;


import javax.annotation.Resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hzizs.analysis.entity.UserInfo;
import com.hzizs.analysis.query.UserInfoQuery;
import com.hzizs.analysis.service.UserInfoService;
import com.hzizs.pojo.ErrorInfo;
import com.hzizs.vo.Container;
/**
 * 用户信息控制器
 * @author crazy_cabbage
 *
 */
@RestController
@RequestMapping("/userInfo")
public class UserInfoAction {
	@Resource
	private UserInfoService userInfoService;

	@GetMapping("/list")
	public ErrorInfo list(UserInfoQuery query ) {
		Container<UserInfo> container = userInfoService.findContainer(query);
		return ErrorInfo.ok(container);
	}

	@GetMapping("/find")
	public ErrorInfo find(Long id) {
		UserInfo userInfo=userInfoService.findById(id);
		return ErrorInfo.ok(userInfo);
	}

 

	@PostMapping("/del")
	public ErrorInfo del(Long id) {
		userInfoService.deleteById(id);
		return ErrorInfo.ok();
	}

	@PostMapping("/save")
	public ErrorInfo save(UserInfo userInfo) {
		userInfoService.save(userInfo);
		return ErrorInfo.ok();
	}

	@PostMapping("/update")
	public ErrorInfo update(UserInfo userInfo) {
		userInfoService.update(userInfo);
		return ErrorInfo.ok();
	}
}
