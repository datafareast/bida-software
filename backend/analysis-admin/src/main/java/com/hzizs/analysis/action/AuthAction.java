package com.hzizs.analysis.action;


import javax.annotation.Resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hzizs.analysis.entity.Auth;
import com.hzizs.analysis.facade.AuthFacade;
import com.hzizs.analysis.query.AuthQuery;
import com.hzizs.analysis.service.AuthService;
import com.hzizs.pojo.ErrorInfo;
import com.hzizs.vo.Container;

/**
 * 权限控制器
 * 
 * @author crazy_cabbage
 *
 */
@RestController
@RequestMapping("/auth")
public class AuthAction {
  @Resource
  private AuthService authService;
  @Resource
  private AuthFacade authFacade;

  @GetMapping("/list")
  public ErrorInfo list(AuthQuery query) {
    Container<Auth> container = authService.findContainer(query);
    return ErrorInfo.ok(container);
  }

  @GetMapping("/find")
  public ErrorInfo find(Long id) {
    Auth auth = authService.findById(id);
    return ErrorInfo.ok(auth);
  }



  @PostMapping("/del")
  public ErrorInfo del(Long[] id) {
    authFacade.deleteByIds(id);
    return ErrorInfo.ok();
  }

  @PostMapping("/save")
  public ErrorInfo save(Auth auth) {
    authFacade.save(auth);
    return ErrorInfo.ok();
  }

  @PostMapping("/update")
  public ErrorInfo update(Auth auth) {
    authFacade.update(auth);
    return ErrorInfo.ok();
  }
}
