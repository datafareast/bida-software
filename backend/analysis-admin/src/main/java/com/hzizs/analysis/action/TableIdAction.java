package com.hzizs.analysis.action;


import javax.annotation.Resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hzizs.analysis.entity.TableId;
import com.hzizs.analysis.query.TableIdQuery;
import com.hzizs.analysis.service.TableIdService;
import com.hzizs.pojo.ErrorInfo;
import com.hzizs.vo.Container;
/**
 * 表主键控制器
 * @author crazy_cabbage
 *
 */
@RestController
@RequestMapping("/tableId")
public class TableIdAction {
	@Resource
	private TableIdService tableIdService;

	@GetMapping("/list")
	public ErrorInfo list(TableIdQuery query ) {
		Container<TableId> container = tableIdService.findContainer(query);
		return ErrorInfo.ok(container);
	}

	@GetMapping("/find")
	public ErrorInfo find(Long id) {
		TableId tableId=tableIdService.findById(id);
		return ErrorInfo.ok(tableId);
	}

 

	@PostMapping("/del")
	public ErrorInfo del(Long id) {
		tableIdService.deleteById(id);
		return ErrorInfo.ok();
	}

	@PostMapping("/save")
	public ErrorInfo save(TableId tableId) {
		tableIdService.save(tableId);
		return ErrorInfo.ok();
	}

	@PostMapping("/update")
	public ErrorInfo update(TableId tableId) {
		tableIdService.update(tableId);
		return ErrorInfo.ok();
	}
}
