package com.hzizs.analysis.action;


import java.util.List;
import javax.annotation.Resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.hzizs.analysis.dto.SysRoleDto;
import com.hzizs.analysis.entity.SysRole;
import com.hzizs.analysis.facade.SysRoleFacade;
import com.hzizs.analysis.query.SysRoleQuery;
import com.hzizs.analysis.service.SysRoleService;
import com.hzizs.pojo.ErrorInfo;
import com.hzizs.vo.Container;

/**
 * 系统角色控制器
 * 
 * @author crazy_cabbage
 *
 */
@RestController
@RequestMapping("/sysRole")
public class SysRoleAction {
  @Resource
  private SysRoleService sysRoleService;
  @Resource
  private SysRoleFacade sysRoleFacade;

  @GetMapping("/list")
  public ErrorInfo list(SysRoleQuery query) {
    Container<SysRole> container = sysRoleService.findContainer(query);
    return ErrorInfo.ok(container);
  }

  @GetMapping("/find")
  public ErrorInfo find(Long id) {
    SysRoleDto dto = sysRoleFacade.findById(id);
    return ErrorInfo.ok(dto);
  }

  @GetMapping("/findAll")
  public ErrorInfo findAll() {
    List<SysRole> sysRoles = sysRoleService.findAll();
    return ErrorInfo.ok(sysRoles);
  }


  @PostMapping("/del")
  public ErrorInfo del(Long[] id) {
    sysRoleFacade.deleteByIds(id);
    return ErrorInfo.ok();
  }

  @PostMapping("/save")
  public ErrorInfo save(SysRoleDto dto) {
    sysRoleFacade.save(dto);
    return ErrorInfo.ok();
  }

  @PostMapping("/update")
  public ErrorInfo update(SysRoleDto dto) {
    sysRoleFacade.update(dto);
    return ErrorInfo.ok();
  }
}
