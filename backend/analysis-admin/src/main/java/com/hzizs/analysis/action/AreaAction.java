package com.hzizs.analysis.action;


import java.util.List;
import javax.annotation.Resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hzizs.analysis.entity.Area;
import com.hzizs.analysis.facade.AreaFacade;
import com.hzizs.analysis.query.AreaQuery;
import com.hzizs.analysis.service.AreaService;
import com.hzizs.pojo.ErrorInfo;
import com.hzizs.vo.Container;

/**
 * 区域控制器
 * 
 * @author crazy_cabbage
 *
 */
@RestController
@RequestMapping("/area")
public class AreaAction {
  @Resource
  private AreaService areaService;

  @Resource
  private AreaFacade areaFacade;

  @GetMapping("/list")
  public ErrorInfo list(AreaQuery query) {
    Container<Area> container = areaService.findContainer(query);
    return ErrorInfo.ok(container);
  }

  @GetMapping("/find")
  public ErrorInfo find(Long id) {
    Area area = areaService.findById(id);
    return ErrorInfo.ok(area);
  }

  @GetMapping("/findByParentId")
  public ErrorInfo findByParentId(Long parentId) {
    List<Area> areas = areaService.findByParentId(parentId);
    return ErrorInfo.ok(areas);
  }

  @PostMapping("/del")
  public ErrorInfo del(Long[] id) {
    areaFacade.deleteByIds(id);
    return ErrorInfo.ok();
  }

  @PostMapping("/save")
  public ErrorInfo save(Area area) {
    areaFacade.save(area);
    return ErrorInfo.ok();
  }

  @PostMapping("/update")
  public ErrorInfo update(Area area) {
    areaFacade.update(area);
    return ErrorInfo.ok();
  }
}
