package com.hzizs.analysis.action;


import javax.annotation.Resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hzizs.analysis.entity.LinkItem;
import com.hzizs.analysis.query.LinkItemQuery;
import com.hzizs.analysis.service.LinkItemService;
import com.hzizs.pojo.ErrorInfo;
import com.hzizs.vo.Container;
/**
 * 友情链接条目控制器
 * @author crazy_cabbage
 *
 */
@RestController
@RequestMapping("/linkItem")
public class LinkItemAction {
	@Resource
	private LinkItemService linkItemService;

	@GetMapping("/list")
	public ErrorInfo list(LinkItemQuery query ) {
		Container<LinkItem> container = linkItemService.findContainer(query);
		return ErrorInfo.ok(container);
	}

	@GetMapping("/find")
	public ErrorInfo find(Long id) {
		LinkItem linkItem=linkItemService.findById(id);
		return ErrorInfo.ok(linkItem);
	}

 

	@PostMapping("/del")
	public ErrorInfo del(Long id) {
		linkItemService.deleteById(id);
		return ErrorInfo.ok();
	}

	@PostMapping("/save")
	public ErrorInfo save(LinkItem linkItem) {
		linkItemService.save(linkItem);
		return ErrorInfo.ok();
	}

	@PostMapping("/update")
	public ErrorInfo update(LinkItem linkItem) {
		linkItemService.update(linkItem);
		return ErrorInfo.ok();
	}
}
