package com.hzizs.analysis.action;


import javax.annotation.Resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hzizs.analysis.entity.SysOption;
import com.hzizs.analysis.query.SysOptionQuery;
import com.hzizs.analysis.service.SysOptionService;
import com.hzizs.pojo.ErrorInfo;
import com.hzizs.vo.Container;
/**
 * 数据选项控制器
 * @author crazy_cabbage
 *
 */
@RestController
@RequestMapping("/sysOption")
public class SysOptionAction {
	@Resource
	private SysOptionService sysOptionService;

	@GetMapping("/list")
	public ErrorInfo list(SysOptionQuery query ) {
		Container<SysOption> container = sysOptionService.findContainer(query);
		return ErrorInfo.ok(container);
	}

	@GetMapping("/find")
	public ErrorInfo find(Long id) {
		SysOption sysOption=sysOptionService.findById(id);
		return ErrorInfo.ok(sysOption);
	}

 

	@PostMapping("/del")
	public ErrorInfo del(Long[] id) {
		sysOptionService.deleteByIds(id);
		return ErrorInfo.ok();
	}

	@PostMapping("/save")
	public ErrorInfo save(SysOption sysOption) {
		sysOptionService.save(sysOption);
		return ErrorInfo.ok();
	}

	@PostMapping("/update")
	public ErrorInfo update(SysOption sysOption) {
		sysOptionService.update(sysOption);
		return ErrorInfo.ok();
	}
}
