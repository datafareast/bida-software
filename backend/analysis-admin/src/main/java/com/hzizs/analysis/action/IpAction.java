package com.hzizs.analysis.action;


import javax.annotation.Resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hzizs.analysis.entity.Ip;
import com.hzizs.analysis.query.IpQuery;
import com.hzizs.analysis.service.IpService;
import com.hzizs.pojo.ErrorInfo;
import com.hzizs.vo.Container;
/**
 * IP控制器
 * @author crazy_cabbage
 *
 */
@RestController
@RequestMapping("/ip")
public class IpAction {
	@Resource
	private IpService ipService;

	@GetMapping("/list")
	public ErrorInfo list(IpQuery query ) {
		Container<Ip> container = ipService.findContainer(query);
		return ErrorInfo.ok(container);
	}

	@GetMapping("/find")
	public ErrorInfo find(Long id) {
		Ip ip=ipService.findById(id);
		return ErrorInfo.ok(ip);
	}

 

	@PostMapping("/del")
	public ErrorInfo del(Long id) {
		ipService.deleteById(id);
		return ErrorInfo.ok();
	}

	@PostMapping("/save")
	public ErrorInfo save(Ip ip) {
		ipService.save(ip);
		return ErrorInfo.ok();
	}

	@PostMapping("/update")
	public ErrorInfo update(Ip ip) {
		ipService.update(ip);
		return ErrorInfo.ok();
	}
}
