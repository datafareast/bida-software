package com.hzizs.analysis.action;


import javax.annotation.Resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hzizs.analysis.entity.Role;
import com.hzizs.analysis.query.RoleQuery;
import com.hzizs.analysis.service.RoleService;
import com.hzizs.pojo.ErrorInfo;
import com.hzizs.vo.Container;
/**
 * 角色控制器
 * @author crazy_cabbage
 *
 */
@RestController
@RequestMapping("/role")
public class RoleAction {
	@Resource
	private RoleService roleService;

	@GetMapping("/list")
	public ErrorInfo list(RoleQuery query ) {
		Container<Role> container = roleService.findContainer(query);
		return ErrorInfo.ok(container);
	}

	@GetMapping("/find")
	public ErrorInfo find(Long id) {
		Role role=roleService.findById(id);
		return ErrorInfo.ok(role);
	}

 

	@PostMapping("/del")
	public ErrorInfo del(Long id) {
		roleService.deleteById(id);
		return ErrorInfo.ok();
	}

	@PostMapping("/save")
	public ErrorInfo save(Role role) {
		roleService.save(role);
		return ErrorInfo.ok();
	}

	@PostMapping("/update")
	public ErrorInfo update(Role role) {
		roleService.update(role);
		return ErrorInfo.ok();
	}
}
