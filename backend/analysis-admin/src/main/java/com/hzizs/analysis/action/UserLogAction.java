package com.hzizs.analysis.action;


import javax.annotation.Resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hzizs.analysis.entity.UserLog;
import com.hzizs.analysis.query.UserLogQuery;
import com.hzizs.analysis.service.UserLogService;
import com.hzizs.pojo.ErrorInfo;
import com.hzizs.vo.Container;
/**
 * 用户日志控制器
 * @author crazy_cabbage
 *
 */
@RestController
@RequestMapping("/userLog")
public class UserLogAction {
	@Resource
	private UserLogService userLogService;

	@GetMapping("/list")
	public ErrorInfo list(UserLogQuery query ) {
		Container<UserLog> container = userLogService.findContainer(query);
		return ErrorInfo.ok(container);
	}

	@GetMapping("/find")
	public ErrorInfo find(Long id) {
		UserLog userLog=userLogService.findById(id);
		return ErrorInfo.ok(userLog);
	}

 

	@PostMapping("/del")
	public ErrorInfo del(Long id) {
		userLogService.deleteById(id);
		return ErrorInfo.ok();
	}

	@PostMapping("/save")
	public ErrorInfo save(UserLog userLog) {
		userLogService.save(userLog);
		return ErrorInfo.ok();
	}

	@PostMapping("/update")
	public ErrorInfo update(UserLog userLog) {
		userLogService.update(userLog);
		return ErrorInfo.ok();
	}
}
