package com.hzizs.analysis.action;


import java.util.List;
import javax.annotation.Resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hzizs.analysis.entity.SysData;
import com.hzizs.analysis.facade.SysDataFacade;
import com.hzizs.analysis.query.SysDataQuery;
import com.hzizs.analysis.service.SysDataService;
import com.hzizs.pojo.ErrorInfo;
import com.hzizs.vo.Container;
/**
 * 数据字典控制器
 * @author crazy_cabbage
 *
 */
@RestController
@RequestMapping("/sysData")
public class SysDataAction {
	@Resource
	private SysDataService sysDataService;
	@Resource
	private SysDataFacade sysDataFacade;

	@GetMapping("/list")
	public ErrorInfo list(SysDataQuery query ) {
		Container<SysData> container = sysDataService.findContainer(query);
		return ErrorInfo.ok(container);
	}

	@GetMapping("/find")
	public ErrorInfo find(Long id) {
		SysData sysData=sysDataService.findById(id);
		return ErrorInfo.ok(sysData);
	}

	@GetMapping("/findAll")
   public ErrorInfo findAll() {
     List<SysData> sysDatas = sysDataService.findAll();
     return ErrorInfo.ok(sysDatas);
   }

	@GetMapping("/findParent")
	public ErrorInfo findParent() {
	  List<SysData> sysDatas = sysDataService.findByParentId(0L);
	  return ErrorInfo.ok(sysDatas);
	}
	
	@GetMapping("/findByParentCode")
	public ErrorInfo findByParentCode(String parentCode) {
	  List<SysData> sysDatas = sysDataService.findByParentCode(parentCode);
	  return ErrorInfo.ok(sysDatas);
	}


	@PostMapping("/del")
	public ErrorInfo del(Long[] id) {
		sysDataFacade.deleteByIds(id);
		return ErrorInfo.ok();
	}

	@PostMapping("/save")
	public ErrorInfo save(SysData sysData) {
		sysDataService.save(sysData);
		return ErrorInfo.ok();
	}

	@PostMapping("/update")
	public ErrorInfo update(SysData sysData) {
		sysDataService.update(sysData);
		return ErrorInfo.ok();
	}
}
