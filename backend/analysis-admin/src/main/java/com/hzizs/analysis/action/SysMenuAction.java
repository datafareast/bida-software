package com.hzizs.analysis.action;


import java.util.List;
import javax.annotation.Resource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.hzizs.analysis.entity.SysMenu;
import com.hzizs.analysis.facade.SysMenuFacade;
import com.hzizs.analysis.query.SysMenuQuery;
import com.hzizs.analysis.service.SysMenuService;
import com.hzizs.pojo.ErrorInfo;
import com.hzizs.vo.Container;
/**
 * 系统菜单控制器
 * @author crazy_cabbage
 *
 */
@RestController
@RequestMapping("/sysMenu")
public class SysMenuAction {
	@Resource
	private SysMenuService sysMenuService;
	
	@Resource
	private SysMenuFacade sysMenuFacade;

	@GetMapping("/list")
	public ErrorInfo list(SysMenuQuery query ) {
		Container<SysMenu> container = sysMenuService.findContainer(query);
		return ErrorInfo.ok(container);
	}

	@GetMapping("/find")
	public ErrorInfo find(Long id) {
		SysMenu sysMenu=sysMenuService.findById(id);
		return ErrorInfo.ok(sysMenu);
	}


    @GetMapping("/findAll")
    public ErrorInfo findAll() {
       List<SysMenu> sysMenus = sysMenuService.findAll();
       return ErrorInfo.ok(sysMenus);
    }

	@PostMapping("/del")
	public ErrorInfo del(Long[] id) {
		sysMenuFacade.deleteByIds(id);
		return ErrorInfo.ok();
	}

	@PostMapping("/save")
	public ErrorInfo save(SysMenu sysMenu) {
		sysMenuService.save(sysMenu);
		return ErrorInfo.ok();
	}

	@PostMapping("/update")
	public ErrorInfo update(SysMenu sysMenu) {
		sysMenuService.update(sysMenu);
		return ErrorInfo.ok();
	}
}
