package com.hzizs.analysis.action;


import javax.annotation.Resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hzizs.analysis.entity.User;
import com.hzizs.analysis.query.UserQuery;
import com.hzizs.analysis.service.UserService;
import com.hzizs.pojo.ErrorInfo;
import com.hzizs.vo.Container;
/**
 * 用户控制器
 * @author crazy_cabbage
 *
 */
@RestController
@RequestMapping("/user")
public class UserAction {
	@Resource
	private UserService userService;

	@GetMapping("/list")
	public ErrorInfo list(UserQuery query ) {
		Container<User> container = userService.findContainer(query);
		return ErrorInfo.ok(container);
	}

	@GetMapping("/find")
	public ErrorInfo find(Long id) {
		User user=userService.findById(id);
		return ErrorInfo.ok(user);
	}

 

	@PostMapping("/del")
	public ErrorInfo del(Long id) {
		userService.deleteById(id);
		return ErrorInfo.ok();
	}

	@PostMapping("/save")
	public ErrorInfo save(User user) {
		userService.save(user);
		return ErrorInfo.ok();
	}

	@PostMapping("/update")
	public ErrorInfo update(User user) {
		userService.update(user);
		return ErrorInfo.ok();
	}
}
