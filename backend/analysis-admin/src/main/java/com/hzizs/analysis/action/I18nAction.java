package com.hzizs.analysis.action;


import javax.annotation.Resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hzizs.analysis.entity.I18n;
import com.hzizs.analysis.query.I18nQuery;
import com.hzizs.analysis.service.I18nService;
import com.hzizs.pojo.ErrorInfo;
import com.hzizs.vo.Container;
/**
 * 国际化控制器
 * @author crazy_cabbage
 *
 */
@RestController
@RequestMapping("/i18n")
public class I18nAction {
	@Resource
	private I18nService i18nService;

	@GetMapping("/list")
	public ErrorInfo list(I18nQuery query ) {
		Container<I18n> container = i18nService.findContainer(query);
		return ErrorInfo.ok(container);
	}

	@GetMapping("/find")
	public ErrorInfo find(Long id) {
		I18n i18n=i18nService.findById(id);
		return ErrorInfo.ok(i18n);
	}

 

	@PostMapping("/del")
	public ErrorInfo del(Long[] id) {
		i18nService.deleteByIds(id);
		return ErrorInfo.ok();
	}

	@PostMapping("/save")
	public ErrorInfo save(I18n i18n) {
		i18nService.save(i18n);
		return ErrorInfo.ok();
	}

	@PostMapping("/update")
	public ErrorInfo update(I18n i18n) {
		i18nService.update(i18n);
		return ErrorInfo.ok();
	}
}
