package com.hzizs.analysis.action;


import javax.annotation.Resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hzizs.analysis.entity.SysFile;
import com.hzizs.analysis.query.SysFileQuery;
import com.hzizs.analysis.service.SysFileService;
import com.hzizs.pojo.ErrorInfo;
import com.hzizs.vo.Container;
/**
 * 系统文件控制器
 * @author crazy_cabbage
 *
 */
@RestController
@RequestMapping("/sysFile")
public class SysFileAction {
	@Resource
	private SysFileService sysFileService;

	@GetMapping("/list")
	public ErrorInfo list(SysFileQuery query ) {
		Container<SysFile> container = sysFileService.findContainer(query);
		return ErrorInfo.ok(container);
	}

	@GetMapping("/find")
	public ErrorInfo find(Long id) {
		SysFile sysFile=sysFileService.findById(id);
		return ErrorInfo.ok(sysFile);
	}

 

	@PostMapping("/del")
	public ErrorInfo del(Long id) {
		sysFileService.deleteById(id);
		return ErrorInfo.ok();
	}

	@PostMapping("/save")
	public ErrorInfo save(SysFile sysFile) {
		sysFileService.save(sysFile);
		return ErrorInfo.ok();
	}

	@PostMapping("/update")
	public ErrorInfo update(SysFile sysFile) {
		sysFileService.update(sysFile);
		return ErrorInfo.ok();
	}
}
