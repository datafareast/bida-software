package com.hzizs.analysis.action;


import javax.annotation.Resource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.hzizs.ActionContext;
import com.hzizs.analysis.dto.SysUserInfoDto;
import com.hzizs.analysis.entity.SysUserInfo;
import com.hzizs.analysis.facade.SysUserFacade;
import com.hzizs.analysis.query.SysUserInfoQuery;
import com.hzizs.analysis.service.SysUserInfoService;
import com.hzizs.constants.Enabled;
import com.hzizs.pojo.ErrorInfo;
import com.hzizs.vo.Container;

/**
 * 系统用户控制器
 * 
 * @author crazy_cabbage
 *
 */
@RestController
@RequestMapping("/sysUser")
public class SysUserAction {
  @Resource
  private SysUserInfoService sysUserInfoService;
  @Resource
  private SysUserFacade sysUserFacade;

  @GetMapping("/list")
  public ErrorInfo list(SysUserInfoQuery query) {
    if (query == null) {
      query = new SysUserInfoQuery();
    }
    query.setIsDel(Enabled.T);
    query.setIsManager(Enabled.F);
    Container<SysUserInfo> container = sysUserInfoService.findContainer(query);
    return ErrorInfo.ok(container);
  }

  @GetMapping("/find")
  public ErrorInfo find(Long id) {
    SysUserInfoDto dto = sysUserFacade.findById(id);
    return ErrorInfo.ok(dto);
  }



  @GetMapping("/info")
  public ErrorInfo info() {
    Long userId = ActionContext.getUserId();
    SysUserInfo userInfo = sysUserInfoService.findById(userId);
    return ErrorInfo.ok(userInfo);
  }

  @PostMapping("/updateOwner")
  public ErrorInfo updateOwner(SysUserInfo userInfo) {
    Long userId = ActionContext.getUserId();
    userInfo.setId(userId);
    sysUserInfoService.updateInfo2(userInfo);
    return ErrorInfo.ok();
  }

  @PostMapping("/resetPassword")
  public ErrorInfo resetPassword(Long id, String password) {
    sysUserFacade.updateResetPassword(id, password);
    return ErrorInfo.ok();
  }

  @PostMapping("/del")
  public ErrorInfo del(Long[] id) {
    sysUserFacade.deleteByIds(id);
    return ErrorInfo.ok();
  }

  @PostMapping("/save")
  public ErrorInfo save(SysUserInfoDto dto) {
    sysUserFacade.save(dto);
    return ErrorInfo.ok();
  }

  @PostMapping("/update")
  public ErrorInfo update(SysUserInfoDto dto) {
    sysUserFacade.update(dto);
    return ErrorInfo.ok();
  }
}
