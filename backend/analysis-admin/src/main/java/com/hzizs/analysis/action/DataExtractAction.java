package com.hzizs.analysis.action;


import java.util.List;
import java.util.Map;
import javax.annotation.Resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hzizs.analysis.entity.DataExtract;
import com.hzizs.analysis.facade.DataExtractFacade;
import com.hzizs.analysis.query.DataExtractQuery;
import com.hzizs.analysis.service.DataExtractService;
import com.hzizs.pojo.ErrorInfo;
import com.hzizs.vo.Container;
/**
 * 数据抽取控制器
 * @author crazy_cabbage
 *
 */
@RestController
@RequestMapping("/dataExtract")
public class DataExtractAction {
	@Resource
	private DataExtractService dataExtractService;
	@Resource
	private DataExtractFacade dataExtractFacade;

	@GetMapping("/list")
	public ErrorInfo list(DataExtractQuery query ) {
		Container<DataExtract> container = dataExtractService.findContainer(query);
		return ErrorInfo.ok(container);
	}

	@GetMapping("/find")
	public ErrorInfo find(Long id) {
		DataExtract dataExtract=dataExtractService.findById(id);
		return ErrorInfo.ok(dataExtract);
	}

	@GetMapping("/preview")
     public ErrorInfo preview(Long dataSourceId,String query) {
       List<Map<String,Object>> list = dataExtractFacade.preview(dataSourceId,query);
       return ErrorInfo.ok(list);
     }

	@PostMapping("/del")
	public ErrorInfo del(Long id) {
		dataExtractService.deleteById(id);
		return ErrorInfo.ok();
	}

	@PostMapping("/save")
	public ErrorInfo save(DataExtract dataExtract) {
		dataExtractService.save(dataExtract);
		return ErrorInfo.ok();
	}

	@PostMapping("/update")
	public ErrorInfo update(DataExtract dataExtract) {
		dataExtractService.update(dataExtract);
		return ErrorInfo.ok();
	}
}
