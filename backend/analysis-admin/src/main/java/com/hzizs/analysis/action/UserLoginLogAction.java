package com.hzizs.analysis.action;


import javax.annotation.Resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hzizs.analysis.entity.UserLoginLog;
import com.hzizs.analysis.query.UserLoginLogQuery;
import com.hzizs.analysis.service.UserLoginLogService;
import com.hzizs.pojo.ErrorInfo;
import com.hzizs.vo.Container;
/**
 * 用户登录日志控制器
 * @author crazy_cabbage
 *
 */
@RestController
@RequestMapping("/userLoginLog")
public class UserLoginLogAction {
	@Resource
	private UserLoginLogService userLoginLogService;

	@GetMapping("/list")
	public ErrorInfo list(UserLoginLogQuery query ) {
		Container<UserLoginLog> container = userLoginLogService.findContainer(query);
		return ErrorInfo.ok(container);
	}

	@GetMapping("/find")
	public ErrorInfo find(Long id) {
		UserLoginLog userLoginLog=userLoginLogService.findById(id);
		return ErrorInfo.ok(userLoginLog);
	}

 

	@PostMapping("/del")
	public ErrorInfo del(Long id) {
		userLoginLogService.deleteById(id);
		return ErrorInfo.ok();
	}

	@PostMapping("/save")
	public ErrorInfo save(UserLoginLog userLoginLog) {
		userLoginLogService.save(userLoginLog);
		return ErrorInfo.ok();
	}

	@PostMapping("/update")
	public ErrorInfo update(UserLoginLog userLoginLog) {
		userLoginLogService.update(userLoginLog);
		return ErrorInfo.ok();
	}
}
