package com.hzizs.analysis.action;


import javax.annotation.Resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hzizs.analysis.entity.RoleMenu;
import com.hzizs.analysis.query.RoleMenuQuery;
import com.hzizs.analysis.service.RoleMenuService;
import com.hzizs.pojo.ErrorInfo;
import com.hzizs.vo.Container;
/**
 * 角色菜单控制器
 * @author crazy_cabbage
 *
 */
@RestController
@RequestMapping("/roleMenu")
public class RoleMenuAction {
	@Resource
	private RoleMenuService roleMenuService;

	@GetMapping("/list")
	public ErrorInfo list(RoleMenuQuery query ) {
		Container<RoleMenu> container = roleMenuService.findContainer(query);
		return ErrorInfo.ok(container);
	}

	@GetMapping("/find")
	public ErrorInfo find(Long id) {
		RoleMenu roleMenu=roleMenuService.findById(id);
		return ErrorInfo.ok(roleMenu);
	}

 

	@PostMapping("/del")
	public ErrorInfo del(Long id) {
		roleMenuService.deleteById(id);
		return ErrorInfo.ok();
	}

	@PostMapping("/save")
	public ErrorInfo save(RoleMenu roleMenu) {
		roleMenuService.save(roleMenu);
		return ErrorInfo.ok();
	}

	@PostMapping("/update")
	public ErrorInfo update(RoleMenu roleMenu) {
		roleMenuService.update(roleMenu);
		return ErrorInfo.ok();
	}
}
