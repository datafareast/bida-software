package com.hzizs.analysis.action;


import javax.annotation.Resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hzizs.analysis.entity.SysUserLog;
import com.hzizs.analysis.query.SysUserLogQuery;
import com.hzizs.analysis.service.SysUserLogService;
import com.hzizs.pojo.ErrorInfo;
import com.hzizs.vo.Container;
/**
 * 系统用户日志控制器
 * @author crazy_cabbage
 *
 */
@RestController
@RequestMapping("/sysUserLog")
public class SysUserLogAction {
	@Resource
	private SysUserLogService sysUserLogService;

	@GetMapping("/list")
	public ErrorInfo list(SysUserLogQuery query ) {
		Container<SysUserLog> container = sysUserLogService.findContainer(query);
		return ErrorInfo.ok(container);
	}

	@GetMapping("/find")
	public ErrorInfo find(Long id) {
		SysUserLog sysUserLog=sysUserLogService.findById(id);
		return ErrorInfo.ok(sysUserLog);
	}

 

	@PostMapping("/del")
	public ErrorInfo del(Long id) {
		sysUserLogService.deleteById(id);
		return ErrorInfo.ok();
	}

	@PostMapping("/save")
	public ErrorInfo save(SysUserLog sysUserLog) {
		sysUserLogService.save(sysUserLog);
		return ErrorInfo.ok();
	}

	@PostMapping("/update")
	public ErrorInfo update(SysUserLog sysUserLog) {
		sysUserLogService.update(sysUserLog);
		return ErrorInfo.ok();
	}
}
