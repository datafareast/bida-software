package com.hzizs.analysis.action;


import javax.annotation.Resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hzizs.analysis.entity.SlideItem;
import com.hzizs.analysis.query.SlideItemQuery;
import com.hzizs.analysis.service.SlideItemService;
import com.hzizs.pojo.ErrorInfo;
import com.hzizs.vo.Container;
/**
 * 轮播图条目控制器
 * @author crazy_cabbage
 *
 */
@RestController
@RequestMapping("/slideItem")
public class SlideItemAction {
	@Resource
	private SlideItemService slideItemService;

	@GetMapping("/list")
	public ErrorInfo list(SlideItemQuery query ) {
		Container<SlideItem> container = slideItemService.findContainer(query);
		return ErrorInfo.ok(container);
	}

	@GetMapping("/find")
	public ErrorInfo find(Long id) {
		SlideItem slideItem=slideItemService.findById(id);
		return ErrorInfo.ok(slideItem);
	}

 

	@PostMapping("/del")
	public ErrorInfo del(Long id) {
		slideItemService.deleteById(id);
		return ErrorInfo.ok();
	}

	@PostMapping("/save")
	public ErrorInfo save(SlideItem slideItem) {
		slideItemService.save(slideItem);
		return ErrorInfo.ok();
	}

	@PostMapping("/update")
	public ErrorInfo update(SlideItem slideItem) {
		slideItemService.update(slideItem);
		return ErrorInfo.ok();
	}
}
