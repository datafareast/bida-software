package com.hzizs.config;

import org.springframework.session.FlushMode;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;

@EnableRedisHttpSession(maxInactiveIntervalInSeconds = 1800, flushMode = FlushMode.ON_SAVE, redisNamespace = "analysis-admin")
public class SessionConfig {

}
