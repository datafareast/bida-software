package com.hzizs.config;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.Filter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.format.FormatterRegistry;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.web.bind.support.ConfigurableWebBindingInitializer;
import org.springframework.web.servlet.config.annotation.ContentNegotiationConfigurer;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.PathMatchConfigurer;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import com.hzizs.spring.converter.StringHttpMessageConverter;
import com.hzizs.spring.converter.json.MappingJackson2HttpMessageConverter;
import com.hzizs.spring.converter.xml.MappingJackson2XmlHttpMessageConverter;
import com.hzizs.spring.core.convert.support.StringToDateConverter;
import com.hzizs.springboot.ProfileType;
import com.hzizs.web.filter.ActionContextFilter;
import com.hzizs.web.filter.ExceptionFilter;
import com.hzizs.web.filter.xss.XSSFilter;

@Configuration
@EnableWebMvc
public class WebMvcConfiguration implements WebMvcConfigurer {
  @Autowired
  private ProfileType profileType;

  @Override
  public void configureContentNegotiation(ContentNegotiationConfigurer configurer) {
    // set path extension to true
    configurer.
//    favorPathExtension(true).
    // set favor parameter to false
        favorParameter(false).
        // ignore the accept headers
        ignoreAcceptHeader(true).
        // dont use Java Activation Framework since we are manually
        // specifying the mediatypes required below
        useRegisteredExtensionsOnly(false).defaultContentType(MediaType.APPLICATION_JSON).mediaType("xml", MediaType.APPLICATION_XML)
        .mediaType("json", MediaType.APPLICATION_JSON).mediaType("jpg", MediaType.IMAGE_JPEG);
  }

  @SuppressWarnings("deprecation")
  @Override
  public void configurePathMatch(PathMatchConfigurer configurer) {
    configurer.setUseSuffixPatternMatch(false);
    configurer.setUseRegisteredSuffixPatternMatch(true);
  }

  @Override
  public void addFormatters(FormatterRegistry registry) {
    registry.addConverter(new StringToDateConverter());
  }

  @Override
  public void extendMessageConverters(List<HttpMessageConverter<?>> converters) {
    converters.clear();
    converters.add(new MappingJackson2HttpMessageConverter());
    converters.add(new MappingJackson2XmlHttpMessageConverter());
    converters.add(new StringHttpMessageConverter());
  }


  @Bean
  public ConfigurableWebBindingInitializer configurableWebBindingInitializer() {
    return new ConfigurableWebBindingInitializer();
  }

  @Bean
  public FilterRegistrationBean<Filter> actionContextFilter() {
    FilterRegistrationBean<Filter> filterBean = new FilterRegistrationBean<Filter>();
    filterBean.setFilter(new ActionContextFilter());
    filterBean.setName("actionContextFilter");
    filterBean.addUrlPatterns("/*");
    filterBean.setOrder(2);
    return filterBean;
  }

  @Bean
  public FilterRegistrationBean<Filter> xssFilter() {
    FilterRegistrationBean<Filter> filterBean = new FilterRegistrationBean<Filter>();
    filterBean.setFilter(new XSSFilter());
    Map<String, String> parameters = new HashMap<String, String>();
    parameters.put("richTextWhitelistConf", "classpath:/xss/richtext_whitelist.json");
    parameters.put("xssConf", "classpath:/xss/xss.json");
    filterBean.setInitParameters(parameters);
    filterBean.setName("xssFilter");
    filterBean.addUrlPatterns("/*");
    filterBean.setOrder(1);
    return filterBean;
  }

  @Bean
  public FilterRegistrationBean<Filter> exceptionFilter() {
    FilterRegistrationBean<Filter> filterBean = new FilterRegistrationBean<Filter>();
    filterBean.setFilter(new ExceptionFilter());
    filterBean.setName("exceptionFilter");
    filterBean.addUrlPatterns("/*");
    filterBean.setOrder(Ordered.HIGHEST_PRECEDENCE);
    return filterBean;
  }
  // @Bean
  // public CorsFilter corsFilter() {
  // UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
  // source.registerCorsConfiguration("/**", buildConfig());
  // CorsFilter corsFilter = new CorsFilter(source);
  // return corsFilter;
  // }

  @Override
  public void addCorsMappings(CorsRegistry registry) {
    registry.addMapping("/**").allowedHeaders("*").allowedOrigins("*").allowedOriginPatterns("*")
        // .allowCredentials(true)
        // .allowedMethods("simple")
        .allowedMethods("GET", "POST", "PUT", "DELETE", "HEAD", "PATCH", "DELETE", "OPTIONS", "TRACE").maxAge(3600L);
  }

  // @Override
  // public void addInterceptors(InterceptorRegistry registry) {
  // //只要这里拦截就可以了
  //// registry.addInterceptor(null).addPathPatterns("/**");
  // }

  @Override
  public void addResourceHandlers(ResourceHandlerRegistry registry) {
    if (!ProfileType.PROD.equals(profileType.getActive())) {
      registry.addResourceHandler("/swagger-ui/**").addResourceLocations("classpath:/META-INF/resources/webjars/springfox-swagger-ui/").resourceChain(false);
    }

  }

  // private CorsConfiguration buildConfig() {
  // CorsConfiguration corsConfiguration = new CorsConfiguration();
  //
  // // 允许跨域访问的域名
  // corsConfiguration.addAllowedOrigin("*");
  // // 请求头
  // corsConfiguration.addAllowedHeader("*");
  // // 请求方法
  // corsConfiguration.addAllowedMethod(HttpMethod.DELETE);
  // corsConfiguration.addAllowedMethod(HttpMethod.POST);
  // corsConfiguration.addAllowedMethod(HttpMethod.GET);
  // corsConfiguration.addAllowedMethod(HttpMethod.PUT);
  // corsConfiguration.addAllowedMethod(HttpMethod.OPTIONS);
  // // 预检请求的有效期，单位为秒。
  // corsConfiguration.setMaxAge(3600L);
  // // 是否支持发送cookie
  // corsConfiguration.setAllowCredentials(true);
  //
  // return corsConfiguration;
  // }

}
