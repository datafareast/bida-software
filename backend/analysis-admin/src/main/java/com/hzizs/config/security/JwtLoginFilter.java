package com.hzizs.config.security;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import com.hzizs.ActionContext;
import com.hzizs.analysis.facade.SysUserFacade;
import com.hzizs.analysis.service.SysOptionService;
import com.hzizs.constants.CommonConstants;
import com.hzizs.dto.LoginLogDto;
import com.hzizs.json.JsonUtil;
import com.hzizs.pojo.ErrorInfo;
import com.hzizs.util.StringUtil;
import com.hzizs.util.prefs.Md5Util;
import com.hzizs.web.util.JwtUtil;
import com.hzizs.web.util.RequestUtil;

public class JwtLoginFilter extends AbstractAuthenticationProcessingFilter {
  private SysUserFacade sysUserFacade;
  private JwtUtil jwtUtil;
  private SysOptionService sysOptionService;

  @Autowired
  public void setJwtUtil(JwtUtil jwtUtil) {
    this.jwtUtil = jwtUtil;
  }

  @Autowired
  public void setSysUserFacade(SysUserFacade sysUserFacade) {
    this.sysUserFacade = sysUserFacade;
  }
  
  @Autowired
  public void setSysOptionService(SysOptionService sysOptionService) {
    this.sysOptionService = sysOptionService;
  }

  @Autowired
  public void setAuthenticationManager(AuthenticationManager authenticationManager) {
    super.setAuthenticationManager(authenticationManager);
  }


  public JwtLoginFilter(String defaultFilterProcessesUrl) {
    super(defaultFilterProcessesUrl);
  }

  @Override
  public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
      throws AuthenticationException, IOException, ServletException {
    String username = request.getParameter("username");
    String password = request.getParameter("password");
    String captcha = request.getParameter("captcha");
    // 用于记录端
    String src = request.getParameter("src");

    if (StringUtil.isEmpty(src)) {
      throw new UsernameNotFoundException("src can't be null");
    }
    // 比较验证码
    if (StringUtil.isEmpty(username)) {
      throw new UsernameNotFoundException("username parameters can't be null");
    }
    // 验证码异常这里判断
    if (StringUtil.isEmpty(password)) {
      throw new UsernameNotFoundException("password parameters can't be null");
    }
    if (StringUtil.isEmpty(captcha)) {
      throw new UsernameNotFoundException("captcha parameters can't be null");
    }
    HttpSession session = request.getSession();
    String sessionCaptcha = (String) session.getAttribute(CommonConstants.CAPTCHA);
    session.removeAttribute(CommonConstants.CAPTCHA);
    if (!captcha.equalsIgnoreCase(sessionCaptcha)) {
      throw new UsernameNotFoundException("验证码出错");

    }
    String salt = sysOptionService.findAdminSalt();
    UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(username, Md5Util.saltPassword(salt, password));
    return getAuthenticationManager().authenticate(authenticationToken);
  }

  @Override
  protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authResult)
      throws IOException, ServletException {
    Map<String, Object> attributes = new HashMap<String, Object>(42);
    String username = ActionContext.getUsername();
    String code = ActionContext.getCorpCode();
    Long userId = ActionContext.getUserId();
    String role = ActionContext.getRole();
    attributes.put(CommonConstants.USERNAME, username);
    attributes.put(CommonConstants.CORP_CODE, code);
    attributes.put(CommonConstants.USER_ID, userId);
    attributes.put(CommonConstants.ROLE, role);
    // 过期时间
    String token = jwtUtil.createToken(attributes);
    // 记录登录日志
    LoginLogDto dto = new LoginLogDto();
    dto.setCorpCode(request.getParameter("corpCode"));
    dto.setUsername(request.getParameter("username"));
    dto.setUserId(userId);
    dto.setIp(RequestUtil.getRemoteAddr(request));
    dto.setUserAgent(RequestUtil.userAgent(request));
    dto.setSrc(request.getParameter("src"));
    sysUserFacade.saveLoginSuccess(dto);
    Map<String, Object> root = new HashMap<String, Object>(1);
    root.put("token", token);
    ErrorInfo errorInfo = new ErrorInfo();
    errorInfo.setObj(root);
    response.setCharacterEncoding("UTF-8");
    response.setContentType("application/json;charset=utf-8");
    response.setStatus(HttpServletResponse.SC_OK);
    // Cookie cookie = new Cookie(JwtAuthenticationFilter.COOKIE_NAME, token);
    // response.addCookie(cookie);
    PrintWriter pw = response.getWriter();

    pw.write(JsonUtil.toString(errorInfo));

    pw.flush();
  }

  @Override
  protected void unsuccessfulAuthentication(HttpServletRequest request, HttpServletResponse response, AuthenticationException failed)
      throws IOException, ServletException {
    Long userId = ActionContext.getUserId();
    // 验证码异常 不记录
    if (userId != null) {
      LoginLogDto dto = new LoginLogDto();
      dto.setUserId(userId);
      dto.setCorpCode(request.getParameter("corpCode"));
      dto.setUsername(request.getParameter("username"));
      dto.setIp(RequestUtil.getRemoteAddr(request));
      dto.setUserAgent(RequestUtil.userAgent(request));
      dto.setSrc(request.getParameter("src"));
      sysUserFacade.saveLoginFailure(dto);
    }
    response.setCharacterEncoding("UTF-8");
    response.setContentType("application/json;charset=utf-8");
    response.setStatus(HttpServletResponse.SC_OK);
    ErrorInfo errorInfo = new ErrorInfo();
    errorInfo.setFlag(false);
    errorInfo.setMsg(failed.getMessage());
    PrintWriter pw = response.getWriter();
    pw.write(JsonUtil.toString(errorInfo));
    pw.flush();
  }

}
