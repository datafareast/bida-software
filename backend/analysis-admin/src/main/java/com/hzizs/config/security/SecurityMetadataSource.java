package com.hzizs.config.security;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.access.SecurityConfig;
import org.springframework.security.web.FilterInvocation;
import org.springframework.security.web.access.intercept.FilterInvocationSecurityMetadataSource;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;
import org.springframework.stereotype.Component;
import com.hzizs.analysis.entity.SysAuth;
import com.hzizs.analysis.service.SysAuthService;


@Component("securityMetadataSource")
public class SecurityMetadataSource implements FilterInvocationSecurityMetadataSource {
  @Resource
  private SysAuthService sysAuthService;
  private Map<RequestMatcher, Collection<ConfigAttribute>> urlMap;

  @PostConstruct
  private void init() {
    List<SysAuth> auths = sysAuthService.findAll();
    Map<RequestMatcher, Collection<ConfigAttribute>> map = auths.parallelStream().collect(Collectors.toMap(auth -> {
      return new AntPathRequestMatcher(auth.getUrl());
    }, auth -> {
      return Arrays.asList(new SecurityConfig(auth.getCode()));
    }));
    urlMap = map;
  }

  @Override
  public Collection<ConfigAttribute> getAttributes(Object object) throws IllegalArgumentException {
    FilterInvocation filterInvocation = (FilterInvocation) object;
    HttpServletRequest request = filterInvocation.getHttpRequest();
    for (RequestMatcher requestMatcher : urlMap.keySet()) {
      if (requestMatcher.matches(request)) {
        return urlMap.get(requestMatcher);
      }
    }
    return null;
  }

  @Override
  public Collection<ConfigAttribute> getAllConfigAttributes() {
    return null;
  }

  @Override
  public boolean supports(Class<?> clazz) {
    return true;
  }

}
