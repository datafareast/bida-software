package com.hzizs.config;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.SchedulingConfigurer;
import org.springframework.scheduling.config.ScheduledTaskRegistrar;

@Configuration("scheduleConfig")
@EnableScheduling
public class ScheduleConfig implements SchedulingConfigurer {
  private ScheduledExecutorService scheduledExecutorService;

  @Override
  public void configureTasks(ScheduledTaskRegistrar taskRegistrar) {
    scheduledExecutorService = Executors.newScheduledThreadPool(5);
    taskRegistrar.setScheduler(scheduledExecutorService);
  }

  public void add(Runnable run) {
    scheduledExecutorService.schedule(run, 1, TimeUnit.MICROSECONDS);
  }
}
