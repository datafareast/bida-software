package com.hzizs.config.security;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import com.hzizs.ActionContext;
import com.hzizs.analysis.facade.SysRoleFacade;
import com.hzizs.err.biz.account.SessionInvalidErr;
import com.hzizs.util.StringUtil;
import com.hzizs.web.util.JwtUtil;

public class JwtAuthenticationFilter extends BasicAuthenticationFilter {
  public static final String TOKEN_HEADER = "Authorization";
  public static final String TOKEN_PREFIX = "Bearer ";
  public static final String COOKIE_NAME = "analysis-admin";
  private JwtUtil jwtUtil;
  private SysRoleFacade sysRoleFacade;

  @Autowired
  public void setJwtUtil(JwtUtil jwtUtil) {
    this.jwtUtil = jwtUtil;
  }

  public JwtAuthenticationFilter(AuthenticationManager authenticationManager) {
    super(authenticationManager);
  }

  @Autowired
  public void setSysRoleFacade(SysRoleFacade sysRoleFacade) {
    this.sysRoleFacade = sysRoleFacade;
  }

  @Override
  protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws IOException, ServletException {
    String tokenHeader = request.getHeader(TOKEN_HEADER);
    String token = null;
    if (StringUtil.isNotEmpty(tokenHeader)) {
      if (tokenHeader.startsWith(TOKEN_PREFIX)) {
        token = tokenHeader.substring(TOKEN_PREFIX.length());
      }
    } else {
      // 兼容网页
      // Cookie[] cookies = request.getCookies();
      // if (cookies != null) {
      // for (Cookie cookie : cookies) {
      // if (cookie.getName().equals(COOKIE_NAME)) {
      // token = cookie.getValue();
      // }
      // }
      // }
    }
    if (token != null) {
      String username = null;
      String role = null;
      Long userId = null;
      try {
        username = jwtUtil.getUsername(token);
        userId = jwtUtil.getUserId(token);
        role = jwtUtil.getRole(token);
        ActionContext.setUserId(userId);
        ActionContext.setUsername(username);
        ActionContext.setRole(role);
      } catch (Exception e) {
        throw new SessionInvalidErr(e);
      }
      if (SecurityContextHolder.getContext().getAuthentication() == null) {
        // 根据角色查找权限集合
        List<String> auths = sysRoleFacade.findAuthByRoleCode(role);
        List<SimpleGrantedAuthority> authorities = auths.parallelStream().map(SimpleGrantedAuthority::new).collect(Collectors.toList());
        UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(username, null, authorities);
        authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
        SecurityContextHolder.getContext().setAuthentication(authentication);
      }
    }
    filterChain.doFilter(request, response);
  }
}
