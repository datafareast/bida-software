package com.hzizs.config.security;

import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import com.hzizs.ActionContext;
import com.hzizs.analysis.facade.SysRoleFacade;
import com.hzizs.analysis.facade.SysUserFacade;
import com.hzizs.dto.UserDto;

@Component
public class UserDetailsServiceImpl implements UserDetailsService {
  private SysUserFacade sysUserFacade;
  private SysRoleFacade sysRoleFacade;

  @Autowired
  public void setSysUserFacade(SysUserFacade sysUserFacade) {
    this.sysUserFacade = sysUserFacade;
  }

  @Autowired
  public void setSysRoleFacade(SysRoleFacade sysRoleFacade) {
    this.sysRoleFacade = sysRoleFacade;
  }

  @Override
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    UserDto userDto = sysUserFacade.findByUsername(username);
    if (userDto == null) {
      throw new UsernameNotFoundException("该用户不存在");
    }
    ActionContext.setUserId(userDto.getUserId());
    ActionContext.setUsername(userDto.getUsername());
    ActionContext.setCorpCode(userDto.getCorpCode());
    ActionContext.setCorpId(userDto.getCorpId());
    ActionContext.setRole(userDto.getRole());
    List<String> auths = sysRoleFacade.findAuthByRoleCode(userDto.getRole());
    List<GrantedAuthority> authorities = auths.parallelStream().map(SimpleGrantedAuthority::new).collect(Collectors.toList());
    User user = new User(userDto.getUsername(), userDto.getPassword(), userDto.isEnabled(), userDto.isLocked(), true, true, authorities);
    return user;
  }
}
