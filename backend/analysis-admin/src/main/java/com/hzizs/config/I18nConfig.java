package com.hzizs.config;

import java.util.Locale;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.i18n.CookieLocaleResolver;

@Configuration
public class I18nConfig {
  @Bean
  public LocaleResolver localeResolver2() {
    CookieLocaleResolver clr = new CookieLocaleResolver();
    clr.setCookieName("i18n");
    clr.setDefaultLocale(Locale.SIMPLIFIED_CHINESE);
    clr.setCookieMaxAge(3600 * 12);
    return clr;
  }
}
