package com.hzizs.config.security;

import java.util.Collection;

import org.springframework.security.access.AccessDecisionManager;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Component;

@Component("securityAccessDecisionManager")
public class SecurityAccessDecisionManager implements AccessDecisionManager {

  @Override
  public void decide(Authentication authentication, Object object, Collection<ConfigAttribute> configAttributes)
      throws AccessDeniedException, InsufficientAuthenticationException {
    if (configAttributes == null) {
      return;
    }
    for (GrantedAuthority ga : authentication.getAuthorities()) {
      for (ConfigAttribute ca : configAttributes) {
        if (ga.getAuthority().equals(ca.getAttribute())) {
          return;
        }
      }
    }
    throw new AccessDeniedException("no permission");

  }

  @Override
  public boolean supports(ConfigAttribute attribute) {
    return true;
  }

  @Override
  public boolean supports(Class<?> clazz) {
    return true;
  }

}
