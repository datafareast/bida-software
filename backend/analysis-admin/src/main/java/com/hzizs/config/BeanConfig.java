package com.hzizs.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import com.hzizs.kaptcha.KaptchaHelper;
import com.hzizs.web.util.JwtUtil;

@Configuration
public class BeanConfig {

  @Bean("kaptchaHelper")
  public KaptchaHelper kaptchaHelper() {
    KaptchaHelper helper = KaptchaHelper.Builder.newInstance().setCharLength(4).build();
    return helper;
  }

  @Bean("jwtUtil")
  public JwtUtil jwtUtil() {
    return new JwtUtil("analysis-admin");
  }


}
