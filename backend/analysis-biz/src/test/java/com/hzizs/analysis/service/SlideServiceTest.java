package com.hzizs.analysis.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.hzizs.test.spring.TestBase;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.annotations.Ignore;

import com.hzizs.vo.Container;
import com.hzizs.analysis.entity.Slide;
import com.hzizs.analysis.query.SlideQuery;

/**
 * 轮播图 测试服务类
 * 
 * @author crazy_cabbage
 */
public class SlideServiceTest extends TestBase {
	private SlideService slideService;

	public SlideServiceTest() {
		slideService = (SlideService) ac.getBean("slideService");
	}

	/**
	 * 统计 轮播图
	 */
	@Test
	@Ignore
	public void count() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		long count = slideService.count(parameters);
		Assert.assertNull(count);
	}

	/**
	 * 删除 轮播图
	 */
	@Test
	@Ignore
	public void deleteEntity() {
		Slide slide = new Slide();
		slide.setId(0L);
		slideService.delete(slide);
	}

	/**
	 * 按条件删除 轮播图
	 */
	@Test
	@Ignore
	public void deleteMap() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", 1L);
		slideService.delete(parameters);

	}

	/**
	 * 按主键删除 轮播图
	 */
	@Test
	@Ignore
	public void deleteById() {
		slideService.deleteById(1L);
	}

	/**
	 * 根据条件查找  轮播图
	 */
	@Test
	@Ignore
	public void find() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		List<Slide> slides = slideService.find(parameters);
		Assert.assertNull(slides);
	}

	/**
	 * 根据主键查找 轮播图
	 */
	@Test
	@Ignore
	public void findById() {
		Slide slide = slideService.findById(1L);
		Assert.assertNull(slide);
	}

	/**
	 * 查找  轮播图
	 */
	@Test
	@Ignore
	public void findContainer() {
		SlideQuery query = new SlideQuery();
		Container<Slide> container = slideService.findContainer(query);
		Assert.assertNull(container);
	}

	/**
	 * 查找单个  轮播图
	 */
	@Test
	@Ignore
	public void findOne() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", 1L);
		Slide slide = slideService.findOne(parameters);
		Assert.assertNull(slide);
	}

	/**
	 * 保存  轮播图
	 */
	@Test
	@Ignore
	public void save() {
		Slide slide = new Slide();
		slideService.save(slide);
	}

	/**
	 * 保存或更新  轮播图
	 */
	@Test
	@Ignore
	public void saveOrUpdate() {
		Slide slide = new Slide();
		slideService.saveOrUpdate(slide);
	}

	/**
	 * 更新  轮播图
	 */
	@Test
	@Ignore
	public void update() {
		Slide slide = new Slide();
		slide.setId(0L);
		slideService.update(slide);
	}

	@Test
	@Ignore
	public void otherMethod() {
		// 新的测试内容
	}

}
