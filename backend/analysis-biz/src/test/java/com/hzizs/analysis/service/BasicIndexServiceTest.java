package com.hzizs.analysis.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.hzizs.test.spring.TestBase;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.annotations.Ignore;

import com.hzizs.vo.Container;
import com.hzizs.analysis.entity.BasicIndex;
import com.hzizs.analysis.query.BasicIndexQuery;

/**
 * 指数基指 测试服务类
 * 
 * @author crazy_cabbage
 */
public class BasicIndexServiceTest extends TestBase {
	private BasicIndexService basicIndexService;

	public BasicIndexServiceTest() {
		basicIndexService = (BasicIndexService) ac.getBean("basicIndexService");
	}

	/**
	 * 统计 指数基指
	 */
	@Test
	@Ignore
	public void count() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		long count = basicIndexService.count(parameters);
		Assert.assertNull(count);
	}

	/**
	 * 删除 指数基指
	 */
	@Test
	@Ignore
	public void deleteEntity() {
		BasicIndex basicIndex = new BasicIndex();
		basicIndex.setId(0L);
		basicIndexService.delete(basicIndex);
	}

	/**
	 * 按条件删除 指数基指
	 */
	@Test
	@Ignore
	public void deleteMap() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", 1L);
		basicIndexService.delete(parameters);

	}

	/**
	 * 按主键删除 指数基指
	 */
	@Test
	@Ignore
	public void deleteById() {
		basicIndexService.deleteById(1L);
	}

	/**
	 * 根据条件查找  指数基指
	 */
	@Test
	@Ignore
	public void find() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		List<BasicIndex> basicIndexs = basicIndexService.find(parameters);
		Assert.assertNull(basicIndexs);
	}

	/**
	 * 根据主键查找 指数基指
	 */
	@Test
	@Ignore
	public void findById() {
		BasicIndex basicIndex = basicIndexService.findById(1L);
		Assert.assertNull(basicIndex);
	}

	/**
	 * 查找  指数基指
	 */
	@Test
	@Ignore
	public void findContainer() {
		BasicIndexQuery query = new BasicIndexQuery();
		Container<BasicIndex> container = basicIndexService.findContainer(query);
		Assert.assertNull(container);
	}

	/**
	 * 查找单个  指数基指
	 */
	@Test
	@Ignore
	public void findOne() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", 1L);
		BasicIndex basicIndex = basicIndexService.findOne(parameters);
		Assert.assertNull(basicIndex);
	}

	/**
	 * 保存  指数基指
	 */
	@Test
	@Ignore
	public void save() {
		BasicIndex basicIndex = new BasicIndex();
		basicIndexService.save(basicIndex);
	}

	/**
	 * 保存或更新  指数基指
	 */
	@Test
	@Ignore
	public void saveOrUpdate() {
		BasicIndex basicIndex = new BasicIndex();
		basicIndexService.saveOrUpdate(basicIndex);
	}

	/**
	 * 更新  指数基指
	 */
	@Test
	@Ignore
	public void update() {
		BasicIndex basicIndex = new BasicIndex();
		basicIndex.setId(0L);
		basicIndexService.update(basicIndex);
	}

	@Test
	@Ignore
	public void otherMethod() {
		// 新的测试内容
	}

}
