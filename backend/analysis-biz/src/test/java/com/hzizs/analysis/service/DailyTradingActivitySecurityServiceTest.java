package com.hzizs.analysis.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.hzizs.test.spring.TestBase;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.annotations.Ignore;

import com.hzizs.vo.Container;
import com.hzizs.analysis.entity.DailyTradingActivitySecurity;
import com.hzizs.analysis.query.DailyTradingActivitySecurityQuery;

/**
 * 证券日交易 测试服务类
 * 
 * @author crazy_cabbage
 */
public class DailyTradingActivitySecurityServiceTest extends TestBase {
	private DailyTradingActivitySecurityService dailyTradingActivitySecurityService;

	public DailyTradingActivitySecurityServiceTest() {
		dailyTradingActivitySecurityService = (DailyTradingActivitySecurityService) ac.getBean("dailyTradingActivitySecurityService");
	}

	/**
	 * 统计 证券日交易
	 */
	@Test
	@Ignore
	public void count() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		long count = dailyTradingActivitySecurityService.count(parameters);
		Assert.assertNull(count);
	}

	/**
	 * 删除 证券日交易
	 */
	@Test
	@Ignore
	public void deleteEntity() {
		DailyTradingActivitySecurity dailyTradingActivitySecurity = new DailyTradingActivitySecurity();
		dailyTradingActivitySecurity.setId(0L);
		dailyTradingActivitySecurityService.delete(dailyTradingActivitySecurity);
	}

	/**
	 * 按条件删除 证券日交易
	 */
	@Test
	@Ignore
	public void deleteMap() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", 1L);
		dailyTradingActivitySecurityService.delete(parameters);

	}

	/**
	 * 按主键删除 证券日交易
	 */
	@Test
	@Ignore
	public void deleteById() {
		dailyTradingActivitySecurityService.deleteById(1L);
	}

	/**
	 * 根据条件查找  证券日交易
	 */
	@Test
	@Ignore
	public void find() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		List<DailyTradingActivitySecurity> dailyTradingActivitySecuritys = dailyTradingActivitySecurityService.find(parameters);
		Assert.assertNull(dailyTradingActivitySecuritys);
	}

	/**
	 * 根据主键查找 证券日交易
	 */
	@Test
	@Ignore
	public void findById() {
		DailyTradingActivitySecurity dailyTradingActivitySecurity = dailyTradingActivitySecurityService.findById(1L);
		Assert.assertNull(dailyTradingActivitySecurity);
	}

	/**
	 * 查找  证券日交易
	 */
	@Test
	@Ignore
	public void findContainer() {
		DailyTradingActivitySecurityQuery query = new DailyTradingActivitySecurityQuery();
		Container<DailyTradingActivitySecurity> container = dailyTradingActivitySecurityService.findContainer(query);
		Assert.assertNull(container);
	}

	/**
	 * 查找单个  证券日交易
	 */
	@Test
	@Ignore
	public void findOne() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", 1L);
		DailyTradingActivitySecurity dailyTradingActivitySecurity = dailyTradingActivitySecurityService.findOne(parameters);
		Assert.assertNull(dailyTradingActivitySecurity);
	}

	/**
	 * 保存  证券日交易
	 */
	@Test
	@Ignore
	public void save() {
		DailyTradingActivitySecurity dailyTradingActivitySecurity = new DailyTradingActivitySecurity();
		dailyTradingActivitySecurityService.save(dailyTradingActivitySecurity);
	}

	/**
	 * 保存或更新  证券日交易
	 */
	@Test
	@Ignore
	public void saveOrUpdate() {
		DailyTradingActivitySecurity dailyTradingActivitySecurity = new DailyTradingActivitySecurity();
		dailyTradingActivitySecurityService.saveOrUpdate(dailyTradingActivitySecurity);
	}

	/**
	 * 更新  证券日交易
	 */
	@Test
	@Ignore
	public void update() {
		DailyTradingActivitySecurity dailyTradingActivitySecurity = new DailyTradingActivitySecurity();
		dailyTradingActivitySecurity.setId(0L);
		dailyTradingActivitySecurityService.update(dailyTradingActivitySecurity);
	}

	@Test
	@Ignore
	public void otherMethod() {
		// 新的测试内容
	}

}
