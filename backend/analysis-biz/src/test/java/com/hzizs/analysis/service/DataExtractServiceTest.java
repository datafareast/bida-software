package com.hzizs.analysis.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.hzizs.test.spring.TestBase;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.annotations.Ignore;

import com.hzizs.vo.Container;
import com.hzizs.analysis.entity.DataExtract;
import com.hzizs.analysis.query.DataExtractQuery;

/**
 * 数据抽取 测试服务类
 * 
 * @author crazy_cabbage
 */
public class DataExtractServiceTest extends TestBase {
	private DataExtractService dataExtractService;

	public DataExtractServiceTest() {
		dataExtractService = (DataExtractService) ac.getBean("dataExtractService");
	}

	/**
	 * 统计 数据抽取
	 */
	@Test
	@Ignore
	public void count() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		long count = dataExtractService.count(parameters);
		Assert.assertNull(count);
	}

	/**
	 * 删除 数据抽取
	 */
	@Test
	@Ignore
	public void deleteEntity() {
		DataExtract dataExtract = new DataExtract();
		dataExtract.setId(0L);
		dataExtractService.delete(dataExtract);
	}

	/**
	 * 按条件删除 数据抽取
	 */
	@Test
	@Ignore
	public void deleteMap() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", 1L);
		dataExtractService.delete(parameters);

	}

	/**
	 * 按主键删除 数据抽取
	 */
	@Test
	@Ignore
	public void deleteById() {
		dataExtractService.deleteById(1L);
	}

	/**
	 * 根据条件查找  数据抽取
	 */
	@Test
	@Ignore
	public void find() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		List<DataExtract> dataExtracts = dataExtractService.find(parameters);
		Assert.assertNull(dataExtracts);
	}

	/**
	 * 根据主键查找 数据抽取
	 */
	@Test
	@Ignore
	public void findById() {
		DataExtract dataExtract = dataExtractService.findById(1L);
		Assert.assertNull(dataExtract);
	}

	/**
	 * 查找  数据抽取
	 */
	@Test
	@Ignore
	public void findContainer() {
		DataExtractQuery query = new DataExtractQuery();
		Container<DataExtract> container = dataExtractService.findContainer(query);
		Assert.assertNull(container);
	}

	/**
	 * 查找单个  数据抽取
	 */
	@Test
	@Ignore
	public void findOne() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", 1L);
		DataExtract dataExtract = dataExtractService.findOne(parameters);
		Assert.assertNull(dataExtract);
	}

	/**
	 * 保存  数据抽取
	 */
	@Test
	@Ignore
	public void save() {
		DataExtract dataExtract = new DataExtract();
		dataExtractService.save(dataExtract);
	}

	/**
	 * 保存或更新  数据抽取
	 */
	@Test
	@Ignore
	public void saveOrUpdate() {
		DataExtract dataExtract = new DataExtract();
		dataExtractService.saveOrUpdate(dataExtract);
	}

	/**
	 * 更新  数据抽取
	 */
	@Test
	@Ignore
	public void update() {
		DataExtract dataExtract = new DataExtract();
		dataExtract.setId(0L);
		dataExtractService.update(dataExtract);
	}

	@Test
	@Ignore
	public void otherMethod() {
		// 新的测试内容
	}

}
