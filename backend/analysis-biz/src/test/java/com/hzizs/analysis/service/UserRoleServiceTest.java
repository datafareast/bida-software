package com.hzizs.analysis.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.hzizs.test.spring.TestBase;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.annotations.Ignore;

import com.hzizs.vo.Container;
import com.hzizs.analysis.entity.UserRole;
import com.hzizs.analysis.query.UserRoleQuery;

/**
 * 用户角色 测试服务类
 * 
 * @author crazy_cabbage
 */
public class UserRoleServiceTest extends TestBase {
	private UserRoleService userRoleService;

	public UserRoleServiceTest() {
		userRoleService = (UserRoleService) ac.getBean("userRoleService");
	}

	/**
	 * 统计 用户角色
	 */
	@Test
	@Ignore
	public void count() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		long count = userRoleService.count(parameters);
		Assert.assertNull(count);
	}

	/**
	 * 删除 用户角色
	 */
	@Test
	@Ignore
	public void deleteEntity() {
		UserRole userRole = new UserRole();
		userRole.setId(0L);
		userRoleService.delete(userRole);
	}

	/**
	 * 按条件删除 用户角色
	 */
	@Test
	@Ignore
	public void deleteMap() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", 1L);
		userRoleService.delete(parameters);

	}

	/**
	 * 按主键删除 用户角色
	 */
	@Test
	@Ignore
	public void deleteById() {
		userRoleService.deleteById(1L);
	}

	/**
	 * 根据条件查找  用户角色
	 */
	@Test
	@Ignore
	public void find() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		List<UserRole> userRoles = userRoleService.find(parameters);
		Assert.assertNull(userRoles);
	}

	/**
	 * 根据主键查找 用户角色
	 */
	@Test
	@Ignore
	public void findById() {
		UserRole userRole = userRoleService.findById(1L);
		Assert.assertNull(userRole);
	}

	/**
	 * 查找  用户角色
	 */
	@Test
	@Ignore
	public void findContainer() {
		UserRoleQuery query = new UserRoleQuery();
		Container<UserRole> container = userRoleService.findContainer(query);
		Assert.assertNull(container);
	}

	/**
	 * 查找单个  用户角色
	 */
	@Test
	@Ignore
	public void findOne() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", 1L);
		UserRole userRole = userRoleService.findOne(parameters);
		Assert.assertNull(userRole);
	}

	/**
	 * 保存  用户角色
	 */
	@Test
	@Ignore
	public void save() {
		UserRole userRole = new UserRole();
		userRoleService.save(userRole);
	}

	/**
	 * 保存或更新  用户角色
	 */
	@Test
	@Ignore
	public void saveOrUpdate() {
		UserRole userRole = new UserRole();
		userRoleService.saveOrUpdate(userRole);
	}

	/**
	 * 更新  用户角色
	 */
	@Test
	@Ignore
	public void update() {
		UserRole userRole = new UserRole();
		userRole.setId(0L);
		userRoleService.update(userRole);
	}

	@Test
	@Ignore
	public void otherMethod() {
		// 新的测试内容
	}

}
