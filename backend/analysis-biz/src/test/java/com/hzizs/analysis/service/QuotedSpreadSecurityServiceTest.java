package com.hzizs.analysis.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.hzizs.test.spring.TestBase;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.annotations.Ignore;

import com.hzizs.vo.Container;
import com.hzizs.analysis.entity.QuotedSpreadSecurity;
import com.hzizs.analysis.query.QuotedSpreadSecurityQuery;

/**
 * 证券报价价差 测试服务类
 * 
 * @author crazy_cabbage
 */
public class QuotedSpreadSecurityServiceTest extends TestBase {
	private QuotedSpreadSecurityService quotedSpreadSecurityService;

	public QuotedSpreadSecurityServiceTest() {
		quotedSpreadSecurityService = (QuotedSpreadSecurityService) ac.getBean("quotedSpreadSecurityService");
	}

	/**
	 * 统计 证券报价价差
	 */
	@Test
	@Ignore
	public void count() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		long count = quotedSpreadSecurityService.count(parameters);
		Assert.assertNull(count);
	}

	/**
	 * 删除 证券报价价差
	 */
	@Test
	@Ignore
	public void deleteEntity() {
		QuotedSpreadSecurity quotedSpreadSecurity = new QuotedSpreadSecurity();
		quotedSpreadSecurity.setId(0L);
		quotedSpreadSecurityService.delete(quotedSpreadSecurity);
	}

	/**
	 * 按条件删除 证券报价价差
	 */
	@Test
	@Ignore
	public void deleteMap() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", 1L);
		quotedSpreadSecurityService.delete(parameters);

	}

	/**
	 * 按主键删除 证券报价价差
	 */
	@Test
	@Ignore
	public void deleteById() {
		quotedSpreadSecurityService.deleteById(1L);
	}

	/**
	 * 根据条件查找  证券报价价差
	 */
	@Test
	@Ignore
	public void find() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		List<QuotedSpreadSecurity> quotedSpreadSecuritys = quotedSpreadSecurityService.find(parameters);
		Assert.assertNull(quotedSpreadSecuritys);
	}

	/**
	 * 根据主键查找 证券报价价差
	 */
	@Test
	@Ignore
	public void findById() {
		QuotedSpreadSecurity quotedSpreadSecurity = quotedSpreadSecurityService.findById(1L);
		Assert.assertNull(quotedSpreadSecurity);
	}

	/**
	 * 查找  证券报价价差
	 */
	@Test
	@Ignore
	public void findContainer() {
		QuotedSpreadSecurityQuery query = new QuotedSpreadSecurityQuery();
		Container<QuotedSpreadSecurity> container = quotedSpreadSecurityService.findContainer(query);
		Assert.assertNull(container);
	}

	/**
	 * 查找单个  证券报价价差
	 */
	@Test
	@Ignore
	public void findOne() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", 1L);
		QuotedSpreadSecurity quotedSpreadSecurity = quotedSpreadSecurityService.findOne(parameters);
		Assert.assertNull(quotedSpreadSecurity);
	}

	/**
	 * 保存  证券报价价差
	 */
	@Test
	@Ignore
	public void save() {
		QuotedSpreadSecurity quotedSpreadSecurity = new QuotedSpreadSecurity();
		quotedSpreadSecurityService.save(quotedSpreadSecurity);
	}

	/**
	 * 保存或更新  证券报价价差
	 */
	@Test
	@Ignore
	public void saveOrUpdate() {
		QuotedSpreadSecurity quotedSpreadSecurity = new QuotedSpreadSecurity();
		quotedSpreadSecurityService.saveOrUpdate(quotedSpreadSecurity);
	}

	/**
	 * 更新  证券报价价差
	 */
	@Test
	@Ignore
	public void update() {
		QuotedSpreadSecurity quotedSpreadSecurity = new QuotedSpreadSecurity();
		quotedSpreadSecurity.setId(0L);
		quotedSpreadSecurityService.update(quotedSpreadSecurity);
	}

	@Test
	@Ignore
	public void otherMethod() {
		// 新的测试内容
	}

}
