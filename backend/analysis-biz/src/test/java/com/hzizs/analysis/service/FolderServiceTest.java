package com.hzizs.analysis.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.hzizs.test.spring.TestBase;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.annotations.Ignore;

import com.hzizs.vo.Container;
import com.hzizs.analysis.entity.Folder;
import com.hzizs.analysis.query.FolderQuery;

/**
 * 文件夹 测试服务类
 * 
 * @author crazy_cabbage
 */
public class FolderServiceTest extends TestBase {
	private FolderService folderService;
    private Long userId = 0L;
	public FolderServiceTest() {
		folderService = (FolderService) ac.getBean("folderService");
	}

	/**
	 * 统计 文件夹
	 */
	@Test
	@Ignore
	public void count() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		long count = folderService.count(parameters,userId);
		Assert.assertNull(count);
	}

	/**
	 * 删除 文件夹
	 */
	@Test
	@Ignore
	public void deleteEntity() {
		Folder folder = new Folder();
		folder.setId(0L);
		folderService.delete(folder,userId);
	}

	/**
	 * 按条件删除 文件夹
	 */
	@Test
	@Ignore
	public void deleteMap() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", 1L);
		folderService.delete(parameters,userId);

	}

	/**
	 * 按主键删除 文件夹
	 */
	@Test
	@Ignore
	public void deleteById() {
		folderService.deleteById(1L,userId);
	}

	/**
	 * 根据条件查找  文件夹
	 */
	@Test
	@Ignore
	public void find() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		List<Folder> folders = folderService.find(parameters,userId);
		Assert.assertNull(folders);
	}

	/**
	 * 根据主键查找 文件夹
	 */
	@Test
	@Ignore
	public void findById() {
		Folder folder = folderService.findById(1L,userId);
		Assert.assertNull(folder);
	}

	/**
	 * 查找  文件夹
	 */
	@Test
	@Ignore
	public void findContainer() {
		FolderQuery query = new FolderQuery();
		Container<Folder> container = folderService.findContainer(query,userId);
		Assert.assertNull(container);
	}

	/**
	 * 查找单个  文件夹
	 */
	@Test
	@Ignore
	public void findOne() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", 1L);
		Folder folder = folderService.findOne(parameters,userId);
		Assert.assertNull(folder);
	}

	/**
	 * 保存  文件夹
	 */
	@Test
	@Ignore
	public void save() {
		Folder folder = new Folder();
		folderService.save(folder,userId);
	}

	/**
	 * 保存或更新  文件夹
	 */
	@Test
	@Ignore
	public void saveOrUpdate() {
		Folder folder = new Folder();
		folderService.saveOrUpdate(folder,userId);
	}

	/**
	 * 更新  文件夹
	 */
	@Test
	@Ignore
	public void update() {
		Folder folder = new Folder();
		folder.setId(0L);
		folderService.update(folder,userId);
	}

	@Test
	@Ignore
	public void otherMethod() {
		// 新的测试内容
	}

}
