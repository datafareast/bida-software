package com.hzizs.analysis.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.hzizs.test.spring.TestBase;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.annotations.Ignore;

import com.hzizs.vo.Container;
import com.hzizs.analysis.entity.I18n;
import com.hzizs.analysis.query.I18nQuery;

/**
 * 国际化 测试服务类
 * 
 * @author crazy_cabbage
 */
public class I18nServiceTest extends TestBase {
	private I18nService i18nService;

	public I18nServiceTest() {
		i18nService = (I18nService) ac.getBean("i18nService");
	}

	/**
	 * 统计 国际化
	 */
	@Test
	@Ignore
	public void count() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		long count = i18nService.count(parameters);
		Assert.assertNull(count);
	}

	/**
	 * 删除 国际化
	 */
	@Test
	@Ignore
	public void deleteEntity() {
		I18n i18n = new I18n();
		i18n.setId(0L);
		i18nService.delete(i18n);
	}

	/**
	 * 按条件删除 国际化
	 */
	@Test
	@Ignore
	public void deleteMap() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", 1L);
		i18nService.delete(parameters);

	}

	/**
	 * 按主键删除 国际化
	 */
	@Test
	@Ignore
	public void deleteById() {
		i18nService.deleteById(1L);
	}

	/**
	 * 根据条件查找  国际化
	 */
	@Test
	@Ignore
	public void find() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		List<I18n> i18ns = i18nService.find(parameters);
		Assert.assertNull(i18ns);
	}

	/**
	 * 根据主键查找 国际化
	 */
	@Test
	@Ignore
	public void findById() {
		I18n i18n = i18nService.findById(1L);
		Assert.assertNull(i18n);
	}

	/**
	 * 查找  国际化
	 */
	@Test
	@Ignore
	public void findContainer() {
		I18nQuery query = new I18nQuery();
		Container<I18n> container = i18nService.findContainer(query);
		Assert.assertNull(container);
	}

	/**
	 * 查找单个  国际化
	 */
	@Test
	@Ignore
	public void findOne() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", 1L);
		I18n i18n = i18nService.findOne(parameters);
		Assert.assertNull(i18n);
	}

	/**
	 * 保存  国际化
	 */
	@Test
	@Ignore
	public void save() {
		I18n i18n = new I18n();
		i18nService.save(i18n);
	}

	/**
	 * 保存或更新  国际化
	 */
	@Test
	@Ignore
	public void saveOrUpdate() {
		I18n i18n = new I18n();
		i18nService.saveOrUpdate(i18n);
	}

	/**
	 * 更新  国际化
	 */
	@Test
	@Ignore
	public void update() {
		I18n i18n = new I18n();
		i18n.setId(0L);
		i18nService.update(i18n);
	}

	@Test
	@Ignore
	public void otherMethod() {
		// 新的测试内容
	}

}
