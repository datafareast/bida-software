package com.hzizs.analysis.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.hzizs.test.spring.TestBase;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.annotations.Ignore;

import com.hzizs.vo.Container;
import com.hzizs.analysis.entity.Area;
import com.hzizs.analysis.query.AreaQuery;

/**
 * 区域 测试服务类
 * 
 * @author crazy_cabbage
 */
public class AreaServiceTest extends TestBase {
	private AreaService areaService;

	public AreaServiceTest() {
		areaService = (AreaService) ac.getBean("areaService");
	}

	/**
	 * 统计 区域
	 */
	@Test
	@Ignore
	public void count() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		long count = areaService.count(parameters);
		Assert.assertNull(count);
	}

	/**
	 * 删除 区域
	 */
	@Test
	@Ignore
	public void deleteEntity() {
		Area area = new Area();
		area.setId(0L);
		areaService.delete(area);
	}

	/**
	 * 按条件删除 区域
	 */
	@Test
	@Ignore
	public void deleteMap() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", 1L);
		areaService.delete(parameters);

	}

	/**
	 * 按主键删除 区域
	 */
	@Test
	@Ignore
	public void deleteById() {
		areaService.deleteById(1L);
	}

	/**
	 * 根据条件查找  区域
	 */
	@Test
	@Ignore
	public void find() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		List<Area> areas = areaService.find(parameters);
		Assert.assertNull(areas);
	}

	/**
	 * 根据主键查找 区域
	 */
	@Test
	@Ignore
	public void findById() {
		Area area = areaService.findById(1L);
		Assert.assertNull(area);
	}

	/**
	 * 查找  区域
	 */
	@Test
	@Ignore
	public void findContainer() {
		AreaQuery query = new AreaQuery();
		Container<Area> container = areaService.findContainer(query);
		Assert.assertNull(container);
	}

	/**
	 * 查找单个  区域
	 */
	@Test
	@Ignore
	public void findOne() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", 1L);
		Area area = areaService.findOne(parameters);
		Assert.assertNull(area);
	}

	/**
	 * 保存  区域
	 */
	@Test
	@Ignore
	public void save() {
		Area area = new Area();
		areaService.save(area);
	}

	/**
	 * 保存或更新  区域
	 */
	@Test
	@Ignore
	public void saveOrUpdate() {
		Area area = new Area();
		areaService.saveOrUpdate(area);
	}

	/**
	 * 更新  区域
	 */
	@Test
	@Ignore
	public void update() {
		Area area = new Area();
		area.setId(0L);
		areaService.update(area);
	}

	@Test
	@Ignore
	public void otherMethod() {
		// 新的测试内容
	}

}
