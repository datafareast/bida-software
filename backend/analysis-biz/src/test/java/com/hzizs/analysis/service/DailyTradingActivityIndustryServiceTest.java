package com.hzizs.analysis.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.hzizs.test.spring.TestBase;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.annotations.Ignore;

import com.hzizs.vo.Container;
import com.hzizs.analysis.entity.DailyTradingActivityIndustry;
import com.hzizs.analysis.query.DailyTradingActivityIndustryQuery;

/**
 * 行业日交易 测试服务类
 * 
 * @author crazy_cabbage
 */
public class DailyTradingActivityIndustryServiceTest extends TestBase {
	private DailyTradingActivityIndustryService dailyTradingActivityIndustryService;

	public DailyTradingActivityIndustryServiceTest() {
		dailyTradingActivityIndustryService = (DailyTradingActivityIndustryService) ac.getBean("dailyTradingActivityIndustryService");
	}

	/**
	 * 统计 行业日交易
	 */
	@Test
	@Ignore
	public void count() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		long count = dailyTradingActivityIndustryService.count(parameters);
		Assert.assertNull(count);
	}

	/**
	 * 删除 行业日交易
	 */
	@Test
	@Ignore
	public void deleteEntity() {
		DailyTradingActivityIndustry dailyTradingActivityIndustry = new DailyTradingActivityIndustry();
		dailyTradingActivityIndustry.setId(0L);
		dailyTradingActivityIndustryService.delete(dailyTradingActivityIndustry);
	}

	/**
	 * 按条件删除 行业日交易
	 */
	@Test
	@Ignore
	public void deleteMap() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", 1L);
		dailyTradingActivityIndustryService.delete(parameters);

	}

	/**
	 * 按主键删除 行业日交易
	 */
	@Test
	@Ignore
	public void deleteById() {
		dailyTradingActivityIndustryService.deleteById(1L);
	}

	/**
	 * 根据条件查找  行业日交易
	 */
	@Test
	@Ignore
	public void find() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		List<DailyTradingActivityIndustry> dailyTradingActivityIndustrys = dailyTradingActivityIndustryService.find(parameters);
		Assert.assertNull(dailyTradingActivityIndustrys);
	}

	/**
	 * 根据主键查找 行业日交易
	 */
	@Test
	@Ignore
	public void findById() {
		DailyTradingActivityIndustry dailyTradingActivityIndustry = dailyTradingActivityIndustryService.findById(1L);
		Assert.assertNull(dailyTradingActivityIndustry);
	}

	/**
	 * 查找  行业日交易
	 */
	@Test
	@Ignore
	public void findContainer() {
		DailyTradingActivityIndustryQuery query = new DailyTradingActivityIndustryQuery();
		Container<DailyTradingActivityIndustry> container = dailyTradingActivityIndustryService.findContainer(query);
		Assert.assertNull(container);
	}

	/**
	 * 查找单个  行业日交易
	 */
	@Test
	@Ignore
	public void findOne() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", 1L);
		DailyTradingActivityIndustry dailyTradingActivityIndustry = dailyTradingActivityIndustryService.findOne(parameters);
		Assert.assertNull(dailyTradingActivityIndustry);
	}

	/**
	 * 保存  行业日交易
	 */
	@Test
	@Ignore
	public void save() {
		DailyTradingActivityIndustry dailyTradingActivityIndustry = new DailyTradingActivityIndustry();
		dailyTradingActivityIndustryService.save(dailyTradingActivityIndustry);
	}

	/**
	 * 保存或更新  行业日交易
	 */
	@Test
	@Ignore
	public void saveOrUpdate() {
		DailyTradingActivityIndustry dailyTradingActivityIndustry = new DailyTradingActivityIndustry();
		dailyTradingActivityIndustryService.saveOrUpdate(dailyTradingActivityIndustry);
	}

	/**
	 * 更新  行业日交易
	 */
	@Test
	@Ignore
	public void update() {
		DailyTradingActivityIndustry dailyTradingActivityIndustry = new DailyTradingActivityIndustry();
		dailyTradingActivityIndustry.setId(0L);
		dailyTradingActivityIndustryService.update(dailyTradingActivityIndustry);
	}

	@Test
	@Ignore
	public void otherMethod() {
		// 新的测试内容
	}

}
