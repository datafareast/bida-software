package com.hzizs.analysis.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.hzizs.test.spring.TestBase;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.annotations.Ignore;

import com.hzizs.vo.Container;
import com.hzizs.analysis.entity.SysUserLoginLog;
import com.hzizs.analysis.query.SysUserLoginLogQuery;

/**
 * 系统用户登录日志 测试服务类
 * 
 * @author crazy_cabbage
 */
public class SysUserLoginLogServiceTest extends TestBase {
	private SysUserLoginLogService sysUserLoginLogService;

	public SysUserLoginLogServiceTest() {
		sysUserLoginLogService = (SysUserLoginLogService) ac.getBean("sysUserLoginLogService");
	}

	/**
	 * 统计 系统用户登录日志
	 */
	@Test
	@Ignore
	public void count() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		long count = sysUserLoginLogService.count(parameters);
		Assert.assertNull(count);
	}

	/**
	 * 删除 系统用户登录日志
	 */
	@Test
	@Ignore
	public void deleteEntity() {
		SysUserLoginLog sysUserLoginLog = new SysUserLoginLog();
		sysUserLoginLog.setId(0L);
		sysUserLoginLogService.delete(sysUserLoginLog);
	}

	/**
	 * 按条件删除 系统用户登录日志
	 */
	@Test
	@Ignore
	public void deleteMap() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", 1L);
		sysUserLoginLogService.delete(parameters);

	}

	/**
	 * 按主键删除 系统用户登录日志
	 */
	@Test
	@Ignore
	public void deleteById() {
		sysUserLoginLogService.deleteById(1L);
	}

	/**
	 * 根据条件查找  系统用户登录日志
	 */
	@Test
	@Ignore
	public void find() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		List<SysUserLoginLog> sysUserLoginLogs = sysUserLoginLogService.find(parameters);
		Assert.assertNull(sysUserLoginLogs);
	}

	/**
	 * 根据主键查找 系统用户登录日志
	 */
	@Test
	@Ignore
	public void findById() {
		SysUserLoginLog sysUserLoginLog = sysUserLoginLogService.findById(1L);
		Assert.assertNull(sysUserLoginLog);
	}

	/**
	 * 查找  系统用户登录日志
	 */
	@Test
	@Ignore
	public void findContainer() {
		SysUserLoginLogQuery query = new SysUserLoginLogQuery();
		Container<SysUserLoginLog> container = sysUserLoginLogService.findContainer(query);
		Assert.assertNull(container);
	}

	/**
	 * 查找单个  系统用户登录日志
	 */
	@Test
	@Ignore
	public void findOne() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", 1L);
		SysUserLoginLog sysUserLoginLog = sysUserLoginLogService.findOne(parameters);
		Assert.assertNull(sysUserLoginLog);
	}

	/**
	 * 保存  系统用户登录日志
	 */
	@Test
	@Ignore
	public void save() {
		SysUserLoginLog sysUserLoginLog = new SysUserLoginLog();
		sysUserLoginLogService.save(sysUserLoginLog);
	}

	/**
	 * 保存或更新  系统用户登录日志
	 */
	@Test
	@Ignore
	public void saveOrUpdate() {
		SysUserLoginLog sysUserLoginLog = new SysUserLoginLog();
		sysUserLoginLogService.saveOrUpdate(sysUserLoginLog);
	}

	/**
	 * 更新  系统用户登录日志
	 */
	@Test
	@Ignore
	public void update() {
		SysUserLoginLog sysUserLoginLog = new SysUserLoginLog();
		sysUserLoginLog.setId(0L);
		sysUserLoginLogService.update(sysUserLoginLog);
	}

	@Test
	@Ignore
	public void otherMethod() {
		// 新的测试内容
	}

}
