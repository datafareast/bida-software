package com.hzizs.analysis.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.hzizs.test.spring.TestBase;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.annotations.Ignore;

import com.hzizs.vo.Container;
import com.hzizs.analysis.entity.SysUser;
import com.hzizs.analysis.query.SysUserQuery;

/**
 * 系统用户 测试服务类
 * 
 * @author crazy_cabbage
 */
public class SysUserServiceTest extends TestBase {
	private SysUserService sysUserService;

	public SysUserServiceTest() {
		sysUserService = (SysUserService) ac.getBean("sysUserService");
	}

	/**
	 * 统计 系统用户
	 */
	@Test
	@Ignore
	public void count() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		long count = sysUserService.count(parameters);
		Assert.assertNull(count);
	}

	/**
	 * 删除 系统用户
	 */
	@Test
	@Ignore
	public void deleteEntity() {
		SysUser sysUser = new SysUser();
		sysUser.setId(0L);
		sysUserService.delete(sysUser);
	}

	/**
	 * 按条件删除 系统用户
	 */
	@Test
	@Ignore
	public void deleteMap() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", 1L);
		sysUserService.delete(parameters);

	}

	/**
	 * 按主键删除 系统用户
	 */
	@Test
	@Ignore
	public void deleteById() {
		sysUserService.deleteById(1L);
	}

	/**
	 * 根据条件查找  系统用户
	 */
	@Test
	@Ignore
	public void find() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		List<SysUser> sysUsers = sysUserService.find(parameters);
		Assert.assertNull(sysUsers);
	}

	/**
	 * 根据主键查找 系统用户
	 */
	@Test
	@Ignore
	public void findById() {
		SysUser sysUser = sysUserService.findById(1L);
		Assert.assertNull(sysUser);
	}

	/**
	 * 查找  系统用户
	 */
	@Test
	@Ignore
	public void findContainer() {
		SysUserQuery query = new SysUserQuery();
		Container<SysUser> container = sysUserService.findContainer(query);
		Assert.assertNull(container);
	}

	/**
	 * 查找单个  系统用户
	 */
	@Test
	@Ignore
	public void findOne() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", 1L);
		SysUser sysUser = sysUserService.findOne(parameters);
		Assert.assertNull(sysUser);
	}

	/**
	 * 保存  系统用户
	 */
	@Test
	@Ignore
	public void save() {
		SysUser sysUser = new SysUser();
		sysUserService.save(sysUser);
	}

	/**
	 * 保存或更新  系统用户
	 */
	@Test
	@Ignore
	public void saveOrUpdate() {
		SysUser sysUser = new SysUser();
		sysUserService.saveOrUpdate(sysUser);
	}

	/**
	 * 更新  系统用户
	 */
	@Test
	@Ignore
	public void update() {
		SysUser sysUser = new SysUser();
		sysUser.setId(0L);
		sysUserService.update(sysUser);
	}

	@Test
	@Ignore
	public void otherMethod() {
		// 新的测试内容
	}

}
