package com.hzizs.analysis.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.hzizs.test.spring.TestBase;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.annotations.Ignore;

import com.hzizs.vo.Container;
import com.hzizs.analysis.entity.User;
import com.hzizs.analysis.query.UserQuery;

/**
 * 用户 测试服务类
 * 
 * @author crazy_cabbage
 */
public class UserServiceTest extends TestBase {
	private UserService userService;

	public UserServiceTest() {
		userService = (UserService) ac.getBean("userService");
	}

	/**
	 * 统计 用户
	 */
	@Test
	@Ignore
	public void count() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		long count = userService.count(parameters);
		Assert.assertNull(count);
	}

	/**
	 * 删除 用户
	 */
	@Test
	@Ignore
	public void deleteEntity() {
		User user = new User();
		user.setId(0L);
		userService.delete(user);
	}

	/**
	 * 按条件删除 用户
	 */
	@Test
	@Ignore
	public void deleteMap() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", 1L);
		userService.delete(parameters);

	}

	/**
	 * 按主键删除 用户
	 */
	@Test
	@Ignore
	public void deleteById() {
		userService.deleteById(1L);
	}

	/**
	 * 根据条件查找  用户
	 */
	@Test
	@Ignore
	public void find() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		List<User> users = userService.find(parameters);
		Assert.assertNull(users);
	}

	/**
	 * 根据主键查找 用户
	 */
	@Test
	@Ignore
	public void findById() {
		User user = userService.findById(1L);
		Assert.assertNull(user);
	}

	/**
	 * 查找  用户
	 */
	@Test
	@Ignore
	public void findContainer() {
		UserQuery query = new UserQuery();
		Container<User> container = userService.findContainer(query);
		Assert.assertNull(container);
	}

	/**
	 * 查找单个  用户
	 */
	@Test
	@Ignore
	public void findOne() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", 1L);
		User user = userService.findOne(parameters);
		Assert.assertNull(user);
	}

	/**
	 * 保存  用户
	 */
	@Test
	@Ignore
	public void save() {
		User user = new User();
		userService.save(user);
	}

	/**
	 * 保存或更新  用户
	 */
	@Test
	@Ignore
	public void saveOrUpdate() {
		User user = new User();
		userService.saveOrUpdate(user);
	}

	/**
	 * 更新  用户
	 */
	@Test
	@Ignore
	public void update() {
		User user = new User();
		user.setId(0L);
		userService.update(user);
	}

	@Test
	@Ignore
	public void otherMethod() {
		// 新的测试内容
	}

}
