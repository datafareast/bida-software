package com.hzizs.analysis.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.hzizs.test.spring.TestBase;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.annotations.Ignore;

import com.hzizs.vo.Container;
import com.hzizs.analysis.entity.DataTopo;
import com.hzizs.analysis.query.DataTopoQuery;

/**
 * 数据拓普图 测试服务类
 * 
 * @author crazy_cabbage
 */
public class DataTopServiceTest extends TestBase {
  private DataTopoService dataTopService;

  public DataTopServiceTest() {
    dataTopService = (DataTopoService) ac.getBean("dataTopService");
  }

  /**
   * 统计 数据拓普图
   */
  @Test
  @Ignore
  public void count() {
    Map<String, Object> parameters = new HashMap<String, Object>();
    long count = dataTopService.count(parameters);
    Assert.assertNull(count);
  }

  /**
   * 删除 数据拓普图
   */
  @Test
  @Ignore
  public void deleteEntity() {
    DataTopo dataTop = new DataTopo();
    dataTop.setId(0L);
    dataTopService.delete(dataTop);
  }

  /**
   * 按条件删除 数据拓普图
   */
  @Test
  @Ignore
  public void deleteMap() {
    Map<String, Object> parameters = new HashMap<String, Object>();
    parameters.put("id", 1L);
    dataTopService.delete(parameters);

  }

  /**
   * 按主键删除 数据拓普图
   */
  @Test
  @Ignore
  public void deleteById() {
    dataTopService.deleteById(1L);
  }

  /**
   * 根据条件查找 数据拓普图
   */
  @Test
  @Ignore
  public void find() {
    Map<String, Object> parameters = new HashMap<String, Object>();
    List<DataTopo> dataTops = dataTopService.find(parameters);
    Assert.assertNull(dataTops);
  }

  /**
   * 根据主键查找 数据拓普图
   */
  @Test
  @Ignore
  public void findById() {
    DataTopo dataTop = dataTopService.findById(1L);
    Assert.assertNull(dataTop);
  }

  /**
   * 查找 数据拓普图
   */
  @Test
  @Ignore
  public void findContainer() {
    DataTopoQuery query = new DataTopoQuery();
    Container<DataTopo> container = dataTopService.findContainer(query);
    Assert.assertNull(container);
  }

  /**
   * 查找单个 数据拓普图
   */
  @Test
  @Ignore
  public void findOne() {
    Map<String, Object> parameters = new HashMap<String, Object>();
    parameters.put("id", 1L);
    DataTopo dataTop = dataTopService.findOne(parameters);
    Assert.assertNull(dataTop);
  }

  /**
   * 保存 数据拓普图
   */
  @Test
  @Ignore
  public void save() {
    DataTopo dataTop = new DataTopo();
    dataTopService.save(dataTop);
  }

  /**
   * 保存或更新 数据拓普图
   */
  @Test
  @Ignore
  public void saveOrUpdate() {
    DataTopo dataTop = new DataTopo();
    dataTopService.saveOrUpdate(dataTop);
  }

  /**
   * 更新 数据拓普图
   */
  @Test
  @Ignore
  public void update() {
    DataTopo dataTop = new DataTopo();
    dataTop.setId(0L);
    dataTopService.update(dataTop);
  }

  @Test
  @Ignore
  public void otherMethod() {
    // 新的测试内容
  }

}
