package com.hzizs.analysis.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.hzizs.test.spring.TestBase;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.annotations.Ignore;

import com.hzizs.vo.Container;
import com.hzizs.analysis.entity.SysOption;
import com.hzizs.analysis.query.SysOptionQuery;

/**
 * 数据选项 测试服务类
 * 
 * @author crazy_cabbage
 */
public class SysOptionServiceTest extends TestBase {
	private SysOptionService sysOptionService;

	public SysOptionServiceTest() {
		sysOptionService = (SysOptionService) ac.getBean("sysOptionService");
	}

	/**
	 * 统计 数据选项
	 */
	@Test
	@Ignore
	public void count() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		long count = sysOptionService.count(parameters);
		Assert.assertNull(count);
	}

	/**
	 * 删除 数据选项
	 */
	@Test
	@Ignore
	public void deleteEntity() {
		SysOption sysOption = new SysOption();
		sysOption.setId(0L);
		sysOptionService.delete(sysOption);
	}

	/**
	 * 按条件删除 数据选项
	 */
	@Test
	@Ignore
	public void deleteMap() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", 1L);
		sysOptionService.delete(parameters);

	}

	/**
	 * 按主键删除 数据选项
	 */
	@Test
	@Ignore
	public void deleteById() {
		sysOptionService.deleteById(1L);
	}

	/**
	 * 根据条件查找  数据选项
	 */
	@Test
	@Ignore
	public void find() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		List<SysOption> sysOptions = sysOptionService.find(parameters);
		Assert.assertNull(sysOptions);
	}

	/**
	 * 根据主键查找 数据选项
	 */
	@Test
	@Ignore
	public void findById() {
		SysOption sysOption = sysOptionService.findById(1L);
		Assert.assertNull(sysOption);
	}

	/**
	 * 查找  数据选项
	 */
	@Test
	@Ignore
	public void findContainer() {
		SysOptionQuery query = new SysOptionQuery();
		Container<SysOption> container = sysOptionService.findContainer(query);
		Assert.assertNull(container);
	}

	/**
	 * 查找单个  数据选项
	 */
	@Test
	@Ignore
	public void findOne() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", 1L);
		SysOption sysOption = sysOptionService.findOne(parameters);
		Assert.assertNull(sysOption);
	}

	/**
	 * 保存  数据选项
	 */
	@Test
	@Ignore
	public void save() {
		SysOption sysOption = new SysOption();
		sysOptionService.save(sysOption);
	}

	/**
	 * 保存或更新  数据选项
	 */
	@Test
	@Ignore
	public void saveOrUpdate() {
		SysOption sysOption = new SysOption();
		sysOptionService.saveOrUpdate(sysOption);
	}

	/**
	 * 更新  数据选项
	 */
	@Test
	@Ignore
	public void update() {
		SysOption sysOption = new SysOption();
		sysOption.setId(0L);
		sysOptionService.update(sysOption);
	}

	@Test
	@Ignore
	public void otherMethod() {
		// 新的测试内容
	}

}
