package com.hzizs.analysis.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.hzizs.test.spring.TestBase;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.annotations.Ignore;

import com.hzizs.vo.Container;
import com.hzizs.analysis.entity.SysAuth;
import com.hzizs.analysis.query.SysAuthQuery;

/**
 * 系统权限 测试服务类
 * 
 * @author crazy_cabbage
 */
public class SysAuthServiceTest extends TestBase {
	private SysAuthService sysAuthService;

	public SysAuthServiceTest() {
		sysAuthService = (SysAuthService) ac.getBean("sysAuthService");
	}

	/**
	 * 统计 系统权限
	 */
	@Test
	@Ignore
	public void count() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		long count = sysAuthService.count(parameters);
		Assert.assertNull(count);
	}

	/**
	 * 删除 系统权限
	 */
	@Test
	@Ignore
	public void deleteEntity() {
		SysAuth sysAuth = new SysAuth();
		sysAuth.setId(0L);
		sysAuthService.delete(sysAuth);
	}

	/**
	 * 按条件删除 系统权限
	 */
	@Test
	@Ignore
	public void deleteMap() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", 1L);
		sysAuthService.delete(parameters);

	}

	/**
	 * 按主键删除 系统权限
	 */
	@Test
	@Ignore
	public void deleteById() {
		sysAuthService.deleteById(1L);
	}

	/**
	 * 根据条件查找  系统权限
	 */
	@Test
	@Ignore
	public void find() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		List<SysAuth> sysAuths = sysAuthService.find(parameters);
		Assert.assertNull(sysAuths);
	}

	/**
	 * 根据主键查找 系统权限
	 */
	@Test
	@Ignore
	public void findById() {
		SysAuth sysAuth = sysAuthService.findById(1L);
		Assert.assertNull(sysAuth);
	}

	/**
	 * 查找  系统权限
	 */
	@Test
	@Ignore
	public void findContainer() {
		SysAuthQuery query = new SysAuthQuery();
		Container<SysAuth> container = sysAuthService.findContainer(query);
		Assert.assertNull(container);
	}

	/**
	 * 查找单个  系统权限
	 */
	@Test
	@Ignore
	public void findOne() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", 1L);
		SysAuth sysAuth = sysAuthService.findOne(parameters);
		Assert.assertNull(sysAuth);
	}

	/**
	 * 保存  系统权限
	 */
	@Test
	@Ignore
	public void save() {
		SysAuth sysAuth = new SysAuth();
		sysAuthService.save(sysAuth);
	}

	/**
	 * 保存或更新  系统权限
	 */
	@Test
	@Ignore
	public void saveOrUpdate() {
		SysAuth sysAuth = new SysAuth();
		sysAuthService.saveOrUpdate(sysAuth);
	}

	/**
	 * 更新  系统权限
	 */
	@Test
	@Ignore
	public void update() {
		SysAuth sysAuth = new SysAuth();
		sysAuth.setId(0L);
		sysAuthService.update(sysAuth);
	}

	@Test
	@Ignore
	public void otherMethod() {
		// 新的测试内容
	}

}
