package com.hzizs.analysis.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.hzizs.test.spring.TestBase;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.annotations.Ignore;

import com.hzizs.vo.Container;
import com.hzizs.analysis.entity.ActivityIndex;
import com.hzizs.analysis.query.ActivityIndexQuery;

/**
 * 指数指标 测试服务类
 * 
 * @author crazy_cabbage
 */
public class ActivityIndexServiceTest extends TestBase {
	private ActivityIndexService activityIndexService;

	public ActivityIndexServiceTest() {
		activityIndexService = (ActivityIndexService) ac.getBean("activityIndexService");
	}

	/**
	 * 统计 指数指标
	 */
	@Test
	@Ignore
	public void count() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		long count = activityIndexService.count(parameters);
		Assert.assertNull(count);
	}

	/**
	 * 删除 指数指标
	 */
	@Test
	@Ignore
	public void deleteEntity() {
		ActivityIndex activityIndex = new ActivityIndex();
		activityIndex.setId(0L);
		activityIndexService.delete(activityIndex);
	}

	/**
	 * 按条件删除 指数指标
	 */
	@Test
	@Ignore
	public void deleteMap() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", 1L);
		activityIndexService.delete(parameters);

	}

	/**
	 * 按主键删除 指数指标
	 */
	@Test
	@Ignore
	public void deleteById() {
		activityIndexService.deleteById(1L);
	}

	/**
	 * 根据条件查找  指数指标
	 */
	@Test
	@Ignore
	public void find() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		List<ActivityIndex> activityIndexs = activityIndexService.find(parameters);
		Assert.assertNull(activityIndexs);
	}

	/**
	 * 根据主键查找 指数指标
	 */
	@Test
	@Ignore
	public void findById() {
		ActivityIndex activityIndex = activityIndexService.findById(1L);
		Assert.assertNull(activityIndex);
	}

	/**
	 * 查找  指数指标
	 */
	@Test
	@Ignore
	public void findContainer() {
		ActivityIndexQuery query = new ActivityIndexQuery();
		Container<ActivityIndex> container = activityIndexService.findContainer(query);
		Assert.assertNull(container);
	}

	/**
	 * 查找单个  指数指标
	 */
	@Test
	@Ignore
	public void findOne() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", 1L);
		ActivityIndex activityIndex = activityIndexService.findOne(parameters);
		Assert.assertNull(activityIndex);
	}

	/**
	 * 保存  指数指标
	 */
	@Test
	@Ignore
	public void save() {
		ActivityIndex activityIndex = new ActivityIndex();
		activityIndexService.save(activityIndex);
	}

	/**
	 * 保存或更新  指数指标
	 */
	@Test
	@Ignore
	public void saveOrUpdate() {
		ActivityIndex activityIndex = new ActivityIndex();
		activityIndexService.saveOrUpdate(activityIndex);
	}

	/**
	 * 更新  指数指标
	 */
	@Test
	@Ignore
	public void update() {
		ActivityIndex activityIndex = new ActivityIndex();
		activityIndex.setId(0L);
		activityIndexService.update(activityIndex);
	}

	@Test
	@Ignore
	public void otherMethod() {
		// 新的测试内容
	}

}
