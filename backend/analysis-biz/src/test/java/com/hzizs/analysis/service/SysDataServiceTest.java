package com.hzizs.analysis.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.hzizs.test.spring.TestBase;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.annotations.Ignore;

import com.hzizs.vo.Container;
import com.hzizs.analysis.entity.SysData;
import com.hzizs.analysis.query.SysDataQuery;

/**
 * 数据字典 测试服务类
 * 
 * @author crazy_cabbage
 */
public class SysDataServiceTest extends TestBase {
	private SysDataService sysDataService;

	public SysDataServiceTest() {
		sysDataService = (SysDataService) ac.getBean("sysDataService");
	}

	/**
	 * 统计 数据字典
	 */
	@Test
	@Ignore
	public void count() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		long count = sysDataService.count(parameters);
		Assert.assertNull(count);
	}

	/**
	 * 删除 数据字典
	 */
	@Test
	@Ignore
	public void deleteEntity() {
		SysData sysData = new SysData();
		sysData.setId(0L);
		sysDataService.delete(sysData);
	}

	/**
	 * 按条件删除 数据字典
	 */
	@Test
	@Ignore
	public void deleteMap() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", 1L);
		sysDataService.delete(parameters);

	}

	/**
	 * 按主键删除 数据字典
	 */
	@Test
	@Ignore
	public void deleteById() {
		sysDataService.deleteById(1L);
	}

	/**
	 * 根据条件查找  数据字典
	 */
	@Test
	@Ignore
	public void find() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		List<SysData> sysDatas = sysDataService.find(parameters);
		Assert.assertNull(sysDatas);
	}

	/**
	 * 根据主键查找 数据字典
	 */
	@Test
	@Ignore
	public void findById() {
		SysData sysData = sysDataService.findById(1L);
		Assert.assertNull(sysData);
	}

	/**
	 * 查找  数据字典
	 */
	@Test
	@Ignore
	public void findContainer() {
		SysDataQuery query = new SysDataQuery();
		Container<SysData> container = sysDataService.findContainer(query);
		Assert.assertNull(container);
	}

	/**
	 * 查找单个  数据字典
	 */
	@Test
	@Ignore
	public void findOne() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", 1L);
		SysData sysData = sysDataService.findOne(parameters);
		Assert.assertNull(sysData);
	}

	/**
	 * 保存  数据字典
	 */
	@Test
	@Ignore
	public void save() {
		SysData sysData = new SysData();
		sysDataService.save(sysData);
	}

	/**
	 * 保存或更新  数据字典
	 */
	@Test
	@Ignore
	public void saveOrUpdate() {
		SysData sysData = new SysData();
		sysDataService.saveOrUpdate(sysData);
	}

	/**
	 * 更新  数据字典
	 */
	@Test
	@Ignore
	public void update() {
		SysData sysData = new SysData();
		sysData.setId(0L);
		sysDataService.update(sysData);
	}

	@Test
	@Ignore
	public void otherMethod() {
		// 新的测试内容
	}

}
