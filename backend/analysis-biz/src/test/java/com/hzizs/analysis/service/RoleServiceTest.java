package com.hzizs.analysis.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.hzizs.test.spring.TestBase;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.annotations.Ignore;

import com.hzizs.vo.Container;
import com.hzizs.analysis.entity.Role;
import com.hzizs.analysis.query.RoleQuery;

/**
 * 角色 测试服务类
 * 
 * @author crazy_cabbage
 */
public class RoleServiceTest extends TestBase {
	private RoleService roleService;

	public RoleServiceTest() {
		roleService = (RoleService) ac.getBean("roleService");
	}

	/**
	 * 统计 角色
	 */
	@Test
	@Ignore
	public void count() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		long count = roleService.count(parameters);
		Assert.assertNull(count);
	}

	/**
	 * 删除 角色
	 */
	@Test
	@Ignore
	public void deleteEntity() {
		Role role = new Role();
		role.setId(0L);
		roleService.delete(role);
	}

	/**
	 * 按条件删除 角色
	 */
	@Test
	@Ignore
	public void deleteMap() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", 1L);
		roleService.delete(parameters);

	}

	/**
	 * 按主键删除 角色
	 */
	@Test
	@Ignore
	public void deleteById() {
		roleService.deleteById(1L);
	}

	/**
	 * 根据条件查找  角色
	 */
	@Test
	@Ignore
	public void find() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		List<Role> roles = roleService.find(parameters);
		Assert.assertNull(roles);
	}

	/**
	 * 根据主键查找 角色
	 */
	@Test
	@Ignore
	public void findById() {
		Role role = roleService.findById(1L);
		Assert.assertNull(role);
	}

	/**
	 * 查找  角色
	 */
	@Test
	@Ignore
	public void findContainer() {
		RoleQuery query = new RoleQuery();
		Container<Role> container = roleService.findContainer(query);
		Assert.assertNull(container);
	}

	/**
	 * 查找单个  角色
	 */
	@Test
	@Ignore
	public void findOne() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", 1L);
		Role role = roleService.findOne(parameters);
		Assert.assertNull(role);
	}

	/**
	 * 保存  角色
	 */
	@Test
	@Ignore
	public void save() {
		Role role = new Role();
		roleService.save(role);
	}

	/**
	 * 保存或更新  角色
	 */
	@Test
	@Ignore
	public void saveOrUpdate() {
		Role role = new Role();
		roleService.saveOrUpdate(role);
	}

	/**
	 * 更新  角色
	 */
	@Test
	@Ignore
	public void update() {
		Role role = new Role();
		role.setId(0L);
		roleService.update(role);
	}

	@Test
	@Ignore
	public void otherMethod() {
		// 新的测试内容
	}

}
