package com.hzizs.analysis.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.hzizs.test.spring.TestBase;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.annotations.Ignore;

import com.hzizs.vo.Container;
import com.hzizs.analysis.entity.SysRoleMenu;
import com.hzizs.analysis.query.SysRoleMenuQuery;

/**
 * 系统角色菜单 测试服务类
 * 
 * @author crazy_cabbage
 */
public class SysRoleMenuServiceTest extends TestBase {
	private SysRoleMenuService sysRoleMenuService;

	public SysRoleMenuServiceTest() {
		sysRoleMenuService = (SysRoleMenuService) ac.getBean("sysRoleMenuService");
	}

	/**
	 * 统计 系统角色菜单
	 */
	@Test
	@Ignore
	public void count() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		long count = sysRoleMenuService.count(parameters);
		Assert.assertNull(count);
	}

	/**
	 * 删除 系统角色菜单
	 */
	@Test
	@Ignore
	public void deleteEntity() {
		SysRoleMenu sysRoleMenu = new SysRoleMenu();
		sysRoleMenu.setId(0L);
		sysRoleMenuService.delete(sysRoleMenu);
	}

	/**
	 * 按条件删除 系统角色菜单
	 */
	@Test
	@Ignore
	public void deleteMap() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", 1L);
		sysRoleMenuService.delete(parameters);

	}

	/**
	 * 按主键删除 系统角色菜单
	 */
	@Test
	@Ignore
	public void deleteById() {
		sysRoleMenuService.deleteById(1L);
	}

	/**
	 * 根据条件查找  系统角色菜单
	 */
	@Test
	@Ignore
	public void find() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		List<SysRoleMenu> sysRoleMenus = sysRoleMenuService.find(parameters);
		Assert.assertNull(sysRoleMenus);
	}

	/**
	 * 根据主键查找 系统角色菜单
	 */
	@Test
	@Ignore
	public void findById() {
		SysRoleMenu sysRoleMenu = sysRoleMenuService.findById(1L);
		Assert.assertNull(sysRoleMenu);
	}

	/**
	 * 查找  系统角色菜单
	 */
	@Test
	@Ignore
	public void findContainer() {
		SysRoleMenuQuery query = new SysRoleMenuQuery();
		Container<SysRoleMenu> container = sysRoleMenuService.findContainer(query);
		Assert.assertNull(container);
	}

	/**
	 * 查找单个  系统角色菜单
	 */
	@Test
	@Ignore
	public void findOne() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", 1L);
		SysRoleMenu sysRoleMenu = sysRoleMenuService.findOne(parameters);
		Assert.assertNull(sysRoleMenu);
	}

	/**
	 * 保存  系统角色菜单
	 */
	@Test
	@Ignore
	public void save() {
		SysRoleMenu sysRoleMenu = new SysRoleMenu();
		sysRoleMenuService.save(sysRoleMenu);
	}

	/**
	 * 保存或更新  系统角色菜单
	 */
	@Test
	@Ignore
	public void saveOrUpdate() {
		SysRoleMenu sysRoleMenu = new SysRoleMenu();
		sysRoleMenuService.saveOrUpdate(sysRoleMenu);
	}

	/**
	 * 更新  系统角色菜单
	 */
	@Test
	@Ignore
	public void update() {
		SysRoleMenu sysRoleMenu = new SysRoleMenu();
		sysRoleMenu.setId(0L);
		sysRoleMenuService.update(sysRoleMenu);
	}

	@Test
	@Ignore
	public void otherMethod() {
		// 新的测试内容
	}

}
