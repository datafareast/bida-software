package com.hzizs.analysis.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.hzizs.test.spring.TestBase;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.annotations.Ignore;

import com.hzizs.vo.Container;
import com.hzizs.analysis.entity.SysMenu;
import com.hzizs.analysis.query.SysMenuQuery;

/**
 * 系统菜单 测试服务类
 * 
 * @author crazy_cabbage
 */
public class SysMenuServiceTest extends TestBase {
	private SysMenuService sysMenuService;

	public SysMenuServiceTest() {
		sysMenuService = (SysMenuService) ac.getBean("sysMenuService");
	}

	/**
	 * 统计 系统菜单
	 */
	@Test
	@Ignore
	public void count() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		long count = sysMenuService.count(parameters);
		Assert.assertNull(count);
	}

	/**
	 * 删除 系统菜单
	 */
	@Test
	@Ignore
	public void deleteEntity() {
		SysMenu sysMenu = new SysMenu();
		sysMenu.setId(0L);
		sysMenuService.delete(sysMenu);
	}

	/**
	 * 按条件删除 系统菜单
	 */
	@Test
	@Ignore
	public void deleteMap() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", 1L);
		sysMenuService.delete(parameters);

	}

	/**
	 * 按主键删除 系统菜单
	 */
	@Test
	@Ignore
	public void deleteById() {
		sysMenuService.deleteById(1L);
	}

	/**
	 * 根据条件查找  系统菜单
	 */
	@Test
	@Ignore
	public void find() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		List<SysMenu> sysMenus = sysMenuService.find(parameters);
		Assert.assertNull(sysMenus);
	}

	/**
	 * 根据主键查找 系统菜单
	 */
	@Test
	@Ignore
	public void findById() {
		SysMenu sysMenu = sysMenuService.findById(1L);
		Assert.assertNull(sysMenu);
	}

	/**
	 * 查找  系统菜单
	 */
	@Test
	@Ignore
	public void findContainer() {
		SysMenuQuery query = new SysMenuQuery();
		Container<SysMenu> container = sysMenuService.findContainer(query);
		Assert.assertNull(container);
	}

	/**
	 * 查找单个  系统菜单
	 */
	@Test
	@Ignore
	public void findOne() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", 1L);
		SysMenu sysMenu = sysMenuService.findOne(parameters);
		Assert.assertNull(sysMenu);
	}

	/**
	 * 保存  系统菜单
	 */
	@Test
	@Ignore
	public void save() {
		SysMenu sysMenu = new SysMenu();
		sysMenuService.save(sysMenu);
	}

	/**
	 * 保存或更新  系统菜单
	 */
	@Test
	@Ignore
	public void saveOrUpdate() {
		SysMenu sysMenu = new SysMenu();
		sysMenuService.saveOrUpdate(sysMenu);
	}

	/**
	 * 更新  系统菜单
	 */
	@Test
	@Ignore
	public void update() {
		SysMenu sysMenu = new SysMenu();
		sysMenu.setId(0L);
		sysMenuService.update(sysMenu);
	}

	@Test
	@Ignore
	public void otherMethod() {
		// 新的测试内容
	}

}
