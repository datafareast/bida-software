package com.hzizs.analysis.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.hzizs.test.spring.TestBase;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.annotations.Ignore;

import com.hzizs.vo.Container;
import com.hzizs.analysis.entity.LinkItem;
import com.hzizs.analysis.query.LinkItemQuery;

/**
 * 友情链接条目 测试服务类
 * 
 * @author crazy_cabbage
 */
public class LinkItemServiceTest extends TestBase {
	private LinkItemService linkItemService;

	public LinkItemServiceTest() {
		linkItemService = (LinkItemService) ac.getBean("linkItemService");
	}

	/**
	 * 统计 友情链接条目
	 */
	@Test
	@Ignore
	public void count() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		long count = linkItemService.count(parameters);
		Assert.assertNull(count);
	}

	/**
	 * 删除 友情链接条目
	 */
	@Test
	@Ignore
	public void deleteEntity() {
		LinkItem linkItem = new LinkItem();
		linkItem.setId(0L);
		linkItemService.delete(linkItem);
	}

	/**
	 * 按条件删除 友情链接条目
	 */
	@Test
	@Ignore
	public void deleteMap() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", 1L);
		linkItemService.delete(parameters);

	}

	/**
	 * 按主键删除 友情链接条目
	 */
	@Test
	@Ignore
	public void deleteById() {
		linkItemService.deleteById(1L);
	}

	/**
	 * 根据条件查找  友情链接条目
	 */
	@Test
	@Ignore
	public void find() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		List<LinkItem> linkItems = linkItemService.find(parameters);
		Assert.assertNull(linkItems);
	}

	/**
	 * 根据主键查找 友情链接条目
	 */
	@Test
	@Ignore
	public void findById() {
		LinkItem linkItem = linkItemService.findById(1L);
		Assert.assertNull(linkItem);
	}

	/**
	 * 查找  友情链接条目
	 */
	@Test
	@Ignore
	public void findContainer() {
		LinkItemQuery query = new LinkItemQuery();
		Container<LinkItem> container = linkItemService.findContainer(query);
		Assert.assertNull(container);
	}

	/**
	 * 查找单个  友情链接条目
	 */
	@Test
	@Ignore
	public void findOne() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", 1L);
		LinkItem linkItem = linkItemService.findOne(parameters);
		Assert.assertNull(linkItem);
	}

	/**
	 * 保存  友情链接条目
	 */
	@Test
	@Ignore
	public void save() {
		LinkItem linkItem = new LinkItem();
		linkItemService.save(linkItem);
	}

	/**
	 * 保存或更新  友情链接条目
	 */
	@Test
	@Ignore
	public void saveOrUpdate() {
		LinkItem linkItem = new LinkItem();
		linkItemService.saveOrUpdate(linkItem);
	}

	/**
	 * 更新  友情链接条目
	 */
	@Test
	@Ignore
	public void update() {
		LinkItem linkItem = new LinkItem();
		linkItem.setId(0L);
		linkItemService.update(linkItem);
	}

	@Test
	@Ignore
	public void otherMethod() {
		// 新的测试内容
	}

}
