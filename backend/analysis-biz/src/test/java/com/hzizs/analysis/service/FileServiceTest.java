package com.hzizs.analysis.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.hzizs.test.spring.TestBase;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.annotations.Ignore;

import com.hzizs.vo.Container;
import com.hzizs.analysis.entity.File;
import com.hzizs.analysis.query.FileQuery;

/**
 * 文件 测试服务类
 * 
 * @author crazy_cabbage
 */
public class FileServiceTest extends TestBase {
	private FileService fileService;
	private Long userId = 0L;

	public FileServiceTest() {
		fileService = (FileService) ac.getBean("fileService");
	}

	/**
	 * 统计 文件
	 */
	@Test
	@Ignore
	public void count() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		long count = fileService.count(parameters,userId);
		Assert.assertNull(count);
	}

	/**
	 * 删除 文件
	 */
	@Test
	@Ignore
	public void deleteEntity() {
		File file = new File();
		file.setId(0L);
		fileService.delete(file,userId);
	}

	/**
	 * 按条件删除 文件
	 */
	@Test
	@Ignore
	public void deleteMap() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", 1L);
		fileService.delete(parameters,userId);

	}

	/**
	 * 按主键删除 文件
	 */
	@Test
	@Ignore
	public void deleteById() {
		fileService.deleteById(1L,userId);
	}

	/**
	 * 根据条件查找  文件
	 */
	@Test
	@Ignore
	public void find() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		List<File> files = fileService.find(parameters,userId);
		Assert.assertNull(files);
	}

	/**
	 * 根据主键查找 文件
	 */
	@Test
	@Ignore
	public void findById() {
		File file = fileService.findById(1L,userId);
		Assert.assertNull(file);
	}

	/**
	 * 查找  文件
	 */
	@Test
	@Ignore
	public void findContainer() {
		FileQuery query = new FileQuery();
		Container<File> container = fileService.findContainer(query,userId);
		Assert.assertNull(container);
	}

	/**
	 * 查找单个  文件
	 */
	@Test
	@Ignore
	public void findOne() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", 1L);
		File file = fileService.findOne(parameters,userId);
		Assert.assertNull(file);
	}

	/**
	 * 保存  文件
	 */
	@Test
	@Ignore
	public void save() {
		File file = new File();
		fileService.save(file,userId);
	}

	/**
	 * 保存或更新  文件
	 */
	@Test
	@Ignore
	public void saveOrUpdate() {
		File file = new File();
		fileService.saveOrUpdate(file,userId);
	}

	/**
	 * 更新  文件
	 */
	@Test
	@Ignore
	public void update() {
		File file = new File();
		file.setId(0L);
		fileService.update(file,userId);
	}

	@Test
	@Ignore
	public void otherMethod() {
		// 新的测试内容
	}

}
