package com.hzizs.analysis.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.hzizs.test.spring.TestBase;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.annotations.Ignore;

import com.hzizs.vo.Container;
import com.hzizs.analysis.entity.SecCat;
import com.hzizs.analysis.query.SecCatQuery;

/**
 * 类别 测试服务类
 * 
 * @author crazy_cabbage
 */
public class SecCatServiceTest extends TestBase {
	private SecCatService secCatService;

	public SecCatServiceTest() {
		secCatService = (SecCatService) ac.getBean("secCatService");
	}

	/**
	 * 统计 类别
	 */
	@Test
	@Ignore
	public void count() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		long count = secCatService.count(parameters);
		Assert.assertNull(count);
	}

	/**
	 * 删除 类别
	 */
	@Test
	@Ignore
	public void deleteEntity() {
		SecCat secCat = new SecCat();
		secCat.setId(0L);
		secCatService.delete(secCat);
	}

	/**
	 * 按条件删除 类别
	 */
	@Test
	@Ignore
	public void deleteMap() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", 1L);
		secCatService.delete(parameters);

	}

	/**
	 * 按主键删除 类别
	 */
	@Test
	@Ignore
	public void deleteById() {
		secCatService.deleteById(1L);
	}

	/**
	 * 根据条件查找  类别
	 */
	@Test
	@Ignore
	public void find() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		List<SecCat> secCats = secCatService.find(parameters);
		Assert.assertNull(secCats);
	}

	/**
	 * 根据主键查找 类别
	 */
	@Test
	@Ignore
	public void findById() {
		SecCat secCat = secCatService.findById(1L);
		Assert.assertNull(secCat);
	}

	/**
	 * 查找  类别
	 */
	@Test
	@Ignore
	public void findContainer() {
		SecCatQuery query = new SecCatQuery();
		Container<SecCat> container = secCatService.findContainer(query);
		Assert.assertNull(container);
	}

	/**
	 * 查找单个  类别
	 */
	@Test
	@Ignore
	public void findOne() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", 1L);
		SecCat secCat = secCatService.findOne(parameters);
		Assert.assertNull(secCat);
	}

	/**
	 * 保存  类别
	 */
	@Test
	@Ignore
	public void save() {
		SecCat secCat = new SecCat();
		secCatService.save(secCat);
	}

	/**
	 * 保存或更新  类别
	 */
	@Test
	@Ignore
	public void saveOrUpdate() {
		SecCat secCat = new SecCat();
		secCatService.saveOrUpdate(secCat);
	}

	/**
	 * 更新  类别
	 */
	@Test
	@Ignore
	public void update() {
		SecCat secCat = new SecCat();
		secCat.setId(0L);
		secCatService.update(secCat);
	}

	@Test
	@Ignore
	public void otherMethod() {
		// 新的测试内容
	}

}
