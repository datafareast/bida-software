package com.hzizs.analysis.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.hzizs.test.spring.TestBase;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.annotations.Ignore;

import com.hzizs.vo.Container;
import com.hzizs.analysis.entity.Link;
import com.hzizs.analysis.query.LinkQuery;

/**
 * 友情链接 测试服务类
 * 
 * @author crazy_cabbage
 */
public class LinkCatServiceTest extends TestBase {
	private LinkService linkCatService;

	public LinkCatServiceTest() {
		linkCatService = (LinkService) ac.getBean("linkCatService");
	}

	/**
	 * 统计 友情链接
	 */
	@Test
	@Ignore
	public void count() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		long count = linkCatService.count(parameters);
		Assert.assertNull(count);
	}

	/**
	 * 删除 友情链接
	 */
	@Test
	@Ignore
	public void deleteEntity() {
		Link linkCat = new Link();
		linkCat.setId(0L);
		linkCatService.delete(linkCat);
	}

	/**
	 * 按条件删除 友情链接
	 */
	@Test
	@Ignore
	public void deleteMap() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", 1L);
		linkCatService.delete(parameters);

	}

	/**
	 * 按主键删除 友情链接
	 */
	@Test
	@Ignore
	public void deleteById() {
		linkCatService.deleteById(1L);
	}

	/**
	 * 根据条件查找  友情链接
	 */
	@Test
	@Ignore
	public void find() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		List<Link> linkCats = linkCatService.find(parameters);
		Assert.assertNull(linkCats);
	}

	/**
	 * 根据主键查找 友情链接
	 */
	@Test
	@Ignore
	public void findById() {
		Link linkCat = linkCatService.findById(1L);
		Assert.assertNull(linkCat);
	}

	/**
	 * 查找  友情链接
	 */
	@Test
	@Ignore
	public void findContainer() {
		LinkQuery query = new LinkQuery();
		Container<Link> container = linkCatService.findContainer(query);
		Assert.assertNull(container);
	}

	/**
	 * 查找单个  友情链接
	 */
	@Test
	@Ignore
	public void findOne() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", 1L);
		Link linkCat = linkCatService.findOne(parameters);
		Assert.assertNull(linkCat);
	}

	/**
	 * 保存  友情链接
	 */
	@Test
	@Ignore
	public void save() {
		Link linkCat = new Link();
		linkCatService.save(linkCat);
	}

	/**
	 * 保存或更新  友情链接
	 */
	@Test
	@Ignore
	public void saveOrUpdate() {
		Link linkCat = new Link();
		linkCatService.saveOrUpdate(linkCat);
	}

	/**
	 * 更新  友情链接
	 */
	@Test
	@Ignore
	public void update() {
		Link linkCat = new Link();
		linkCat.setId(0L);
		linkCatService.update(linkCat);
	}

	@Test
	@Ignore
	public void otherMethod() {
		// 新的测试内容
	}

}
