package com.hzizs.analysis.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.hzizs.test.spring.TestBase;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.annotations.Ignore;

import com.hzizs.vo.Container;
import com.hzizs.analysis.entity.SysUserLog;
import com.hzizs.analysis.query.SysUserLogQuery;

/**
 * 系统用户日志 测试服务类
 * 
 * @author crazy_cabbage
 */
public class SysUserLogServiceTest extends TestBase {
	private SysUserLogService sysUserLogService;

	public SysUserLogServiceTest() {
		sysUserLogService = (SysUserLogService) ac.getBean("sysUserLogService");
	}

	/**
	 * 统计 系统用户日志
	 */
	@Test
	@Ignore
	public void count() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		long count = sysUserLogService.count(parameters);
		Assert.assertNull(count);
	}

	/**
	 * 删除 系统用户日志
	 */
	@Test
	@Ignore
	public void deleteEntity() {
		SysUserLog sysUserLog = new SysUserLog();
		sysUserLog.setId(0L);
		sysUserLogService.delete(sysUserLog);
	}

	/**
	 * 按条件删除 系统用户日志
	 */
	@Test
	@Ignore
	public void deleteMap() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", 1L);
		sysUserLogService.delete(parameters);

	}

	/**
	 * 按主键删除 系统用户日志
	 */
	@Test
	@Ignore
	public void deleteById() {
		sysUserLogService.deleteById(1L);
	}

	/**
	 * 根据条件查找  系统用户日志
	 */
	@Test
	@Ignore
	public void find() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		List<SysUserLog> sysUserLogs = sysUserLogService.find(parameters);
		Assert.assertNull(sysUserLogs);
	}

	/**
	 * 根据主键查找 系统用户日志
	 */
	@Test
	@Ignore
	public void findById() {
		SysUserLog sysUserLog = sysUserLogService.findById(1L);
		Assert.assertNull(sysUserLog);
	}

	/**
	 * 查找  系统用户日志
	 */
	@Test
	@Ignore
	public void findContainer() {
		SysUserLogQuery query = new SysUserLogQuery();
		Container<SysUserLog> container = sysUserLogService.findContainer(query);
		Assert.assertNull(container);
	}

	/**
	 * 查找单个  系统用户日志
	 */
	@Test
	@Ignore
	public void findOne() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", 1L);
		SysUserLog sysUserLog = sysUserLogService.findOne(parameters);
		Assert.assertNull(sysUserLog);
	}

	/**
	 * 保存  系统用户日志
	 */
	@Test
	@Ignore
	public void save() {
		SysUserLog sysUserLog = new SysUserLog();
		sysUserLogService.save(sysUserLog);
	}

	/**
	 * 保存或更新  系统用户日志
	 */
	@Test
	@Ignore
	public void saveOrUpdate() {
		SysUserLog sysUserLog = new SysUserLog();
		sysUserLogService.saveOrUpdate(sysUserLog);
	}

	/**
	 * 更新  系统用户日志
	 */
	@Test
	@Ignore
	public void update() {
		SysUserLog sysUserLog = new SysUserLog();
		sysUserLog.setId(0L);
		sysUserLogService.update(sysUserLog);
	}

	@Test
	@Ignore
	public void otherMethod() {
		// 新的测试内容
	}

}
