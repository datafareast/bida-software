package com.hzizs.analysis.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.hzizs.test.spring.TestBase;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.annotations.Ignore;

import com.hzizs.vo.Container;
import com.hzizs.analysis.entity.Cms;
import com.hzizs.analysis.query.CmsQuery;

/**
 * 内容管理系统 测试服务类
 * 
 * @author crazy_cabbage
 */
public class CmsServiceTest extends TestBase {
	private CmsService cmsService;

	public CmsServiceTest() {
		cmsService = (CmsService) ac.getBean("cmsService");
	}

	/**
	 * 统计 内容管理系统
	 */
	@Test
	@Ignore
	public void count() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		long count = cmsService.count(parameters);
		Assert.assertNull(count);
	}

	/**
	 * 删除 内容管理系统
	 */
	@Test
	@Ignore
	public void deleteEntity() {
		Cms cms = new Cms();
		cms.setId(0L);
		cmsService.delete(cms);
	}

	/**
	 * 按条件删除 内容管理系统
	 */
	@Test
	@Ignore
	public void deleteMap() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", 1L);
		cmsService.delete(parameters);

	}

	/**
	 * 按主键删除 内容管理系统
	 */
	@Test
	@Ignore
	public void deleteById() {
		cmsService.deleteById(1L);
	}

	/**
	 * 根据条件查找  内容管理系统
	 */
	@Test
	@Ignore
	public void find() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		List<Cms> cmss = cmsService.find(parameters);
		Assert.assertNull(cmss);
	}

	/**
	 * 根据主键查找 内容管理系统
	 */
	@Test
	@Ignore
	public void findById() {
		Cms cms = cmsService.findById(1L);
		Assert.assertNull(cms);
	}

	/**
	 * 查找  内容管理系统
	 */
	@Test
	@Ignore
	public void findContainer() {
		CmsQuery query = new CmsQuery();
		Container<Cms> container = cmsService.findContainer(query);
		Assert.assertNull(container);
	}

	/**
	 * 查找单个  内容管理系统
	 */
	@Test
	@Ignore
	public void findOne() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", 1L);
		Cms cms = cmsService.findOne(parameters);
		Assert.assertNull(cms);
	}

	/**
	 * 保存  内容管理系统
	 */
	@Test
	@Ignore
	public void save() {
		Cms cms = new Cms();
		cmsService.save(cms);
	}

	/**
	 * 保存或更新  内容管理系统
	 */
	@Test
	@Ignore
	public void saveOrUpdate() {
		Cms cms = new Cms();
		cmsService.saveOrUpdate(cms);
	}

	/**
	 * 更新  内容管理系统
	 */
	@Test
	@Ignore
	public void update() {
		Cms cms = new Cms();
		cms.setId(0L);
		cmsService.update(cms);
	}

	@Test
	@Ignore
	public void otherMethod() {
		// 新的测试内容
	}

}
