package com.hzizs.analysis.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.hzizs.test.spring.TestBase;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.annotations.Ignore;

import com.hzizs.vo.Container;
import com.hzizs.analysis.entity.SysFile;
import com.hzizs.analysis.query.SysFileQuery;

/**
 * 系统文件 测试服务类
 * 
 * @author crazy_cabbage
 */
public class SysFileServiceTest extends TestBase {
	private SysFileService sysFileService;

	public SysFileServiceTest() {
		sysFileService = (SysFileService) ac.getBean("sysFileService");
	}

	/**
	 * 统计 系统文件
	 */
	@Test
	@Ignore
	public void count() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		long count = sysFileService.count(parameters);
		Assert.assertNull(count);
	}

	/**
	 * 删除 系统文件
	 */
	@Test
	@Ignore
	public void deleteEntity() {
		SysFile sysFile = new SysFile();
		sysFile.setId(0L);
		sysFileService.delete(sysFile);
	}

	/**
	 * 按条件删除 系统文件
	 */
	@Test
	@Ignore
	public void deleteMap() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", 1L);
		sysFileService.delete(parameters);

	}

	/**
	 * 按主键删除 系统文件
	 */
	@Test
	@Ignore
	public void deleteById() {
		sysFileService.deleteById(1L);
	}

	/**
	 * 根据条件查找  系统文件
	 */
	@Test
	@Ignore
	public void find() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		List<SysFile> sysFiles = sysFileService.find(parameters);
		Assert.assertNull(sysFiles);
	}

	/**
	 * 根据主键查找 系统文件
	 */
	@Test
	@Ignore
	public void findById() {
		SysFile sysFile = sysFileService.findById(1L);
		Assert.assertNull(sysFile);
	}

	/**
	 * 查找  系统文件
	 */
	@Test
	@Ignore
	public void findContainer() {
		SysFileQuery query = new SysFileQuery();
		Container<SysFile> container = sysFileService.findContainer(query);
		Assert.assertNull(container);
	}

	/**
	 * 查找单个  系统文件
	 */
	@Test
	@Ignore
	public void findOne() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", 1L);
		SysFile sysFile = sysFileService.findOne(parameters);
		Assert.assertNull(sysFile);
	}

	/**
	 * 保存  系统文件
	 */
	@Test
	@Ignore
	public void save() {
		SysFile sysFile = new SysFile();
		sysFileService.save(sysFile);
	}

	/**
	 * 保存或更新  系统文件
	 */
	@Test
	@Ignore
	public void saveOrUpdate() {
		SysFile sysFile = new SysFile();
		sysFileService.saveOrUpdate(sysFile);
	}

	/**
	 * 更新  系统文件
	 */
	@Test
	@Ignore
	public void update() {
		SysFile sysFile = new SysFile();
		sysFile.setId(0L);
		sysFileService.update(sysFile);
	}

	@Test
	@Ignore
	public void otherMethod() {
		// 新的测试内容
	}

}
