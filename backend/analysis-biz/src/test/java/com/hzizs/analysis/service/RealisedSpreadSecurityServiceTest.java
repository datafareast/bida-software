package com.hzizs.analysis.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.hzizs.test.spring.TestBase;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.annotations.Ignore;

import com.hzizs.vo.Container;
import com.hzizs.analysis.entity.RealisedSpreadSecurity;
import com.hzizs.analysis.query.RealisedSpreadSecurityQuery;

/**
 * 证券实现价差 测试服务类
 * 
 * @author crazy_cabbage
 */
public class RealisedSpreadSecurityServiceTest extends TestBase {
	private RealisedSpreadSecurityService realisedSpreadSecurityService;

	public RealisedSpreadSecurityServiceTest() {
		realisedSpreadSecurityService = (RealisedSpreadSecurityService) ac.getBean("realisedSpreadSecurityService");
	}

	/**
	 * 统计 证券实现价差
	 */
	@Test
	@Ignore
	public void count() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		long count = realisedSpreadSecurityService.count(parameters);
		Assert.assertNull(count);
	}

	/**
	 * 删除 证券实现价差
	 */
	@Test
	@Ignore
	public void deleteEntity() {
		RealisedSpreadSecurity realisedSpreadSecurity = new RealisedSpreadSecurity();
		realisedSpreadSecurity.setId(0L);
		realisedSpreadSecurityService.delete(realisedSpreadSecurity);
	}

	/**
	 * 按条件删除 证券实现价差
	 */
	@Test
	@Ignore
	public void deleteMap() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", 1L);
		realisedSpreadSecurityService.delete(parameters);

	}

	/**
	 * 按主键删除 证券实现价差
	 */
	@Test
	@Ignore
	public void deleteById() {
		realisedSpreadSecurityService.deleteById(1L);
	}

	/**
	 * 根据条件查找  证券实现价差
	 */
	@Test
	@Ignore
	public void find() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		List<RealisedSpreadSecurity> realisedSpreadSecuritys = realisedSpreadSecurityService.find(parameters);
		Assert.assertNull(realisedSpreadSecuritys);
	}

	/**
	 * 根据主键查找 证券实现价差
	 */
	@Test
	@Ignore
	public void findById() {
		RealisedSpreadSecurity realisedSpreadSecurity = realisedSpreadSecurityService.findById(1L);
		Assert.assertNull(realisedSpreadSecurity);
	}

	/**
	 * 查找  证券实现价差
	 */
	@Test
	@Ignore
	public void findContainer() {
		RealisedSpreadSecurityQuery query = new RealisedSpreadSecurityQuery();
		Container<RealisedSpreadSecurity> container = realisedSpreadSecurityService.findContainer(query);
		Assert.assertNull(container);
	}

	/**
	 * 查找单个  证券实现价差
	 */
	@Test
	@Ignore
	public void findOne() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", 1L);
		RealisedSpreadSecurity realisedSpreadSecurity = realisedSpreadSecurityService.findOne(parameters);
		Assert.assertNull(realisedSpreadSecurity);
	}

	/**
	 * 保存  证券实现价差
	 */
	@Test
	@Ignore
	public void save() {
		RealisedSpreadSecurity realisedSpreadSecurity = new RealisedSpreadSecurity();
		realisedSpreadSecurityService.save(realisedSpreadSecurity);
	}

	/**
	 * 保存或更新  证券实现价差
	 */
	@Test
	@Ignore
	public void saveOrUpdate() {
		RealisedSpreadSecurity realisedSpreadSecurity = new RealisedSpreadSecurity();
		realisedSpreadSecurityService.saveOrUpdate(realisedSpreadSecurity);
	}

	/**
	 * 更新  证券实现价差
	 */
	@Test
	@Ignore
	public void update() {
		RealisedSpreadSecurity realisedSpreadSecurity = new RealisedSpreadSecurity();
		realisedSpreadSecurity.setId(0L);
		realisedSpreadSecurityService.update(realisedSpreadSecurity);
	}

	@Test
	@Ignore
	public void otherMethod() {
		// 新的测试内容
	}

}
