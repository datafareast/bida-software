package com.hzizs.analysis.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.hzizs.test.spring.TestBase;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.annotations.Ignore;

import com.hzizs.vo.Container;
import com.hzizs.analysis.entity.SlideItem;
import com.hzizs.analysis.query.SlideItemQuery;

/**
 * 轮播图条目 测试服务类
 * 
 * @author crazy_cabbage
 */
public class SlideItemServiceTest extends TestBase {
	private SlideItemService slideItemService;

	public SlideItemServiceTest() {
		slideItemService = (SlideItemService) ac.getBean("slideItemService");
	}

	/**
	 * 统计 轮播图条目
	 */
	@Test
	@Ignore
	public void count() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		long count = slideItemService.count(parameters);
		Assert.assertNull(count);
	}

	/**
	 * 删除 轮播图条目
	 */
	@Test
	@Ignore
	public void deleteEntity() {
		SlideItem slideItem = new SlideItem();
		slideItem.setId(0L);
		slideItemService.delete(slideItem);
	}

	/**
	 * 按条件删除 轮播图条目
	 */
	@Test
	@Ignore
	public void deleteMap() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", 1L);
		slideItemService.delete(parameters);

	}

	/**
	 * 按主键删除 轮播图条目
	 */
	@Test
	@Ignore
	public void deleteById() {
		slideItemService.deleteById(1L);
	}

	/**
	 * 根据条件查找  轮播图条目
	 */
	@Test
	@Ignore
	public void find() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		List<SlideItem> slideItems = slideItemService.find(parameters);
		Assert.assertNull(slideItems);
	}

	/**
	 * 根据主键查找 轮播图条目
	 */
	@Test
	@Ignore
	public void findById() {
		SlideItem slideItem = slideItemService.findById(1L);
		Assert.assertNull(slideItem);
	}

	/**
	 * 查找  轮播图条目
	 */
	@Test
	@Ignore
	public void findContainer() {
		SlideItemQuery query = new SlideItemQuery();
		Container<SlideItem> container = slideItemService.findContainer(query);
		Assert.assertNull(container);
	}

	/**
	 * 查找单个  轮播图条目
	 */
	@Test
	@Ignore
	public void findOne() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", 1L);
		SlideItem slideItem = slideItemService.findOne(parameters);
		Assert.assertNull(slideItem);
	}

	/**
	 * 保存  轮播图条目
	 */
	@Test
	@Ignore
	public void save() {
		SlideItem slideItem = new SlideItem();
		slideItemService.save(slideItem);
	}

	/**
	 * 保存或更新  轮播图条目
	 */
	@Test
	@Ignore
	public void saveOrUpdate() {
		SlideItem slideItem = new SlideItem();
		slideItemService.saveOrUpdate(slideItem);
	}

	/**
	 * 更新  轮播图条目
	 */
	@Test
	@Ignore
	public void update() {
		SlideItem slideItem = new SlideItem();
		slideItem.setId(0L);
		slideItemService.update(slideItem);
	}

	@Test
	@Ignore
	public void otherMethod() {
		// 新的测试内容
	}

}
