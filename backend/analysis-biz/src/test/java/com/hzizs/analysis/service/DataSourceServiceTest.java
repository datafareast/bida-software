package com.hzizs.analysis.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.hzizs.test.spring.TestBase;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.annotations.Ignore;

import com.hzizs.vo.Container;
import com.hzizs.analysis.entity.DataSource;
import com.hzizs.analysis.query.DataSourceQuery;

/**
 * 数据源 测试服务类
 * 
 * @author crazy_cabbage
 */
public class DataSourceServiceTest extends TestBase {
	private DataSourceService dataSourceService;

	public DataSourceServiceTest() {
		dataSourceService = (DataSourceService) ac.getBean("dataSourceService");
	}

	/**
	 * 统计 数据源
	 */
	@Test
	@Ignore
	public void count() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		long count = dataSourceService.count(parameters);
		Assert.assertNull(count);
	}

	/**
	 * 删除 数据源
	 */
	@Test
	@Ignore
	public void deleteEntity() {
		DataSource dataSource = new DataSource();
		dataSource.setId(0L);
		dataSourceService.delete(dataSource);
	}

	/**
	 * 按条件删除 数据源
	 */
	@Test
	@Ignore
	public void deleteMap() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", 1L);
		dataSourceService.delete(parameters);

	}

	/**
	 * 按主键删除 数据源
	 */
	@Test
	@Ignore
	public void deleteById() {
		dataSourceService.deleteById(1L);
	}

	/**
	 * 根据条件查找  数据源
	 */
	@Test
	@Ignore
	public void find() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		List<DataSource> dataSources = dataSourceService.find(parameters);
		Assert.assertNull(dataSources);
	}

	/**
	 * 根据主键查找 数据源
	 */
	@Test
	@Ignore
	public void findById() {
		DataSource dataSource = dataSourceService.findById(1L);
		Assert.assertNull(dataSource);
	}

	/**
	 * 查找  数据源
	 */
	@Test
	@Ignore
	public void findContainer() {
		DataSourceQuery query = new DataSourceQuery();
		Container<DataSource> container = dataSourceService.findContainer(query);
		Assert.assertNull(container);
	}

	/**
	 * 查找单个  数据源
	 */
	@Test
	@Ignore
	public void findOne() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", 1L);
		DataSource dataSource = dataSourceService.findOne(parameters);
		Assert.assertNull(dataSource);
	}

	/**
	 * 保存  数据源
	 */
	@Test
	@Ignore
	public void save() {
		DataSource dataSource = new DataSource();
		dataSourceService.save(dataSource);
	}

	/**
	 * 保存或更新  数据源
	 */
	@Test
	@Ignore
	public void saveOrUpdate() {
		DataSource dataSource = new DataSource();
		dataSourceService.saveOrUpdate(dataSource);
	}

	/**
	 * 更新  数据源
	 */
	@Test
	@Ignore
	public void update() {
		DataSource dataSource = new DataSource();
		dataSource.setId(0L);
		dataSourceService.update(dataSource);
	}

	@Test
	@Ignore
	public void otherMethod() {
		// 新的测试内容
	}

}
