package com.hzizs.analysis.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.hzizs.test.spring.TestBase;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.annotations.Ignore;

import com.hzizs.vo.Container;
import com.hzizs.analysis.entity.Industry;
import com.hzizs.analysis.query.IndustryQuery;

/**
 * 行业 测试服务类
 * 
 * @author crazy_cabbage
 */
public class IndustryServiceTest extends TestBase {
	private IndustryService industryService;

	public IndustryServiceTest() {
		industryService = (IndustryService) ac.getBean("industryService");
	}

	/**
	 * 统计 行业
	 */
	@Test
	@Ignore
	public void count() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		long count = industryService.count(parameters);
		Assert.assertNull(count);
	}

	/**
	 * 删除 行业
	 */
	@Test
	@Ignore
	public void deleteEntity() {
		Industry industry = new Industry();
		industry.setId(0L);
		industryService.delete(industry);
	}

	/**
	 * 按条件删除 行业
	 */
	@Test
	@Ignore
	public void deleteMap() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", 1L);
		industryService.delete(parameters);

	}

	/**
	 * 按主键删除 行业
	 */
	@Test
	@Ignore
	public void deleteById() {
		industryService.deleteById(1L);
	}

	/**
	 * 根据条件查找  行业
	 */
	@Test
	@Ignore
	public void find() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		List<Industry> industrys = industryService.find(parameters);
		Assert.assertNull(industrys);
	}

	/**
	 * 根据主键查找 行业
	 */
	@Test
	@Ignore
	public void findById() {
		Industry industry = industryService.findById(1L);
		Assert.assertNull(industry);
	}

	/**
	 * 查找  行业
	 */
	@Test
	@Ignore
	public void findContainer() {
		IndustryQuery query = new IndustryQuery();
		Container<Industry> container = industryService.findContainer(query);
		Assert.assertNull(container);
	}

	/**
	 * 查找单个  行业
	 */
	@Test
	@Ignore
	public void findOne() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", 1L);
		Industry industry = industryService.findOne(parameters);
		Assert.assertNull(industry);
	}

	/**
	 * 保存  行业
	 */
	@Test
	@Ignore
	public void save() {
		Industry industry = new Industry();
		industryService.save(industry);
	}

	/**
	 * 保存或更新  行业
	 */
	@Test
	@Ignore
	public void saveOrUpdate() {
		Industry industry = new Industry();
		industryService.saveOrUpdate(industry);
	}

	/**
	 * 更新  行业
	 */
	@Test
	@Ignore
	public void update() {
		Industry industry = new Industry();
		industry.setId(0L);
		industryService.update(industry);
	}

	@Test
	@Ignore
	public void otherMethod() {
		// 新的测试内容
	}

}
