package com.hzizs.analysis.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.hzizs.test.spring.TestBase;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.annotations.Ignore;

import com.hzizs.vo.Container;
import com.hzizs.analysis.entity.QuotedSpreadMarket;
import com.hzizs.analysis.query.QuotedSpreadMarketQuery;

/**
 * 市场报价价差 测试服务类
 * 
 * @author crazy_cabbage
 */
public class QuotedSpreadMarketServiceTest extends TestBase {
	private QuotedSpreadMarketService quotedSpreadMarketService;

	public QuotedSpreadMarketServiceTest() {
		quotedSpreadMarketService = (QuotedSpreadMarketService) ac.getBean("quotedSpreadMarketService");
	}

	/**
	 * 统计 市场报价价差
	 */
	@Test
	@Ignore
	public void count() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		long count = quotedSpreadMarketService.count(parameters);
		Assert.assertNull(count);
	}

	/**
	 * 删除 市场报价价差
	 */
	@Test
	@Ignore
	public void deleteEntity() {
		QuotedSpreadMarket quotedSpreadMarket = new QuotedSpreadMarket();
		quotedSpreadMarket.setId(0L);
		quotedSpreadMarketService.delete(quotedSpreadMarket);
	}

	/**
	 * 按条件删除 市场报价价差
	 */
	@Test
	@Ignore
	public void deleteMap() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", 1L);
		quotedSpreadMarketService.delete(parameters);

	}

	/**
	 * 按主键删除 市场报价价差
	 */
	@Test
	@Ignore
	public void deleteById() {
		quotedSpreadMarketService.deleteById(1L);
	}

	/**
	 * 根据条件查找  市场报价价差
	 */
	@Test
	@Ignore
	public void find() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		List<QuotedSpreadMarket> quotedSpreadMarkets = quotedSpreadMarketService.find(parameters);
		Assert.assertNull(quotedSpreadMarkets);
	}

	/**
	 * 根据主键查找 市场报价价差
	 */
	@Test
	@Ignore
	public void findById() {
		QuotedSpreadMarket quotedSpreadMarket = quotedSpreadMarketService.findById(1L);
		Assert.assertNull(quotedSpreadMarket);
	}

	/**
	 * 查找  市场报价价差
	 */
	@Test
	@Ignore
	public void findContainer() {
		QuotedSpreadMarketQuery query = new QuotedSpreadMarketQuery();
		Container<QuotedSpreadMarket> container = quotedSpreadMarketService.findContainer(query);
		Assert.assertNull(container);
	}

	/**
	 * 查找单个  市场报价价差
	 */
	@Test
	@Ignore
	public void findOne() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", 1L);
		QuotedSpreadMarket quotedSpreadMarket = quotedSpreadMarketService.findOne(parameters);
		Assert.assertNull(quotedSpreadMarket);
	}

	/**
	 * 保存  市场报价价差
	 */
	@Test
	@Ignore
	public void save() {
		QuotedSpreadMarket quotedSpreadMarket = new QuotedSpreadMarket();
		quotedSpreadMarketService.save(quotedSpreadMarket);
	}

	/**
	 * 保存或更新  市场报价价差
	 */
	@Test
	@Ignore
	public void saveOrUpdate() {
		QuotedSpreadMarket quotedSpreadMarket = new QuotedSpreadMarket();
		quotedSpreadMarketService.saveOrUpdate(quotedSpreadMarket);
	}

	/**
	 * 更新  市场报价价差
	 */
	@Test
	@Ignore
	public void update() {
		QuotedSpreadMarket quotedSpreadMarket = new QuotedSpreadMarket();
		quotedSpreadMarket.setId(0L);
		quotedSpreadMarketService.update(quotedSpreadMarket);
	}

	@Test
	@Ignore
	public void otherMethod() {
		// 新的测试内容
	}

}
