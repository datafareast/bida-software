package com.hzizs.analysis.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.hzizs.test.spring.TestBase;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.annotations.Ignore;

import com.hzizs.vo.Container;
import com.hzizs.analysis.entity.SysI18n;
import com.hzizs.analysis.query.SysI18nQuery;

/**
 * 系统国际化 测试服务类
 * 
 * @author crazy_cabbage
 */
public class SysI18nServiceTest extends TestBase {
	private SysI18nService sysI18nService;

	public SysI18nServiceTest() {
		sysI18nService = (SysI18nService) ac.getBean("sysI18nService");
	}

	/**
	 * 统计 系统国际化
	 */
	@Test
	@Ignore
	public void count() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		long count = sysI18nService.count(parameters);
		Assert.assertNull(count);
	}

	/**
	 * 删除 系统国际化
	 */
	@Test
	@Ignore
	public void deleteEntity() {
		SysI18n sysI18n = new SysI18n();
		sysI18n.setId(0L);
		sysI18nService.delete(sysI18n);
	}

	/**
	 * 按条件删除 系统国际化
	 */
	@Test
	@Ignore
	public void deleteMap() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", 1L);
		sysI18nService.delete(parameters);

	}

	/**
	 * 按主键删除 系统国际化
	 */
	@Test
	@Ignore
	public void deleteById() {
		sysI18nService.deleteById(1L);
	}

	/**
	 * 根据条件查找  系统国际化
	 */
	@Test
	@Ignore
	public void find() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		List<SysI18n> sysI18ns = sysI18nService.find(parameters);
		Assert.assertNull(sysI18ns);
	}

	/**
	 * 根据主键查找 系统国际化
	 */
	@Test
	@Ignore
	public void findById() {
		SysI18n sysI18n = sysI18nService.findById(1L);
		Assert.assertNull(sysI18n);
	}

	/**
	 * 查找  系统国际化
	 */
	@Test
	@Ignore
	public void findContainer() {
		SysI18nQuery query = new SysI18nQuery();
		Container<SysI18n> container = sysI18nService.findContainer(query);
		Assert.assertNull(container);
	}

	/**
	 * 查找单个  系统国际化
	 */
	@Test
	@Ignore
	public void findOne() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", 1L);
		SysI18n sysI18n = sysI18nService.findOne(parameters);
		Assert.assertNull(sysI18n);
	}

	/**
	 * 保存  系统国际化
	 */
	@Test
	@Ignore
	public void save() {
		SysI18n sysI18n = new SysI18n();
		sysI18nService.save(sysI18n);
	}

	/**
	 * 保存或更新  系统国际化
	 */
	@Test
	@Ignore
	public void saveOrUpdate() {
		SysI18n sysI18n = new SysI18n();
		sysI18nService.saveOrUpdate(sysI18n);
	}

	/**
	 * 更新  系统国际化
	 */
	@Test
	@Ignore
	public void update() {
		SysI18n sysI18n = new SysI18n();
		sysI18n.setId(0L);
		sysI18nService.update(sysI18n);
	}

	@Test
	@Ignore
	public void otherMethod() {
		// 新的测试内容
	}

}
