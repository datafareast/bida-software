package com.hzizs.analysis.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.hzizs.test.spring.TestBase;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.annotations.Ignore;

import com.hzizs.vo.Container;
import com.hzizs.analysis.entity.SysRole;
import com.hzizs.analysis.query.SysRoleQuery;

/**
 * 系统角色 测试服务类
 * 
 * @author crazy_cabbage
 */
public class SysRoleServiceTest extends TestBase {
	private SysRoleService sysRoleService;

	public SysRoleServiceTest() {
		sysRoleService = (SysRoleService) ac.getBean("sysRoleService");
	}

	/**
	 * 统计 系统角色
	 */
	@Test
	@Ignore
	public void count() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		long count = sysRoleService.count(parameters);
		Assert.assertNull(count);
	}

	/**
	 * 删除 系统角色
	 */
	@Test
	@Ignore
	public void deleteEntity() {
		SysRole sysRole = new SysRole();
		sysRole.setId(0L);
		sysRoleService.delete(sysRole);
	}

	/**
	 * 按条件删除 系统角色
	 */
	@Test
	@Ignore
	public void deleteMap() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", 1L);
		sysRoleService.delete(parameters);

	}

	/**
	 * 按主键删除 系统角色
	 */
	@Test
	@Ignore
	public void deleteById() {
		sysRoleService.deleteById(1L);
	}

	/**
	 * 根据条件查找  系统角色
	 */
	@Test
	@Ignore
	public void find() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		List<SysRole> sysRoles = sysRoleService.find(parameters);
		Assert.assertNull(sysRoles);
	}

	/**
	 * 根据主键查找 系统角色
	 */
	@Test
	@Ignore
	public void findById() {
		SysRole sysRole = sysRoleService.findById(1L);
		Assert.assertNull(sysRole);
	}

	/**
	 * 查找  系统角色
	 */
	@Test
	@Ignore
	public void findContainer() {
		SysRoleQuery query = new SysRoleQuery();
		Container<SysRole> container = sysRoleService.findContainer(query);
		Assert.assertNull(container);
	}

	/**
	 * 查找单个  系统角色
	 */
	@Test
	@Ignore
	public void findOne() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", 1L);
		SysRole sysRole = sysRoleService.findOne(parameters);
		Assert.assertNull(sysRole);
	}

	/**
	 * 保存  系统角色
	 */
	@Test
	@Ignore
	public void save() {
		SysRole sysRole = new SysRole();
		sysRoleService.save(sysRole);
	}

	/**
	 * 保存或更新  系统角色
	 */
	@Test
	@Ignore
	public void saveOrUpdate() {
		SysRole sysRole = new SysRole();
		sysRoleService.saveOrUpdate(sysRole);
	}

	/**
	 * 更新  系统角色
	 */
	@Test
	@Ignore
	public void update() {
		SysRole sysRole = new SysRole();
		sysRole.setId(0L);
		sysRoleService.update(sysRole);
	}

	@Test
	@Ignore
	public void otherMethod() {
		// 新的测试内容
	}

}
