package com.hzizs.analysis.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.hzizs.test.spring.TestBase;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.annotations.Ignore;

import com.hzizs.vo.Container;
import com.hzizs.analysis.entity.Security;
import com.hzizs.analysis.query.SecurityQuery;

/**
 * 证券 测试服务类
 * 
 * @author crazy_cabbage
 */
public class SecurityServiceTest extends TestBase {
	private SecurityService securityService;

	public SecurityServiceTest() {
		securityService = (SecurityService) ac.getBean("securityService");
	}

	/**
	 * 统计 证券
	 */
	@Test
	@Ignore
	public void count() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		long count = securityService.count(parameters);
		Assert.assertNull(count);
	}

	/**
	 * 删除 证券
	 */
	@Test
	@Ignore
	public void deleteEntity() {
		Security security = new Security();
		security.setId(0L);
		securityService.delete(security);
	}

	/**
	 * 按条件删除 证券
	 */
	@Test
	@Ignore
	public void deleteMap() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", 1L);
		securityService.delete(parameters);

	}

	/**
	 * 按主键删除 证券
	 */
	@Test
	@Ignore
	public void deleteById() {
		securityService.deleteById(1L);
	}

	/**
	 * 根据条件查找  证券
	 */
	@Test
	@Ignore
	public void find() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		List<Security> securitys = securityService.find(parameters);
		Assert.assertNull(securitys);
	}

	/**
	 * 根据主键查找 证券
	 */
	@Test
	@Ignore
	public void findById() {
		Security security = securityService.findById(1L);
		Assert.assertNull(security);
	}

	/**
	 * 查找  证券
	 */
	@Test
	@Ignore
	public void findContainer() {
		SecurityQuery query = new SecurityQuery();
		Container<Security> container = securityService.findContainer(query);
		Assert.assertNull(container);
	}

	/**
	 * 查找单个  证券
	 */
	@Test
	@Ignore
	public void findOne() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", 1L);
		Security security = securityService.findOne(parameters);
		Assert.assertNull(security);
	}

	/**
	 * 保存  证券
	 */
	@Test
	@Ignore
	public void save() {
		Security security = new Security();
		securityService.save(security);
	}

	/**
	 * 保存或更新  证券
	 */
	@Test
	@Ignore
	public void saveOrUpdate() {
		Security security = new Security();
		securityService.saveOrUpdate(security);
	}

	/**
	 * 更新  证券
	 */
	@Test
	@Ignore
	public void update() {
		Security security = new Security();
		security.setId(0L);
		securityService.update(security);
	}

	@Test
	@Ignore
	public void otherMethod() {
		// 新的测试内容
	}

}
