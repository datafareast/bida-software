package com.hzizs.analysis.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.hzizs.test.spring.TestBase;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.annotations.Ignore;

import com.hzizs.vo.Container;
import com.hzizs.analysis.entity.Menu;
import com.hzizs.analysis.query.MenuQuery;

/**
 * 菜单 测试服务类
 * 
 * @author crazy_cabbage
 */
public class MenuServiceTest extends TestBase {
	private MenuService menuService;

	public MenuServiceTest() {
		menuService = (MenuService) ac.getBean("menuService");
	}

	/**
	 * 统计 菜单
	 */
	@Test
	@Ignore
	public void count() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		long count = menuService.count(parameters);
		Assert.assertNull(count);
	}

	/**
	 * 删除 菜单
	 */
	@Test
	@Ignore
	public void deleteEntity() {
		Menu menu = new Menu();
		menu.setId(0L);
		menuService.delete(menu);
	}

	/**
	 * 按条件删除 菜单
	 */
	@Test
	@Ignore
	public void deleteMap() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", 1L);
		menuService.delete(parameters);

	}

	/**
	 * 按主键删除 菜单
	 */
	@Test
	@Ignore
	public void deleteById() {
		menuService.deleteById(1L);
	}

	/**
	 * 根据条件查找  菜单
	 */
	@Test
	@Ignore
	public void find() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		List<Menu> menus = menuService.find(parameters);
		Assert.assertNull(menus);
	}

	/**
	 * 根据主键查找 菜单
	 */
	@Test
	@Ignore
	public void findById() {
		Menu menu = menuService.findById(1L);
		Assert.assertNull(menu);
	}

	/**
	 * 查找  菜单
	 */
	@Test
	@Ignore
	public void findContainer() {
		MenuQuery query = new MenuQuery();
		Container<Menu> container = menuService.findContainer(query);
		Assert.assertNull(container);
	}

	/**
	 * 查找单个  菜单
	 */
	@Test
	@Ignore
	public void findOne() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", 1L);
		Menu menu = menuService.findOne(parameters);
		Assert.assertNull(menu);
	}

	/**
	 * 保存  菜单
	 */
	@Test
	@Ignore
	public void save() {
		Menu menu = new Menu();
		menuService.save(menu);
	}

	/**
	 * 保存或更新  菜单
	 */
	@Test
	@Ignore
	public void saveOrUpdate() {
		Menu menu = new Menu();
		menuService.saveOrUpdate(menu);
	}

	/**
	 * 更新  菜单
	 */
	@Test
	@Ignore
	public void update() {
		Menu menu = new Menu();
		menu.setId(0L);
		menuService.update(menu);
	}

	@Test
	@Ignore
	public void otherMethod() {
		// 新的测试内容
	}

}
