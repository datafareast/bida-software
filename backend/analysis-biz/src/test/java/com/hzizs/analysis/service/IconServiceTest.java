package com.hzizs.analysis.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.hzizs.test.spring.TestBase;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.annotations.Ignore;

import com.hzizs.vo.Container;
import com.hzizs.analysis.entity.Icon;
import com.hzizs.analysis.query.IconQuery;

/**
 * 图标库 测试服务类
 * 
 * @author crazy_cabbage
 */
public class IconServiceTest extends TestBase {
	private IconService iconService;

	public IconServiceTest() {
		iconService = (IconService) ac.getBean("iconService");
	}

	/**
	 * 统计 图标库
	 */
	@Test
	@Ignore
	public void count() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		long count = iconService.count(parameters);
		Assert.assertNull(count);
	}

	/**
	 * 删除 图标库
	 */
	@Test
	@Ignore
	public void deleteEntity() {
		Icon icon = new Icon();
		icon.setId(0L);
		iconService.delete(icon);
	}

	/**
	 * 按条件删除 图标库
	 */
	@Test
	@Ignore
	public void deleteMap() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", 1L);
		iconService.delete(parameters);

	}

	/**
	 * 按主键删除 图标库
	 */
	@Test
	@Ignore
	public void deleteById() {
		iconService.deleteById(1L);
	}

	/**
	 * 根据条件查找  图标库
	 */
	@Test
	@Ignore
	public void find() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		List<Icon> icons = iconService.find(parameters);
		Assert.assertNull(icons);
	}

	/**
	 * 根据主键查找 图标库
	 */
	@Test
	@Ignore
	public void findById() {
		Icon icon = iconService.findById(1L);
		Assert.assertNull(icon);
	}

	/**
	 * 查找  图标库
	 */
	@Test
	@Ignore
	public void findContainer() {
		IconQuery query = new IconQuery();
		Container<Icon> container = iconService.findContainer(query);
		Assert.assertNull(container);
	}

	/**
	 * 查找单个  图标库
	 */
	@Test
	@Ignore
	public void findOne() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", 1L);
		Icon icon = iconService.findOne(parameters);
		Assert.assertNull(icon);
	}

	/**
	 * 保存  图标库
	 */
	@Test
	@Ignore
	public void save() {
		Icon icon = new Icon();
		iconService.save(icon);
	}

	/**
	 * 保存或更新  图标库
	 */
	@Test
	@Ignore
	public void saveOrUpdate() {
		Icon icon = new Icon();
		iconService.saveOrUpdate(icon);
	}

	/**
	 * 更新  图标库
	 */
	@Test
	@Ignore
	public void update() {
		Icon icon = new Icon();
		icon.setId(0L);
		iconService.update(icon);
	}

	@Test
	@Ignore
	public void otherMethod() {
		// 新的测试内容
	}

}
