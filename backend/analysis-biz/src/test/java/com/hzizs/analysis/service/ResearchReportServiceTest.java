package com.hzizs.analysis.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.hzizs.test.spring.TestBase;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.annotations.Ignore;

import com.hzizs.vo.Container;
import com.hzizs.analysis.entity.ResearchReport;
import com.hzizs.analysis.query.ResearchReportQuery;

/**
 * 研究报告 测试服务类
 * 
 * @author crazy_cabbage
 */
public class ResearchReportServiceTest extends TestBase {
	private ResearchReportService researchReportService;

	public ResearchReportServiceTest() {
		researchReportService = (ResearchReportService) ac.getBean("researchReportService");
	}

	/**
	 * 统计 研究报告
	 */
	@Test
	@Ignore
	public void count() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		long count = researchReportService.count(parameters);
		Assert.assertNull(count);
	}

	/**
	 * 删除 研究报告
	 */
	@Test
	@Ignore
	public void deleteEntity() {
		ResearchReport researchReport = new ResearchReport();
		researchReport.setId(0L);
		researchReportService.delete(researchReport);
	}

	/**
	 * 按条件删除 研究报告
	 */
	@Test
	@Ignore
	public void deleteMap() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", 1L);
		researchReportService.delete(parameters);

	}

	/**
	 * 按主键删除 研究报告
	 */
	@Test
	@Ignore
	public void deleteById() {
		researchReportService.deleteById(1L);
	}

	/**
	 * 根据条件查找  研究报告
	 */
	@Test
	@Ignore
	public void find() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		List<ResearchReport> researchReports = researchReportService.find(parameters);
		Assert.assertNull(researchReports);
	}

	/**
	 * 根据主键查找 研究报告
	 */
	@Test
	@Ignore
	public void findById() {
		ResearchReport researchReport = researchReportService.findById(1L);
		Assert.assertNull(researchReport);
	}

	/**
	 * 查找  研究报告
	 */
	@Test
	@Ignore
	public void findContainer() {
		ResearchReportQuery query = new ResearchReportQuery();
		Container<ResearchReport> container = researchReportService.findContainer(query);
		Assert.assertNull(container);
	}

	/**
	 * 查找单个  研究报告
	 */
	@Test
	@Ignore
	public void findOne() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", 1L);
		ResearchReport researchReport = researchReportService.findOne(parameters);
		Assert.assertNull(researchReport);
	}

	/**
	 * 保存  研究报告
	 */
	@Test
	@Ignore
	public void save() {
		ResearchReport researchReport = new ResearchReport();
		researchReportService.save(researchReport);
	}

	/**
	 * 保存或更新  研究报告
	 */
	@Test
	@Ignore
	public void saveOrUpdate() {
		ResearchReport researchReport = new ResearchReport();
		researchReportService.saveOrUpdate(researchReport);
	}

	/**
	 * 更新  研究报告
	 */
	@Test
	@Ignore
	public void update() {
		ResearchReport researchReport = new ResearchReport();
		researchReport.setId(0L);
		researchReportService.update(researchReport);
	}

	@Test
	@Ignore
	public void otherMethod() {
		// 新的测试内容
	}

}
