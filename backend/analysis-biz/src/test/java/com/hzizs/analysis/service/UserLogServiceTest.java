package com.hzizs.analysis.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.hzizs.test.spring.TestBase;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.annotations.Ignore;

import com.hzizs.vo.Container;
import com.hzizs.analysis.entity.UserLog;
import com.hzizs.analysis.query.UserLogQuery;

/**
 * 用户日志 测试服务类
 * 
 * @author crazy_cabbage
 */
public class UserLogServiceTest extends TestBase {
	private UserLogService userLogService;

	public UserLogServiceTest() {
		userLogService = (UserLogService) ac.getBean("userLogService");
	}

	/**
	 * 统计 用户日志
	 */
	@Test
	@Ignore
	public void count() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		long count = userLogService.count(parameters);
		Assert.assertNull(count);
	}

	/**
	 * 删除 用户日志
	 */
	@Test
	@Ignore
	public void deleteEntity() {
		UserLog userLog = new UserLog();
		userLog.setId(0L);
		userLogService.delete(userLog);
	}

	/**
	 * 按条件删除 用户日志
	 */
	@Test
	@Ignore
	public void deleteMap() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", 1L);
		userLogService.delete(parameters);

	}

	/**
	 * 按主键删除 用户日志
	 */
	@Test
	@Ignore
	public void deleteById() {
		userLogService.deleteById(1L);
	}

	/**
	 * 根据条件查找  用户日志
	 */
	@Test
	@Ignore
	public void find() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		List<UserLog> userLogs = userLogService.find(parameters);
		Assert.assertNull(userLogs);
	}

	/**
	 * 根据主键查找 用户日志
	 */
	@Test
	@Ignore
	public void findById() {
		UserLog userLog = userLogService.findById(1L);
		Assert.assertNull(userLog);
	}

	/**
	 * 查找  用户日志
	 */
	@Test
	@Ignore
	public void findContainer() {
		UserLogQuery query = new UserLogQuery();
		Container<UserLog> container = userLogService.findContainer(query);
		Assert.assertNull(container);
	}

	/**
	 * 查找单个  用户日志
	 */
	@Test
	@Ignore
	public void findOne() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", 1L);
		UserLog userLog = userLogService.findOne(parameters);
		Assert.assertNull(userLog);
	}

	/**
	 * 保存  用户日志
	 */
	@Test
	@Ignore
	public void save() {
		UserLog userLog = new UserLog();
		userLogService.save(userLog);
	}

	/**
	 * 保存或更新  用户日志
	 */
	@Test
	@Ignore
	public void saveOrUpdate() {
		UserLog userLog = new UserLog();
		userLogService.saveOrUpdate(userLog);
	}

	/**
	 * 更新  用户日志
	 */
	@Test
	@Ignore
	public void update() {
		UserLog userLog = new UserLog();
		userLog.setId(0L);
		userLogService.update(userLog);
	}

	@Test
	@Ignore
	public void otherMethod() {
		// 新的测试内容
	}

}
