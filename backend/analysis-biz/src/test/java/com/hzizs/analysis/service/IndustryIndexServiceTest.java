package com.hzizs.analysis.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.hzizs.test.spring.TestBase;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.annotations.Ignore;

import com.hzizs.vo.Container;
import com.hzizs.analysis.entity.IndustryIndex;
import com.hzizs.analysis.query.IndustryIndexQuery;

/**
 * 行业指标 测试服务类
 * 
 * @author crazy_cabbage
 */
public class IndustryIndexServiceTest extends TestBase {
	private IndustryIndexService industryIndexService;

	public IndustryIndexServiceTest() {
		industryIndexService = (IndustryIndexService) ac.getBean("industryIndexService");
	}

	/**
	 * 统计 行业指标
	 */
	@Test
	@Ignore
	public void count() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		long count = industryIndexService.count(parameters);
		Assert.assertNull(count);
	}

	/**
	 * 删除 行业指标
	 */
	@Test
	@Ignore
	public void deleteEntity() {
		IndustryIndex industryIndex = new IndustryIndex();
		industryIndex.setId(0L);
		industryIndexService.delete(industryIndex);
	}

	/**
	 * 按条件删除 行业指标
	 */
	@Test
	@Ignore
	public void deleteMap() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", 1L);
		industryIndexService.delete(parameters);

	}

	/**
	 * 按主键删除 行业指标
	 */
	@Test
	@Ignore
	public void deleteById() {
		industryIndexService.deleteById(1L);
	}

	/**
	 * 根据条件查找  行业指标
	 */
	@Test
	@Ignore
	public void find() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		List<IndustryIndex> industryIndexs = industryIndexService.find(parameters);
		Assert.assertNull(industryIndexs);
	}

	/**
	 * 根据主键查找 行业指标
	 */
	@Test
	@Ignore
	public void findById() {
		IndustryIndex industryIndex = industryIndexService.findById(1L);
		Assert.assertNull(industryIndex);
	}

	/**
	 * 查找  行业指标
	 */
	@Test
	@Ignore
	public void findContainer() {
		IndustryIndexQuery query = new IndustryIndexQuery();
		Container<IndustryIndex> container = industryIndexService.findContainer(query);
		Assert.assertNull(container);
	}

	/**
	 * 查找单个  行业指标
	 */
	@Test
	@Ignore
	public void findOne() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", 1L);
		IndustryIndex industryIndex = industryIndexService.findOne(parameters);
		Assert.assertNull(industryIndex);
	}

	/**
	 * 保存  行业指标
	 */
	@Test
	@Ignore
	public void save() {
		IndustryIndex industryIndex = new IndustryIndex();
		industryIndexService.save(industryIndex);
	}

	/**
	 * 保存或更新  行业指标
	 */
	@Test
	@Ignore
	public void saveOrUpdate() {
		IndustryIndex industryIndex = new IndustryIndex();
		industryIndexService.saveOrUpdate(industryIndex);
	}

	/**
	 * 更新  行业指标
	 */
	@Test
	@Ignore
	public void update() {
		IndustryIndex industryIndex = new IndustryIndex();
		industryIndex.setId(0L);
		industryIndexService.update(industryIndex);
	}

	@Test
	@Ignore
	public void otherMethod() {
		// 新的测试内容
	}

}
