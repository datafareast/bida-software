package com.hzizs.analysis.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.hzizs.test.spring.TestBase;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.annotations.Ignore;

import com.hzizs.vo.Container;
import com.hzizs.analysis.entity.SecAnalysisItem;
import com.hzizs.analysis.query.SecAnalysisItemQuery;

/**
 * 业绩分析条目 测试服务类
 * 
 * @author crazy_cabbage
 */
public class SecAnalysisItemServiceTest extends TestBase {
	private SecAnalysisItemService secAnalysisItemService;

	public SecAnalysisItemServiceTest() {
		secAnalysisItemService = (SecAnalysisItemService) ac.getBean("secAnalysisItemService");
	}

	/**
	 * 统计 业绩分析条目
	 */
	@Test
	@Ignore
	public void count() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		long count = secAnalysisItemService.count(parameters);
		Assert.assertNull(count);
	}

	/**
	 * 删除 业绩分析条目
	 */
	@Test
	@Ignore
	public void deleteEntity() {
		SecAnalysisItem secAnalysisItem = new SecAnalysisItem();
		secAnalysisItem.setId(0L);
		secAnalysisItemService.delete(secAnalysisItem);
	}

	/**
	 * 按条件删除 业绩分析条目
	 */
	@Test
	@Ignore
	public void deleteMap() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", 1L);
		secAnalysisItemService.delete(parameters);

	}

	/**
	 * 按主键删除 业绩分析条目
	 */
	@Test
	@Ignore
	public void deleteById() {
		secAnalysisItemService.deleteById(1L);
	}

	/**
	 * 根据条件查找  业绩分析条目
	 */
	@Test
	@Ignore
	public void find() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		List<SecAnalysisItem> secAnalysisItems = secAnalysisItemService.find(parameters);
		Assert.assertNull(secAnalysisItems);
	}

	/**
	 * 根据主键查找 业绩分析条目
	 */
	@Test
	@Ignore
	public void findById() {
		SecAnalysisItem secAnalysisItem = secAnalysisItemService.findById(1L);
		Assert.assertNull(secAnalysisItem);
	}

	/**
	 * 查找  业绩分析条目
	 */
	@Test
	@Ignore
	public void findContainer() {
		SecAnalysisItemQuery query = new SecAnalysisItemQuery();
		Container<SecAnalysisItem> container = secAnalysisItemService.findContainer(query);
		Assert.assertNull(container);
	}

	/**
	 * 查找单个  业绩分析条目
	 */
	@Test
	@Ignore
	public void findOne() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", 1L);
		SecAnalysisItem secAnalysisItem = secAnalysisItemService.findOne(parameters);
		Assert.assertNull(secAnalysisItem);
	}

	/**
	 * 保存  业绩分析条目
	 */
	@Test
	@Ignore
	public void save() {
		SecAnalysisItem secAnalysisItem = new SecAnalysisItem();
		secAnalysisItemService.save(secAnalysisItem);
	}

	/**
	 * 保存或更新  业绩分析条目
	 */
	@Test
	@Ignore
	public void saveOrUpdate() {
		SecAnalysisItem secAnalysisItem = new SecAnalysisItem();
		secAnalysisItemService.saveOrUpdate(secAnalysisItem);
	}

	/**
	 * 更新  业绩分析条目
	 */
	@Test
	@Ignore
	public void update() {
		SecAnalysisItem secAnalysisItem = new SecAnalysisItem();
		secAnalysisItem.setId(0L);
		secAnalysisItemService.update(secAnalysisItem);
	}

	@Test
	@Ignore
	public void otherMethod() {
		// 新的测试内容
	}

}
