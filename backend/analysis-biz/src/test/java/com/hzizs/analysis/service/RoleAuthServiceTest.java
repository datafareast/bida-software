package com.hzizs.analysis.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.hzizs.test.spring.TestBase;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.annotations.Ignore;

import com.hzizs.vo.Container;
import com.hzizs.analysis.entity.RoleAuth;
import com.hzizs.analysis.query.RoleAuthQuery;

/**
 * 角色权限 测试服务类
 * 
 * @author crazy_cabbage
 */
public class RoleAuthServiceTest extends TestBase {
	private RoleAuthService roleAuthService;

	public RoleAuthServiceTest() {
		roleAuthService = (RoleAuthService) ac.getBean("roleAuthService");
	}

	/**
	 * 统计 角色权限
	 */
	@Test
	@Ignore
	public void count() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		long count = roleAuthService.count(parameters);
		Assert.assertNull(count);
	}

	/**
	 * 删除 角色权限
	 */
	@Test
	@Ignore
	public void deleteEntity() {
		RoleAuth roleAuth = new RoleAuth();
		roleAuth.setId(0L);
		roleAuthService.delete(roleAuth);
	}

	/**
	 * 按条件删除 角色权限
	 */
	@Test
	@Ignore
	public void deleteMap() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", 1L);
		roleAuthService.delete(parameters);

	}

	/**
	 * 按主键删除 角色权限
	 */
	@Test
	@Ignore
	public void deleteById() {
		roleAuthService.deleteById(1L);
	}

	/**
	 * 根据条件查找  角色权限
	 */
	@Test
	@Ignore
	public void find() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		List<RoleAuth> roleAuths = roleAuthService.find(parameters);
		Assert.assertNull(roleAuths);
	}

	/**
	 * 根据主键查找 角色权限
	 */
	@Test
	@Ignore
	public void findById() {
		RoleAuth roleAuth = roleAuthService.findById(1L);
		Assert.assertNull(roleAuth);
	}

	/**
	 * 查找  角色权限
	 */
	@Test
	@Ignore
	public void findContainer() {
		RoleAuthQuery query = new RoleAuthQuery();
		Container<RoleAuth> container = roleAuthService.findContainer(query);
		Assert.assertNull(container);
	}

	/**
	 * 查找单个  角色权限
	 */
	@Test
	@Ignore
	public void findOne() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", 1L);
		RoleAuth roleAuth = roleAuthService.findOne(parameters);
		Assert.assertNull(roleAuth);
	}

	/**
	 * 保存  角色权限
	 */
	@Test
	@Ignore
	public void save() {
		RoleAuth roleAuth = new RoleAuth();
		roleAuthService.save(roleAuth);
	}

	/**
	 * 保存或更新  角色权限
	 */
	@Test
	@Ignore
	public void saveOrUpdate() {
		RoleAuth roleAuth = new RoleAuth();
		roleAuthService.saveOrUpdate(roleAuth);
	}

	/**
	 * 更新  角色权限
	 */
	@Test
	@Ignore
	public void update() {
		RoleAuth roleAuth = new RoleAuth();
		roleAuth.setId(0L);
		roleAuthService.update(roleAuth);
	}

	@Test
	@Ignore
	public void otherMethod() {
		// 新的测试内容
	}

}
