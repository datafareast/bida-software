package com.hzizs.analysis.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.hzizs.test.spring.TestBase;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.annotations.Ignore;

import com.hzizs.vo.Container;
import com.hzizs.analysis.entity.IndexType;
import com.hzizs.analysis.query.IndexTypeQuery;

/**
 * 指标类型 测试服务类
 * 
 * @author crazy_cabbage
 */
public class IndexTypeServiceTest extends TestBase {
	private IndexTypeService indexTypeService;

	public IndexTypeServiceTest() {
		indexTypeService = (IndexTypeService) ac.getBean("indexTypeService");
	}

	/**
	 * 统计 指标类型
	 */
	@Test
	@Ignore
	public void count() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		long count = indexTypeService.count(parameters);
		Assert.assertNull(count);
	}

	/**
	 * 删除 指标类型
	 */
	@Test
	@Ignore
	public void deleteEntity() {
		IndexType indexType = new IndexType();
		indexType.setId(0L);
		indexTypeService.delete(indexType);
	}

	/**
	 * 按条件删除 指标类型
	 */
	@Test
	@Ignore
	public void deleteMap() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", 1L);
		indexTypeService.delete(parameters);

	}

	/**
	 * 按主键删除 指标类型
	 */
	@Test
	@Ignore
	public void deleteById() {
		indexTypeService.deleteById(1L);
	}

	/**
	 * 根据条件查找  指标类型
	 */
	@Test
	@Ignore
	public void find() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		List<IndexType> indexTypes = indexTypeService.find(parameters);
		Assert.assertNull(indexTypes);
	}

	/**
	 * 根据主键查找 指标类型
	 */
	@Test
	@Ignore
	public void findById() {
		IndexType indexType = indexTypeService.findById(1L);
		Assert.assertNull(indexType);
	}

	/**
	 * 查找  指标类型
	 */
	@Test
	@Ignore
	public void findContainer() {
		IndexTypeQuery query = new IndexTypeQuery();
		Container<IndexType> container = indexTypeService.findContainer(query);
		Assert.assertNull(container);
	}

	/**
	 * 查找单个  指标类型
	 */
	@Test
	@Ignore
	public void findOne() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", 1L);
		IndexType indexType = indexTypeService.findOne(parameters);
		Assert.assertNull(indexType);
	}

	/**
	 * 保存  指标类型
	 */
	@Test
	@Ignore
	public void save() {
		IndexType indexType = new IndexType();
		indexTypeService.save(indexType);
	}

	/**
	 * 保存或更新  指标类型
	 */
	@Test
	@Ignore
	public void saveOrUpdate() {
		IndexType indexType = new IndexType();
		indexTypeService.saveOrUpdate(indexType);
	}

	/**
	 * 更新  指标类型
	 */
	@Test
	@Ignore
	public void update() {
		IndexType indexType = new IndexType();
		indexType.setId(0L);
		indexTypeService.update(indexType);
	}

	@Test
	@Ignore
	public void otherMethod() {
		// 新的测试内容
	}

}
