package com.hzizs.analysis.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.hzizs.test.spring.TestBase;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.annotations.Ignore;

import com.hzizs.vo.Container;
import com.hzizs.analysis.entity.UserInfo;
import com.hzizs.analysis.query.UserInfoQuery;

/**
 * 用户信息 测试服务类
 * 
 * @author crazy_cabbage
 */
public class UserInfoServiceTest extends TestBase {
	private UserInfoService userInfoService;

	public UserInfoServiceTest() {
		userInfoService = (UserInfoService) ac.getBean("userInfoService");
	}

	/**
	 * 统计 用户信息
	 */
	@Test
	@Ignore
	public void count() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		long count = userInfoService.count(parameters);
		Assert.assertNull(count);
	}

	/**
	 * 删除 用户信息
	 */
	@Test
	@Ignore
	public void deleteEntity() {
		UserInfo userInfo = new UserInfo();
		userInfo.setId(0L);
		userInfoService.delete(userInfo);
	}

	/**
	 * 按条件删除 用户信息
	 */
	@Test
	@Ignore
	public void deleteMap() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", 1L);
		userInfoService.delete(parameters);

	}

	/**
	 * 按主键删除 用户信息
	 */
	@Test
	@Ignore
	public void deleteById() {
		userInfoService.deleteById(1L);
	}

	/**
	 * 根据条件查找  用户信息
	 */
	@Test
	@Ignore
	public void find() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		List<UserInfo> userInfos = userInfoService.find(parameters);
		Assert.assertNull(userInfos);
	}

	/**
	 * 根据主键查找 用户信息
	 */
	@Test
	@Ignore
	public void findById() {
		UserInfo userInfo = userInfoService.findById(1L);
		Assert.assertNull(userInfo);
	}

	/**
	 * 查找  用户信息
	 */
	@Test
	@Ignore
	public void findContainer() {
		UserInfoQuery query = new UserInfoQuery();
		Container<UserInfo> container = userInfoService.findContainer(query);
		Assert.assertNull(container);
	}

	/**
	 * 查找单个  用户信息
	 */
	@Test
	@Ignore
	public void findOne() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", 1L);
		UserInfo userInfo = userInfoService.findOne(parameters);
		Assert.assertNull(userInfo);
	}

	/**
	 * 保存  用户信息
	 */
	@Test
	@Ignore
	public void save() {
		UserInfo userInfo = new UserInfo();
		userInfoService.save(userInfo);
	}

	/**
	 * 保存或更新  用户信息
	 */
	@Test
	@Ignore
	public void saveOrUpdate() {
		UserInfo userInfo = new UserInfo();
		userInfoService.saveOrUpdate(userInfo);
	}

	/**
	 * 更新  用户信息
	 */
	@Test
	@Ignore
	public void update() {
		UserInfo userInfo = new UserInfo();
		userInfo.setId(0L);
		userInfoService.update(userInfo);
	}

	@Test
	@Ignore
	public void otherMethod() {
		// 新的测试内容
	}

}
