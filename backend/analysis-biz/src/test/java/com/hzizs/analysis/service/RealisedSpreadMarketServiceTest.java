package com.hzizs.analysis.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.hzizs.test.spring.TestBase;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.annotations.Ignore;

import com.hzizs.vo.Container;
import com.hzizs.analysis.entity.RealisedSpreadMarket;
import com.hzizs.analysis.query.RealisedSpreadMarketQuery;

/**
 * 市场实现价差 测试服务类
 * 
 * @author crazy_cabbage
 */
public class RealisedSpreadMarketServiceTest extends TestBase {
	private RealisedSpreadMarketService realisedSpreadMarketService;

	public RealisedSpreadMarketServiceTest() {
		realisedSpreadMarketService = (RealisedSpreadMarketService) ac.getBean("realisedSpreadMarketService");
	}

	/**
	 * 统计 市场实现价差
	 */
	@Test
	@Ignore
	public void count() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		long count = realisedSpreadMarketService.count(parameters);
		Assert.assertNull(count);
	}

	/**
	 * 删除 市场实现价差
	 */
	@Test
	@Ignore
	public void deleteEntity() {
		RealisedSpreadMarket realisedSpreadMarket = new RealisedSpreadMarket();
		realisedSpreadMarket.setId(0L);
		realisedSpreadMarketService.delete(realisedSpreadMarket);
	}

	/**
	 * 按条件删除 市场实现价差
	 */
	@Test
	@Ignore
	public void deleteMap() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", 1L);
		realisedSpreadMarketService.delete(parameters);

	}

	/**
	 * 按主键删除 市场实现价差
	 */
	@Test
	@Ignore
	public void deleteById() {
		realisedSpreadMarketService.deleteById(1L);
	}

	/**
	 * 根据条件查找  市场实现价差
	 */
	@Test
	@Ignore
	public void find() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		List<RealisedSpreadMarket> realisedSpreadMarkets = realisedSpreadMarketService.find(parameters);
		Assert.assertNull(realisedSpreadMarkets);
	}

	/**
	 * 根据主键查找 市场实现价差
	 */
	@Test
	@Ignore
	public void findById() {
		RealisedSpreadMarket realisedSpreadMarket = realisedSpreadMarketService.findById(1L);
		Assert.assertNull(realisedSpreadMarket);
	}

	/**
	 * 查找  市场实现价差
	 */
	@Test
	@Ignore
	public void findContainer() {
		RealisedSpreadMarketQuery query = new RealisedSpreadMarketQuery();
		Container<RealisedSpreadMarket> container = realisedSpreadMarketService.findContainer(query);
		Assert.assertNull(container);
	}

	/**
	 * 查找单个  市场实现价差
	 */
	@Test
	@Ignore
	public void findOne() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", 1L);
		RealisedSpreadMarket realisedSpreadMarket = realisedSpreadMarketService.findOne(parameters);
		Assert.assertNull(realisedSpreadMarket);
	}

	/**
	 * 保存  市场实现价差
	 */
	@Test
	@Ignore
	public void save() {
		RealisedSpreadMarket realisedSpreadMarket = new RealisedSpreadMarket();
		realisedSpreadMarketService.save(realisedSpreadMarket);
	}

	/**
	 * 保存或更新  市场实现价差
	 */
	@Test
	@Ignore
	public void saveOrUpdate() {
		RealisedSpreadMarket realisedSpreadMarket = new RealisedSpreadMarket();
		realisedSpreadMarketService.saveOrUpdate(realisedSpreadMarket);
	}

	/**
	 * 更新  市场实现价差
	 */
	@Test
	@Ignore
	public void update() {
		RealisedSpreadMarket realisedSpreadMarket = new RealisedSpreadMarket();
		realisedSpreadMarket.setId(0L);
		realisedSpreadMarketService.update(realisedSpreadMarket);
	}

	@Test
	@Ignore
	public void otherMethod() {
		// 新的测试内容
	}

}
