package com.hzizs.analysis.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.hzizs.test.spring.TestBase;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.annotations.Ignore;

import com.hzizs.vo.Container;
import com.hzizs.analysis.entity.ImitatePosition;
import com.hzizs.analysis.query.ImitatePositionQuery;

/**
 * 模拟持仓 测试服务类
 * 
 * @author crazy_cabbage
 */
public class ImitatePositionServiceTest extends TestBase {
	private ImitatePositionService imitatePositionService;

	public ImitatePositionServiceTest() {
		imitatePositionService = (ImitatePositionService) ac.getBean("imitatePositionService");
	}

	/**
	 * 统计 模拟持仓
	 */
	@Test
	@Ignore
	public void count() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		long count = imitatePositionService.count(parameters);
		Assert.assertNull(count);
	}

	/**
	 * 删除 模拟持仓
	 */
	@Test
	@Ignore
	public void deleteEntity() {
		ImitatePosition imitatePosition = new ImitatePosition();
		imitatePosition.setId(0L);
		imitatePositionService.delete(imitatePosition);
	}

	/**
	 * 按条件删除 模拟持仓
	 */
	@Test
	@Ignore
	public void deleteMap() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", 1L);
		imitatePositionService.delete(parameters);

	}

	/**
	 * 按主键删除 模拟持仓
	 */
	@Test
	@Ignore
	public void deleteById() {
		imitatePositionService.deleteById(1L);
	}

	/**
	 * 根据条件查找  模拟持仓
	 */
	@Test
	@Ignore
	public void find() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		List<ImitatePosition> imitatePositions = imitatePositionService.find(parameters);
		Assert.assertNull(imitatePositions);
	}

	/**
	 * 根据主键查找 模拟持仓
	 */
	@Test
	@Ignore
	public void findById() {
		ImitatePosition imitatePosition = imitatePositionService.findById(1L);
		Assert.assertNull(imitatePosition);
	}

	/**
	 * 查找  模拟持仓
	 */
	@Test
	@Ignore
	public void findContainer() {
		ImitatePositionQuery query = new ImitatePositionQuery();
		Container<ImitatePosition> container = imitatePositionService.findContainer(query);
		Assert.assertNull(container);
	}

	/**
	 * 查找单个  模拟持仓
	 */
	@Test
	@Ignore
	public void findOne() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", 1L);
		ImitatePosition imitatePosition = imitatePositionService.findOne(parameters);
		Assert.assertNull(imitatePosition);
	}

	/**
	 * 保存  模拟持仓
	 */
	@Test
	@Ignore
	public void save() {
		ImitatePosition imitatePosition = new ImitatePosition();
		imitatePositionService.save(imitatePosition);
	}

	/**
	 * 保存或更新  模拟持仓
	 */
	@Test
	@Ignore
	public void saveOrUpdate() {
		ImitatePosition imitatePosition = new ImitatePosition();
		imitatePositionService.saveOrUpdate(imitatePosition);
	}

	/**
	 * 更新  模拟持仓
	 */
	@Test
	@Ignore
	public void update() {
		ImitatePosition imitatePosition = new ImitatePosition();
		imitatePosition.setId(0L);
		imitatePositionService.update(imitatePosition);
	}

	@Test
	@Ignore
	public void otherMethod() {
		// 新的测试内容
	}

}
