package com.hzizs.analysis.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.hzizs.test.spring.TestBase;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.annotations.Ignore;

import com.hzizs.vo.Container;
import com.hzizs.analysis.entity.Auth;
import com.hzizs.analysis.query.AuthQuery;

/**
 * 权限 测试服务类
 * 
 * @author crazy_cabbage
 */
public class AuthServiceTest extends TestBase {
	private AuthService authService;

	public AuthServiceTest() {
		authService = (AuthService) ac.getBean("authService");
	}

	/**
	 * 统计 权限
	 */
	@Test
	@Ignore
	public void count() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		long count = authService.count(parameters);
		Assert.assertNull(count);
	}

	/**
	 * 删除 权限
	 */
	@Test
	@Ignore
	public void deleteEntity() {
		Auth auth = new Auth();
		auth.setId(0L);
		authService.delete(auth);
	}

	/**
	 * 按条件删除 权限
	 */
	@Test
	@Ignore
	public void deleteMap() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", 1L);
		authService.delete(parameters);

	}

	/**
	 * 按主键删除 权限
	 */
	@Test
	@Ignore
	public void deleteById() {
		authService.deleteById(1L);
	}

	/**
	 * 根据条件查找  权限
	 */
	@Test
	@Ignore
	public void find() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		List<Auth> auths = authService.find(parameters);
		Assert.assertNull(auths);
	}

	/**
	 * 根据主键查找 权限
	 */
	@Test
	@Ignore
	public void findById() {
		Auth auth = authService.findById(1L);
		Assert.assertNull(auth);
	}

	/**
	 * 查找  权限
	 */
	@Test
	@Ignore
	public void findContainer() {
		AuthQuery query = new AuthQuery();
		Container<Auth> container = authService.findContainer(query);
		Assert.assertNull(container);
	}

	/**
	 * 查找单个  权限
	 */
	@Test
	@Ignore
	public void findOne() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", 1L);
		Auth auth = authService.findOne(parameters);
		Assert.assertNull(auth);
	}

	/**
	 * 保存  权限
	 */
	@Test
	@Ignore
	public void save() {
		Auth auth = new Auth();
		authService.save(auth);
	}

	/**
	 * 保存或更新  权限
	 */
	@Test
	@Ignore
	public void saveOrUpdate() {
		Auth auth = new Auth();
		authService.saveOrUpdate(auth);
	}

	/**
	 * 更新  权限
	 */
	@Test
	@Ignore
	public void update() {
		Auth auth = new Auth();
		auth.setId(0L);
		authService.update(auth);
	}

	@Test
	@Ignore
	public void otherMethod() {
		// 新的测试内容
	}

}
