package com.hzizs.analysis.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.hzizs.test.spring.TestBase;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.annotations.Ignore;

import com.hzizs.vo.Container;
import com.hzizs.analysis.entity.SysUserInfo;
import com.hzizs.analysis.query.SysUserInfoQuery;

/**
 * 系统用户信息 测试服务类
 * 
 * @author crazy_cabbage
 */
public class SysUserInfoServiceTest extends TestBase {
	private SysUserInfoService sysUserInfoService;

	public SysUserInfoServiceTest() {
		sysUserInfoService = (SysUserInfoService) ac.getBean("sysUserInfoService");
	}

	/**
	 * 统计 系统用户信息
	 */
	@Test
	@Ignore
	public void count() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		long count = sysUserInfoService.count(parameters);
		Assert.assertNull(count);
	}

	/**
	 * 删除 系统用户信息
	 */
	@Test
	@Ignore
	public void deleteEntity() {
		SysUserInfo sysUserInfo = new SysUserInfo();
		sysUserInfo.setId(0L);
		sysUserInfoService.delete(sysUserInfo);
	}

	/**
	 * 按条件删除 系统用户信息
	 */
	@Test
	@Ignore
	public void deleteMap() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", 1L);
		sysUserInfoService.delete(parameters);

	}

	/**
	 * 按主键删除 系统用户信息
	 */
	@Test
	@Ignore
	public void deleteById() {
		sysUserInfoService.deleteById(1L);
	}

	/**
	 * 根据条件查找  系统用户信息
	 */
	@Test
	@Ignore
	public void find() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		List<SysUserInfo> sysUserInfos = sysUserInfoService.find(parameters);
		Assert.assertNull(sysUserInfos);
	}

	/**
	 * 根据主键查找 系统用户信息
	 */
	@Test
	@Ignore
	public void findById() {
		SysUserInfo sysUserInfo = sysUserInfoService.findById(1L);
		Assert.assertNull(sysUserInfo);
	}

	/**
	 * 查找  系统用户信息
	 */
	@Test
	@Ignore
	public void findContainer() {
		SysUserInfoQuery query = new SysUserInfoQuery();
		Container<SysUserInfo> container = sysUserInfoService.findContainer(query);
		Assert.assertNull(container);
	}

	/**
	 * 查找单个  系统用户信息
	 */
	@Test
	@Ignore
	public void findOne() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", 1L);
		SysUserInfo sysUserInfo = sysUserInfoService.findOne(parameters);
		Assert.assertNull(sysUserInfo);
	}

	/**
	 * 保存  系统用户信息
	 */
	@Test
	@Ignore
	public void save() {
		SysUserInfo sysUserInfo = new SysUserInfo();
		sysUserInfoService.save(sysUserInfo);
	}

	/**
	 * 保存或更新  系统用户信息
	 */
	@Test
	@Ignore
	public void saveOrUpdate() {
		SysUserInfo sysUserInfo = new SysUserInfo();
		sysUserInfoService.saveOrUpdate(sysUserInfo);
	}

	/**
	 * 更新  系统用户信息
	 */
	@Test
	@Ignore
	public void update() {
		SysUserInfo sysUserInfo = new SysUserInfo();
		sysUserInfo.setId(0L);
		sysUserInfoService.update(sysUserInfo);
	}

	@Test
	@Ignore
	public void otherMethod() {
		// 新的测试内容
	}

}
