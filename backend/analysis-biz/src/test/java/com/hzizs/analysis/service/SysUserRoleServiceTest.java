package com.hzizs.analysis.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.hzizs.test.spring.TestBase;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.annotations.Ignore;

import com.hzizs.vo.Container;
import com.hzizs.analysis.entity.SysUserRole;
import com.hzizs.analysis.query.SysUserRoleQuery;

/**
 * 系统用户角色 测试服务类
 * 
 * @author crazy_cabbage
 */
public class SysUserRoleServiceTest extends TestBase {
	private SysUserRoleService sysUserRoleService;

	public SysUserRoleServiceTest() {
		sysUserRoleService = (SysUserRoleService) ac.getBean("sysUserRoleService");
	}

	/**
	 * 统计 系统用户角色
	 */
	@Test
	@Ignore
	public void count() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		long count = sysUserRoleService.count(parameters);
		Assert.assertNull(count);
	}

	/**
	 * 删除 系统用户角色
	 */
	@Test
	@Ignore
	public void deleteEntity() {
		SysUserRole sysUserRole = new SysUserRole();
		sysUserRole.setId(0L);
		sysUserRoleService.delete(sysUserRole);
	}

	/**
	 * 按条件删除 系统用户角色
	 */
	@Test
	@Ignore
	public void deleteMap() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", 1L);
		sysUserRoleService.delete(parameters);

	}

	/**
	 * 按主键删除 系统用户角色
	 */
	@Test
	@Ignore
	public void deleteById() {
		sysUserRoleService.deleteById(1L);
	}

	/**
	 * 根据条件查找  系统用户角色
	 */
	@Test
	@Ignore
	public void find() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		List<SysUserRole> sysUserRoles = sysUserRoleService.find(parameters);
		Assert.assertNull(sysUserRoles);
	}

	/**
	 * 根据主键查找 系统用户角色
	 */
	@Test
	@Ignore
	public void findById() {
		SysUserRole sysUserRole = sysUserRoleService.findById(1L);
		Assert.assertNull(sysUserRole);
	}

	/**
	 * 查找  系统用户角色
	 */
	@Test
	@Ignore
	public void findContainer() {
		SysUserRoleQuery query = new SysUserRoleQuery();
		Container<SysUserRole> container = sysUserRoleService.findContainer(query);
		Assert.assertNull(container);
	}

	/**
	 * 查找单个  系统用户角色
	 */
	@Test
	@Ignore
	public void findOne() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", 1L);
		SysUserRole sysUserRole = sysUserRoleService.findOne(parameters);
		Assert.assertNull(sysUserRole);
	}

	/**
	 * 保存  系统用户角色
	 */
	@Test
	@Ignore
	public void save() {
		SysUserRole sysUserRole = new SysUserRole();
		sysUserRoleService.save(sysUserRole);
	}

	/**
	 * 保存或更新  系统用户角色
	 */
	@Test
	@Ignore
	public void saveOrUpdate() {
		SysUserRole sysUserRole = new SysUserRole();
		sysUserRoleService.saveOrUpdate(sysUserRole);
	}

	/**
	 * 更新  系统用户角色
	 */
	@Test
	@Ignore
	public void update() {
		SysUserRole sysUserRole = new SysUserRole();
		sysUserRole.setId(0L);
		sysUserRoleService.update(sysUserRole);
	}

	@Test
	@Ignore
	public void otherMethod() {
		// 新的测试内容
	}

}
