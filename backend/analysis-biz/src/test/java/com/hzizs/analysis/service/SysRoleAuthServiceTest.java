package com.hzizs.analysis.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.hzizs.test.spring.TestBase;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.annotations.Ignore;

import com.hzizs.vo.Container;
import com.hzizs.analysis.entity.SysRoleAuth;
import com.hzizs.analysis.query.SysRoleAuthQuery;

/**
 * 系统角色权限 测试服务类
 * 
 * @author crazy_cabbage
 */
public class SysRoleAuthServiceTest extends TestBase {
	private SysRoleAuthService sysRoleAuthService;

	public SysRoleAuthServiceTest() {
		sysRoleAuthService = (SysRoleAuthService) ac.getBean("sysRoleAuthService");
	}

	/**
	 * 统计 系统角色权限
	 */
	@Test
	@Ignore
	public void count() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		long count = sysRoleAuthService.count(parameters);
		Assert.assertNull(count);
	}

	/**
	 * 删除 系统角色权限
	 */
	@Test
	@Ignore
	public void deleteEntity() {
		SysRoleAuth sysRoleAuth = new SysRoleAuth();
		sysRoleAuth.setId(0L);
		sysRoleAuthService.delete(sysRoleAuth);
	}

	/**
	 * 按条件删除 系统角色权限
	 */
	@Test
	@Ignore
	public void deleteMap() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", 1L);
		sysRoleAuthService.delete(parameters);

	}

	/**
	 * 按主键删除 系统角色权限
	 */
	@Test
	@Ignore
	public void deleteById() {
		sysRoleAuthService.deleteById(1L);
	}

	/**
	 * 根据条件查找  系统角色权限
	 */
	@Test
	@Ignore
	public void find() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		List<SysRoleAuth> sysRoleAuths = sysRoleAuthService.find(parameters);
		Assert.assertNull(sysRoleAuths);
	}

	/**
	 * 根据主键查找 系统角色权限
	 */
	@Test
	@Ignore
	public void findById() {
		SysRoleAuth sysRoleAuth = sysRoleAuthService.findById(1L);
		Assert.assertNull(sysRoleAuth);
	}

	/**
	 * 查找  系统角色权限
	 */
	@Test
	@Ignore
	public void findContainer() {
		SysRoleAuthQuery query = new SysRoleAuthQuery();
		Container<SysRoleAuth> container = sysRoleAuthService.findContainer(query);
		Assert.assertNull(container);
	}

	/**
	 * 查找单个  系统角色权限
	 */
	@Test
	@Ignore
	public void findOne() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", 1L);
		SysRoleAuth sysRoleAuth = sysRoleAuthService.findOne(parameters);
		Assert.assertNull(sysRoleAuth);
	}

	/**
	 * 保存  系统角色权限
	 */
	@Test
	@Ignore
	public void save() {
		SysRoleAuth sysRoleAuth = new SysRoleAuth();
		sysRoleAuthService.save(sysRoleAuth);
	}

	/**
	 * 保存或更新  系统角色权限
	 */
	@Test
	@Ignore
	public void saveOrUpdate() {
		SysRoleAuth sysRoleAuth = new SysRoleAuth();
		sysRoleAuthService.saveOrUpdate(sysRoleAuth);
	}

	/**
	 * 更新  系统角色权限
	 */
	@Test
	@Ignore
	public void update() {
		SysRoleAuth sysRoleAuth = new SysRoleAuth();
		sysRoleAuth.setId(0L);
		sysRoleAuthService.update(sysRoleAuth);
	}

	@Test
	@Ignore
	public void otherMethod() {
		// 新的测试内容
	}

}
