package com.hzizs.analysis.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.hzizs.test.spring.TestBase;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.annotations.Ignore;

import com.hzizs.vo.Container;
import com.hzizs.analysis.entity.RefSecurity;
import com.hzizs.analysis.query.RefSecurityQuery;

/**
 * 证券参考数据 测试服务类
 * 
 * @author crazy_cabbage
 */
public class RefSecurityServiceTest extends TestBase {
	private RefSecurityService refSecurityService;

	public RefSecurityServiceTest() {
		refSecurityService = (RefSecurityService) ac.getBean("refSecurityService");
	}

	/**
	 * 统计 证券参考数据
	 */
	@Test
	@Ignore
	public void count() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		long count = refSecurityService.count(parameters);
		Assert.assertNull(count);
	}

	/**
	 * 删除 证券参考数据
	 */
	@Test
	@Ignore
	public void deleteEntity() {
		RefSecurity refSecurity = new RefSecurity();
		refSecurity.setId(0L);
		refSecurityService.delete(refSecurity);
	}

	/**
	 * 按条件删除 证券参考数据
	 */
	@Test
	@Ignore
	public void deleteMap() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", 1L);
		refSecurityService.delete(parameters);

	}

	/**
	 * 按主键删除 证券参考数据
	 */
	@Test
	@Ignore
	public void deleteById() {
		refSecurityService.deleteById(1L);
	}

	/**
	 * 根据条件查找  证券参考数据
	 */
	@Test
	@Ignore
	public void find() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		List<RefSecurity> refSecuritys = refSecurityService.find(parameters);
		Assert.assertNull(refSecuritys);
	}

	/**
	 * 根据主键查找 证券参考数据
	 */
	@Test
	@Ignore
	public void findById() {
		RefSecurity refSecurity = refSecurityService.findById(1L);
		Assert.assertNull(refSecurity);
	}

	/**
	 * 查找  证券参考数据
	 */
	@Test
	@Ignore
	public void findContainer() {
		RefSecurityQuery query = new RefSecurityQuery();
		Container<RefSecurity> container = refSecurityService.findContainer(query);
		Assert.assertNull(container);
	}

	/**
	 * 查找单个  证券参考数据
	 */
	@Test
	@Ignore
	public void findOne() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", 1L);
		RefSecurity refSecurity = refSecurityService.findOne(parameters);
		Assert.assertNull(refSecurity);
	}

	/**
	 * 保存  证券参考数据
	 */
	@Test
	@Ignore
	public void save() {
		RefSecurity refSecurity = new RefSecurity();
		refSecurityService.save(refSecurity);
	}

	/**
	 * 保存或更新  证券参考数据
	 */
	@Test
	@Ignore
	public void saveOrUpdate() {
		RefSecurity refSecurity = new RefSecurity();
		refSecurityService.saveOrUpdate(refSecurity);
	}

	/**
	 * 更新  证券参考数据
	 */
	@Test
	@Ignore
	public void update() {
		RefSecurity refSecurity = new RefSecurity();
		refSecurity.setId(0L);
		refSecurityService.update(refSecurity);
	}

	@Test
	@Ignore
	public void otherMethod() {
		// 新的测试内容
	}

}
