package com.hzizs.analysis.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.hzizs.test.spring.TestBase;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.annotations.Ignore;

import com.hzizs.vo.Container;
import com.hzizs.analysis.entity.UserLoginLog;
import com.hzizs.analysis.query.UserLoginLogQuery;

/**
 * 用户登录日志 测试服务类
 * 
 * @author crazy_cabbage
 */
public class UserLoginLogServiceTest extends TestBase {
	private UserLoginLogService userLoginLogService;

	public UserLoginLogServiceTest() {
		userLoginLogService = (UserLoginLogService) ac.getBean("userLoginLogService");
	}

	/**
	 * 统计 用户登录日志
	 */
	@Test
	@Ignore
	public void count() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		long count = userLoginLogService.count(parameters);
		Assert.assertNull(count);
	}

	/**
	 * 删除 用户登录日志
	 */
	@Test
	@Ignore
	public void deleteEntity() {
		UserLoginLog userLoginLog = new UserLoginLog();
		userLoginLog.setId(0L);
		userLoginLogService.delete(userLoginLog);
	}

	/**
	 * 按条件删除 用户登录日志
	 */
	@Test
	@Ignore
	public void deleteMap() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", 1L);
		userLoginLogService.delete(parameters);

	}

	/**
	 * 按主键删除 用户登录日志
	 */
	@Test
	@Ignore
	public void deleteById() {
		userLoginLogService.deleteById(1L);
	}

	/**
	 * 根据条件查找  用户登录日志
	 */
	@Test
	@Ignore
	public void find() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		List<UserLoginLog> userLoginLogs = userLoginLogService.find(parameters);
		Assert.assertNull(userLoginLogs);
	}

	/**
	 * 根据主键查找 用户登录日志
	 */
	@Test
	@Ignore
	public void findById() {
		UserLoginLog userLoginLog = userLoginLogService.findById(1L);
		Assert.assertNull(userLoginLog);
	}

	/**
	 * 查找  用户登录日志
	 */
	@Test
	@Ignore
	public void findContainer() {
		UserLoginLogQuery query = new UserLoginLogQuery();
		Container<UserLoginLog> container = userLoginLogService.findContainer(query);
		Assert.assertNull(container);
	}

	/**
	 * 查找单个  用户登录日志
	 */
	@Test
	@Ignore
	public void findOne() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", 1L);
		UserLoginLog userLoginLog = userLoginLogService.findOne(parameters);
		Assert.assertNull(userLoginLog);
	}

	/**
	 * 保存  用户登录日志
	 */
	@Test
	@Ignore
	public void save() {
		UserLoginLog userLoginLog = new UserLoginLog();
		userLoginLogService.save(userLoginLog);
	}

	/**
	 * 保存或更新  用户登录日志
	 */
	@Test
	@Ignore
	public void saveOrUpdate() {
		UserLoginLog userLoginLog = new UserLoginLog();
		userLoginLogService.saveOrUpdate(userLoginLog);
	}

	/**
	 * 更新  用户登录日志
	 */
	@Test
	@Ignore
	public void update() {
		UserLoginLog userLoginLog = new UserLoginLog();
		userLoginLog.setId(0L);
		userLoginLogService.update(userLoginLog);
	}

	@Test
	@Ignore
	public void otherMethod() {
		// 新的测试内容
	}

}
