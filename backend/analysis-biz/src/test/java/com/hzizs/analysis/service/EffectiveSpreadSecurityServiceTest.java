package com.hzizs.analysis.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.hzizs.test.spring.TestBase;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.annotations.Ignore;

import com.hzizs.vo.Container;
import com.hzizs.analysis.entity.EffectiveSpreadSecurity;
import com.hzizs.analysis.query.EffectiveSpreadSecurityQuery;

/**
 * 证券有效价差 测试服务类
 * 
 * @author crazy_cabbage
 */
public class EffectiveSpreadSecurityServiceTest extends TestBase {
	private EffectiveSpreadSecurityService effectiveSpreadSecurityService;

	public EffectiveSpreadSecurityServiceTest() {
		effectiveSpreadSecurityService = (EffectiveSpreadSecurityService) ac.getBean("effectiveSpreadSecurityService");
	}

	/**
	 * 统计 证券有效价差
	 */
	@Test
	@Ignore
	public void count() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		long count = effectiveSpreadSecurityService.count(parameters);
		Assert.assertNull(count);
	}

	/**
	 * 删除 证券有效价差
	 */
	@Test
	@Ignore
	public void deleteEntity() {
		EffectiveSpreadSecurity effectiveSpreadSecurity = new EffectiveSpreadSecurity();
		effectiveSpreadSecurity.setId(0L);
		effectiveSpreadSecurityService.delete(effectiveSpreadSecurity);
	}

	/**
	 * 按条件删除 证券有效价差
	 */
	@Test
	@Ignore
	public void deleteMap() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", 1L);
		effectiveSpreadSecurityService.delete(parameters);

	}

	/**
	 * 按主键删除 证券有效价差
	 */
	@Test
	@Ignore
	public void deleteById() {
		effectiveSpreadSecurityService.deleteById(1L);
	}

	/**
	 * 根据条件查找  证券有效价差
	 */
	@Test
	@Ignore
	public void find() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		List<EffectiveSpreadSecurity> effectiveSpreadSecuritys = effectiveSpreadSecurityService.find(parameters);
		Assert.assertNull(effectiveSpreadSecuritys);
	}

	/**
	 * 根据主键查找 证券有效价差
	 */
	@Test
	@Ignore
	public void findById() {
		EffectiveSpreadSecurity effectiveSpreadSecurity = effectiveSpreadSecurityService.findById(1L);
		Assert.assertNull(effectiveSpreadSecurity);
	}

	/**
	 * 查找  证券有效价差
	 */
	@Test
	@Ignore
	public void findContainer() {
		EffectiveSpreadSecurityQuery query = new EffectiveSpreadSecurityQuery();
		Container<EffectiveSpreadSecurity> container = effectiveSpreadSecurityService.findContainer(query);
		Assert.assertNull(container);
	}

	/**
	 * 查找单个  证券有效价差
	 */
	@Test
	@Ignore
	public void findOne() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", 1L);
		EffectiveSpreadSecurity effectiveSpreadSecurity = effectiveSpreadSecurityService.findOne(parameters);
		Assert.assertNull(effectiveSpreadSecurity);
	}

	/**
	 * 保存  证券有效价差
	 */
	@Test
	@Ignore
	public void save() {
		EffectiveSpreadSecurity effectiveSpreadSecurity = new EffectiveSpreadSecurity();
		effectiveSpreadSecurityService.save(effectiveSpreadSecurity);
	}

	/**
	 * 保存或更新  证券有效价差
	 */
	@Test
	@Ignore
	public void saveOrUpdate() {
		EffectiveSpreadSecurity effectiveSpreadSecurity = new EffectiveSpreadSecurity();
		effectiveSpreadSecurityService.saveOrUpdate(effectiveSpreadSecurity);
	}

	/**
	 * 更新  证券有效价差
	 */
	@Test
	@Ignore
	public void update() {
		EffectiveSpreadSecurity effectiveSpreadSecurity = new EffectiveSpreadSecurity();
		effectiveSpreadSecurity.setId(0L);
		effectiveSpreadSecurityService.update(effectiveSpreadSecurity);
	}

	@Test
	@Ignore
	public void otherMethod() {
		// 新的测试内容
	}

}
