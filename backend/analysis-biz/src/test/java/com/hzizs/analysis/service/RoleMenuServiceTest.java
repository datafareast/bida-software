package com.hzizs.analysis.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.hzizs.test.spring.TestBase;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.annotations.Ignore;

import com.hzizs.vo.Container;
import com.hzizs.analysis.entity.RoleMenu;
import com.hzizs.analysis.query.RoleMenuQuery;

/**
 * 角色菜单 测试服务类
 * 
 * @author crazy_cabbage
 */
public class RoleMenuServiceTest extends TestBase {
	private RoleMenuService roleMenuService;

	public RoleMenuServiceTest() {
		roleMenuService = (RoleMenuService) ac.getBean("roleMenuService");
	}

	/**
	 * 统计 角色菜单
	 */
	@Test
	@Ignore
	public void count() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		long count = roleMenuService.count(parameters);
		Assert.assertNull(count);
	}

	/**
	 * 删除 角色菜单
	 */
	@Test
	@Ignore
	public void deleteEntity() {
		RoleMenu roleMenu = new RoleMenu();
		roleMenu.setId(0L);
		roleMenuService.delete(roleMenu);
	}

	/**
	 * 按条件删除 角色菜单
	 */
	@Test
	@Ignore
	public void deleteMap() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", 1L);
		roleMenuService.delete(parameters);

	}

	/**
	 * 按主键删除 角色菜单
	 */
	@Test
	@Ignore
	public void deleteById() {
		roleMenuService.deleteById(1L);
	}

	/**
	 * 根据条件查找  角色菜单
	 */
	@Test
	@Ignore
	public void find() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		List<RoleMenu> roleMenus = roleMenuService.find(parameters);
		Assert.assertNull(roleMenus);
	}

	/**
	 * 根据主键查找 角色菜单
	 */
	@Test
	@Ignore
	public void findById() {
		RoleMenu roleMenu = roleMenuService.findById(1L);
		Assert.assertNull(roleMenu);
	}

	/**
	 * 查找  角色菜单
	 */
	@Test
	@Ignore
	public void findContainer() {
		RoleMenuQuery query = new RoleMenuQuery();
		Container<RoleMenu> container = roleMenuService.findContainer(query);
		Assert.assertNull(container);
	}

	/**
	 * 查找单个  角色菜单
	 */
	@Test
	@Ignore
	public void findOne() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", 1L);
		RoleMenu roleMenu = roleMenuService.findOne(parameters);
		Assert.assertNull(roleMenu);
	}

	/**
	 * 保存  角色菜单
	 */
	@Test
	@Ignore
	public void save() {
		RoleMenu roleMenu = new RoleMenu();
		roleMenuService.save(roleMenu);
	}

	/**
	 * 保存或更新  角色菜单
	 */
	@Test
	@Ignore
	public void saveOrUpdate() {
		RoleMenu roleMenu = new RoleMenu();
		roleMenuService.saveOrUpdate(roleMenu);
	}

	/**
	 * 更新  角色菜单
	 */
	@Test
	@Ignore
	public void update() {
		RoleMenu roleMenu = new RoleMenu();
		roleMenu.setId(0L);
		roleMenuService.update(roleMenu);
	}

	@Test
	@Ignore
	public void otherMethod() {
		// 新的测试内容
	}

}
