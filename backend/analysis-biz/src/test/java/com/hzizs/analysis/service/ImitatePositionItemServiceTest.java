package com.hzizs.analysis.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.hzizs.test.spring.TestBase;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.annotations.Ignore;

import com.hzizs.vo.Container;
import com.hzizs.analysis.entity.ImitatePositionItem;
import com.hzizs.analysis.query.ImitatePositionItemQuery;

/**
 * 模拟持仓条目 测试服务类
 * 
 * @author crazy_cabbage
 */
public class ImitatePositionItemServiceTest extends TestBase {
	private ImitatePositionItemService imitatePositionItemService;

	public ImitatePositionItemServiceTest() {
		imitatePositionItemService = (ImitatePositionItemService) ac.getBean("imitatePositionItemService");
	}

	/**
	 * 统计 模拟持仓条目
	 */
	@Test
	@Ignore
	public void count() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		long count = imitatePositionItemService.count(parameters);
		Assert.assertNull(count);
	}

	/**
	 * 删除 模拟持仓条目
	 */
	@Test
	@Ignore
	public void deleteEntity() {
		ImitatePositionItem imitatePositionItem = new ImitatePositionItem();
		imitatePositionItem.setId(0L);
		imitatePositionItemService.delete(imitatePositionItem);
	}

	/**
	 * 按条件删除 模拟持仓条目
	 */
	@Test
	@Ignore
	public void deleteMap() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", 1L);
		imitatePositionItemService.delete(parameters);

	}

	/**
	 * 按主键删除 模拟持仓条目
	 */
	@Test
	@Ignore
	public void deleteById() {
		imitatePositionItemService.deleteById(1L);
	}

	/**
	 * 根据条件查找  模拟持仓条目
	 */
	@Test
	@Ignore
	public void find() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		List<ImitatePositionItem> imitatePositionItems = imitatePositionItemService.find(parameters);
		Assert.assertNull(imitatePositionItems);
	}

	/**
	 * 根据主键查找 模拟持仓条目
	 */
	@Test
	@Ignore
	public void findById() {
		ImitatePositionItem imitatePositionItem = imitatePositionItemService.findById(1L);
		Assert.assertNull(imitatePositionItem);
	}

	/**
	 * 查找  模拟持仓条目
	 */
	@Test
	@Ignore
	public void findContainer() {
		ImitatePositionItemQuery query = new ImitatePositionItemQuery();
		Container<ImitatePositionItem> container = imitatePositionItemService.findContainer(query);
		Assert.assertNull(container);
	}

	/**
	 * 查找单个  模拟持仓条目
	 */
	@Test
	@Ignore
	public void findOne() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", 1L);
		ImitatePositionItem imitatePositionItem = imitatePositionItemService.findOne(parameters);
		Assert.assertNull(imitatePositionItem);
	}

	/**
	 * 保存  模拟持仓条目
	 */
	@Test
	@Ignore
	public void save() {
		ImitatePositionItem imitatePositionItem = new ImitatePositionItem();
		imitatePositionItemService.save(imitatePositionItem);
	}

	/**
	 * 保存或更新  模拟持仓条目
	 */
	@Test
	@Ignore
	public void saveOrUpdate() {
		ImitatePositionItem imitatePositionItem = new ImitatePositionItem();
		imitatePositionItemService.saveOrUpdate(imitatePositionItem);
	}

	/**
	 * 更新  模拟持仓条目
	 */
	@Test
	@Ignore
	public void update() {
		ImitatePositionItem imitatePositionItem = new ImitatePositionItem();
		imitatePositionItem.setId(0L);
		imitatePositionItemService.update(imitatePositionItem);
	}

	@Test
	@Ignore
	public void otherMethod() {
		// 新的测试内容
	}

}
