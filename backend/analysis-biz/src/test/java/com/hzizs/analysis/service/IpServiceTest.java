package com.hzizs.analysis.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.hzizs.test.spring.TestBase;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.annotations.Ignore;

import com.hzizs.vo.Container;
import com.hzizs.analysis.entity.Ip;
import com.hzizs.analysis.query.IpQuery;

/**
 * IP 测试服务类
 * 
 * @author crazy_cabbage
 */
public class IpServiceTest extends TestBase {
	private IpService ipService;

	public IpServiceTest() {
		ipService = (IpService) ac.getBean("ipService");
	}

	/**
	 * 统计 IP
	 */
	@Test
	@Ignore
	public void count() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		long count = ipService.count(parameters);
		Assert.assertNull(count);
	}

	/**
	 * 删除 IP
	 */
	@Test
	@Ignore
	public void deleteEntity() {
		Ip ip = new Ip();
		ip.setId(0L);
		ipService.delete(ip);
	}

	/**
	 * 按条件删除 IP
	 */
	@Test
	@Ignore
	public void deleteMap() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", 1L);
		ipService.delete(parameters);

	}

	/**
	 * 按主键删除 IP
	 */
	@Test
	@Ignore
	public void deleteById() {
		ipService.deleteById(1L);
	}

	/**
	 * 根据条件查找  IP
	 */
	@Test
	@Ignore
	public void find() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		List<Ip> ips = ipService.find(parameters);
		Assert.assertNull(ips);
	}

	/**
	 * 根据主键查找 IP
	 */
	@Test
	@Ignore
	public void findById() {
		Ip ip = ipService.findById(1L);
		Assert.assertNull(ip);
	}

	/**
	 * 查找  IP
	 */
	@Test
	@Ignore
	public void findContainer() {
		IpQuery query = new IpQuery();
		Container<Ip> container = ipService.findContainer(query);
		Assert.assertNull(container);
	}

	/**
	 * 查找单个  IP
	 */
	@Test
	@Ignore
	public void findOne() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", 1L);
		Ip ip = ipService.findOne(parameters);
		Assert.assertNull(ip);
	}

	/**
	 * 保存  IP
	 */
	@Test
	@Ignore
	public void save() {
		Ip ip = new Ip();
		ipService.save(ip);
	}

	/**
	 * 保存或更新  IP
	 */
	@Test
	@Ignore
	public void saveOrUpdate() {
		Ip ip = new Ip();
		ipService.saveOrUpdate(ip);
	}

	/**
	 * 更新  IP
	 */
	@Test
	@Ignore
	public void update() {
		Ip ip = new Ip();
		ip.setId(0L);
		ipService.update(ip);
	}

	@Test
	@Ignore
	public void otherMethod() {
		// 新的测试内容
	}

}
