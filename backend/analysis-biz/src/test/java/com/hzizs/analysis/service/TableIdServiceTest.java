package com.hzizs.analysis.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.hzizs.test.spring.TestBase;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.annotations.Ignore;

import com.hzizs.vo.Container;
import com.hzizs.analysis.entity.TableId;
import com.hzizs.analysis.query.TableIdQuery;

/**
 * 表主键 测试服务类
 * 
 * @author crazy_cabbage
 */
public class TableIdServiceTest extends TestBase {
	private TableIdService tableIdService;

	public TableIdServiceTest() {
		tableIdService = (TableIdService) ac.getBean("tableIdService");
	}

	/**
	 * 统计 表主键
	 */
	@Test
	@Ignore
	public void count() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		long count = tableIdService.count(parameters);
		Assert.assertNull(count);
	}

	/**
	 * 删除 表主键
	 */
	@Test
	@Ignore
	public void deleteEntity() {
		TableId tableId = new TableId();
		tableId.setId(0L);
		tableIdService.delete(tableId);
	}

	/**
	 * 按条件删除 表主键
	 */
	@Test
	@Ignore
	public void deleteMap() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", 1L);
		tableIdService.delete(parameters);

	}

	/**
	 * 按主键删除 表主键
	 */
	@Test
	@Ignore
	public void deleteById() {
		tableIdService.deleteById(1L);
	}

	/**
	 * 根据条件查找  表主键
	 */
	@Test
	@Ignore
	public void find() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		List<TableId> tableIds = tableIdService.find(parameters);
		Assert.assertNull(tableIds);
	}

	/**
	 * 根据主键查找 表主键
	 */
	@Test
	@Ignore
	public void findById() {
		TableId tableId = tableIdService.findById(1L);
		Assert.assertNull(tableId);
	}

	/**
	 * 查找  表主键
	 */
	@Test
	@Ignore
	public void findContainer() {
		TableIdQuery query = new TableIdQuery();
		Container<TableId> container = tableIdService.findContainer(query);
		Assert.assertNull(container);
	}

	/**
	 * 查找单个  表主键
	 */
	@Test
	@Ignore
	public void findOne() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", 1L);
		TableId tableId = tableIdService.findOne(parameters);
		Assert.assertNull(tableId);
	}

	/**
	 * 保存  表主键
	 */
	@Test
	@Ignore
	public void save() {
		TableId tableId = new TableId();
		tableIdService.save(tableId);
	}

	/**
	 * 保存或更新  表主键
	 */
	@Test
	@Ignore
	public void saveOrUpdate() {
		TableId tableId = new TableId();
		tableIdService.saveOrUpdate(tableId);
	}

	/**
	 * 更新  表主键
	 */
	@Test
	@Ignore
	public void update() {
		TableId tableId = new TableId();
		tableId.setId(0L);
		tableIdService.update(tableId);
	}

	@Test
	@Ignore
	public void otherMethod() {
		// 新的测试内容
	}

}
