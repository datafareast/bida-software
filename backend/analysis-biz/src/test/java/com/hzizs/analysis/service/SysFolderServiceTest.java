package com.hzizs.analysis.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.hzizs.test.spring.TestBase;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.annotations.Ignore;

import com.hzizs.vo.Container;
import com.hzizs.analysis.entity.SysFolder;
import com.hzizs.analysis.query.SysFolderQuery;

/**
 * 系统文件夹 测试服务类
 * 
 * @author crazy_cabbage
 */
public class SysFolderServiceTest extends TestBase {
	private SysFolderService sysFolderService;

	public SysFolderServiceTest() {
		sysFolderService = (SysFolderService) ac.getBean("sysFolderService");
	}

	/**
	 * 统计 系统文件夹
	 */
	@Test
	@Ignore
	public void count() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		long count = sysFolderService.count(parameters);
		Assert.assertNull(count);
	}

	/**
	 * 删除 系统文件夹
	 */
	@Test
	@Ignore
	public void deleteEntity() {
		SysFolder sysFolder = new SysFolder();
		sysFolder.setId(0L);
		sysFolderService.delete(sysFolder);
	}

	/**
	 * 按条件删除 系统文件夹
	 */
	@Test
	@Ignore
	public void deleteMap() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", 1L);
		sysFolderService.delete(parameters);

	}

	/**
	 * 按主键删除 系统文件夹
	 */
	@Test
	@Ignore
	public void deleteById() {
		sysFolderService.deleteById(1L);
	}

	/**
	 * 根据条件查找  系统文件夹
	 */
	@Test
	@Ignore
	public void find() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		List<SysFolder> sysFolders = sysFolderService.find(parameters);
		Assert.assertNull(sysFolders);
	}

	/**
	 * 根据主键查找 系统文件夹
	 */
	@Test
	@Ignore
	public void findById() {
		SysFolder sysFolder = sysFolderService.findById(1L);
		Assert.assertNull(sysFolder);
	}

	/**
	 * 查找  系统文件夹
	 */
	@Test
	@Ignore
	public void findContainer() {
		SysFolderQuery query = new SysFolderQuery();
		Container<SysFolder> container = sysFolderService.findContainer(query);
		Assert.assertNull(container);
	}

	/**
	 * 查找单个  系统文件夹
	 */
	@Test
	@Ignore
	public void findOne() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", 1L);
		SysFolder sysFolder = sysFolderService.findOne(parameters);
		Assert.assertNull(sysFolder);
	}

	/**
	 * 保存  系统文件夹
	 */
	@Test
	@Ignore
	public void save() {
		SysFolder sysFolder = new SysFolder();
		sysFolderService.save(sysFolder);
	}

	/**
	 * 保存或更新  系统文件夹
	 */
	@Test
	@Ignore
	public void saveOrUpdate() {
		SysFolder sysFolder = new SysFolder();
		sysFolderService.saveOrUpdate(sysFolder);
	}

	/**
	 * 更新  系统文件夹
	 */
	@Test
	@Ignore
	public void update() {
		SysFolder sysFolder = new SysFolder();
		sysFolder.setId(0L);
		sysFolderService.update(sysFolder);
	}

	@Test
	@Ignore
	public void otherMethod() {
		// 新的测试内容
	}

}
