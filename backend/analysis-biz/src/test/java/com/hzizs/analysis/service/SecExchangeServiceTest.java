package com.hzizs.analysis.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.hzizs.test.spring.TestBase;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.annotations.Ignore;

import com.hzizs.vo.Container;
import com.hzizs.analysis.entity.SecExchange;
import com.hzizs.analysis.query.SecExchangeQuery;

/**
 * 交易所 测试服务类
 * 
 * @author crazy_cabbage
 */
public class SecExchangeServiceTest extends TestBase {
	private SecExchangeService secExchangeService;

	public SecExchangeServiceTest() {
		secExchangeService = (SecExchangeService) ac.getBean("secExchangeService");
	}

	/**
	 * 统计 交易所
	 */
	@Test
	@Ignore
	public void count() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		long count = secExchangeService.count(parameters);
		Assert.assertNull(count);
	}

	/**
	 * 删除 交易所
	 */
	@Test
	@Ignore
	public void deleteEntity() {
		SecExchange secExchange = new SecExchange();
		secExchange.setId(0L);
		secExchangeService.delete(secExchange);
	}

	/**
	 * 按条件删除 交易所
	 */
	@Test
	@Ignore
	public void deleteMap() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", 1L);
		secExchangeService.delete(parameters);

	}

	/**
	 * 按主键删除 交易所
	 */
	@Test
	@Ignore
	public void deleteById() {
		secExchangeService.deleteById(1L);
	}

	/**
	 * 根据条件查找  交易所
	 */
	@Test
	@Ignore
	public void find() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		List<SecExchange> secExchanges = secExchangeService.find(parameters);
		Assert.assertNull(secExchanges);
	}

	/**
	 * 根据主键查找 交易所
	 */
	@Test
	@Ignore
	public void findById() {
		SecExchange secExchange = secExchangeService.findById(1L);
		Assert.assertNull(secExchange);
	}

	/**
	 * 查找  交易所
	 */
	@Test
	@Ignore
	public void findContainer() {
		SecExchangeQuery query = new SecExchangeQuery();
		Container<SecExchange> container = secExchangeService.findContainer(query);
		Assert.assertNull(container);
	}

	/**
	 * 查找单个  交易所
	 */
	@Test
	@Ignore
	public void findOne() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", 1L);
		SecExchange secExchange = secExchangeService.findOne(parameters);
		Assert.assertNull(secExchange);
	}

	/**
	 * 保存  交易所
	 */
	@Test
	@Ignore
	public void save() {
		SecExchange secExchange = new SecExchange();
		secExchangeService.save(secExchange);
	}

	/**
	 * 保存或更新  交易所
	 */
	@Test
	@Ignore
	public void saveOrUpdate() {
		SecExchange secExchange = new SecExchange();
		secExchangeService.saveOrUpdate(secExchange);
	}

	/**
	 * 更新  交易所
	 */
	@Test
	@Ignore
	public void update() {
		SecExchange secExchange = new SecExchange();
		secExchange.setId(0L);
		secExchangeService.update(secExchange);
	}

	@Test
	@Ignore
	public void otherMethod() {
		// 新的测试内容
	}

}
