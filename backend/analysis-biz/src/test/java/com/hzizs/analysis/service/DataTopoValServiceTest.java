package com.hzizs.analysis.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.hzizs.test.spring.TestBase;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.annotations.Ignore;

import com.hzizs.vo.Container;
import com.hzizs.analysis.entity.DataTopoVal;
import com.hzizs.analysis.query.DataTopoValQuery;

/**
 * 数据拓普值 测试服务类
 * 
 * @author crazy_cabbage
 */
public class DataTopoValServiceTest extends TestBase {
	private DataTopoValService dataTopoValService;

	public DataTopoValServiceTest() {
		dataTopoValService = (DataTopoValService) ac.getBean("dataTopoValService");
	}

	/**
	 * 统计 数据拓普值
	 */
	@Test
	@Ignore
	public void count() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		long count = dataTopoValService.count(parameters);
		Assert.assertNull(count);
	}

	/**
	 * 删除 数据拓普值
	 */
	@Test
	@Ignore
	public void deleteEntity() {
		DataTopoVal dataTopoVal = new DataTopoVal();
		dataTopoVal.setId(0L);
		dataTopoValService.delete(dataTopoVal);
	}

	/**
	 * 按条件删除 数据拓普值
	 */
	@Test
	@Ignore
	public void deleteMap() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", 1L);
		dataTopoValService.delete(parameters);

	}

	/**
	 * 按主键删除 数据拓普值
	 */
	@Test
	@Ignore
	public void deleteById() {
		dataTopoValService.deleteById(1L);
	}

	/**
	 * 根据条件查找  数据拓普值
	 */
	@Test
	@Ignore
	public void find() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		List<DataTopoVal> dataTopoVals = dataTopoValService.find(parameters);
		Assert.assertNull(dataTopoVals);
	}

	/**
	 * 根据主键查找 数据拓普值
	 */
	@Test
	@Ignore
	public void findById() {
		DataTopoVal dataTopoVal = dataTopoValService.findById(1L);
		Assert.assertNull(dataTopoVal);
	}

	/**
	 * 查找  数据拓普值
	 */
	@Test
	@Ignore
	public void findContainer() {
		DataTopoValQuery query = new DataTopoValQuery();
		Container<DataTopoVal> container = dataTopoValService.findContainer(query);
		Assert.assertNull(container);
	}

	/**
	 * 查找单个  数据拓普值
	 */
	@Test
	@Ignore
	public void findOne() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", 1L);
		DataTopoVal dataTopoVal = dataTopoValService.findOne(parameters);
		Assert.assertNull(dataTopoVal);
	}

	/**
	 * 保存  数据拓普值
	 */
	@Test
	@Ignore
	public void save() {
		DataTopoVal dataTopoVal = new DataTopoVal();
		dataTopoValService.save(dataTopoVal);
	}

	/**
	 * 保存或更新  数据拓普值
	 */
	@Test
	@Ignore
	public void saveOrUpdate() {
		DataTopoVal dataTopoVal = new DataTopoVal();
		dataTopoValService.saveOrUpdate(dataTopoVal);
	}

	/**
	 * 更新  数据拓普值
	 */
	@Test
	@Ignore
	public void update() {
		DataTopoVal dataTopoVal = new DataTopoVal();
		dataTopoVal.setId(0L);
		dataTopoValService.update(dataTopoVal);
	}

	@Test
	@Ignore
	public void otherMethod() {
		// 新的测试内容
	}

}
