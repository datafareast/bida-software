package com.hzizs.analysis.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.hzizs.test.spring.TestBase;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.annotations.Ignore;

import com.hzizs.vo.Container;
import com.hzizs.analysis.entity.Upgrade;
import com.hzizs.analysis.query.UpgradeQuery;

/**
 * 升级信息 测试服务类
 * 
 * @author crazy_cabbage
 */
public class UpgradeServiceTest extends TestBase {
	private UpgradeService upgradeService;

	public UpgradeServiceTest() {
		upgradeService = (UpgradeService) ac.getBean("upgradeService");
	}

	/**
	 * 统计 升级信息
	 */
	@Test
	@Ignore
	public void count() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		long count = upgradeService.count(parameters);
		Assert.assertNull(count);
	}

	/**
	 * 删除 升级信息
	 */
	@Test
	@Ignore
	public void deleteEntity() {
		Upgrade upgrade = new Upgrade();
		upgrade.setId(0L);
		upgradeService.delete(upgrade);
	}

	/**
	 * 按条件删除 升级信息
	 */
	@Test
	@Ignore
	public void deleteMap() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", 1L);
		upgradeService.delete(parameters);

	}

	/**
	 * 按主键删除 升级信息
	 */
	@Test
	@Ignore
	public void deleteById() {
		upgradeService.deleteById(1L);
	}

	/**
	 * 根据条件查找  升级信息
	 */
	@Test
	@Ignore
	public void find() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		List<Upgrade> upgrades = upgradeService.find(parameters);
		Assert.assertNull(upgrades);
	}

	/**
	 * 根据主键查找 升级信息
	 */
	@Test
	@Ignore
	public void findById() {
		Upgrade upgrade = upgradeService.findById(1L);
		Assert.assertNull(upgrade);
	}

	/**
	 * 查找  升级信息
	 */
	@Test
	@Ignore
	public void findContainer() {
		UpgradeQuery query = new UpgradeQuery();
		Container<Upgrade> container = upgradeService.findContainer(query);
		Assert.assertNull(container);
	}

	/**
	 * 查找单个  升级信息
	 */
	@Test
	@Ignore
	public void findOne() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", 1L);
		Upgrade upgrade = upgradeService.findOne(parameters);
		Assert.assertNull(upgrade);
	}

	/**
	 * 保存  升级信息
	 */
	@Test
	@Ignore
	public void save() {
		Upgrade upgrade = new Upgrade();
		upgradeService.save(upgrade);
	}

	/**
	 * 保存或更新  升级信息
	 */
	@Test
	@Ignore
	public void saveOrUpdate() {
		Upgrade upgrade = new Upgrade();
		upgradeService.saveOrUpdate(upgrade);
	}

	/**
	 * 更新  升级信息
	 */
	@Test
	@Ignore
	public void update() {
		Upgrade upgrade = new Upgrade();
		upgrade.setId(0L);
		upgradeService.update(upgrade);
	}

	@Test
	@Ignore
	public void otherMethod() {
		// 新的测试内容
	}

}
