package com.hzizs.analysis.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.hzizs.test.spring.TestBase;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.annotations.Ignore;

import com.hzizs.vo.Container;
import com.hzizs.analysis.entity.SecAnalysis;
import com.hzizs.analysis.query.SecAnalysisQuery;

/**
 * 业绩分析 测试服务类
 * 
 * @author crazy_cabbage
 */
public class SecAnalysisServiceTest extends TestBase {
	private SecAnalysisService secAnalysisService;

	public SecAnalysisServiceTest() {
		secAnalysisService = (SecAnalysisService) ac.getBean("secAnalysisService");
	}

	/**
	 * 统计 业绩分析
	 */
	@Test
	@Ignore
	public void count() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		long count = secAnalysisService.count(parameters);
		Assert.assertNull(count);
	}

	/**
	 * 删除 业绩分析
	 */
	@Test
	@Ignore
	public void deleteEntity() {
		SecAnalysis secAnalysis = new SecAnalysis();
		secAnalysis.setId(0L);
		secAnalysisService.delete(secAnalysis);
	}

	/**
	 * 按条件删除 业绩分析
	 */
	@Test
	@Ignore
	public void deleteMap() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", 1L);
		secAnalysisService.delete(parameters);

	}

	/**
	 * 按主键删除 业绩分析
	 */
	@Test
	@Ignore
	public void deleteById() {
		secAnalysisService.deleteById(1L);
	}

	/**
	 * 根据条件查找  业绩分析
	 */
	@Test
	@Ignore
	public void find() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		List<SecAnalysis> secAnalysiss = secAnalysisService.find(parameters);
		Assert.assertNull(secAnalysiss);
	}

	/**
	 * 根据主键查找 业绩分析
	 */
	@Test
	@Ignore
	public void findById() {
		SecAnalysis secAnalysis = secAnalysisService.findById(1L);
		Assert.assertNull(secAnalysis);
	}

	/**
	 * 查找  业绩分析
	 */
	@Test
	@Ignore
	public void findContainer() {
		SecAnalysisQuery query = new SecAnalysisQuery();
		Container<SecAnalysis> container = secAnalysisService.findContainer(query);
		Assert.assertNull(container);
	}

	/**
	 * 查找单个  业绩分析
	 */
	@Test
	@Ignore
	public void findOne() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", 1L);
		SecAnalysis secAnalysis = secAnalysisService.findOne(parameters);
		Assert.assertNull(secAnalysis);
	}

	/**
	 * 保存  业绩分析
	 */
	@Test
	@Ignore
	public void save() {
		SecAnalysis secAnalysis = new SecAnalysis();
		secAnalysisService.save(secAnalysis);
	}

	/**
	 * 保存或更新  业绩分析
	 */
	@Test
	@Ignore
	public void saveOrUpdate() {
		SecAnalysis secAnalysis = new SecAnalysis();
		secAnalysisService.saveOrUpdate(secAnalysis);
	}

	/**
	 * 更新  业绩分析
	 */
	@Test
	@Ignore
	public void update() {
		SecAnalysis secAnalysis = new SecAnalysis();
		secAnalysis.setId(0L);
		secAnalysisService.update(secAnalysis);
	}

	@Test
	@Ignore
	public void otherMethod() {
		// 新的测试内容
	}

}
