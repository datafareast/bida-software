package com.hzizs.analysis.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.hzizs.test.spring.TestBase;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.annotations.Ignore;

import com.hzizs.vo.Container;
import com.hzizs.analysis.entity.EffectiveSpreadMarket;
import com.hzizs.analysis.query.EffectiveSpreadMarketQuery;

/**
 * 市场有效价差 测试服务类
 * 
 * @author crazy_cabbage
 */
public class EffectiveSpreadMarketServiceTest extends TestBase {
	private EffectiveSpreadMarketService effectiveSpreadMarketService;

	public EffectiveSpreadMarketServiceTest() {
		effectiveSpreadMarketService = (EffectiveSpreadMarketService) ac.getBean("effectiveSpreadMarketService");
	}

	/**
	 * 统计 市场有效价差
	 */
	@Test
	@Ignore
	public void count() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		long count = effectiveSpreadMarketService.count(parameters);
		Assert.assertNull(count);
	}

	/**
	 * 删除 市场有效价差
	 */
	@Test
	@Ignore
	public void deleteEntity() {
		EffectiveSpreadMarket effectiveSpreadMarket = new EffectiveSpreadMarket();
		effectiveSpreadMarket.setId(0L);
		effectiveSpreadMarketService.delete(effectiveSpreadMarket);
	}

	/**
	 * 按条件删除 市场有效价差
	 */
	@Test
	@Ignore
	public void deleteMap() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", 1L);
		effectiveSpreadMarketService.delete(parameters);

	}

	/**
	 * 按主键删除 市场有效价差
	 */
	@Test
	@Ignore
	public void deleteById() {
		effectiveSpreadMarketService.deleteById(1L);
	}

	/**
	 * 根据条件查找  市场有效价差
	 */
	@Test
	@Ignore
	public void find() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		List<EffectiveSpreadMarket> effectiveSpreadMarkets = effectiveSpreadMarketService.find(parameters);
		Assert.assertNull(effectiveSpreadMarkets);
	}

	/**
	 * 根据主键查找 市场有效价差
	 */
	@Test
	@Ignore
	public void findById() {
		EffectiveSpreadMarket effectiveSpreadMarket = effectiveSpreadMarketService.findById(1L);
		Assert.assertNull(effectiveSpreadMarket);
	}

	/**
	 * 查找  市场有效价差
	 */
	@Test
	@Ignore
	public void findContainer() {
		EffectiveSpreadMarketQuery query = new EffectiveSpreadMarketQuery();
		Container<EffectiveSpreadMarket> container = effectiveSpreadMarketService.findContainer(query);
		Assert.assertNull(container);
	}

	/**
	 * 查找单个  市场有效价差
	 */
	@Test
	@Ignore
	public void findOne() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("id", 1L);
		EffectiveSpreadMarket effectiveSpreadMarket = effectiveSpreadMarketService.findOne(parameters);
		Assert.assertNull(effectiveSpreadMarket);
	}

	/**
	 * 保存  市场有效价差
	 */
	@Test
	@Ignore
	public void save() {
		EffectiveSpreadMarket effectiveSpreadMarket = new EffectiveSpreadMarket();
		effectiveSpreadMarketService.save(effectiveSpreadMarket);
	}

	/**
	 * 保存或更新  市场有效价差
	 */
	@Test
	@Ignore
	public void saveOrUpdate() {
		EffectiveSpreadMarket effectiveSpreadMarket = new EffectiveSpreadMarket();
		effectiveSpreadMarketService.saveOrUpdate(effectiveSpreadMarket);
	}

	/**
	 * 更新  市场有效价差
	 */
	@Test
	@Ignore
	public void update() {
		EffectiveSpreadMarket effectiveSpreadMarket = new EffectiveSpreadMarket();
		effectiveSpreadMarket.setId(0L);
		effectiveSpreadMarketService.update(effectiveSpreadMarket);
	}

	@Test
	@Ignore
	public void otherMethod() {
		// 新的测试内容
	}

}
