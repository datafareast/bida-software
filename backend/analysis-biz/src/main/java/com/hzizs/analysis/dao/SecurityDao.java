package com.hzizs.analysis.dao;

import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.analysis.entity.Security;
/**
 * 证券 数据库操作类	 
 * @author crazy_cabbage
 *
 */
public interface SecurityDao extends MyBatisDao<Security> {

}
