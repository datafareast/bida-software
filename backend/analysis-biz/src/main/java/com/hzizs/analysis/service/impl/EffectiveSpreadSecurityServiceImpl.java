package com.hzizs.analysis.service.impl;

import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.mybatis.service.impl.MyBatisServiceImpl;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.hzizs.Assert;
import com.hzizs.analysis.dao.EffectiveSpreadSecurityDao;
import com.hzizs.analysis.entity.EffectiveSpreadSecurity;
import com.hzizs.analysis.service.EffectiveSpreadSecurityService;

/**
 * 证券有效价差 服务实现类
 * 
 * @author crazy_cabbage
 *
 */
public class EffectiveSpreadSecurityServiceImpl extends MyBatisServiceImpl<EffectiveSpreadSecurity> implements EffectiveSpreadSecurityService {


  private EffectiveSpreadSecurityDao effectiveSpreadSecurityDao;

  public void setEffectiveSpreadSecurityDao(EffectiveSpreadSecurityDao effectiveSpreadSecurityDao) {
    this.effectiveSpreadSecurityDao = effectiveSpreadSecurityDao;
  }

  @Override
  protected MyBatisDao<EffectiveSpreadSecurity> getDao() {
    return effectiveSpreadSecurityDao;
  }

  // ========================================业务方法========================================
  @Override
  public List<EffectiveSpreadSecurity> find(Long securityId, Date startTime, Date endTime) {
    Assert.assertNotNull(securityId, message.getMessage("securityId.isnull"));
    Assert.assertNotNull(startTime, message.getMessage("startTime.isnull"));
    Assert.assertNotNull(endTime, message.getMessage("endTime.isnull"));
    Map<String, Object> parameters = new HashMap<String, Object>(3);
    parameters.put("securityId", securityId);
    parameters.put("startTime", startTime);
    parameters.put("endTime", endTime);
    return find(parameters);
  }
}
