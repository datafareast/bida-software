package com.hzizs.analysis.service.impl;
import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.mybatis.service.impl.MyBatisServiceImpl;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.hzizs.Assert;
import com.hzizs.analysis.constants.Constants.AreaCat;
import com.hzizs.analysis.dao.AreaDao;
import com.hzizs.analysis.entity.Area;
import com.hzizs.analysis.service.AreaService;
/**
 * 区域 服务实现类
 * @author crazy_cabbage
 *
 */
public class AreaServiceImpl extends MyBatisServiceImpl<Area> implements AreaService {
	

	private AreaDao areaDao;
	public void setAreaDao(AreaDao areaDao) {
		this.areaDao = areaDao;
	}
	@Override
	protected MyBatisDao<Area> getDao() {
		return areaDao;
	}
	//========================================业务方法========================================
  @Override
  public List<Area> findByParentId(Long parentId) {
    Assert.assertNotNull(parentId, message.getMessage("parentId.isnull"));
    Map<String, Object> parameters = new HashMap<String,Object>(1);
    parameters.put("parentId", parentId);
    return find(parameters);
  }
  @Override
  public Area findByParentIdName(Long parentId, String name) {
    Assert.assertNotNull(parentId, message.getMessage("parentId.isnull"));
    Assert.assertNotEmptyString(name, message.getMessage("name.isnull"));
    Map<String, Object> parameters = new HashMap<String,Object>(2);
    parameters.put("parentId", parentId);
    parameters.put("name", name);
    return findOne(parameters);
  }
  @Override
  public Area findByCode(String code) {
    Assert.assertNotEmptyString(code, message.getMessage("code.isnull"));
    Map<String, Object> parameters = new HashMap<String,Object>(1);
    parameters.put("code", code);
    return findOne(parameters);
  }
  @Override
  public long countByParentId(Long parentId) {
    Assert.assertNotNull(parentId,message.getMessage("parentId.isnull"));
    Map<String, Object> parameters  = new HashMap<String,Object>(1);
    parameters.put("parentId", parentId);
    return count(parameters);
  }
  @Override
  public List<Area> findProvinceCityData() {
    List<String> cats = Arrays.asList(AreaCat.PROVINCE,AreaCat.CITY);
    Map<String, Object> parameters = new HashMap<String,Object>(1);
    parameters.put("cats", cats);
    return find(parameters);
  }
}
