package com.hzizs.analysis.dao;

import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.analysis.entity.BasicIndex;
/**
 * 指数基指 数据库操作类	 
 * @author crazy_cabbage
 *
 */
public interface BasicIndexDao extends MyBatisDao<BasicIndex> {

}
