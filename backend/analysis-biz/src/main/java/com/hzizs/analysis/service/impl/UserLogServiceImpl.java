package com.hzizs.analysis.service.impl;
import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.mybatis.service.impl.MyBatisServiceImpl;


import com.hzizs.analysis.dao.UserLogDao;
import com.hzizs.analysis.entity.UserLog;
import com.hzizs.analysis.service.UserLogService;
/**
 * 用户日志 服务实现类
 * @author crazy_cabbage
 *
 */
public class UserLogServiceImpl extends MyBatisServiceImpl<UserLog> implements UserLogService {
	

	private UserLogDao userLogDao;
	public void setUserLogDao(UserLogDao userLogDao) {
		this.userLogDao = userLogDao;
	}
	@Override
	protected MyBatisDao<UserLog> getDao() {
		return userLogDao;
	}
	//========================================业务方法========================================
}
