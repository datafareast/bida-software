package com.hzizs.analysis.facade.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import com.hzizs.Assert;
import com.hzizs.analysis.dto.AreaDto;
import com.hzizs.analysis.entity.Area;
import com.hzizs.analysis.facade.AreaFacade;
import com.hzizs.analysis.service.AreaService;
import com.hzizs.facade.impl.BaseFacadeImpl;

public class AreaFacadeImpl extends BaseFacadeImpl implements AreaFacade {
  private AreaService areaService;
  

  public void setAreaService(AreaService areaService) {
    this.areaService = areaService;
  }
//====================================================================================
  @Override
  public AreaDto findById(Long id) {
    Area area = areaService.findById(id);
    if(area==null) {
      return null;
    }
    AreaDto dto = new AreaDto();
    dto.setArea(area);
    List<Long> parentIds = new ArrayList<Long>();
    Long parentId = area.getParentId();
    while(parentId!=0L) {
      parentIds.add(parentId);
      area = areaService.findById(parentId);
      parentId = area.getParentId();
    }
    Collections.reverse(parentIds);
    dto.setParentIds(parentIds);
    return dto;
  }

  @Override
  public Area save(Area area) {
    Long parentId = area.getParentId();
    String name = area.getName();
    Area po = areaService.findByParentIdName(parentId,name);
    if(po!=null) {
      Assert.customException(message.getMessage("name.exists"));
    }
    String code =area.getCode();
    po =areaService.findByCode(code);
    if(po!=null) {
      Assert.customException(message.getMessage("code.exists"));
    }
    return areaService.save(area);
  }

  @Override
  public Area update(Area area) {
    Long parentId = area.getParentId();
    String name = area.getName();
    Area po = areaService.findByParentIdName(parentId,name);
    if(po!=null) {
      if(!area.getId().equals(po.getId())) {
        Assert.customException("name.exists");
      }
    }
    String code = area.getCode();
    po = areaService.findByCode(code);
    if(po!=null) {
      if(!area.getId().equals(po.getId())) {
        Assert.customException("code.exists");
      }
    }
    return areaService.update(area);
  }

  @Override
  public void deleteByIds(Long[] ids) {
    if(ids!=null) {
      for(Long id:ids) {
        deleteById(id);
      }
    }

  }

  @Override
  public void deleteById(Long id) {
    long num = areaService.countByParentId(id);
    if(num>0L) {
      Assert.customException("area.hasChild");
    }
    areaService.deleteById(id);

  }

}
