package com.hzizs.analysis.dao.impl;
import java.util.List;

import com.fasterxml.jackson.core.type.TypeReference;
import com.hzizs.mybatis.dao.impl.MyBatisDaoImpl;
import com.hzizs.analysis.dao.ActivityIndexDao;
import com.hzizs.analysis.entity.ActivityIndex;
/**
 * 指数指标 数据库操作类实现类	 
 * @author crazy_cabbage
 *
 */
public class ActivityIndexDaoImpl extends MyBatisDaoImpl<ActivityIndex> implements ActivityIndexDao {



	@Override
	public TypeReference<List<ActivityIndex>> getListEntityType() {
		return new TypeReference<List<ActivityIndex>>() {
		};
	}
	//========================================业务方法========================================
}
