package com.hzizs.analysis.dao.impl;

import java.util.List;

import com.fasterxml.jackson.core.type.TypeReference;
import com.hzizs.mybatis.dao.impl.MyBatisDaoImpl;
import com.hzizs.analysis.dao.SecExchangeDao;
import com.hzizs.analysis.entity.SecExchange;

/**
 * 交易所 数据库操作类实现类
 * 
 * @author crazy_cabbage
 *
 */
public class SecExchangeDaoImpl extends MyBatisDaoImpl<SecExchange> implements SecExchangeDao {



  @Override
  public TypeReference<List<SecExchange>> getListEntityType() {
    return new TypeReference<List<SecExchange>>() {
    };
  }
  // ========================================业务方法========================================
}
