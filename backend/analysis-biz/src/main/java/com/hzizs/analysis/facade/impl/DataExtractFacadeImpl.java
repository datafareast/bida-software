package com.hzizs.analysis.facade.impl;

import java.util.List;
import java.util.Map;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import com.hzizs.analysis.constants.Constants.DbType;
import com.hzizs.analysis.entity.DataSource;
import com.hzizs.analysis.facade.DataExtractFacade;
import com.hzizs.analysis.service.DataSourceService;
import com.hzizs.facade.impl.BaseFacadeImpl;

public class DataExtractFacadeImpl extends BaseFacadeImpl implements DataExtractFacade {
  private DataSourceService dataSourceService;
  
  
  public void setDataSourceService(DataSourceService dataSourceService) {
    this.dataSourceService = dataSourceService;
  }


  @Override
  public List<Map<String, Object>> preview(Long dataSourceId, String query) {
    DataSource dataSource = dataSourceService.findById(dataSourceId);
    DriverManagerDataSource driverManagerDataSource = new DriverManagerDataSource(dataSource.getUrl(), dataSource.getUsername(), dataSource.getPassword());
   if(DbType.ORACLE.equals(dataSource.getDbType())) {
     driverManagerDataSource.setDriverClassName("oracle.jdbc.driver.OracleDriver");
   }else  if(DbType.MYSQL.equals(dataSource.getDbType())){
     driverManagerDataSource.setDriverClassName("com.mysql.cj.jdbc.Driver");
   }
   JdbcTemplate jdbcTemplate = new JdbcTemplate(driverManagerDataSource);
    return   jdbcTemplate.queryForList(query);
  }


}
