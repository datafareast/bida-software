package com.hzizs.analysis.dao;

import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.analysis.entity.McapSecurity;
/**
 * 证券市值 数据库操作类	 
 * @author crazy_cabbage
 *
 */
public interface McapSecurityDao extends MyBatisDao<McapSecurity> {

}
