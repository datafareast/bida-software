package com.hzizs.analysis.facade.impl;

import com.hzizs.Assert;
import com.hzizs.analysis.facade.SlideFacade;
import com.hzizs.analysis.service.SlideItemService;
import com.hzizs.analysis.service.SlideService;
import com.hzizs.facade.impl.BaseFacadeImpl;

public class SlideFacadeImpl extends BaseFacadeImpl implements SlideFacade {

  private SlideService slideService;
  private SlideItemService slideItemService;

  public void setSlideService(SlideService slideService) {
    this.slideService = slideService;
  }

  public void setSlideItemService(SlideItemService slideItemService) {
    this.slideItemService = slideItemService;
  }
  // ===========================================================================

  @Override
  public void deleteByIds(Long[] ids) {
    if (ids != null) {
      for (Long id : ids) {
        deleteById(id);
      }
    }

  }

  @Override
  public void deleteById(Long id) {
    long num = slideItemService.countBySlideId(id);
    if (num > 0) {
      Assert.customException(message.getMessage("slideId.used"));
    }
    slideService.deleteById(id);

  }

}
