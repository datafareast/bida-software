package com.hzizs.analysis.dao.impl;
import java.util.List;

import com.fasterxml.jackson.core.type.TypeReference;
import com.hzizs.mybatis.dao.impl.MyBatisDaoImpl;
import com.hzizs.analysis.dao.SecIndexDao;
import com.hzizs.analysis.entity.SecIndex;
/**
 * 常用指标 数据库操作类实现类	 
 * @author crazy_cabbage
 *
 */
public class SecIndexDaoImpl extends MyBatisDaoImpl<SecIndex> implements SecIndexDao {



	@Override
	public TypeReference<List<SecIndex>> getListEntityType() {
		return new TypeReference<List<SecIndex>>() {
		};
	}
	//========================================业务方法========================================
}
