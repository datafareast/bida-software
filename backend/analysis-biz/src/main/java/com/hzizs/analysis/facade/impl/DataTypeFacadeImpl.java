package com.hzizs.analysis.facade.impl;

import com.hzizs.Assert;
import com.hzizs.analysis.entity.DataType;
import com.hzizs.analysis.facade.DataTypeFacade;
import com.hzizs.analysis.service.DataTopoService;
import com.hzizs.analysis.service.DataTypeService;
import com.hzizs.facade.impl.BaseFacadeImpl;

public class DataTypeFacadeImpl extends BaseFacadeImpl implements DataTypeFacade {
  private DataTypeService dataTypeService;
  private DataTopoService dataTopoService;
  public void setDataTypeService(DataTypeService dataTypeService) {
    this.dataTypeService = dataTypeService;
  }

  public void setDataTopoService(DataTopoService dataTopoService) {
    this.dataTopoService = dataTopoService;
  }


  // ============================================业务==============================================
  @Override
  public void deleteByIds(Long[] ids) {
    if (ids != null) {
      for (Long id : ids) {
        deleteById(id);
      }
    }
  }

  @Override
  public void deleteById(Long id) {
    long num = dataTopoService.countByDataTypeId(id);
    if(num>0L) {
      Assert.customException(message.getMessage("dataTypeId.used"));
    }
    dataTypeService.deleteById(id);
  }

  @Override
  public DataType save(DataType dataType) {
    String name = dataType.getName();
    Long dataIndustryId = dataType.getDataIndustryId();
    DataType po = dataTypeService.findByNameDataIndustryId(name, dataIndustryId);
    if (po != null) {
      Assert.customException(message.getMessage("name.exists"));
    }
    return dataTypeService.save(dataType);
  }

  @Override
  public DataType update(DataType dataType) {
    String name = dataType.getName();
    Long dataIndustryId = dataType.getDataIndustryId();
    DataType po = dataTypeService.findByNameDataIndustryId(name, dataIndustryId);
    if (po != null&&!po.getId().equals(dataType.getId())) {
      Assert.customException(message.getMessage("name.exists"));
    }
    return dataTypeService.update(dataType);
  }

}
