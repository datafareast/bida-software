package com.hzizs.analysis.service.impl;

import java.util.HashMap;
import java.util.Map;
import com.hzizs.Assert;
import com.hzizs.analysis.dao.FileDao;
import com.hzizs.analysis.entity.File;
import com.hzizs.analysis.service.FileService;
import com.hzizs.mybatis.dao.MyBatisShardingDao;
import com.hzizs.mybatis.service.impl.MyBatisShardingServiceImpl;

/**
 * 文件 服务实现类
 * 
 * @author crazy_cabbage
 *
 */
public class FileServiceImpl extends MyBatisShardingServiceImpl<File> implements FileService {


  private FileDao fileDao;

  public void setFileDao(FileDao fileDao) {
    this.fileDao = fileDao;
  }

  @Override
  protected MyBatisShardingDao<File> getDao() {
    return fileDao;
  }

  // ========================================业务方法========================================
  @Override
  public File findByName(String name, Long userId) {
    Assert.assertNotEmptyString(name, message.getMessage("name.isnull"));
    Assert.assertNotNull(userId, message.getMessage("userId.isnull"));
    Map<String, Object> parameters = new HashMap<String, Object>(2);
    parameters.put("name", name);
    parameters.put("userId", userId);
    return findOne(parameters, userId);
  }
}
