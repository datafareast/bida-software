package com.hzizs.analysis.service.impl;
import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.mybatis.service.impl.MyBatisServiceImpl;
import java.util.HashMap;
import java.util.Map;
import com.hzizs.Assert;
import com.hzizs.analysis.dao.RoleDao;
import com.hzizs.analysis.entity.Role;
import com.hzizs.analysis.service.RoleService;
/**
 * 角色 服务实现类
 * @author crazy_cabbage
 *
 */
public class RoleServiceImpl extends MyBatisServiceImpl<Role> implements RoleService {
	

	private RoleDao roleDao;
	public void setRoleDao(RoleDao roleDao) {
		this.roleDao = roleDao;
	}
	@Override
	protected MyBatisDao<Role> getDao() {
		return roleDao;
	}
	//========================================业务方法========================================
  @Override
  public Role findByCode(String code) {
    Assert.assertNotEmptyString(code, message.getMessage("code.isnull"));
    Map<String, Object> parameters = new HashMap<String,Object>(1);
    parameters.put("code", code);
    return findOne(parameters);
  }
}
