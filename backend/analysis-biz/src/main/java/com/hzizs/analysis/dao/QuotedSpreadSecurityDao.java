package com.hzizs.analysis.dao;

import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.analysis.entity.QuotedSpreadSecurity;
/**
 * 证券报价价差 数据库操作类	 
 * @author crazy_cabbage
 *
 */
public interface QuotedSpreadSecurityDao extends MyBatisDao<QuotedSpreadSecurity> {

}
