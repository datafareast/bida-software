package com.hzizs.analysis.service.impl;
import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.mybatis.service.impl.MyBatisServiceImpl;


import com.hzizs.analysis.dao.SecurityDao;
import com.hzizs.analysis.entity.Security;
import com.hzizs.analysis.service.SecurityService;
/**
 * 证券 服务实现类
 * @author crazy_cabbage
 *
 */
public class SecurityServiceImpl extends MyBatisServiceImpl<Security> implements SecurityService {
	

	private SecurityDao securityDao;
	public void setSecurityDao(SecurityDao securityDao) {
		this.securityDao = securityDao;
	}
	@Override
	protected MyBatisDao<Security> getDao() {
		return securityDao;
	}
	//========================================业务方法========================================
}
