package com.hzizs.analysis.dao;

import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.analysis.entity.EffectiveSpreadMarket;
/**
 * 市场有效价差 数据库操作类	 
 * @author crazy_cabbage
 *
 */
public interface EffectiveSpreadMarketDao extends MyBatisDao<EffectiveSpreadMarket> {

}
