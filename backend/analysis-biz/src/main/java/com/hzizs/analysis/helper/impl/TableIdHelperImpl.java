package com.hzizs.analysis.helper.impl;

import com.hzizs.analysis.entity.TableId;
import com.hzizs.analysis.service.TableIdService;
import com.hzizs.helper.TableIdHelper;
import com.hzizs.pojo.IdCounter;

public class TableIdHelperImpl implements TableIdHelper {
  private TableIdService tableIdService;
  

  public void setTableIdService(TableIdService tableIdService) {
    this.tableIdService = tableIdService;
  }

//================================================================================
  @Override
  public IdCounter get(String code) {
    TableId tableId = tableIdService.doSaveOrGet(code);
    IdCounter idCounter = new IdCounter();
    idCounter.setSeed(tableId.getVal());
    idCounter.setStep(tableId.getStep());
    return idCounter;
  }

}
