package com.hzizs.analysis.dao;

import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.analysis.entity.SysData;
/**
 * 数据字典 数据库操作类	 
 * @author crazy_cabbage
 *
 */
public interface SysDataDao extends MyBatisDao<SysData> {

}
