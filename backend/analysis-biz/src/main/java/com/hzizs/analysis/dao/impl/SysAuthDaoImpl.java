package com.hzizs.analysis.dao.impl;
import java.util.List;

import com.fasterxml.jackson.core.type.TypeReference;
import com.hzizs.mybatis.dao.impl.MyBatisDaoImpl;
import com.hzizs.analysis.dao.SysAuthDao;
import com.hzizs.analysis.entity.SysAuth;
/**
 * 系统权限 数据库操作类实现类	 
 * @author crazy_cabbage
 *
 */
public class SysAuthDaoImpl extends MyBatisDaoImpl<SysAuth> implements SysAuthDao {



	@Override
	public TypeReference<List<SysAuth>> getListEntityType() {
		return new TypeReference<List<SysAuth>>() {
		};
	}
	//========================================业务方法========================================
}
