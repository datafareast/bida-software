package com.hzizs.analysis.dao;

import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.analysis.entity.TableId;
/**
 * 表主键 数据库操作类	 
 * @author crazy_cabbage
 *
 */
public interface TableIdDao extends MyBatisDao<TableId> {
  /**
   * 根据编码查找 表主键
   * @param code 编码
   * @return 找到的表主键
   */
  TableId findByCode(String code);
}
