package com.hzizs.analysis.service.impl;

import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.mybatis.service.impl.MyBatisServiceImpl;


import com.hzizs.analysis.dao.SecAnalysisDao;
import com.hzizs.analysis.entity.SecAnalysis;
import com.hzizs.analysis.service.SecAnalysisService;

/**
 * 业绩分析 服务实现类
 * 
 * @author crazy_cabbage
 *
 */
public class SecAnalysisServiceImpl extends MyBatisServiceImpl<SecAnalysis> implements SecAnalysisService {


  private SecAnalysisDao secAnalysisDao;

  public void setSecAnalysisDao(SecAnalysisDao secAnalysisDao) {
    this.secAnalysisDao = secAnalysisDao;
  }

  @Override
  protected MyBatisDao<SecAnalysis> getDao() {
    return secAnalysisDao;
  }
  // ========================================业务方法========================================
}
