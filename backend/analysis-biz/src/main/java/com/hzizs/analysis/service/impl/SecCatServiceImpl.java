package com.hzizs.analysis.service.impl;

import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.mybatis.service.impl.MyBatisServiceImpl;


import com.hzizs.analysis.dao.SecCatDao;
import com.hzizs.analysis.entity.SecCat;
import com.hzizs.analysis.service.SecCatService;

/**
 * 类别 服务实现类
 * 
 * @author crazy_cabbage
 *
 */
public class SecCatServiceImpl extends MyBatisServiceImpl<SecCat> implements SecCatService {


  private SecCatDao secCatDao;

  public void setSecCatDao(SecCatDao secCatDao) {
    this.secCatDao = secCatDao;
  }

  @Override
  protected MyBatisDao<SecCat> getDao() {
    return secCatDao;
  }
  // ========================================业务方法========================================
}
