package com.hzizs.analysis.dao;

import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.analysis.entity.SecPosition;

/**
 * 持仓分析 数据库操作类
 * 
 * @author crazy_cabbage
 *
 */
public interface SecPositionDao extends MyBatisDao<SecPosition> {

}
