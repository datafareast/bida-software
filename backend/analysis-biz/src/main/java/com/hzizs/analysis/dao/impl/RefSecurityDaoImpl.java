package com.hzizs.analysis.dao.impl;
import java.util.List;

import com.fasterxml.jackson.core.type.TypeReference;
import com.hzizs.mybatis.dao.impl.MyBatisDaoImpl;
import com.hzizs.analysis.dao.RefSecurityDao;
import com.hzizs.analysis.entity.RefSecurity;
/**
 * 证券参考数据 数据库操作类实现类	 
 * @author crazy_cabbage
 *
 */
public class RefSecurityDaoImpl extends MyBatisDaoImpl<RefSecurity> implements RefSecurityDao {



	@Override
	public TypeReference<List<RefSecurity>> getListEntityType() {
		return new TypeReference<List<RefSecurity>>() {
		};
	}
	//========================================业务方法========================================
}
