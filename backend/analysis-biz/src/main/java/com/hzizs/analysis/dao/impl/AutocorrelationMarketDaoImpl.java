package com.hzizs.analysis.dao.impl;
import java.util.List;

import com.fasterxml.jackson.core.type.TypeReference;
import com.hzizs.mybatis.dao.impl.MyBatisDaoImpl;
import com.hzizs.analysis.dao.AutocorrelationMarketDao;
import com.hzizs.analysis.entity.AutocorrelationMarket;
/**
 * 市场自相关 数据库操作类实现类	 
 * @author crazy_cabbage
 *
 */
public class AutocorrelationMarketDaoImpl extends MyBatisDaoImpl<AutocorrelationMarket> implements AutocorrelationMarketDao {



	@Override
	public TypeReference<List<AutocorrelationMarket>> getListEntityType() {
		return new TypeReference<List<AutocorrelationMarket>>() {
		};
	}
	//========================================业务方法========================================
}
