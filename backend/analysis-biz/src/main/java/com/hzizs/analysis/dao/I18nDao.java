package com.hzizs.analysis.dao;

import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.analysis.entity.I18n;
/**
 * 国际化 数据库操作类	 
 * @author crazy_cabbage
 *
 */
public interface I18nDao extends MyBatisDao<I18n> {

}
