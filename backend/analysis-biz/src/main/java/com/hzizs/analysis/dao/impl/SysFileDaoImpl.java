package com.hzizs.analysis.dao.impl;
import java.util.List;

import com.fasterxml.jackson.core.type.TypeReference;
import com.hzizs.mybatis.dao.impl.MyBatisDaoImpl;
import com.hzizs.analysis.dao.SysFileDao;
import com.hzizs.analysis.entity.SysFile;
/**
 * 系统文件 数据库操作类实现类	 
 * @author crazy_cabbage
 *
 */
public class SysFileDaoImpl extends MyBatisDaoImpl<SysFile> implements SysFileDao {



	@Override
	public TypeReference<List<SysFile>> getListEntityType() {
		return new TypeReference<List<SysFile>>() {
		};
	}
	//========================================业务方法========================================
}
