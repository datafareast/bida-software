package com.hzizs.analysis.dao.impl;
import java.util.List;

import com.fasterxml.jackson.core.type.TypeReference;
import com.hzizs.mybatis.dao.impl.MyBatisDaoImpl;
import com.hzizs.analysis.dao.SysUserLogDao;
import com.hzizs.analysis.entity.SysUserLog;
/**
 * 系统用户日志 数据库操作类实现类	 
 * @author crazy_cabbage
 *
 */
public class SysUserLogDaoImpl extends MyBatisDaoImpl<SysUserLog> implements SysUserLogDao {



	@Override
	public TypeReference<List<SysUserLog>> getListEntityType() {
		return new TypeReference<List<SysUserLog>>() {
		};
	}
	//========================================业务方法========================================
}
