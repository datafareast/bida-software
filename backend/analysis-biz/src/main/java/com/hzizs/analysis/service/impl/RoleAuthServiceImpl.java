package com.hzizs.analysis.service.impl;

import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.mybatis.service.impl.MyBatisServiceImpl;
import com.hzizs.util.CollectionUtil;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.hzizs.Assert;
import com.hzizs.analysis.dao.RoleAuthDao;
import com.hzizs.analysis.entity.RoleAuth;
import com.hzizs.analysis.service.RoleAuthService;

/**
 * 角色权限 服务实现类
 * 
 * @author crazy_cabbage
 *
 */
public class RoleAuthServiceImpl extends MyBatisServiceImpl<RoleAuth> implements RoleAuthService {


  private RoleAuthDao roleAuthDao;

  public void setRoleAuthDao(RoleAuthDao roleAuthDao) {
    this.roleAuthDao = roleAuthDao;
  }

  @Override
  protected MyBatisDao<RoleAuth> getDao() {
    return roleAuthDao;
  }

  // ========================================业务方法========================================
  @Override
  public List<RoleAuth> findByRoleId(Long roleId) {
    Assert.assertNotNull(roleId, message.getMessage("roleId.isnull"));
    Map<String, Object> parameters = new HashMap<String, Object>(1);
    parameters.put("roleId", roleId);
    return find(parameters);
  }

  @Override
  public void delete(Long roleId, List<Long> authIds) {
    if (CollectionUtil.isEmpty(authIds)) {
      return;
    }
    Assert.assertNotNull(roleId, message.getMessage("roleId.isnull"));
    Map<String, Object> parameters = new HashMap<String, Object>(2);
    parameters.put("roleId", roleId);
    parameters.put("authIds", authIds);

  }

  @Override
  public void deleteByRoleId(Long roleId) {
    Assert.assertNotNull(roleId, message.getMessage("roleId.isnull"));
    Map<String, Object> parameters = new HashMap<String, Object>(1);
    parameters.put("roleId", roleId);
    delete(parameters);

  }

  @Override
  public long countByAuthId(Long authId) {
    Assert.assertNotNull(authId, message.getMessage("authId.isnull"));
    Map<String, Object> parameters = new HashMap<String, Object>(1);
    parameters.put("authId", authId);
    return count(parameters);
  }
}
