package com.hzizs.analysis.dao;

import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.analysis.entity.DataTopo;

/**
 * 数据拓普图 数据库操作类
 * @author crazy_cabbage
 */
public interface DataTopoDao extends MyBatisDao<DataTopo> {

}
