package com.hzizs.analysis.dao;

import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.analysis.entity.Role;
/**
 * 角色 数据库操作类	 
 * @author crazy_cabbage
 *
 */
public interface RoleDao extends MyBatisDao<Role> {

}
