package com.hzizs.analysis.dao;

import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.analysis.entity.User;
/**
 * 用户 数据库操作类	 
 * @author crazy_cabbage
 *
 */
public interface UserDao extends MyBatisDao<User> {

}
