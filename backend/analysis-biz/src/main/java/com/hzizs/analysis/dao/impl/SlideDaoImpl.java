package com.hzizs.analysis.dao.impl;
import java.util.List;

import com.fasterxml.jackson.core.type.TypeReference;
import com.hzizs.mybatis.dao.impl.MyBatisDaoImpl;
import com.hzizs.analysis.dao.SlideDao;
import com.hzizs.analysis.entity.Slide;
/**
 * 轮播图 数据库操作类实现类	 
 * @author crazy_cabbage
 *
 */
public class SlideDaoImpl extends MyBatisDaoImpl<Slide> implements SlideDao {



	@Override
	public TypeReference<List<Slide>> getListEntityType() {
		return new TypeReference<List<Slide>>() {
		};
	}
	//========================================业务方法========================================
}
