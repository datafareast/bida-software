package com.hzizs.analysis.service.impl;
import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.mybatis.service.impl.MyBatisServiceImpl;


import com.hzizs.analysis.dao.SecIndexDao;
import com.hzizs.analysis.entity.SecIndex;
import com.hzizs.analysis.service.SecIndexService;
/**
 * 常用指标 服务实现类
 * @author crazy_cabbage
 *
 */
public class SecIndexServiceImpl extends MyBatisServiceImpl<SecIndex> implements SecIndexService {
	

	private SecIndexDao secIndexDao;
	public void setSecIndexDao(SecIndexDao secIndexDao) {
		this.secIndexDao = secIndexDao;
	}
	@Override
	protected MyBatisDao<SecIndex> getDao() {
		return secIndexDao;
	}
	//========================================业务方法========================================
}
