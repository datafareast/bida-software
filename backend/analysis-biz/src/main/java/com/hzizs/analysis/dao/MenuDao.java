package com.hzizs.analysis.dao;

import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.analysis.entity.Menu;
/**
 * 菜单 数据库操作类	 
 * @author crazy_cabbage
 *
 */
public interface MenuDao extends MyBatisDao<Menu> {

}
