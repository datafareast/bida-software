package com.hzizs.analysis.service.impl;

import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.mybatis.service.impl.MyBatisServiceImpl;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.hzizs.analysis.dao.CmsDao;
import com.hzizs.analysis.entity.Cms;
import com.hzizs.analysis.service.CmsService;
import com.hzizs.constants.Enabled;

/**
 * 内容管理系统 服务实现类
 * 
 * @author crazy_cabbage
 *
 */
public class CmsServiceImpl extends MyBatisServiceImpl<Cms> implements CmsService {


  private CmsDao cmsDao;

  public void setCmsDao(CmsDao cmsDao) {
    this.cmsDao = cmsDao;
  }

  @Override
  protected MyBatisDao<Cms> getDao() {
    return cmsDao;
  }

  // ========================================业务方法========================================
  @Override
  public List<Cms> findNav() {
    Map<String, Object> parameters = new HashMap<String, Object>(1);
    parameters.put("isLeaf", Enabled.F);
    return find(parameters);
  }

}
