package com.hzizs.analysis.facade.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import com.hzizs.analysis.constants.Constants.IndexTypeCode;
import com.hzizs.analysis.dto.IndexDto;
import com.hzizs.analysis.dto.IndexItem;
import com.hzizs.analysis.dto.Item;
import com.hzizs.analysis.entity.AutocorrelationMarket;
import com.hzizs.analysis.entity.AutocorrelationSecurity;
import com.hzizs.analysis.entity.EffectiveSpreadMarket;
import com.hzizs.analysis.entity.EffectiveSpreadSecurity;
import com.hzizs.analysis.entity.Market;
import com.hzizs.analysis.entity.QuotedSpreadMarket;
import com.hzizs.analysis.entity.QuotedSpreadSecurity;
import com.hzizs.analysis.entity.RealisedSpreadMarket;
import com.hzizs.analysis.entity.RealisedSpreadSecurity;
import com.hzizs.analysis.entity.RefSecurity;
import com.hzizs.analysis.facade.IndexTypeFacade;
import com.hzizs.analysis.query.DataQuery;
import com.hzizs.analysis.service.AutocorrelationMarketService;
import com.hzizs.analysis.service.AutocorrelationSecurityService;
import com.hzizs.analysis.service.EffectiveSpreadMarketService;
import com.hzizs.analysis.service.EffectiveSpreadSecurityService;
import com.hzizs.analysis.service.MarketService;
import com.hzizs.analysis.service.QuotedSpreadMarketService;
import com.hzizs.analysis.service.QuotedSpreadSecurityService;
import com.hzizs.analysis.service.RealisedSpreadMarketService;
import com.hzizs.analysis.service.RealisedSpreadSecurityService;
import com.hzizs.analysis.service.RefSecurityService;
import com.hzizs.facade.impl.BaseFacadeImpl;
import com.hzizs.util.CollectionUtil;
import com.hzizs.util.StreamUtil;
import com.hzizs.util.StringUtil;

public class IndexTypeFacadeImpl extends BaseFacadeImpl implements IndexTypeFacade {
  private AutocorrelationSecurityService autocorrelationSecurityService;
  private RealisedSpreadSecurityService realisedSpreadSecurityService;
  private EffectiveSpreadSecurityService effectiveSpreadSecurityService;
  private QuotedSpreadSecurityService quotedSpreadSecurityService;
  private RefSecurityService refSecurityService;
  private AutocorrelationMarketService autocorrelationMarketService;
  private RealisedSpreadMarketService realisedSpreadMarketService;
  private QuotedSpreadMarketService quotedSpreadMarketService;
  private EffectiveSpreadMarketService effectiveSpreadMarketService;
  private MarketService marketService;


  public void setAutocorrelationSecurityService(AutocorrelationSecurityService autocorrelationSecurityService) {
    this.autocorrelationSecurityService = autocorrelationSecurityService;
  }

  public void setRealisedSpreadSecurityService(RealisedSpreadSecurityService realisedSpreadSecurityService) {
    this.realisedSpreadSecurityService = realisedSpreadSecurityService;
  }

  public void setEffectiveSpreadSecurityService(EffectiveSpreadSecurityService effectiveSpreadSecurityService) {
    this.effectiveSpreadSecurityService = effectiveSpreadSecurityService;
  }

  public void setQuotedSpreadSecurityService(QuotedSpreadSecurityService quotedSpreadSecurityService) {
    this.quotedSpreadSecurityService = quotedSpreadSecurityService;
  }

  public void setRefSecurityService(RefSecurityService refSecurityService) {
    this.refSecurityService = refSecurityService;
  }

  public void setAutocorrelationMarketService(AutocorrelationMarketService autocorrelationMarketService) {
    this.autocorrelationMarketService = autocorrelationMarketService;
  }

  public void setRealisedSpreadMarketService(RealisedSpreadMarketService realisedSpreadMarketService) {
    this.realisedSpreadMarketService = realisedSpreadMarketService;
  }

  public void setQuotedSpreadMarketService(QuotedSpreadMarketService quotedSpreadMarketService) {
    this.quotedSpreadMarketService = quotedSpreadMarketService;
  }

  public void setEffectiveSpreadMarketService(EffectiveSpreadMarketService effectiveSpreadMarketService) {
    this.effectiveSpreadMarketService = effectiveSpreadMarketService;
  }

  public void setMarketService(MarketService marketService) {
    this.marketService = marketService;
  }

  @Override
  public IndexDto findData(DataQuery query) {
    Date startTime = query.getStartTime();
    Date endTime = query.getEndTime();
    List<IndexItem> items = new ArrayList<IndexItem>();
    if (CollectionUtil.isNotEmpty(query.getMarket()) && CollectionUtil.isNotEmpty(query.getMarketIndexType())) {
      String indexType = CollectionUtil.join(query.getMarketIndexType());
      for (Long marketId : query.getMarket()) {
        Market market = marketService.findById(marketId);
        String name = market.getName();
        if (indexType.contains("AA")) {
          totalAMarket(indexType, marketId, startTime, endTime, name, items);
        }
        if (indexType.contains("R")) {
          totalRMarket(indexType, marketId, startTime, endTime, name, items);
        }
        if (indexType.contains("Q")) {
          totalQMarket(indexType, marketId, startTime, endTime, name, items);
        }
        if (indexType.contains("E")) {
          totalEMarket(indexType, marketId, startTime, endTime, name, items);
        }
      }
    }
    if (CollectionUtil.isNotEmpty(query.getSecurity()) && CollectionUtil.isNotEmpty(query.getSecurityIndexType())) {
      String indexType = CollectionUtil.join(query.getSecurityIndexType());
      for (Long securityId : query.getSecurity()) {
        RefSecurity refSecurity = refSecurityService.findBySecurityId(securityId);
        String name = refSecurity.getSecurityName();
        if (indexType.contains("AA")) {
          totalASecurity(indexType, securityId, startTime, endTime, name, items);
        }
        if (indexType.contains("R")) {
          totalRSecurity(indexType, securityId, startTime, endTime, name, items);
        }
        if (indexType.contains("Q")) {
          totalQSecurity(indexType, securityId, startTime, endTime, name, items);
        }
        if (indexType.contains("E")) {
          totalESecurity(indexType, securityId, startTime, endTime, name, items);
        }
    }
    }
    List<String> legend = items.parallelStream().map(IndexItem::getName).collect(Collectors.toList());
    List<Date> xdate = new ArrayList<Date>();
    for (IndexItem indexItem : items) {
      List<Date> iDate = indexItem.getItem().parallelStream().map(Item::getDate).collect(Collectors.toList());
      xdate = StreamUtil.union(xdate, iDate);
    }
    Collections.sort(xdate);
    // 差异化，补全
    for (IndexItem indexItem : items) {
      List<Date> iDate = indexItem.getItem().parallelStream().map(Item::getDate).collect(Collectors.toList());
      List<Date> date = StreamUtil.subtract(xdate, iDate);
      List<Item> is = indexItem.getItem();
      // 补全日期和值
      date.parallelStream().forEach(d -> {
        Item item = new Item();
        item.setDate(d);
        item.setValue(null);
        is.add(item);
      });
      Collections.sort(is, (a, b) -> {
        return a.getDate().compareTo(b.getDate());
      });
      indexItem.setData(is.parallelStream().map(Item::getValue).collect(Collectors.toList()));
    }
    IndexDto dto = new IndexDto();
    dto.setItems(items);
    dto.setXdate(xdate);
    dto.setLegend(legend);
    return dto;
  }

  private void totalAMarket(String indexType, Long marketId, Date startTime, Date endTime, String name, List<IndexItem> items) {
    List<AutocorrelationMarket> autocorrelationMarkets = autocorrelationMarketService.find(marketId, startTime, endTime);

    if (indexType.contains(IndexTypeCode.AA10s)) {
      Stream<Item> stream = autocorrelationMarkets.parallelStream().map(a -> {
        Item item = new Item();
        item.setDate(a.getOccDate());
        item.setValue(a.getAutocorrelation10sec());
        return item;
      });
      String legendName = legendName(IndexTypeCode.AA10s, name);
      addItem(items, stream, legendName);
    }
    if (indexType.contains(IndexTypeCode.AA60s)) {
      Stream<Item> stream = autocorrelationMarkets.parallelStream().map(a -> {
        Item item = new Item();
        item.setDate(a.getOccDate());
        item.setValue(a.getAutocorrelation60sec());
        return item;
      });
      String legendName = legendName(IndexTypeCode.AA60s, name);
      addItem(items, stream, legendName);
    }
  }

  private void totalQMarket(String indexType, Long marketId, Date startTime, Date endTime, String name, List<IndexItem> items) {
    List<QuotedSpreadMarket> quotedSpreadMarkets = quotedSpreadMarketService.find(marketId, startTime, endTime);

    if (indexType.contains(IndexTypeCode.QA)) {
      Stream<Item> stream = quotedSpreadMarkets.parallelStream().map(a -> {
        Item item = new Item();
        item.setDate(a.getOccDate());
        item.setValue(a.getQuotedSpreadAbs());
        return item;
      });
      String legendName = legendName(IndexTypeCode.QA, name);
      addItem(items, stream, legendName);
    }
    if (indexType.contains(IndexTypeCode.QB)) {
      Stream<Item> stream = quotedSpreadMarkets.parallelStream().map(a -> {
        Item item = new Item();
        item.setDate(a.getOccDate());
        item.setValue(a.getQuotedSpreadBps());
        return item;
      });
      String legendName = legendName(IndexTypeCode.QB, name);
      addItem(items, stream, legendName);
    }
  }

  private void totalEMarket(String indexType, Long marketId, Date startTime, Date endTime, String name, List<IndexItem> items) {
    List<EffectiveSpreadMarket> effectiveSpreadMarkets = effectiveSpreadMarketService.find(marketId, startTime, endTime);
    if (indexType.contains(IndexTypeCode.EA)) {
      Stream<Item> stream = effectiveSpreadMarkets.parallelStream().map(a -> {
        Item item = new Item();
        item.setDate(a.getOccDate());
        item.setValue(a.getEffectiveSpreadAbs());
        return item;
      });
      String legendName = legendName(IndexTypeCode.EA, name);
      addItem(items, stream, legendName);
    }
    if (indexType.contains(IndexTypeCode.EB)) {
      Stream<Item> stream = effectiveSpreadMarkets.parallelStream().map(a -> {
        Item item = new Item();
        item.setDate(a.getOccDate());
        item.setValue(a.getEffectiveSpreadBps());
        return item;
      });
      String legendName = legendName(IndexTypeCode.EB, name);
      addItem(items, stream, legendName);
    }
  }

  private void totalRMarket(String indexType, Long marketId, Date startTime, Date endTime, String name, List<IndexItem> items) {
    List<RealisedSpreadMarket> realisedSpreadMarkets = realisedSpreadMarketService.find(marketId, startTime, endTime);
    if (indexType.contains(IndexTypeCode.RA1s)) {
      Stream<Item> stream = realisedSpreadMarkets.parallelStream().map(a -> {
        Item item = new Item();
        item.setDate(a.getOccDate());
        item.setValue(a.getRealisedSpread1secAbs());
        return item;
      });
      String legendName = legendName(IndexTypeCode.RA1s, name);
      addItem(items, stream, legendName);
    }
    if (indexType.contains(IndexTypeCode.RA10s)) {
      Stream<Item> stream = realisedSpreadMarkets.parallelStream().map(a -> {
        Item item = new Item();
        item.setDate(a.getOccDate());
        item.setValue(a.getRealisedSpread10secAbs());
        return item;
      });
      String legendName = legendName(IndexTypeCode.RA10s, name);
      addItem(items, stream, legendName);
    }
    if (indexType.contains(IndexTypeCode.RA30s)) {
      Stream<Item> stream = realisedSpreadMarkets.parallelStream().map(

          a -> {
            Item item = new Item();
            item.setDate(a.getOccDate());
            item.setValue(a.getRealisedSpread30secAbs());
            return item;
          }

      );
      String legendName = legendName(IndexTypeCode.RA30s, name);
      addItem(items, stream, legendName);
    }
    if (indexType.contains(IndexTypeCode.RA1m)) {
      Stream<Item> stream = realisedSpreadMarkets.parallelStream().map(

          a -> {
            Item item = new Item();
            item.setDate(a.getOccDate());
            item.setValue(a.getRealisedSpread1minAbs());
            return item;
          });
      String legendName = legendName(IndexTypeCode.RA1m, name);
      addItem(items, stream, legendName);
    }
    if (indexType.contains(IndexTypeCode.RA5m)) {
      Stream<Item> stream = realisedSpreadMarkets.parallelStream().map(a -> {
        Item item = new Item();
        item.setDate(a.getOccDate());
        item.setValue(a.getRealisedSpread5minAbs());
        return item;
      });
      String legendName = legendName(IndexTypeCode.RA5m, name);
      addItem(items, stream, legendName);
    }
    if (indexType.contains(IndexTypeCode.RA10m)) {
      Stream<Item> stream = realisedSpreadMarkets.parallelStream().map(a -> {
        Item item = new Item();
        item.setDate(a.getOccDate());
        item.setValue(a.getRealisedSpread10minAbs());
        return item;
      });
      String legendName = legendName(IndexTypeCode.RA10m, name);
      addItem(items, stream, legendName);
    }
    if (indexType.contains(IndexTypeCode.RB1s)) {
      Stream<Item> stream = realisedSpreadMarkets.parallelStream().map(a -> {
        Item item = new Item();
        item.setDate(a.getOccDate());
        item.setValue(a.getRealisedSpread1secBps());
        return item;
      });
      String legendName = legendName(IndexTypeCode.RB1s, name);
      addItem(items, stream, legendName);
    }
    if (indexType.contains(IndexTypeCode.RB10s)) {
      Stream<Item> stream = realisedSpreadMarkets.parallelStream().map(a -> {
        Item item = new Item();
        item.setDate(a.getOccDate());
        item.setValue(a.getRealisedSpread10secBps());
        return item;
      });
      String legendName = legendName(IndexTypeCode.RB10s, name);
      addItem(items, stream, legendName);
    }
    if (indexType.contains(IndexTypeCode.RB30s)) {
      Stream<Item> stream = realisedSpreadMarkets.parallelStream().map(a -> {
        Item item = new Item();
        item.setDate(a.getOccDate());
        item.setValue(a.getRealisedSpread30secBps());
        return item;
      });
      String legendName = legendName(IndexTypeCode.RB30s, name);
      addItem(items, stream, legendName);

    }
    if (indexType.contains(IndexTypeCode.RB1m)) {
      Stream<Item> stream = realisedSpreadMarkets.parallelStream().map(a -> {
        Item item = new Item();
        item.setDate(a.getOccDate());
        item.setValue(a.getRealisedSpread1minBps());
        return item;
      }

      );
      String legendName = legendName(IndexTypeCode.RB1m, name);
      addItem(items, stream, legendName);
    }
    if (indexType.contains(IndexTypeCode.RB5m)) {
      Stream<Item> stream = realisedSpreadMarkets.parallelStream().map(a -> {
        Item item = new Item();
        item.setDate(a.getOccDate());
        item.setValue(a.getRealisedSpread5minBps());
        return item;
      });
      String legendName = legendName(IndexTypeCode.RB5m, name);
      addItem(items, stream, legendName);
    }
    if (indexType.contains(IndexTypeCode.RB10m)) {
      Stream<Item> stream = realisedSpreadMarkets.parallelStream().map(a -> {
        Item item = new Item();
        item.setDate(a.getOccDate());
        item.setValue(a.getRealisedSpread10minBps());
        return item;
      });
      String legendName = legendName(IndexTypeCode.RB10m, name);
      addItem(items, stream, legendName);
    }
  }

  private void totalASecurity(String indexType, Long securityId, Date startTime, Date endTime, String name, List<IndexItem> items) {
    List<AutocorrelationSecurity> autocorrelationSecurities = autocorrelationSecurityService.find(securityId, startTime, endTime);

    if (indexType.contains(IndexTypeCode.AA10s)) {
      Stream<Item> stream = autocorrelationSecurities.parallelStream().map(a -> {
        Item item = new Item();
        item.setDate(a.getOccDate());
        item.setValue(a.getAutocorrelation10sec());
        return item;
      });
      String legendName = legendName(IndexTypeCode.AA10s, name);
      addItem(items, stream, legendName);
    }
    if (indexType.contains(IndexTypeCode.AA60s)) {
      Stream<Item> stream = autocorrelationSecurities.parallelStream().map(a -> {
        Item item = new Item();
        item.setDate(a.getOccDate());
        item.setValue(a.getAutocorrelation60sec());
        return item;
      }

      );
      String legendName = legendName(IndexTypeCode.AA60s, name);
      addItem(items, stream, legendName);
    }
  }

  private void totalQSecurity(String indexType, Long securityId, Date startTime, Date endTime, String name, List<IndexItem> items) {
    List<QuotedSpreadSecurity> quotedSpreadSecurities = quotedSpreadSecurityService.find(securityId, startTime, endTime);

    if (indexType.contains(IndexTypeCode.QA)) {
      Stream<Item> stream = quotedSpreadSecurities.parallelStream().map(a -> {
        Item item = new Item();
        item.setDate(a.getOccDate());
        item.setValue(a.getQuotedSpreadAbs());
        return item;
      });
      String legendName = legendName(IndexTypeCode.QA, name);
      addItem(items, stream, legendName);
    }
    if (indexType.contains(IndexTypeCode.QB)) {
      Stream<Item> stream = quotedSpreadSecurities.parallelStream().map(a -> {
        Item item = new Item();
        item.setDate(a.getOccDate());
        item.setValue(a.getQuotedSpreadBps());
        return item;
      });
      String legendName = legendName(IndexTypeCode.QB, name);
      addItem(items, stream, legendName);
    }
  }

  private void totalESecurity(String indexType, Long securityId, Date startTime, Date endTime, String name, List<IndexItem> items) {
    List<EffectiveSpreadSecurity> effectiveSpreadseSecurities = effectiveSpreadSecurityService.find(securityId, startTime, endTime);

    if (indexType.contains(IndexTypeCode.EA)) {
      Stream<Item> stream = effectiveSpreadseSecurities.parallelStream().map(a -> {
        Item item = new Item();
        item.setDate(a.getOccDate());
        item.setValue(a.getEffectiveSpreadAbs());
        return item;
      }

      );
      String legendName = legendName(IndexTypeCode.EA, name);
      addItem(items, stream, legendName);
    }
    if (indexType.contains(IndexTypeCode.EB)) {
      Stream<Item> stream = effectiveSpreadseSecurities.parallelStream().map(a -> {
        Item item = new Item();
        item.setDate(a.getOccDate());
        item.setValue(a.getEffectiveSpreadBps());
        return item;
      });
      String legendName = legendName(IndexTypeCode.EB, name);
      addItem(items, stream, legendName);
    }
  }

  private void totalRSecurity(String indexType, Long securityId, Date startTime, Date endTime, String name, List<IndexItem> items) {
    List<RealisedSpreadSecurity> realisedSpreadSecurities = realisedSpreadSecurityService.find(securityId, startTime, endTime);

    if (indexType.contains(IndexTypeCode.RA1s)) {
      Stream<Item> stream = realisedSpreadSecurities.parallelStream().map(a -> {
        Item item = new Item();
        item.setDate(a.getOccDate());
        item.setValue(a.getRealisedSpread1secAbs());
        return item;
      });
      String legendName = legendName(IndexTypeCode.RA1s, name);
      addItem(items, stream, legendName);
    }
    if (indexType.contains(IndexTypeCode.RA10s)) {
      Stream<Item> stream = realisedSpreadSecurities.parallelStream().map(a -> {
        Item item = new Item();
        item.setDate(a.getOccDate());
        item.setValue(a.getRealisedSpread10secAbs());
        return item;
      }

      );
      String legendName = legendName(IndexTypeCode.RA10s, name);
      addItem(items, stream, legendName);
    }
    if (indexType.contains(IndexTypeCode.RA30s)) {
      Stream<Item> stream = realisedSpreadSecurities.parallelStream().map(a -> {
        Item item = new Item();
        item.setDate(a.getOccDate());
        item.setValue(a.getRealisedSpread30secAbs());
        return item;
      });
      String legendName = legendName(IndexTypeCode.RA30s, name);
      addItem(items, stream, legendName);
    }
    if (indexType.contains(IndexTypeCode.RA1m)) {
      Stream<Item> stream = realisedSpreadSecurities.parallelStream().map(a -> {
        Item item = new Item();
        item.setDate(a.getOccDate());
        item.setValue(a.getRealisedSpread1minAbs());
        return item;
      });
      String legendName = legendName(IndexTypeCode.RA1m, name);
      addItem(items, stream, legendName);
    }
    if (indexType.contains(IndexTypeCode.RA5m)) {
      Stream<Item> stream = realisedSpreadSecurities.parallelStream().map(a -> {
        Item item = new Item();
        item.setDate(a.getOccDate());
        item.setValue(a.getRealisedSpread5minAbs());
        return item;
      });
      String legendName = legendName(IndexTypeCode.RA5m, name);
      addItem(items, stream, legendName);
    }
    if (indexType.contains(IndexTypeCode.RA10m)) {
      Stream<Item> stream = realisedSpreadSecurities.parallelStream().map(a -> {
        Item item = new Item();
        item.setDate(a.getOccDate());
        item.setValue(a.getRealisedSpread10minAbs());
        return item;
      });
      String legendName = legendName(IndexTypeCode.RA10m, name);
      addItem(items, stream, legendName);
    }
    if (indexType.contains(IndexTypeCode.RB1s)) {
      Stream<Item> stream = realisedSpreadSecurities.parallelStream().map(a -> {
        Item item = new Item();
        item.setDate(a.getOccDate());
        item.setValue(a.getRealisedSpread1secBps());
        return item;
      });
      String legendName = legendName(IndexTypeCode.RB1s, name);
      addItem(items, stream, legendName);
    }
    if (indexType.contains(IndexTypeCode.RB10s)) {
      Stream<Item> stream = realisedSpreadSecurities.parallelStream().map(a -> {
        Item item = new Item();
        item.setDate(a.getOccDate());
        item.setValue(a.getRealisedSpread10secBps());
        return item;
      });
      String legendName = legendName(IndexTypeCode.RB10s, name);
      addItem(items, stream, legendName);
    }
    if (indexType.contains(IndexTypeCode.RB30s)) {
      Stream<Item> stream = realisedSpreadSecurities.parallelStream().map(a -> {
        Item item = new Item();
        item.setDate(a.getOccDate());
        item.setValue(a.getRealisedSpread30secBps());
        return item;
      });
      String legendName = legendName(IndexTypeCode.RB30s, name);
      addItem(items, stream, legendName);

    }
    if (indexType.contains(IndexTypeCode.RB1m)) {
      Stream<Item> stream = realisedSpreadSecurities.parallelStream().map(a -> {
        Item item = new Item();
        item.setDate(a.getOccDate());
        item.setValue(a.getRealisedSpread1minBps());
        return item;
      });
      String legendName = legendName(IndexTypeCode.RB1m, name);
      addItem(items, stream, legendName);
    }
    if (indexType.contains(IndexTypeCode.RB5m)) {
      Stream<Item> stream = realisedSpreadSecurities.parallelStream().map(a -> {
        Item item = new Item();
        item.setDate(a.getOccDate());
        item.setValue(a.getRealisedSpread5minBps());
        return item;
      });
      String legendName = legendName(IndexTypeCode.RB5m, name);
      addItem(items, stream, legendName);
    }
    if (indexType.contains(IndexTypeCode.RB10m)) {
      Stream<Item> stream = realisedSpreadSecurities.parallelStream().map(a -> {
        Item item = new Item();
        item.setDate(a.getOccDate());
        item.setValue(a.getRealisedSpread10minBps());
        return item;
      });
      String legendName = legendName(IndexTypeCode.RB10m, name);
      addItem(items, stream, legendName);
    }
  }

  private String legendName(String code, String name) {
    String legend = StringUtil.EMPTY;
    switch (code) {
      case IndexTypeCode.AA10s:
        legend = "自相关10秒";
        break;
      case IndexTypeCode.AA60s:
        legend = "自相关10秒";
        break;
      case IndexTypeCode.RA1s:
        legend = "基点实现价差1秒";
        break;
      case IndexTypeCode.RA10s:
        legend = "基点实现价差10秒";
        break;
      case IndexTypeCode.RA30s:
        legend = "基点实现价差30秒";
        break;
      case IndexTypeCode.RA1m:
        legend = "基点实现价差1分钟";
        break;
      case IndexTypeCode.RA5m:
        legend = "基点实现价差5分钟";
        break;
      case IndexTypeCode.RA10m:
        legend = "基点实现价差10分钟";
        break;
      case IndexTypeCode.RB1s:
        legend = "绝对实现价差1秒";
        break;
      case IndexTypeCode.RB10s:
        legend = "绝对实现价差10秒";
        break;
      case IndexTypeCode.RB30s:
        legend = "绝对实现价差30秒";
        break;
      case IndexTypeCode.RB1m:
        legend = "绝对实现价差1分钟";
        break;
      case IndexTypeCode.RB5m:
        legend = "绝对实现价差5分钟";
        break;
      case IndexTypeCode.RB10m:
        legend = "绝对实现价差10分钟";
        break;
      case IndexTypeCode.QA:
        legend = "基点报价价差";
        break;
      case IndexTypeCode.QB:
        legend = "绝对报价价差";
        break;
      case IndexTypeCode.EA:
        legend = "基点有效价差";
        break;
      case IndexTypeCode.EB:
        legend = "绝对有效价差";
        break;
    }
    return legend + "-" + name;
  }

  private void addItem(List<IndexItem> items, Stream<Item> stream, String name) {
    List<Item> data = stream.collect(Collectors.toList());
    IndexItem indexItem = new IndexItem();
    indexItem.setName(name);
    indexItem.setItem(data);
    items.add(indexItem);
  }

}
