package com.hzizs.analysis.dao;

import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.analysis.entity.SysFile;
/**
 * 系统文件 数据库操作类	 
 * @author crazy_cabbage
 *
 */
public interface SysFileDao extends MyBatisDao<SysFile> {

}
