package com.hzizs.analysis.service.impl;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.hzizs.Assert;
import com.hzizs.analysis.dao.I18nDao;
import com.hzizs.analysis.entity.I18n;
import com.hzizs.analysis.service.I18nService;
import com.hzizs.compress.jdk.util.JdkZipUtil;
import com.hzizs.json.JsonUtil;
import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.mybatis.service.impl.MyBatisServiceImpl;
/**
 * 国际化 服务实现类
 * @author crazy_cabbage
 *
 */
public class I18nServiceImpl extends MyBatisServiceImpl<I18n> implements I18nService {
	

	private I18nDao i18nDao;
	public void setI18nDao(I18nDao i18nDao) {
		this.i18nDao = i18nDao;
	}
	@Override
	protected MyBatisDao<I18n> getDao() {
		return i18nDao;
	}
	//========================================业务方法========================================
	 @Override
	  public Map<String, Object> findMap() {
	    String key = getDao().tableName() + ":list:all:map";
	    if (bizCacheFlag.isFlag() && cacheHelper.exists(key)) {
	      String val = cacheHelper.get(key);
	      val = JdkZipUtil.uncompress(val);
	      @SuppressWarnings("unchecked")
	      Map<String, Object> root = JsonUtil.read(val, Map.class);
	      return root;
	    } else {
	      List<I18n> i18ns = findAll();
	      Map<String, Object> root = new HashMap<String, Object>();
	      Map<String, Object> zh = new HashMap<String, Object>();
	      Map<String, Object> en = new HashMap<String, Object>();
	      for (I18n i18n : i18ns) {
	        Map<String, Object> lastZh = zh;
	        Map<String, Object> lastEn = en;
	        String[] keys = i18n.getCode().split("\\.");
	        for (int i = 0; i < keys.length; i++) {
	          if (i == keys.length - 1) {
	            lastZh.put(keys[i], i18n.getName());
	            lastEn.put(keys[i], i18n.getNameEn());
	          } else {
	            @SuppressWarnings("unchecked")
	            Map<String, Object> newZh = (Map<String, Object>) lastZh.get(keys[i]);
	            @SuppressWarnings("unchecked")
	            Map<String, Object> newEn = (Map<String, Object>) lastEn.get(keys[i]);
	            if (newZh == null) {
	              newZh = new HashMap<String, Object>();
	              newEn = new HashMap<String, Object>();
	            }
	            lastZh.put(keys[i], newZh);
	            lastEn.put(keys[i], newEn);
	            lastZh = newZh;
	            lastEn = newEn;
	          }
	        }
	      }
	      root.put("zh", zh);
	      root.put("en", en);
	      if (bizCacheFlag.isFlag()) {
	        cacheHelper.put(key, JdkZipUtil.compress(JsonUtil.toString(root)));
	      }
	      return root;
	    }
	  }

	  @Override
	  public I18n findByCode(String code) {
	    Assert.assertNotEmptyString(code, message.getMessage("code.isnull"));
	    Map<String, Object> parameters = new HashMap<String, Object>(1);
	    parameters.put("code", code);
	    return findOne(parameters);
	  }
}
