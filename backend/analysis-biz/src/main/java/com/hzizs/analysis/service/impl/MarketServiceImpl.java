package com.hzizs.analysis.service.impl;
import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.mybatis.service.impl.MyBatisServiceImpl;


import com.hzizs.analysis.dao.MarketDao;
import com.hzizs.analysis.entity.Market;
import com.hzizs.analysis.service.MarketService;
/**
 * 市场 服务实现类
 * @author crazy_cabbage
 *
 */
public class MarketServiceImpl extends MyBatisServiceImpl<Market> implements MarketService {
	

	private MarketDao marketDao;
	public void setMarketDao(MarketDao marketDao) {
		this.marketDao = marketDao;
	}
	@Override
	protected MyBatisDao<Market> getDao() {
		return marketDao;
	}
	//========================================业务方法========================================
}
