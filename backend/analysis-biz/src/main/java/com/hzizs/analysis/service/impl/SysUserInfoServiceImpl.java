package com.hzizs.analysis.service.impl;

import com.hzizs.analysis.dao.SysUserInfoDao;
import com.hzizs.analysis.entity.SysUserInfo;
import com.hzizs.analysis.service.SysUserInfoService;
import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.mybatis.service.impl.MyBatisServiceImpl;
import com.hzizs.util.BeanUtil;

/**
 * 系统用户信息 服务实现类
 * 
 * @author crazy_cabbage
 *
 */
public class SysUserInfoServiceImpl extends MyBatisServiceImpl<SysUserInfo> implements SysUserInfoService {


  private SysUserInfoDao sysUserInfoDao;

  public void setSysUserInfoDao(SysUserInfoDao sysUserInfoDao) {
    this.sysUserInfoDao = sysUserInfoDao;
  }

  @Override
  protected MyBatisDao<SysUserInfo> getDao() {
    return sysUserInfoDao;
  }

  // ========================================业务方法========================================
  @Override
  public void updateInfo(SysUserInfo userInfo) {
    SysUserInfo po = findById(userInfo.getId());
    BeanUtil.copyExclude(userInfo, po, "id", "createTime", "isDel", "isManager", "lockTime", "username");
    update(po);
  }

  @Override
  public void updateInfo2(SysUserInfo userInfo) {
    SysUserInfo po = findById(userInfo.getId());
    BeanUtil.copyExclude(userInfo, po, "id", "createTime", "enabled", "isDel", "isManager", "lockTime", "username");
    update(po);
  }
}
