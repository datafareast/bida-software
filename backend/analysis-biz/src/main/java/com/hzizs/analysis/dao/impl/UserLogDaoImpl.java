package com.hzizs.analysis.dao.impl;
import java.util.List;

import com.fasterxml.jackson.core.type.TypeReference;
import com.hzizs.mybatis.dao.impl.MyBatisDaoImpl;
import com.hzizs.analysis.dao.UserLogDao;
import com.hzizs.analysis.entity.UserLog;
/**
 * 用户日志 数据库操作类实现类	 
 * @author crazy_cabbage
 *
 */
public class UserLogDaoImpl extends MyBatisDaoImpl<UserLog> implements UserLogDao {



	@Override
	public TypeReference<List<UserLog>> getListEntityType() {
		return new TypeReference<List<UserLog>>() {
		};
	}
	//========================================业务方法========================================
}
