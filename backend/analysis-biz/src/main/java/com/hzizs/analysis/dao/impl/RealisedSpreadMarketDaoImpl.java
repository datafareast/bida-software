package com.hzizs.analysis.dao.impl;
import java.util.List;

import com.fasterxml.jackson.core.type.TypeReference;
import com.hzizs.mybatis.dao.impl.MyBatisDaoImpl;
import com.hzizs.analysis.dao.RealisedSpreadMarketDao;
import com.hzizs.analysis.entity.RealisedSpreadMarket;
/**
 * 市场实现价差 数据库操作类实现类	 
 * @author crazy_cabbage
 *
 */
public class RealisedSpreadMarketDaoImpl extends MyBatisDaoImpl<RealisedSpreadMarket> implements RealisedSpreadMarketDao {



	@Override
	public TypeReference<List<RealisedSpreadMarket>> getListEntityType() {
		return new TypeReference<List<RealisedSpreadMarket>>() {
		};
	}
	//========================================业务方法========================================
}
