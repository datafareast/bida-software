package com.hzizs.analysis.dao;

import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.analysis.entity.Area;

/**
 * 区域 数据库操作类
 * 
 * @author crazy_cabbage
 *
 */
public interface AreaDao extends MyBatisDao<Area> {

}
