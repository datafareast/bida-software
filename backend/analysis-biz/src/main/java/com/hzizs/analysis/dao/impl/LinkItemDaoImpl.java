package com.hzizs.analysis.dao.impl;
import java.util.List;

import com.fasterxml.jackson.core.type.TypeReference;
import com.hzizs.mybatis.dao.impl.MyBatisDaoImpl;
import com.hzizs.analysis.dao.LinkItemDao;
import com.hzizs.analysis.entity.LinkItem;
/**
 * 友情链接条目 数据库操作类实现类	 
 * @author crazy_cabbage
 *
 */
public class LinkItemDaoImpl extends MyBatisDaoImpl<LinkItem> implements LinkItemDao {



	@Override
	public TypeReference<List<LinkItem>> getListEntityType() {
		return new TypeReference<List<LinkItem>>() {
		};
	}
	//========================================业务方法========================================
}
