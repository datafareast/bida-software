package com.hzizs.analysis.service.impl;

import java.util.HashMap;
import java.util.Map;
import com.hzizs.Assert;
import com.hzizs.analysis.dao.SysUserDao;
import com.hzizs.analysis.entity.SysUser;
import com.hzizs.analysis.service.SysUserService;
import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.mybatis.service.impl.MyBatisServiceImpl;

/**
 * 系统用户 服务实现类
 * 
 * @author crazy_cabbage
 *
 */
public class SysUserServiceImpl extends MyBatisServiceImpl<SysUser> implements SysUserService {


  private SysUserDao sysUserDao;

  public void setSysUserDao(SysUserDao sysUserDao) {
    this.sysUserDao = sysUserDao;
  }

  @Override
  protected MyBatisDao<SysUser> getDao() {
    return sysUserDao;
  }

  // ========================================业务方法========================================
  @Override
  public SysUser findByUsername(String username) {
    Assert.assertNotEmptyString(username, message.getMessage("username.isnull"));
    Map<String, Object> parameters = new HashMap<String, Object>(1);
    parameters.put("username", username);
    return findOne(parameters);
  }
}
