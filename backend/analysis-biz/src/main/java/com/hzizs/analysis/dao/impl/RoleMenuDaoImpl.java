package com.hzizs.analysis.dao.impl;
import java.util.List;

import com.fasterxml.jackson.core.type.TypeReference;
import com.hzizs.mybatis.dao.impl.MyBatisDaoImpl;
import com.hzizs.analysis.dao.RoleMenuDao;
import com.hzizs.analysis.entity.RoleMenu;
/**
 * 角色菜单 数据库操作类实现类	 
 * @author crazy_cabbage
 *
 */
public class RoleMenuDaoImpl extends MyBatisDaoImpl<RoleMenu> implements RoleMenuDao {



	@Override
	public TypeReference<List<RoleMenu>> getListEntityType() {
		return new TypeReference<List<RoleMenu>>() {
		};
	}
	//========================================业务方法========================================
}
