package com.hzizs.analysis.service.impl;
import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.mybatis.service.impl.MyBatisServiceImpl;


import com.hzizs.analysis.dao.ImitatePositionItemDao;
import com.hzizs.analysis.entity.ImitatePositionItem;
import com.hzizs.analysis.service.ImitatePositionItemService;
/**
 * 模拟持仓条目 服务实现类
 * @author crazy_cabbage
 *
 */
public class ImitatePositionItemServiceImpl extends MyBatisServiceImpl<ImitatePositionItem> implements ImitatePositionItemService {
	

	private ImitatePositionItemDao imitatePositionItemDao;
	public void setImitatePositionItemDao(ImitatePositionItemDao imitatePositionItemDao) {
		this.imitatePositionItemDao = imitatePositionItemDao;
	}
	@Override
	protected MyBatisDao<ImitatePositionItem> getDao() {
		return imitatePositionItemDao;
	}
	//========================================业务方法========================================
}
