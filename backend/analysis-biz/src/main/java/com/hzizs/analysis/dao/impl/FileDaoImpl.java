package com.hzizs.analysis.dao.impl;
import java.util.List;

import com.fasterxml.jackson.core.type.TypeReference;
import com.hzizs.mybatis.dao.impl.MyBatisShardingDaoImpl;
import com.hzizs.analysis.dao.FileDao;
import com.hzizs.analysis.entity.File;
/**
 * 文件 数据库操作类实现类	 
 * @author crazy_cabbage
 *
 */
public class FileDaoImpl extends MyBatisShardingDaoImpl<File> implements FileDao {



	@Override
	public TypeReference<List<File>> getListEntityType() {
		return new TypeReference<List<File>>() {
		};
	}
	//========================================业务方法========================================

  @Override
  public String shardingFieldName() {
    return "userId";
  }
 
}
