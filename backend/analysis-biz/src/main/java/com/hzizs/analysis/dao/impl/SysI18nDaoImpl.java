package com.hzizs.analysis.dao.impl;
import java.util.List;

import com.fasterxml.jackson.core.type.TypeReference;
import com.hzizs.mybatis.dao.impl.MyBatisDaoImpl;
import com.hzizs.analysis.dao.SysI18nDao;
import com.hzizs.analysis.entity.SysI18n;
/**
 * 系统国际化 数据库操作类实现类	 
 * @author crazy_cabbage
 *
 */
public class SysI18nDaoImpl extends MyBatisDaoImpl<SysI18n> implements SysI18nDao {



	@Override
	public TypeReference<List<SysI18n>> getListEntityType() {
		return new TypeReference<List<SysI18n>>() {
		};
	}
	//========================================业务方法========================================
}
