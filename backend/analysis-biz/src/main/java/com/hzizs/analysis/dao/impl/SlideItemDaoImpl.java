package com.hzizs.analysis.dao.impl;
import java.util.List;

import com.fasterxml.jackson.core.type.TypeReference;
import com.hzizs.mybatis.dao.impl.MyBatisDaoImpl;
import com.hzizs.analysis.dao.SlideItemDao;
import com.hzizs.analysis.entity.SlideItem;
/**
 * 轮播图条目 数据库操作类实现类	 
 * @author crazy_cabbage
 *
 */
public class SlideItemDaoImpl extends MyBatisDaoImpl<SlideItem> implements SlideItemDao {



	@Override
	public TypeReference<List<SlideItem>> getListEntityType() {
		return new TypeReference<List<SlideItem>>() {
		};
	}
	//========================================业务方法========================================
}
