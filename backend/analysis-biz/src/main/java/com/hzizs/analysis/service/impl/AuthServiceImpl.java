package com.hzizs.analysis.service.impl;

import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.mybatis.service.impl.MyBatisServiceImpl;
import java.util.HashMap;
import java.util.Map;
import com.hzizs.Assert;
import com.hzizs.analysis.dao.AuthDao;
import com.hzizs.analysis.entity.Auth;
import com.hzizs.analysis.service.AuthService;

/**
 * 权限 服务实现类
 * 
 * @author crazy_cabbage
 *
 */
public class AuthServiceImpl extends MyBatisServiceImpl<Auth> implements AuthService {


  private AuthDao authDao;

  public void setAuthDao(AuthDao authDao) {
    this.authDao = authDao;
  }

  @Override
  protected MyBatisDao<Auth> getDao() {
    return authDao;
  }

  // ========================================业务方法========================================
  @Override
  public Auth findByCode(String code) {
    Assert.assertNotEmptyString(code, message.getMessage("code.isnull"));
    Map<String, Object> parameters = new HashMap<String, Object>(1);
    parameters.put("code", code);
    return findOne(parameters);
  }
}
