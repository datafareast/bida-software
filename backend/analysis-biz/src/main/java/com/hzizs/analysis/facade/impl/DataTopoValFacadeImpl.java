package com.hzizs.analysis.facade.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import com.hzizs.Assert;
import com.hzizs.analysis.entity.DataTopo;
import com.hzizs.analysis.entity.DataTopoVal;
import com.hzizs.analysis.facade.DataTopoValFacade;
import com.hzizs.analysis.service.DataTopoService;
import com.hzizs.analysis.service.DataTopoValService;
import com.hzizs.constants.SymbolConstants;
import com.hzizs.facade.impl.BaseFacadeImpl;
import com.hzizs.util.StringUtil;

public class DataTopoValFacadeImpl extends BaseFacadeImpl implements DataTopoValFacade {

  private DataTopoService dataTopoService;
  private  DataTopoValService dataTopoValService;
  public void setDataTopoService(DataTopoService dataTopoService) {
    this.dataTopoService = dataTopoService;
  }
  public void setDataTopoValService(DataTopoValService dataTopoValService) {
    this.dataTopoValService = dataTopoValService;
  }

  // =======================================业务方法==============================

  @Override
  public void save(DataTopoVal dataTopoVal) {
    Long dataTopoId = dataTopoVal.getDataTopoId();
    Long dataTopoParentId = dataTopoVal.getDataTopoParentId();
    Long dataTypeId = dataTopoVal.getDataTypeId();
    Date occTime = dataTopoVal.getOccTime();
    Assert.assertNotNull(occTime, message.getMessage("occTime.isnull"));
    Assert.assertNotNull(dataTypeId, message.getMessage("dataTypeId.isnull"));
    Assert.assertNotNull(dataTopoId, message.getMessage("dataTopoId.isnull"));
    Assert.assertNotNull(dataTopoParentId, message.getMessage("dataTopoParentId.isnull"));
    DataTopoVal po =dataTopoValService.find(dataTypeId,dataTopoId,dataTopoParentId,occTime);
    if(po!=null) {
      Assert.customException(message.getMessage("dataTopoVal.exists"));
    }
    DataTopo topo = dataTopoService.findById(dataTopoId);
    String parentIds = topo.getParentIds();
    if(StringUtil.isEmpty(parentIds)) {
      Assert.customException(message.getMessage("dataTopoParentId.error"));
    }
    List<String> pIds = Arrays.asList(parentIds.split(SymbolConstants.COMMA));
    if(!pIds.contains(dataTopoParentId+"")) {
      Assert.customException(message.getMessage("dataTopoParentId.error"));
    }
    dataTopoVal.setCode(topo.getCode());
    dataTopoValService.save(dataTopoVal);
  }

  @Override
  public void update(DataTopoVal dataTopoVal) {
    Long dataTopoId = dataTopoVal.getDataTopoId();
    Long dataTopoParentId = dataTopoVal.getDataTopoParentId();
    Long dataTypeId = dataTopoVal.getDataTypeId();
    Date occTime = dataTopoVal.getOccTime();
    Assert.assertNotNull(occTime, message.getMessage("occTime.isnull"));
    Assert.assertNotNull(dataTypeId, message.getMessage("dataTypeId.isnull"));
    Assert.assertNotNull(dataTopoId, message.getMessage("dataTopoId.isnull"));
    Assert.assertNotNull(dataTopoParentId, message.getMessage("dataTopoParentId.isnull"));
    DataTopoVal po =dataTopoValService.find(dataTypeId,dataTopoId,dataTopoParentId,occTime);
    if(po!=null&&!po.getId().equals(dataTopoVal.getId())) {
      Assert.customException(message.getMessage("dataTopoVal.exists"));
    }
    DataTopo topo = dataTopoService.findById(dataTopoId);
    String parentIds = topo.getParentIds();
    if(StringUtil.isEmpty(parentIds)) {
      Assert.customException(message.getMessage("dataTopoParentId.error"));
    }
    List<String> pIds = Arrays.asList(parentIds.split(SymbolConstants.COMMA));
    if(!pIds.contains(dataTopoParentId+"")) {
      Assert.customException(message.getMessage("dataTopoParentId.error"));
    }
    dataTopoVal.setCode(topo.getCode());
    dataTopoValService.update(dataTopoVal);
  }
  @Override
  public List<Map<String, Object>> findData(Long dataTopoId) {
    List<DataTopoVal> dataTopoVals = dataTopoValService.findByDataTopoId(dataTopoId);
    List<Map<String, Object>> list = new ArrayList<Map<String,Object>>();
    //日期分组，合并成数据
    dataTopoVals.parallelStream().collect(Collectors.groupingBy(DataTopoVal::getOccTime)).forEach((k,v)->{
      Map<String, Object> map = new HashMap<String,Object>();
      map.put("occTime", k);
      v.parallelStream().forEach(a->{
        map.put(a.getDataTopoParentId()+"", a.getVal());
      });
      list.add(map);
    });
    return list;
  }

}
