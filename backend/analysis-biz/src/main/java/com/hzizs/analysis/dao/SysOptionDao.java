package com.hzizs.analysis.dao;

import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.analysis.entity.SysOption;
/**
 * 数据选项 数据库操作类	 
 * @author crazy_cabbage
 *
 */
public interface SysOptionDao extends MyBatisDao<SysOption> {

}
