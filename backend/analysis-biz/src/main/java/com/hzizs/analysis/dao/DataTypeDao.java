package com.hzizs.analysis.dao;

import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.analysis.entity.DataType;
/**
 * 数据种类 数据库操作类	 
 * @author crazy_cabbage
 *
 */
public interface DataTypeDao extends MyBatisDao<DataType> {

}
