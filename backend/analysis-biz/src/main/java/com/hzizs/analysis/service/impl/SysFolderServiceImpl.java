package com.hzizs.analysis.service.impl;
import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.mybatis.service.impl.MyBatisServiceImpl;


import com.hzizs.analysis.dao.SysFolderDao;
import com.hzizs.analysis.entity.SysFolder;
import com.hzizs.analysis.service.SysFolderService;
/**
 * 系统文件夹 服务实现类
 * @author crazy_cabbage
 *
 */
public class SysFolderServiceImpl extends MyBatisServiceImpl<SysFolder> implements SysFolderService {
	

	private SysFolderDao sysFolderDao;
	public void setSysFolderDao(SysFolderDao sysFolderDao) {
		this.sysFolderDao = sysFolderDao;
	}
	@Override
	protected MyBatisDao<SysFolder> getDao() {
		return sysFolderDao;
	}
	//========================================业务方法========================================
}
