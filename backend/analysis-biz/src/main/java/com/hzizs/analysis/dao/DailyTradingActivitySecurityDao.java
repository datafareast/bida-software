package com.hzizs.analysis.dao;

import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.analysis.entity.DailyTradingActivitySecurity;
/**
 * 证券日交易 数据库操作类	 
 * @author crazy_cabbage
 *
 */
public interface DailyTradingActivitySecurityDao extends MyBatisDao<DailyTradingActivitySecurity> {

}
