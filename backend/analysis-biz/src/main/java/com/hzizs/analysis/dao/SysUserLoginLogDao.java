package com.hzizs.analysis.dao;

import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.analysis.entity.SysUserLoginLog;
/**
 * 系统用户登录日志 数据库操作类	 
 * @author crazy_cabbage
 *
 */
public interface SysUserLoginLogDao extends MyBatisDao<SysUserLoginLog> {

}
