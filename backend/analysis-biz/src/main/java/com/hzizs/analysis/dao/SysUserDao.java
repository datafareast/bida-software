package com.hzizs.analysis.dao;

import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.analysis.entity.SysUser;
/**
 * 系统用户 数据库操作类	 
 * @author crazy_cabbage
 *
 */
public interface SysUserDao extends MyBatisDao<SysUser> {

}
