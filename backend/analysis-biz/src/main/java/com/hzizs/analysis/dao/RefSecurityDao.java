package com.hzizs.analysis.dao;

import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.analysis.entity.RefSecurity;
/**
 * 证券参考数据 数据库操作类	 
 * @author crazy_cabbage
 *
 */
public interface RefSecurityDao extends MyBatisDao<RefSecurity> {

}
