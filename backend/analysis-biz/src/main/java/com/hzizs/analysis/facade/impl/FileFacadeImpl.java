package com.hzizs.analysis.facade.impl;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.http.Consts;
import com.hzizs.Assert;
import com.hzizs.analysis.entity.File;
import com.hzizs.analysis.entity.Folder;
import com.hzizs.analysis.facade.FileFacade;
import com.hzizs.analysis.service.FileService;
import com.hzizs.analysis.service.FolderService;
import com.hzizs.analysis.service.SysOptionService;
import com.hzizs.facade.impl.BaseFacadeImpl;
import com.hzizs.json.JsonUtil;
import com.hzizs.net.pojo.NetRet;
import com.hzizs.net.util.HttpClientUtil;
import com.hzizs.pojo.ErrorInfo;

public class FileFacadeImpl extends BaseFacadeImpl implements FileFacade {
   private FileService fileService;
   private FolderService folderService;
   private SysOptionService sysOptionService;
   
   
  public void setFileService(FileService fileService) {
    this.fileService = fileService;
  }

  public void setFolderService(FolderService folderService) {
    this.folderService = folderService;
  }

  public void setSysOptionService(SysOptionService sysOptionService) {
    this.sysOptionService = sysOptionService;
  }

  //======================================================================
  @Override
  public void deleteByIds(Long[] ids, Long userId) {
    if(ids!=null) {
      for(Long id:ids) {
        deleteById(id, userId);
      }
    }
    
  }

  @Override
  public void deleteById(Long id, Long userId) {
    File file = fileService.findById(id,userId);
    if(file==null) {
      return ;
    }
    String uploadServer = sysOptionService.findUploadServer();
    Map<String, List<String>> map = new HashMap<String,List<String>>();
    map.put("filename", Arrays.asList(file.getPath()));
    NetRet netRet = HttpClientUtil.post(uploadServer+"/del", Consts.UTF_8, map);
    ErrorInfo errorInfo = JsonUtil.read(netRet.getVal(), ErrorInfo.class);
    if(!errorInfo.isFlag()) {
      Assert.customException(message.getMessage("file.del.err"));
    }
    fileService.deleteById(id,userId);
  }

  @Override
  public void updateMove(Long[] ids, Long folderId, Long userId) {
    Folder folder = folderService.findById(folderId,userId);
    Assert.assertNotNull(folder, message.getMessage("folderId.err"));
    if(ids!=null) {
      for(Long id:ids) {
        File file = fileService.findById(id,userId);
        file.setFolderId(folderId);
        fileService.update(file,userId);
      }
    }
  }

}
