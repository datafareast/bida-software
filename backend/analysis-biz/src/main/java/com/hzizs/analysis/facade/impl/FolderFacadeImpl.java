package com.hzizs.analysis.facade.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import com.hzizs.Assert;
import com.hzizs.analysis.constants.Constants.FolderCat;
import com.hzizs.analysis.entity.File;
import com.hzizs.analysis.entity.Folder;
import com.hzizs.analysis.facade.FolderFacade;
import com.hzizs.analysis.service.FileService;
import com.hzizs.analysis.service.FolderService;
import com.hzizs.analysis.service.SysOptionService;
import com.hzizs.constants.CommonConstants;
import com.hzizs.facade.impl.BaseFacadeImpl;

public class FolderFacadeImpl extends BaseFacadeImpl implements FolderFacade {
  private FolderService folderService;
  private FileService fileService;
  private SysOptionService sysOptionService;

  public void setFolderService(FolderService folderService) {
    this.folderService = folderService;
  }

  public void setFileService(FileService fileService) {
    this.fileService = fileService;
  }

  public void setSysOptionService(SysOptionService sysOptionService) {
    this.sysOptionService = sysOptionService;
  }

  // =============================================业务方法==================================
  @Override
  public void deleteByIds(Long[] ids, Long userId) {
    if (ids != null) {
      for (Long id : ids) {
        deleteById(id, userId);
      }
    }
  }

  @Override
  public void deleteById(Long id, Long userId) {
    Folder folder = folderService.findById(id,userId);
    if(!FolderCat.DIY.equals(folder.getCat())) {
      return ;
    }
    Long folderId = folderService.findDefault(userId);
    //指下面的文件移动默认文件夹下面，200个文件，查一次，避免有人存了N个文件，导致查询 ，查死
    Map<String, Object> parameters = new HashMap<String,Object>(6);
    parameters.put("folderId", id);
    parameters.put("userId", userId);
    parameters.put(CommonConstants.FIRST_RESULT, 0);
    parameters.put(CommonConstants.OFFSET, 200);
    parameters.put(CommonConstants.MAX_RESULT,200);
    List<File> files = fileService.find(parameters,userId);
    while(!files.isEmpty()) {
      for(File file:files) {
        file.setFolderId(folderId);
        fileService.update(file,userId);
      }
      files = fileService.find(parameters,userId);
    }
     folderService.deleteById(id,userId);
  }

  @Override
  public void save(Folder folder, Long userId) {
    //文件夹的数量要控制
    List<Folder> folders = folderService.findAll(userId);
    int foldersNum = sysOptionService.findFolderNum();
    if(foldersNum <= folders.size()) {
      Assert.customException(message.getMessage("folder.isfull"));
    }
    Set<String> names = folders.parallelStream().map(Folder::getName).collect(Collectors.toSet());
    String name =folder.getName();
    String tmpName = name;
    int i = 1;
     while(names.contains(tmpName)) {
       i++;
       tmpName = name+i;
     }
     folder.setName(tmpName);
     folderService.save(folder,userId);

  }

  @Override
  public void update(Folder folder, Long userId) {
    Long id = folder.getId();
    String name = folder.getName();
    Folder po = folderService.findById(id,userId);
    if(po.getName().equals(name)) {
      return;
    }
    List<Folder> folders = folderService.findAll(userId);
    Set<String> names = folders.parallelStream().map(Folder::getName).collect(Collectors.toSet());
    if(names.contains(name)) {
      Assert.customException(message.getMessage("name.exists"));
    }
    po.setName(name);
    folderService.update(po,userId);
  }

}
