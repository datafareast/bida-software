package com.hzizs.analysis.dao.impl;
import java.util.List;

import com.fasterxml.jackson.core.type.TypeReference;
import com.hzizs.mybatis.dao.impl.MyBatisDaoImpl;
import com.hzizs.analysis.dao.RoleAuthDao;
import com.hzizs.analysis.entity.RoleAuth;
/**
 * 角色权限 数据库操作类实现类	 
 * @author crazy_cabbage
 *
 */
public class RoleAuthDaoImpl extends MyBatisDaoImpl<RoleAuth> implements RoleAuthDao {



	@Override
	public TypeReference<List<RoleAuth>> getListEntityType() {
		return new TypeReference<List<RoleAuth>>() {
		};
	}
	//========================================业务方法========================================
}
