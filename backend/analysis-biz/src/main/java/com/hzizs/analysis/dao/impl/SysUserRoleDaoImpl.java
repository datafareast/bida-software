package com.hzizs.analysis.dao.impl;
import java.util.List;

import com.fasterxml.jackson.core.type.TypeReference;
import com.hzizs.mybatis.dao.impl.MyBatisDaoImpl;
import com.hzizs.analysis.dao.SysUserRoleDao;
import com.hzizs.analysis.entity.SysUserRole;
/**
 * 系统用户角色 数据库操作类实现类	 
 * @author crazy_cabbage
 *
 */
public class SysUserRoleDaoImpl extends MyBatisDaoImpl<SysUserRole> implements SysUserRoleDao {



	@Override
	public TypeReference<List<SysUserRole>> getListEntityType() {
		return new TypeReference<List<SysUserRole>>() {
		};
	}
	//========================================业务方法========================================
}
