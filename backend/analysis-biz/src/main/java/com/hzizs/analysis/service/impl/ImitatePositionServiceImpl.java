package com.hzizs.analysis.service.impl;
import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.mybatis.service.impl.MyBatisServiceImpl;


import com.hzizs.analysis.dao.ImitatePositionDao;
import com.hzizs.analysis.entity.ImitatePosition;
import com.hzizs.analysis.service.ImitatePositionService;
/**
 * 模拟持仓 服务实现类
 * @author crazy_cabbage
 *
 */
public class ImitatePositionServiceImpl extends MyBatisServiceImpl<ImitatePosition> implements ImitatePositionService {
	

	private ImitatePositionDao imitatePositionDao;
	public void setImitatePositionDao(ImitatePositionDao imitatePositionDao) {
		this.imitatePositionDao = imitatePositionDao;
	}
	@Override
	protected MyBatisDao<ImitatePosition> getDao() {
		return imitatePositionDao;
	}
	//========================================业务方法========================================
}
