package com.hzizs.analysis.dao;

import com.hzizs.analysis.entity.File;
import com.hzizs.mybatis.dao.MyBatisShardingDao;
/**
 * 文件 数据库操作类	 
 * @author crazy_cabbage
 *
 */
public interface FileDao extends MyBatisShardingDao<File> {

}
