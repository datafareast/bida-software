package com.hzizs.analysis.dao;

import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.analysis.entity.Market;
/**
 * 市场 数据库操作类	 
 * @author crazy_cabbage
 *
 */
public interface MarketDao extends MyBatisDao<Market> {

}
