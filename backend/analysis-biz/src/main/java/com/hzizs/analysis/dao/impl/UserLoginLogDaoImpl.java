package com.hzizs.analysis.dao.impl;
import java.util.List;

import com.fasterxml.jackson.core.type.TypeReference;
import com.hzizs.mybatis.dao.impl.MyBatisDaoImpl;
import com.hzizs.analysis.dao.UserLoginLogDao;
import com.hzizs.analysis.entity.UserLoginLog;
/**
 * 用户登录日志 数据库操作类实现类	 
 * @author crazy_cabbage
 *
 */
public class UserLoginLogDaoImpl extends MyBatisDaoImpl<UserLoginLog> implements UserLoginLogDao {



	@Override
	public TypeReference<List<UserLoginLog>> getListEntityType() {
		return new TypeReference<List<UserLoginLog>>() {
		};
	}
	//========================================业务方法========================================
}
