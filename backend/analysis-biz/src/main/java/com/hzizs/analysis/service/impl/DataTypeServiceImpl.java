package com.hzizs.analysis.service.impl;
import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.mybatis.service.impl.MyBatisServiceImpl;
import java.util.HashMap;
import java.util.Map;
import com.hzizs.Assert;
import com.hzizs.analysis.dao.DataTypeDao;
import com.hzizs.analysis.entity.DataType;
import com.hzizs.analysis.service.DataTypeService;
/**
 * 数据种类 服务实现类
 * @author crazy_cabbage
 *
 */
public class DataTypeServiceImpl extends MyBatisServiceImpl<DataType> implements DataTypeService {
	

	private DataTypeDao dataTypeDao;
	public void setDataTypeDao(DataTypeDao dataTypeDao) {
		this.dataTypeDao = dataTypeDao;
	}
	@Override
	protected MyBatisDao<DataType> getDao() {
		return dataTypeDao;
	}
	//========================================业务方法========================================
  @Override
  public long countByDataIndustryId(Long dataIndustryId) {
    Assert.assertNotNull(dataIndustryId, message.getMessage("dataIndustryId.isnull"));
    Map<String, Object> parameters = new HashMap<String,Object>(1);
    parameters.put("dataIndustryId", dataIndustryId);
    return count(parameters);
  }
  @Override
  public DataType findByNameDataIndustryId(String name, Long dataIndustryId) {
    Assert.assertNotEmptyString(name, message.getMessage("name.isnull"));
    Assert.assertNotNull(dataIndustryId, message.getMessage("dataIndustryId.isnull"));
    Map<String, Object> parameters = new HashMap<String, Object>(2);
    parameters.put("name", name);
    parameters.put("dataIndustryId", dataIndustryId);
    return findOne(parameters);
  }
}
