package com.hzizs.analysis.facade.impl;

import com.hzizs.Assert;
import com.hzizs.analysis.facade.SysDataFacade;
import com.hzizs.analysis.service.SysDataService;
import com.hzizs.facade.impl.BaseFacadeImpl;

public class SysDataFacadeImpl extends BaseFacadeImpl implements SysDataFacade {
private SysDataService sysDataService;

  
  public void setSysDataService(SysDataService sysDataService) {
  this.sysDataService = sysDataService;
}

  //===================================================================
  @Override
  public void deleteByIds(Long[] ids) {
    if(ids!=null) {
      for(Long id:ids) {
        deleteById(id);
      }
    }

  }

  @Override
  public void deleteById(Long id) {
    long num = sysDataService.countByParentId(id);
    if(num >0) {
      Assert.customException(message.getMessage("sysData.hasChild"));
    }
    sysDataService.deleteById(id);
    

  }

}
