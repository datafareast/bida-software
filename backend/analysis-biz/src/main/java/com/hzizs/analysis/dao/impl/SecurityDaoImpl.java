package com.hzizs.analysis.dao.impl;
import java.util.List;

import com.fasterxml.jackson.core.type.TypeReference;
import com.hzizs.mybatis.dao.impl.MyBatisDaoImpl;
import com.hzizs.analysis.dao.SecurityDao;
import com.hzizs.analysis.entity.Security;
/**
 * 证券 数据库操作类实现类	 
 * @author crazy_cabbage
 *
 */
public class SecurityDaoImpl extends MyBatisDaoImpl<Security> implements SecurityDao {



	@Override
	public TypeReference<List<Security>> getListEntityType() {
		return new TypeReference<List<Security>>() {
		};
	}
	//========================================业务方法========================================
}
