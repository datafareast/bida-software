package com.hzizs.analysis.dao.impl;
import java.util.List;

import com.fasterxml.jackson.core.type.TypeReference;
import com.hzizs.mybatis.dao.impl.MyBatisDaoImpl;
import com.hzizs.analysis.dao.SysUserDao;
import com.hzizs.analysis.entity.SysUser;
/**
 * 系统用户 数据库操作类实现类	 
 * @author crazy_cabbage
 *
 */
public class SysUserDaoImpl extends MyBatisDaoImpl<SysUser> implements SysUserDao {



	@Override
	public TypeReference<List<SysUser>> getListEntityType() {
		return new TypeReference<List<SysUser>>() {
		};
	}
	//========================================业务方法========================================
}
