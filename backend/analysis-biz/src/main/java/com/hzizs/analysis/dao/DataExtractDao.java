package com.hzizs.analysis.dao;

import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.analysis.entity.DataExtract;
/**
 * 数据抽取 数据库操作类	 
 * @author crazy_cabbage
 *
 */
public interface DataExtractDao extends MyBatisDao<DataExtract> {

}
