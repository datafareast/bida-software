package com.hzizs.analysis.service.impl;
import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.mybatis.service.impl.MyBatisServiceImpl;
import java.util.HashMap;
import java.util.Map;
import com.hzizs.Assert;
import com.hzizs.analysis.dao.ResearchReportDao;
import com.hzizs.analysis.entity.ResearchReport;
import com.hzizs.analysis.service.ResearchReportService;
/**
 * 研究报告 服务实现类
 * @author crazy_cabbage
 *
 */
public class ResearchReportServiceImpl extends MyBatisServiceImpl<ResearchReport> implements ResearchReportService {
	

	private ResearchReportDao researchReportDao;
	public void setResearchReportDao(ResearchReportDao researchReportDao) {
		this.researchReportDao = researchReportDao;
	}
	@Override
	protected MyBatisDao<ResearchReport> getDao() {
		return researchReportDao;
	}
	//========================================业务方法========================================
  @Override
  public ResearchReport findByName(String name) {
    Assert.assertNotEmptyString(name, message.getMessage("name.isnull"));
    Map<String, Object> parameters = new HashMap<String, Object>(1);
    parameters.put("name", name);
    return findOne(parameters);
  }
}
