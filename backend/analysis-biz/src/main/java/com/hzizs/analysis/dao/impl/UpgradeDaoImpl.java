package com.hzizs.analysis.dao.impl;
import java.util.List;

import com.fasterxml.jackson.core.type.TypeReference;
import com.hzizs.mybatis.dao.impl.MyBatisDaoImpl;
import com.hzizs.analysis.dao.UpgradeDao;
import com.hzizs.analysis.entity.Upgrade;
/**
 * 升级信息 数据库操作类实现类	 
 * @author crazy_cabbage
 *
 */
public class UpgradeDaoImpl extends MyBatisDaoImpl<Upgrade> implements UpgradeDao {



	@Override
	public TypeReference<List<Upgrade>> getListEntityType() {
		return new TypeReference<List<Upgrade>>() {
		};
	}
	//========================================业务方法========================================
}
