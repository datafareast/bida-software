package com.hzizs.analysis.facade.impl;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import com.hzizs.Assert;
import com.hzizs.analysis.dto.SysUserInfoDto;
import com.hzizs.analysis.entity.SysMenu;
import com.hzizs.analysis.entity.SysRole;
import com.hzizs.analysis.entity.SysRoleMenu;
import com.hzizs.analysis.entity.SysUser;
import com.hzizs.analysis.entity.SysUserInfo;
import com.hzizs.analysis.entity.SysUserLog;
import com.hzizs.analysis.entity.SysUserLoginLog;
import com.hzizs.analysis.entity.SysUserRole;
import com.hzizs.analysis.facade.SysUserFacade;
import com.hzizs.analysis.service.IpService;
import com.hzizs.analysis.service.SysMenuService;
import com.hzizs.analysis.service.SysOptionService;
import com.hzizs.analysis.service.SysRoleMenuService;
import com.hzizs.analysis.service.SysRoleService;
import com.hzizs.analysis.service.SysUserInfoService;
import com.hzizs.analysis.service.SysUserLogService;
import com.hzizs.analysis.service.SysUserLoginLogService;
import com.hzizs.analysis.service.SysUserRoleService;
import com.hzizs.analysis.service.SysUserService;
import com.hzizs.constants.Enabled;
import com.hzizs.constants.SymbolConstants;
import com.hzizs.dto.LoginLogDto;
import com.hzizs.dto.UserDto;
import com.hzizs.facade.impl.BaseFacadeImpl;
import com.hzizs.util.DateUtil;
import com.hzizs.util.prefs.Md5Util;

public class SysUserFacadeImpl extends BaseFacadeImpl implements SysUserFacade {
  private SysUserService sysUserService;
  private SysUserRoleService sysUserRoleService;
  private SysUserInfoService sysUserInfoService;
  private SysUserLogService sysUserLogService;
  private SysRoleService sysRoleService;
  private SysRoleMenuService sysRoleMenuService;
  private SysUserLoginLogService sysUserLoginLogService;
  private SysOptionService sysOptionService;
  private IpService ipService;
  private SysMenuService sysMenuService;

  
  public void setSysUserService(SysUserService sysUserService) {
    this.sysUserService = sysUserService;
  }

  public void setSysUserRoleService(SysUserRoleService sysUserRoleService) {
    this.sysUserRoleService = sysUserRoleService;
  }

  public void setSysUserInfoService(SysUserInfoService sysUserInfoService) {
    this.sysUserInfoService = sysUserInfoService;
  }

  public void setSysUserLogService(SysUserLogService sysUserLogService) {
    this.sysUserLogService = sysUserLogService;
  }

  public void setSysRoleService(SysRoleService sysRoleService) {
    this.sysRoleService = sysRoleService;
  }

  public void setSysRoleMenuService(SysRoleMenuService sysRoleMenuService) {
    this.sysRoleMenuService = sysRoleMenuService;
  }

  public void setSysUserLoginLogService(SysUserLoginLogService sysUserLoginLogService) {
    this.sysUserLoginLogService = sysUserLoginLogService;
  }

  public void setSysOptionService(SysOptionService sysOptionService) {
    this.sysOptionService = sysOptionService;
  }

  public void setIpService(IpService ipService) {
    this.ipService = ipService;
  }

  public void setSysMenuService(SysMenuService sysMenuService) {
    this.sysMenuService = sysMenuService;
  }

  //================================================================================
  @Override
  public void save(SysUserInfoDto dto) {
    SysUser user = dto.getUser();
    SysUser po = sysUserService.findByUsername(user.getUsername());
    if(po!=null) {
      Assert.customException(message.getMessage("username.exists"));
    }
    String salt = sysOptionService.findAdminSalt();
    user.setPassword(Md5Util.saltPassword(salt, user.getPassword()));
    sysUserService.save(user);
    SysUserInfo userInfo = dto.getUserInfo();
    userInfo.setId(user.getId());
    userInfo.setIsManager(Enabled.F);
    userInfo.setIsDel(Enabled.F);
    userInfo.setUsername(user.getUsername());
    userInfo.setEnabled(Enabled.T);
    sysUserInfoService.save(userInfo);
    SysUserRole userRole = dto.getUserRole();
    userRole.setUserId(user.getId());
    sysUserRoleService.save(userRole);
    SysUserLog userLog = new SysUserLog();
    userLog.setId(user.getId());
    userLog.setErrTimes(0);
    sysUserLogService.save(userLog);

  }

  @Override
  public SysUserInfoDto findById(Long id) {
    SysUserInfoDto dto = new SysUserInfoDto();
    SysUserInfo userInfo = sysUserInfoService.findById(id);
    dto.setUserInfo(userInfo);
    SysUserRole userRole = sysUserRoleService.findByUserId(id);
    dto.setUserRole(userRole);
    return dto;
  }

  @Override
  public void update(SysUserInfoDto dto) {
    SysUserInfo userInfo = dto.getUserInfo();
    Long userId = userInfo.getId();
    sysUserInfoService.updateInfo(userInfo);
    SysUserRole userRole = dto.getUserRole();
    SysUserRole userRole2 = sysUserRoleService.findByUserId(userId);
    if(!userRole2.getRoleId().equals(userRole.getRoleId())) {
      userRole2.setRoleId(userRole.getRoleId());
      sysUserRoleService.update(userRole2);
    }

  }

  @Override
  public UserDto findByUsername(String username) {
    SysUser user = sysUserService.findByUsername(username);
    if(user==null) {
      return null;
    }
    SysUserInfo userInfo = sysUserInfoService.findById(user.getId());
    UserDto dto = new UserDto();
    dto.setUserId(user.getId());
    dto.setUsername(user.getUsername());
    dto.setPassword(user.getPassword());
    if(Enabled.T.equals(userInfo.getIsManager())) {
      dto.setRole(SymbolConstants.ASTERISK);
    }else {
      SysUserRole userRole = sysUserRoleService.findByUserId(user.getId());
      SysRole role = sysRoleService.findById(userRole.getRoleId());
      dto.setRole(role.getCode());
    }
    dto.setEnabled(Enabled.T.equals(userInfo.getEnabled()));
    if(userInfo.getLockTime()==null) {
      dto.setLocked(true);
    }else {
      dto.setLocked(userInfo.getLockTime().after(new Date()));
    }
    return dto;
  }

  @Override
  public void saveLoginSuccess(LoginLogDto dto) {
    Long userId = dto.getUserId();
    String ip = dto.getIp();
    String userAgent = dto.getUserAgent();
    String src = dto.getSrc();
    SysUserInfo userInfo = sysUserInfoService.findById(userId);
    userInfo.setLockTime(null);
    sysUserInfoService.update(userInfo);
    SysUserLog userLog = sysUserLogService.findById(userId);
    userLog.setLastIp(userLog.getLoginIp());
    userLog.setLastTime(userLog.getLoginTime());
    userLog.setLoginTime(new Date());
    userLog.setLoginIp(ip);
    sysUserLogService.update(userLog);
    SysUserLoginLog userLoginLog = new SysUserLoginLog();
    String ipAddr = ipService.findAddrByIp(ip);
    userLoginLog.setUserId(userId);
    userLoginLog.setIp(ip);
    userLoginLog.setIpAddr(ipAddr);
    userLoginLog.setUserAgent(userAgent);
    userLoginLog.setRet(Enabled.T);
    userLoginLog.setSrc(src);
    userLoginLog.setCat("0");
    sysUserLoginLogService.save(userLoginLog);

  }

  @Override
  public void saveLoginFailure(LoginLogDto dto) {
    Long userId = dto.getUserId();
    String ip = dto.getIp();
    String userAgent = dto.getUserAgent();
    String src=dto.getSrc();
    SysUserLog userLog = sysUserLogService.findById(userId);
    userLog.setErrTimes(userLog.getErrTimes()+1);
    int lockTimes = sysOptionService.findLockTimes();
    if(lockTimes>=userLog.getErrTimes()) {
      int lockTime = sysOptionService.findLockTime();
      userLog.setLockTime(DateUtil.add(new Date(), Calendar.HOUR_OF_DAY, lockTime));
      SysUserInfo userInfo = sysUserInfoService.findById(userId);
      userInfo.setLockTime(userLog.getLockTime());
      sysUserInfoService.update(userInfo);
    }
    sysUserLogService.update(userLog);
    String ipAddr = ipService.findAddrByIp(ip);
    SysUserLoginLog userLoginLog = new SysUserLoginLog();
    userLoginLog.setUserId(userId);
    userLoginLog.setUserAgent(userAgent);
    userLoginLog.setRet(Enabled.F);
    userLoginLog.setSrc(src);
    userLoginLog.setIp(ip);
    userLoginLog.setIpAddr(ipAddr);
    userLoginLog.setCat("0");
    sysUserLoginLogService.save(userLoginLog);

  }

  @Override
  public Map<String, Object> findInfo(Long userId, String role) {
    Map<String, Object> root = new HashMap<String,Object>(4);
    root.put("role", role);
    SysUserInfo userInfo = sysUserInfoService.findById(userId);
    root.put("headImg", userInfo.getHeadImg());
    List<SysMenu> menus = findMenu(role);
    root.put("menus", menus);
    List<String> btns = findBtn(role);
    root.put("btns", btns);
    return root;
  }

  @Override
  public void deleteByIds(Long[] ids) {
    if(ids!=null) {
      for(Long id:ids) {
        deleteById(id);
      }
    }

  }

  @Override
  public void deleteById(Long id) {
    SysUserInfo userInfo = sysUserInfoService.findById(id);
    userInfo.setIsDel(Enabled.T);
    sysUserInfoService.update(userInfo);

  }

  @Override
  public void updatePassword(Long userId, String oldPassword, String password) {
    SysUser sysUser = sysUserService.findById(userId);
    String salt = sysOptionService.findAdminSalt();
    if(!sysUser.getPassword().equals(Md5Util.saltPassword(salt, oldPassword))){
      Assert.customException(message.getMessage("oldPassword.err"));
    }
    sysUser.setPassword(Md5Util.saltPassword(salt, password));
    sysUserService.update(sysUser);
    

  }

  @Override
  public List<SysMenu> findMenu(String roleCode) {
    List<SysMenu> menus = sysMenuService.findMenu();
    Stream<SysMenu> stream = menus.parallelStream();
    if(!SymbolConstants.ASTERISK.equals(roleCode)) {
      SysRole sysRole = sysRoleService.findByCode(roleCode);
      List<SysRoleMenu> roleMenus = sysRoleMenuService.findByRoleId(sysRole.getId());
      Set<Long> menuIds = roleMenus.parallelStream().map(SysRoleMenu::getMenuId).collect(Collectors.toSet());
      stream = stream.filter(sysMenu->menuIds.contains(sysMenu.getId()));
    }
    return stream.collect(Collectors.toList());
  }

  @Override
  public List<String> findBtn(String roleCode) {
    List<SysMenu> menus = sysMenuService.findBtn();
    Stream<SysMenu> stream = menus.parallelStream();
    if(!SymbolConstants.ASTERISK.equals(roleCode)) {
      SysRole role =sysRoleService.findByCode(roleCode);
      List<SysRoleMenu> roleMenus = sysRoleMenuService.findByRoleId(role.getId());
      Set<Long> menuIds = roleMenus.parallelStream().map(SysRoleMenu::getMenuId).collect(Collectors.toSet());
      stream = stream.filter(sysMenu->menuIds.contains(sysMenu.getId()));
    }
    return stream.map(SysMenu::getCode).collect(Collectors.toList());
  }

  @Override
  public void updateResetPassword(Long id, String password) {
    SysUser user = sysUserService.findById(id);
    String salt = sysOptionService.findAdminSalt();
    user.setPassword(Md5Util.saltPassword(salt, password));
    sysUserService.update(user);

  }

}
