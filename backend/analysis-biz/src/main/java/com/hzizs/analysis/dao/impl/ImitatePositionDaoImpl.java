package com.hzizs.analysis.dao.impl;
import java.util.List;

import com.fasterxml.jackson.core.type.TypeReference;
import com.hzizs.mybatis.dao.impl.MyBatisDaoImpl;
import com.hzizs.analysis.dao.ImitatePositionDao;
import com.hzizs.analysis.entity.ImitatePosition;
/**
 * 模拟持仓 数据库操作类实现类	 
 * @author crazy_cabbage
 *
 */
public class ImitatePositionDaoImpl extends MyBatisDaoImpl<ImitatePosition> implements ImitatePositionDao {



	@Override
	public TypeReference<List<ImitatePosition>> getListEntityType() {
		return new TypeReference<List<ImitatePosition>>() {
		};
	}
	//========================================业务方法========================================
}
