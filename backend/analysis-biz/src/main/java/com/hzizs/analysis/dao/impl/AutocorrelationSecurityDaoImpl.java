package com.hzizs.analysis.dao.impl;
import java.util.List;

import com.fasterxml.jackson.core.type.TypeReference;
import com.hzizs.mybatis.dao.impl.MyBatisDaoImpl;
import com.hzizs.analysis.dao.AutocorrelationSecurityDao;
import com.hzizs.analysis.entity.AutocorrelationSecurity;
/**
 * 证券自相关 数据库操作类实现类	 
 * @author crazy_cabbage
 *
 */
public class AutocorrelationSecurityDaoImpl extends MyBatisDaoImpl<AutocorrelationSecurity> implements AutocorrelationSecurityDao {



	@Override
	public TypeReference<List<AutocorrelationSecurity>> getListEntityType() {
		return new TypeReference<List<AutocorrelationSecurity>>() {
		};
	}
	//========================================业务方法========================================
}
