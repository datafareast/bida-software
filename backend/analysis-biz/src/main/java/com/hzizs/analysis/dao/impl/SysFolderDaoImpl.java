package com.hzizs.analysis.dao.impl;
import java.util.List;

import com.fasterxml.jackson.core.type.TypeReference;
import com.hzizs.mybatis.dao.impl.MyBatisDaoImpl;
import com.hzizs.analysis.dao.SysFolderDao;
import com.hzizs.analysis.entity.SysFolder;
/**
 * 系统文件夹 数据库操作类实现类	 
 * @author crazy_cabbage
 *
 */
public class SysFolderDaoImpl extends MyBatisDaoImpl<SysFolder> implements SysFolderDao {



	@Override
	public TypeReference<List<SysFolder>> getListEntityType() {
		return new TypeReference<List<SysFolder>>() {
		};
	}
	//========================================业务方法========================================
}
