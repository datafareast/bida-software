package com.hzizs.analysis.service.impl;
import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.mybatis.service.impl.MyBatisServiceImpl;


import com.hzizs.analysis.dao.IndustryIndexDao;
import com.hzizs.analysis.entity.IndustryIndex;
import com.hzizs.analysis.service.IndustryIndexService;
/**
 * 行业指标 服务实现类
 * @author crazy_cabbage
 *
 */
public class IndustryIndexServiceImpl extends MyBatisServiceImpl<IndustryIndex> implements IndustryIndexService {
	

	private IndustryIndexDao industryIndexDao;
	public void setIndustryIndexDao(IndustryIndexDao industryIndexDao) {
		this.industryIndexDao = industryIndexDao;
	}
	@Override
	protected MyBatisDao<IndustryIndex> getDao() {
		return industryIndexDao;
	}
	//========================================业务方法========================================
}
