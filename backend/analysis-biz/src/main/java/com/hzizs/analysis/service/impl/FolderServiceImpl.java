package com.hzizs.analysis.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.hzizs.Assert;
import com.hzizs.analysis.constants.Constants.FolderCat;
import com.hzizs.analysis.dao.FolderDao;
import com.hzizs.analysis.entity.Folder;
import com.hzizs.analysis.service.FolderService;
import com.hzizs.mybatis.dao.MyBatisShardingDao;
import com.hzizs.mybatis.service.impl.MyBatisShardingServiceImpl;

/**
 * 文件夹 服务实现类
 * 
 * @author crazy_cabbage
 *
 */
public class FolderServiceImpl extends MyBatisShardingServiceImpl<Folder> implements FolderService {


  private FolderDao folderDao;

  public void setFolderDao(FolderDao folderDao) {
    this.folderDao = folderDao;
  }

  @Override
  protected MyBatisShardingDao<Folder> getDao() {
    return folderDao;
  }

  // ========================================业务方法========================================
  @Override
  public Long saveOrFindDefault(Long userId) {
    Long folderId = findDefault(userId);
    if (folderId == null) {
      saveDefault(userId);
      folderId = findDefault(userId);
    }
    return folderId;
  }

  @Override
  public Long findDefault(Long userId) {
    Assert.assertNotNull(userId, message.getMessage("userId.isnull"));
    Map<String, Object> parameters = new HashMap<String, Object>(3);
    parameters.put("userId", userId);
    parameters.put("cat", FolderCat.DEFAULT);
    Folder folder = findOne(parameters, userId);
    if (folder == null) {
      return null;
    }
    return folder.getId();
  }

  @Override
  public void saveDefault(Long userId) {
    List<Folder> folders = new ArrayList<Folder>(2);
    Folder allFolder = new Folder();
    allFolder.setCat(FolderCat.ALL);
    allFolder.setUserId(userId);
    allFolder.setName("全部");
    folders.add(allFolder);
    Folder defaultFolder = new Folder();
    defaultFolder.setCat(FolderCat.DEFAULT);
    defaultFolder.setUserId(userId);
    defaultFolder.setName("默认");
    folders.add(defaultFolder);
    saveBatch(folders, userId);

  }
}
