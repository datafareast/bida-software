package com.hzizs.analysis.dao.impl;
import java.util.List;

import com.fasterxml.jackson.core.type.TypeReference;
import com.hzizs.mybatis.dao.impl.MyBatisDaoImpl;
import com.hzizs.analysis.dao.ImitatePositionItemDao;
import com.hzizs.analysis.entity.ImitatePositionItem;
/**
 * 模拟持仓条目 数据库操作类实现类	 
 * @author crazy_cabbage
 *
 */
public class ImitatePositionItemDaoImpl extends MyBatisDaoImpl<ImitatePositionItem> implements ImitatePositionItemDao {



	@Override
	public TypeReference<List<ImitatePositionItem>> getListEntityType() {
		return new TypeReference<List<ImitatePositionItem>>() {
		};
	}
	//========================================业务方法========================================
}
