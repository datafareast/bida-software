package com.hzizs.analysis.dao.impl;
import java.util.List;

import com.fasterxml.jackson.core.type.TypeReference;
import com.hzizs.mybatis.dao.impl.MyBatisDaoImpl;
import com.hzizs.analysis.dao.BasicIndexDao;
import com.hzizs.analysis.entity.BasicIndex;
/**
 * 指数基指 数据库操作类实现类	 
 * @author crazy_cabbage
 *
 */
public class BasicIndexDaoImpl extends MyBatisDaoImpl<BasicIndex> implements BasicIndexDao {



	@Override
	public TypeReference<List<BasicIndex>> getListEntityType() {
		return new TypeReference<List<BasicIndex>>() {
		};
	}
	//========================================业务方法========================================
}
