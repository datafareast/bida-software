package com.hzizs.analysis.dao;

import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.analysis.entity.ImitatePosition;
/**
 * 模拟持仓 数据库操作类	 
 * @author crazy_cabbage
 *
 */
public interface ImitatePositionDao extends MyBatisDao<ImitatePosition> {

}
