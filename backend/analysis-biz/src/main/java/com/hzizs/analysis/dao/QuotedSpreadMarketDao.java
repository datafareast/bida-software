package com.hzizs.analysis.dao;

import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.analysis.entity.QuotedSpreadMarket;
/**
 * 市场报价价差 数据库操作类	 
 * @author crazy_cabbage
 *
 */
public interface QuotedSpreadMarketDao extends MyBatisDao<QuotedSpreadMarket> {

}
