package com.hzizs.analysis.dao;

import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.analysis.entity.SysUserRole;
/**
 * 系统用户角色 数据库操作类	 
 * @author crazy_cabbage
 *
 */
public interface SysUserRoleDao extends MyBatisDao<SysUserRole> {

}
