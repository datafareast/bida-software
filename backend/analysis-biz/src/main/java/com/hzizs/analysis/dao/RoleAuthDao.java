package com.hzizs.analysis.dao;

import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.analysis.entity.RoleAuth;
/**
 * 角色权限 数据库操作类	 
 * @author crazy_cabbage
 *
 */
public interface RoleAuthDao extends MyBatisDao<RoleAuth> {

}
