package com.hzizs.analysis.service.impl;
import com.hzizs.analysis.dao.UserInfoDao;
import com.hzizs.analysis.entity.UserInfo;
import com.hzizs.analysis.service.UserInfoService;
import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.mybatis.service.impl.MyBatisServiceImpl;
import com.hzizs.util.BeanUtil;
/**
 * 用户信息 服务实现类
 * @author crazy_cabbage
 *
 */
public class UserInfoServiceImpl extends MyBatisServiceImpl<UserInfo> implements UserInfoService {
	

	private UserInfoDao userInfoDao;
	public void setUserInfoDao(UserInfoDao userInfoDao) {
		this.userInfoDao = userInfoDao;
	}
	@Override
	protected MyBatisDao<UserInfo> getDao() {
		return userInfoDao;
	}
	//========================================业务方法========================================
	 @Override
	  public void updateInfo(UserInfo userInfo) {
	    UserInfo po = findById(userInfo.getId());
	    BeanUtil.copyExclude(userInfo, po, "id", "createTime", "isDel", "isManager", "lockTime", "username");
	    update(po);
	  }

	  @Override
	  public void updateInfo2(UserInfo userInfo) {
	    UserInfo po = findById(userInfo.getId());
	    BeanUtil.copyExclude(userInfo, po, "id", "createTime", "enabled", "isDel", "isManager", "lockTime", "username");
	    update(po);
	  }
}
