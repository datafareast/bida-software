package com.hzizs.analysis.facade.impl;

import com.hzizs.Assert;
import com.hzizs.analysis.facade.LinkFacade;
import com.hzizs.analysis.service.LinkItemService;
import com.hzizs.analysis.service.LinkService;
import com.hzizs.facade.impl.BaseFacadeImpl;

public class LinkFacadeImpl extends BaseFacadeImpl implements LinkFacade {
  private LinkService linkService;
  private LinkItemService linkItemService;

  public void setLinkService(LinkService linkService) {
    this.linkService = linkService;
  }

  public void setLinkItemService(LinkItemService linkItemService) {
    this.linkItemService = linkItemService;
  }

  // =========================================================================
  @Override
  public void deleteByIds(Long[] ids) {
    if (ids != null) {
      for (Long id : ids) {
        deleteById(id);
      }
    }

  }

  @Override
  public void deleteById(Long id) {
    long num = linkItemService.countByLinkId(id);
    if (num > 0L) {
      Assert.customException(message.getMessage("linkId.used"));
    }
    linkService.deleteById(id);
  }

}
