package com.hzizs.analysis.service.impl;
import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.mybatis.service.impl.MyBatisServiceImpl;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.hzizs.Assert;
import com.hzizs.analysis.dao.EffectiveSpreadMarketDao;
import com.hzizs.analysis.entity.EffectiveSpreadMarket;
import com.hzizs.analysis.service.EffectiveSpreadMarketService;
/**
 * 市场有效价差 服务实现类
 * @author crazy_cabbage
 *
 */
public class EffectiveSpreadMarketServiceImpl extends MyBatisServiceImpl<EffectiveSpreadMarket> implements EffectiveSpreadMarketService {
	

	private EffectiveSpreadMarketDao effectiveSpreadMarketDao;
	public void setEffectiveSpreadMarketDao(EffectiveSpreadMarketDao effectiveSpreadMarketDao) {
		this.effectiveSpreadMarketDao = effectiveSpreadMarketDao;
	}
	@Override
	protected MyBatisDao<EffectiveSpreadMarket> getDao() {
		return effectiveSpreadMarketDao;
	}
	//========================================业务方法========================================
  @Override
  public List<EffectiveSpreadMarket> find(Long marketId, Date startTime, Date endTime) {
    Assert.assertNotNull(marketId, message.getMessage("marketId.isnull"));
    Assert.assertNotNull(startTime, message.getMessage("startTime.isnull"));
    Assert.assertNotNull(endTime, message.getMessage("endTime.isnull"));
    Map<String, Object> parameters = new HashMap<String,Object>(3);
    parameters.put("marketId", marketId);
    parameters.put("startTime", startTime);
    parameters.put("endTime", endTime);
    return find(parameters);
  }
}
