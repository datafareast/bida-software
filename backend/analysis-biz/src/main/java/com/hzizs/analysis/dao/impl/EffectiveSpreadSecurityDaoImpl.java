package com.hzizs.analysis.dao.impl;
import java.util.List;

import com.fasterxml.jackson.core.type.TypeReference;
import com.hzizs.mybatis.dao.impl.MyBatisDaoImpl;
import com.hzizs.analysis.dao.EffectiveSpreadSecurityDao;
import com.hzizs.analysis.entity.EffectiveSpreadSecurity;
/**
 * 证券有效价差 数据库操作类实现类	 
 * @author crazy_cabbage
 *
 */
public class EffectiveSpreadSecurityDaoImpl extends MyBatisDaoImpl<EffectiveSpreadSecurity> implements EffectiveSpreadSecurityDao {



	@Override
	public TypeReference<List<EffectiveSpreadSecurity>> getListEntityType() {
		return new TypeReference<List<EffectiveSpreadSecurity>>() {
		};
	}
	//========================================业务方法========================================
}
