package com.hzizs.analysis.dao.impl;
import java.util.List;

import com.fasterxml.jackson.core.type.TypeReference;
import com.hzizs.mybatis.dao.impl.MyBatisDaoImpl;
import com.hzizs.analysis.dao.IconDao;
import com.hzizs.analysis.entity.Icon;
/**
 * 图标库 数据库操作类实现类	 
 * @author crazy_cabbage
 *
 */
public class IconDaoImpl extends MyBatisDaoImpl<Icon> implements IconDao {



	@Override
	public TypeReference<List<Icon>> getListEntityType() {
		return new TypeReference<List<Icon>>() {
		};
	}
	//========================================业务方法========================================
}
