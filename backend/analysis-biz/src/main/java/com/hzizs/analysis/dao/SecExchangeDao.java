package com.hzizs.analysis.dao;

import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.analysis.entity.SecExchange;

/**
 * 交易所 数据库操作类
 * 
 * @author crazy_cabbage
 *
 */
public interface SecExchangeDao extends MyBatisDao<SecExchange> {

}
