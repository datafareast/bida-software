package com.hzizs.analysis.dao.impl;

import java.util.List;

import com.fasterxml.jackson.core.type.TypeReference;
import com.hzizs.mybatis.dao.impl.MyBatisDaoImpl;
import com.hzizs.analysis.dao.DataTopoDao;
import com.hzizs.analysis.entity.DataTopo;

/**
 * 数据拓普图 数据库操作类实现类
 * 
 * @author crazy_cabbage
 *
 */
public class DataTopoDaoImpl extends MyBatisDaoImpl<DataTopo> implements DataTopoDao {

  @Override
  public TypeReference<List<DataTopo>> getListEntityType() {
    return new TypeReference<List<DataTopo>>() {
    };
  }
  // ========================================业务方法========================================
}
