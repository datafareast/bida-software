package com.hzizs.analysis.service.impl;
import java.util.HashMap;
import java.util.Map;
import com.hzizs.Assert;
import com.hzizs.analysis.dao.SysUserRoleDao;
import com.hzizs.analysis.entity.SysUserRole;
import com.hzizs.analysis.service.SysUserRoleService;
import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.mybatis.service.impl.MyBatisServiceImpl;
/**
 * 系统用户角色 服务实现类
 * @author crazy_cabbage
 *
 */
public class SysUserRoleServiceImpl extends MyBatisServiceImpl<SysUserRole> implements SysUserRoleService {
	

	private SysUserRoleDao sysUserRoleDao;
	public void setSysUserRoleDao(SysUserRoleDao sysUserRoleDao) {
		this.sysUserRoleDao = sysUserRoleDao;
	}
	@Override
	protected MyBatisDao<SysUserRole> getDao() {
		return sysUserRoleDao;
	}
	//========================================业务方法========================================
	@Override
	  public SysUserRole findByUserId(Long userId) {
	    Assert.assertNotNull(userId, message.getMessage("userId.isnull"));
	    Map<String, Object> parameters = new HashMap<String, Object>(1);
	    parameters.put("userId", userId);
	    return findOne(parameters);
	  }

	  @Override
	  public long countByRoleId(Long roleId) {
	    Assert.assertNotNull(roleId, message.getMessage("roleId.isnull"));
	    Map<String, Object> parameters = new HashMap<String, Object>(1);
	    parameters.put("roleId", roleId);
	    return count(parameters);
	  }
}
