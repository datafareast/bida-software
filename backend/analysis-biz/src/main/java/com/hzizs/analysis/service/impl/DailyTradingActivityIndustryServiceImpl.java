package com.hzizs.analysis.service.impl;
import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.mybatis.service.impl.MyBatisServiceImpl;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.hzizs.Assert;
import com.hzizs.analysis.dao.DailyTradingActivityIndustryDao;
import com.hzizs.analysis.entity.DailyTradingActivityIndustry;
import com.hzizs.analysis.service.DailyTradingActivityIndustryService;
/**
 * 行业日交易 服务实现类
 * @author crazy_cabbage
 *
 */
public class DailyTradingActivityIndustryServiceImpl extends MyBatisServiceImpl<DailyTradingActivityIndustry> implements DailyTradingActivityIndustryService {
	

	private DailyTradingActivityIndustryDao dailyTradingActivityIndustryDao;
	public void setDailyTradingActivityIndustryDao(DailyTradingActivityIndustryDao dailyTradingActivityIndustryDao) {
		this.dailyTradingActivityIndustryDao = dailyTradingActivityIndustryDao;
	}
	@Override
	protected MyBatisDao<DailyTradingActivityIndustry> getDao() {
		return dailyTradingActivityIndustryDao;
	}
	//========================================业务方法========================================
  @Override
  public List<DailyTradingActivityIndustry> find(Long marketId, Date startTime, Date endTime) {
    Assert.assertNotNull(marketId, message.getMessage("marketId.isnull"));
    Assert.assertNotNull(startTime, message.getMessage("startTime.isnull"));
    Assert.assertNotNull(endTime, message.getMessage("endTime.isnull"));
    Map<String, Object> parameters = new HashMap<String,Object>(3);
    parameters.put("marketId", marketId);
    parameters.put("startTime", startTime);
    parameters.put("endTime", endTime);
    return find(parameters);
  }
}
