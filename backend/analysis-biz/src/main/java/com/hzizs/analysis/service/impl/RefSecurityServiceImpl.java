package com.hzizs.analysis.service.impl;
import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.mybatis.service.impl.MyBatisServiceImpl;
import java.util.HashMap;
import java.util.Map;
import com.hzizs.Assert;
import com.hzizs.analysis.dao.RefSecurityDao;
import com.hzizs.analysis.entity.RefSecurity;
import com.hzizs.analysis.service.RefSecurityService;
/**
 * 证券参考数据 服务实现类
 * @author crazy_cabbage
 *
 */
public class RefSecurityServiceImpl extends MyBatisServiceImpl<RefSecurity> implements RefSecurityService {
	

	private RefSecurityDao refSecurityDao;
	public void setRefSecurityDao(RefSecurityDao refSecurityDao) {
		this.refSecurityDao = refSecurityDao;
	}
	@Override
	protected MyBatisDao<RefSecurity> getDao() {
		return refSecurityDao;
	}
	//========================================业务方法========================================
  @Override
  public RefSecurity findBySecurityId(Long securityId) {
    Assert.assertNotNull(securityId, message.getMessage("securityId.isnull"));
    Map<String, Object> parameters = new HashMap<String, Object>(1);
    parameters.put("securityId", securityId);
    return findOne(parameters);
  }
}
