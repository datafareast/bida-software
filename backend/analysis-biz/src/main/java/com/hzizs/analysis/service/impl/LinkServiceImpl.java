package com.hzizs.analysis.service.impl;
import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.mybatis.service.impl.MyBatisServiceImpl;


import com.hzizs.analysis.dao.LinkDao;
import com.hzizs.analysis.entity.Link;
import com.hzizs.analysis.service.LinkService;
/**
 * 友情链接 服务实现类
 * @author crazy_cabbage
 *
 */
public class LinkServiceImpl extends MyBatisServiceImpl<Link> implements LinkService {
	

	private LinkDao linkDao;
	public void setLinkDao(LinkDao linkDao) {
		this.linkDao = linkDao;
	}
	@Override
	protected MyBatisDao<Link> getDao() {
		return linkDao;
	}
	//========================================业务方法========================================
}
