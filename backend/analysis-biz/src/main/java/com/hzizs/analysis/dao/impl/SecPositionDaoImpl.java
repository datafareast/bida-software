package com.hzizs.analysis.dao.impl;

import java.util.List;

import com.fasterxml.jackson.core.type.TypeReference;
import com.hzizs.mybatis.dao.impl.MyBatisDaoImpl;
import com.hzizs.analysis.dao.SecPositionDao;
import com.hzizs.analysis.entity.SecPosition;

/**
 * 持仓分析 数据库操作类实现类
 * 
 * @author crazy_cabbage
 *
 */
public class SecPositionDaoImpl extends MyBatisDaoImpl<SecPosition> implements SecPositionDao {



  @Override
  public TypeReference<List<SecPosition>> getListEntityType() {
    return new TypeReference<List<SecPosition>>() {
    };
  }
  // ========================================业务方法========================================
}
