package com.hzizs.analysis.service.impl;
import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.mybatis.service.impl.MyBatisServiceImpl;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.hzizs.Assert;
import com.hzizs.analysis.dao.DataTopoValDao;
import com.hzizs.analysis.entity.DataTopoVal;
import com.hzizs.analysis.service.DataTopoValService;
/**
 * 数据拓普值 服务实现类
 * @author crazy_cabbage
 *
 */
public class DataTopoValServiceImpl extends MyBatisServiceImpl<DataTopoVal> implements DataTopoValService {
	

	private DataTopoValDao dataTopoValDao;
	public void setDataTopoValDao(DataTopoValDao dataTopoValDao) {
		this.dataTopoValDao = dataTopoValDao;
	}
	@Override
	protected MyBatisDao<DataTopoVal> getDao() {
		return dataTopoValDao;
	}
	//========================================业务方法========================================
  @Override
  public DataTopoVal find(Long dataTypeId, Long dataTopoId, Long dataTopoParentId, Date occTime) {
    Assert.assertNotNull(occTime, message.getMessage("occTime.isnull"));
    Assert.assertNotNull(dataTypeId, message.getMessage("dataTypeId.isnull"));
    Assert.assertNotNull(dataTopoId, message.getMessage("dataTopoId.isnull"));
    Assert.assertNotNull(dataTopoParentId, message.getMessage("dataTopoParentId.isnull"));
    Map<String, Object> parameters = new HashMap<String, Object>();
    parameters.put("occTime", occTime);
    parameters.put("dataTypeId", dataTypeId);
    parameters.put("dataTopoId", dataTopoId);
    parameters.put("dataTopoParentId", dataTopoParentId);
    return findOne(parameters);
  }
  @Override
  public List<DataTopoVal> findByDataTopoId(Long dataTopoId) {
    Assert.assertNotNull(dataTopoId, message.getMessage("dataTopoId.isnull"));
    Map<String, Object> parameters = new HashMap<String, Object>();
    parameters.put("dataTopoId", dataTopoId);
    return find(parameters);
  }
}
