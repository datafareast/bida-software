package com.hzizs.analysis.service.impl;
import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.mybatis.service.impl.MyBatisServiceImpl;


import com.hzizs.analysis.dao.McapSecurityDao;
import com.hzizs.analysis.entity.McapSecurity;
import com.hzizs.analysis.service.McapSecurityService;
/**
 * 证券市值 服务实现类
 * @author crazy_cabbage
 *
 */
public class McapSecurityServiceImpl extends MyBatisServiceImpl<McapSecurity> implements McapSecurityService {
	

	private McapSecurityDao mcapSecurityDao;
	public void setMcapSecurityDao(McapSecurityDao mcapSecurityDao) {
		this.mcapSecurityDao = mcapSecurityDao;
	}
	@Override
	protected MyBatisDao<McapSecurity> getDao() {
		return mcapSecurityDao;
	}
	//========================================业务方法========================================
}
