package com.hzizs.analysis.dao;

import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.analysis.entity.SysRoleMenu;
/**
 * 系统角色菜单 数据库操作类	 
 * @author crazy_cabbage
 *
 */
public interface SysRoleMenuDao extends MyBatisDao<SysRoleMenu> {

}
