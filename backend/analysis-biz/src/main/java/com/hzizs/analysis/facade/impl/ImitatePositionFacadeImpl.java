package com.hzizs.analysis.facade.impl;

import java.util.List;
import com.hzizs.Assert;
import com.hzizs.analysis.constants.Constants.ImitateSta;
import com.hzizs.analysis.dto.ImitatePositionDto;
import com.hzizs.analysis.entity.ImitatePosition;
import com.hzizs.analysis.entity.ImitatePositionItem;
import com.hzizs.analysis.facade.ImitatePositionFacade;
import com.hzizs.analysis.service.ImitatePositionItemService;
import com.hzizs.analysis.service.ImitatePositionService;
import com.hzizs.facade.impl.BaseFacadeImpl;
import com.hzizs.util.CollectionUtil;

public class ImitatePositionFacadeImpl extends BaseFacadeImpl implements ImitatePositionFacade {
  private ImitatePositionService imitatePositionService;
  private ImitatePositionItemService imitatePositionItemService;


  public void setImitatePositionService(ImitatePositionService imitatePositionService) {
    this.imitatePositionService = imitatePositionService;
  }


  public void setImitatePositionItemService(ImitatePositionItemService imitatePositionItemService) {
    this.imitatePositionItemService = imitatePositionItemService;
  }


  @Override
  public void save(ImitatePositionDto dto) {
     ImitatePosition position = dto.getImitatePosition();
     position.setSta(ImitateSta.STA_0);
     imitatePositionService.save(position);
     List<ImitatePositionItem> items = dto.getImitatePositionItems();
     if(CollectionUtil.isEmpty(items)) {
       Assert.customException(message.getMessage("item.isnull"));
     }
     for(ImitatePositionItem item:items) {
       item.setImitatePositionId(position.getId());
     }
     imitatePositionItemService.saveBatch(items);


  }

}
