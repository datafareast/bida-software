package com.hzizs.analysis.dao.impl;
import java.util.List;

import com.fasterxml.jackson.core.type.TypeReference;
import com.hzizs.mybatis.dao.impl.MyBatisDaoImpl;
import com.hzizs.analysis.dao.QuotedSpreadSecurityDao;
import com.hzizs.analysis.entity.QuotedSpreadSecurity;
/**
 * 证券报价价差 数据库操作类实现类	 
 * @author crazy_cabbage
 *
 */
public class QuotedSpreadSecurityDaoImpl extends MyBatisDaoImpl<QuotedSpreadSecurity> implements QuotedSpreadSecurityDao {



	@Override
	public TypeReference<List<QuotedSpreadSecurity>> getListEntityType() {
		return new TypeReference<List<QuotedSpreadSecurity>>() {
		};
	}
	//========================================业务方法========================================
}
