package com.hzizs.analysis.service.impl;

import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.mybatis.service.impl.MyBatisServiceImpl;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.hzizs.Assert;
import com.hzizs.analysis.dao.SecPositionDao;
import com.hzizs.analysis.entity.SecPosition;
import com.hzizs.analysis.service.SecPositionService;

/**
 * 持仓分析 服务实现类
 * 
 * @author crazy_cabbage
 *
 */
public class SecPositionServiceImpl extends MyBatisServiceImpl<SecPosition> implements SecPositionService {


  private SecPositionDao secPositionDao;

  public void setSecPositionDao(SecPositionDao secPositionDao) {
    this.secPositionDao = secPositionDao;
  }

  @Override
  protected MyBatisDao<SecPosition> getDao() {
    return secPositionDao;
  }
  // ========================================业务方法========================================

  @Override
  public List<SecPosition> findByOccDate(Date occDate) {
    Assert.assertNotNull(occDate, message.getMessage("occDate.isnull"));
    Map<String, Object> parameters = new HashMap<String, Object>();
    parameters.put("occDate", occDate);
    return find(parameters);
  }
}
