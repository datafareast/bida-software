package com.hzizs.analysis.dao;

import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.analysis.entity.ResearchReport;
/**
 * 研究报告 数据库操作类	 
 * @author crazy_cabbage
 *
 */
public interface ResearchReportDao extends MyBatisDao<ResearchReport> {

}
