package com.hzizs.analysis.service.impl;
import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.mybatis.service.impl.MyBatisServiceImpl;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.hzizs.Assert;
import com.hzizs.analysis.dao.AutocorrelationSecurityDao;
import com.hzizs.analysis.entity.AutocorrelationSecurity;
import com.hzizs.analysis.service.AutocorrelationSecurityService;
/**
 * 证券自相关 服务实现类
 * @author crazy_cabbage
 *
 */
public class AutocorrelationSecurityServiceImpl extends MyBatisServiceImpl<AutocorrelationSecurity> implements AutocorrelationSecurityService {
	

	private AutocorrelationSecurityDao autocorrelationSecurityDao;
	public void setAutocorrelationSecurityDao(AutocorrelationSecurityDao autocorrelationSecurityDao) {
		this.autocorrelationSecurityDao = autocorrelationSecurityDao;
	}
	@Override
	protected MyBatisDao<AutocorrelationSecurity> getDao() {
		return autocorrelationSecurityDao;
	}
	//========================================业务方法========================================
  @Override
  public List<AutocorrelationSecurity> find(Long securityId, Date startTime, Date endTime) {
    Assert.assertNotNull(securityId, message.getMessage("securityId.isnull"));
    Assert.assertNotNull(startTime, message.getMessage("startTime.isnull"));
    Assert.assertNotNull(endTime, message.getMessage("endTime.isnull"));
    Map<String, Object> parameters = new HashMap<String,Object>(3);
    parameters.put("securityId", securityId);
    parameters.put("startTime", startTime);
    parameters.put("endTime", endTime);
    return find(parameters);
  }
}
