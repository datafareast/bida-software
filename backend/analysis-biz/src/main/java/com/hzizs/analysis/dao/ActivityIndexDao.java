package com.hzizs.analysis.dao;

import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.analysis.entity.ActivityIndex;
/**
 * 指数指标 数据库操作类	 
 * @author crazy_cabbage
 *
 */
public interface ActivityIndexDao extends MyBatisDao<ActivityIndex> {

}
