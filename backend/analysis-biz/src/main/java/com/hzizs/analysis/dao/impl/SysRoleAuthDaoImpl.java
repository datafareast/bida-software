package com.hzizs.analysis.dao.impl;
import java.util.List;

import com.fasterxml.jackson.core.type.TypeReference;
import com.hzizs.mybatis.dao.impl.MyBatisDaoImpl;
import com.hzizs.analysis.dao.SysRoleAuthDao;
import com.hzizs.analysis.entity.SysRoleAuth;
/**
 * 系统角色权限 数据库操作类实现类	 
 * @author crazy_cabbage
 *
 */
public class SysRoleAuthDaoImpl extends MyBatisDaoImpl<SysRoleAuth> implements SysRoleAuthDao {



	@Override
	public TypeReference<List<SysRoleAuth>> getListEntityType() {
		return new TypeReference<List<SysRoleAuth>>() {
		};
	}
	//========================================业务方法========================================
}
