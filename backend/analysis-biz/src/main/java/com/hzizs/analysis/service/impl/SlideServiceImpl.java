package com.hzizs.analysis.service.impl;
import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.mybatis.service.impl.MyBatisServiceImpl;


import com.hzizs.analysis.dao.SlideDao;
import com.hzizs.analysis.entity.Slide;
import com.hzizs.analysis.service.SlideService;
/**
 * 轮播图 服务实现类
 * @author crazy_cabbage
 *
 */
public class SlideServiceImpl extends MyBatisServiceImpl<Slide> implements SlideService {
	

	private SlideDao slideDao;
	public void setSlideDao(SlideDao slideDao) {
		this.slideDao = slideDao;
	}
	@Override
	protected MyBatisDao<Slide> getDao() {
		return slideDao;
	}
	//========================================业务方法========================================
}
