package com.hzizs.analysis.facade.impl;

import com.hzizs.Assert;
import com.hzizs.analysis.entity.Auth;
import com.hzizs.analysis.facade.AuthFacade;
import com.hzizs.analysis.service.AuthService;
import com.hzizs.analysis.service.RoleAuthService;
import com.hzizs.facade.impl.BaseFacadeImpl;

public class AuthFacadeImpl extends BaseFacadeImpl implements AuthFacade {
  private RoleAuthService roleAuthService;
  private AuthService authService;
  
  public void setRoleAuthService(RoleAuthService roleAuthService) {
    this.roleAuthService = roleAuthService;
  }

  public void setAuthService(AuthService authService) {
    this.authService = authService;
  }

  //===================================================================
  @Override
  public void deleteById(Long id) {
    long num = roleAuthService.countByAuthId(id);
    if(num>0) {
      Assert.customException(message.getMessage("authId.used"));
    }
    authService.deleteById(id);
  }

  @Override
  public void deleteByIds(Long[] ids) {
    if(ids!=null) {
      for(Long id:ids) {
        deleteById(id);
      }
    }
  }

  @Override
  public void save(Auth auth) {
    String code = auth.getCode();
    Auth po = authService.findByCode(code);
    if(po!=null) {
      Assert.customException(message.getMessage("code.exists"));
    }
    authService.save(auth);
  }

  @Override
  public void update(Auth auth) {
    String code = auth.getCode();
    Auth po = authService.findByCode(code);
    if(po!=null) {
      if(!po.getId().equals(auth.getId())) {
        Assert.customException(message.getMessage("code.exists"));
      }
    }
    authService.update(auth);
  }

}
