package com.hzizs.analysis.service.impl;
import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.mybatis.service.impl.MyBatisServiceImpl;


import com.hzizs.analysis.dao.IconDao;
import com.hzizs.analysis.entity.Icon;
import com.hzizs.analysis.service.IconService;
/**
 * 图标库 服务实现类
 * @author crazy_cabbage
 *
 */
public class IconServiceImpl extends MyBatisServiceImpl<Icon> implements IconService {
	

	private IconDao iconDao;
	public void setIconDao(IconDao iconDao) {
		this.iconDao = iconDao;
	}
	@Override
	protected MyBatisDao<Icon> getDao() {
		return iconDao;
	}
	//========================================业务方法========================================
}
