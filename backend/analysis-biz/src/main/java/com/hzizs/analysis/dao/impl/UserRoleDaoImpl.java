package com.hzizs.analysis.dao.impl;
import java.util.List;

import com.fasterxml.jackson.core.type.TypeReference;
import com.hzizs.mybatis.dao.impl.MyBatisDaoImpl;
import com.hzizs.analysis.dao.UserRoleDao;
import com.hzizs.analysis.entity.UserRole;
/**
 * 用户角色 数据库操作类实现类	 
 * @author crazy_cabbage
 *
 */
public class UserRoleDaoImpl extends MyBatisDaoImpl<UserRole> implements UserRoleDao {



	@Override
	public TypeReference<List<UserRole>> getListEntityType() {
		return new TypeReference<List<UserRole>>() {
		};
	}
	//========================================业务方法========================================
}
