package com.hzizs.analysis.dao;

import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.analysis.entity.SecAnalysisItem;

/**
 * 业绩分析条目 数据库操作类
 * 
 * @author crazy_cabbage
 *
 */
public interface SecAnalysisItemDao extends MyBatisDao<SecAnalysisItem> {

}
