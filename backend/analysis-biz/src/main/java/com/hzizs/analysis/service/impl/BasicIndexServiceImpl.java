package com.hzizs.analysis.service.impl;
import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.mybatis.service.impl.MyBatisServiceImpl;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.hzizs.analysis.dao.BasicIndexDao;
import com.hzizs.analysis.entity.BasicIndex;
import com.hzizs.analysis.service.BasicIndexService;
import com.hzizs.constants.Enabled;
/**
 * 指数基指 服务实现类
 * @author crazy_cabbage
 *
 */
public class BasicIndexServiceImpl extends MyBatisServiceImpl<BasicIndex> implements BasicIndexService {
	

	private BasicIndexDao basicIndexDao;
	public void setBasicIndexDao(BasicIndexDao basicIndexDao) {
		this.basicIndexDao = basicIndexDao;
	}
	@Override
	protected MyBatisDao<BasicIndex> getDao() {
		return basicIndexDao;
	}
	//========================================业务方法========================================
  @Override
  public List<BasicIndex> findEnabledAll() {
    Map<String, Object> parameters = new HashMap<String,Object>();
    parameters.put("enabled", Enabled.T);
    return find(parameters);
  }
}
