package com.hzizs.analysis.service.impl;
import java.util.HashMap;
import java.util.Map;
import com.hzizs.Assert;
import com.hzizs.analysis.dao.UserDao;
import com.hzizs.analysis.entity.User;
import com.hzizs.analysis.service.UserService;
import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.mybatis.service.impl.MyBatisServiceImpl;
/**
 * 用户 服务实现类
 * @author crazy_cabbage
 *
 */
public class UserServiceImpl extends MyBatisServiceImpl<User> implements UserService {
	

	private UserDao userDao;
	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}
	@Override
	protected MyBatisDao<User> getDao() {
		return userDao;
	}
	//========================================业务方法========================================

	  @Override
	  public User findByUsername(String username) {
	    Assert.assertNotEmptyString(username,message.getMessage("username.isnull"));
	    Map<String, Object> parameters = new HashMap<String,Object>(1);
	    parameters.put("username", username);
	    return findOne(parameters);
	  }
}
