package com.hzizs.analysis.facade.impl;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import com.hzizs.Assert;
import com.hzizs.analysis.dto.RoleDto;
import com.hzizs.analysis.entity.Auth;
import com.hzizs.analysis.entity.Menu;
import com.hzizs.analysis.entity.Role;
import com.hzizs.analysis.entity.RoleAuth;
import com.hzizs.analysis.entity.RoleMenu;
import com.hzizs.analysis.facade.RoleFacade;
import com.hzizs.analysis.service.AuthService;
import com.hzizs.analysis.service.MenuService;
import com.hzizs.analysis.service.RoleAuthService;
import com.hzizs.analysis.service.RoleMenuService;
import com.hzizs.analysis.service.RoleService;
import com.hzizs.analysis.service.UserRoleService;
import com.hzizs.constants.Enabled;
import com.hzizs.constants.SymbolConstants;
import com.hzizs.facade.impl.BaseFacadeImpl;
import com.hzizs.util.StreamUtil;
import com.hzizs.util.StringUtil;

public class RoleFacadeImpl extends BaseFacadeImpl implements RoleFacade {
  private RoleService roleService;
  private RoleMenuService roleMenuService;
  private MenuService menuService;
  private AuthService authService;
  private RoleAuthService  roleAuthService;
  private UserRoleService userRoleService;
  
  
  public void setRoleService(RoleService roleService) {
    this.roleService = roleService;
  }

  public void setRoleMenuService(RoleMenuService roleMenuService) {
    this.roleMenuService = roleMenuService;
  }

  public void setMenuService(MenuService menuService) {
    this.menuService = menuService;
  }

  public void setAuthService(AuthService authService) {
    this.authService = authService;
  }

  public void setRoleAuthService(RoleAuthService roleAuthService) {
    this.roleAuthService = roleAuthService;
  }

  public void setUserRoleService(UserRoleService userRoleService) {
    this.userRoleService = userRoleService;
  }
//========================================================================
  @Override
  public RoleDto findById(Long id) {
    Role role  =roleService.findById(id);
    RoleDto dto  = new RoleDto();
    dto.setRole(role);
    List<RoleMenu> roleMenus = roleMenuService.findByRoleId(id);
    List<Long> menuIds = roleMenus.parallelStream().filter(roleMenu->Enabled.F.equals(roleMenu.getIsHalf()))
        .map(RoleMenu::getMenuId).collect(Collectors.toList());
    List<Long> halfMenuIds = roleMenus.parallelStream().filter(roleMenu->Enabled.F.equals(roleMenu.getIsHalf()))
        .map(RoleMenu::getMenuId).collect(Collectors.toList());
    dto.setMenuIds(menuIds);
    dto.setHalfMenuIds(halfMenuIds);
    return dto;
  }

  @Override
  public void save(RoleDto dto) {
    Role role = dto.getRole();
    roleService.save(role);
    Long roleId = role.getId();
    final List<Long> menuIds = dto.getMenuIds();
    List<Long> halfMenuIds = dto.getHalfMenuIds();
    List<Long> allMenuIds = StreamUtil.unionAll(menuIds, halfMenuIds);
    List<RoleMenu> allRoleMenus = allMenuIds.parallelStream().map(menuId->{
      RoleMenu roleMenu = new RoleMenu();
      roleMenu.setRoleId(roleId);
      roleMenu.setMenuId(menuId);
      roleMenu.setIsHalf(menuIds.contains(menuId)?Enabled.F:Enabled.T);
      return roleMenu;
    }).collect(Collectors.toList());
    roleMenuService.saveBatch(allRoleMenus);
    List<Menu> menus = menuService.findAll();
    List<String> authCodes  = menus.parallelStream().filter(menu->allMenuIds.contains(menu.getId())).flatMap(menu->{
      String authCode = menu.getAuthCode();
      if(StringUtil.isEmpty(authCode)) {
        return Stream.empty();
      }else {
        return Stream.of(authCode.split(SymbolConstants.COMMA));
      }
    }).distinct().collect(Collectors.toList());
    List<Auth> auths = authService.findAll();
    Map<String, Long> authMap = auths.parallelStream().collect(Collectors.toMap(Auth::getCode,Auth::getId));
    List<RoleAuth> roleAuths = authCodes.parallelStream().map(code->{
      Long authId = authMap.get(code);
      RoleAuth roleAuth = new RoleAuth();
      roleAuth.setAuthId(authId);
      roleAuth.setRoleId(roleId);
      return roleAuth;
    }).collect(Collectors.toList());
    roleAuthService.saveBatch(roleAuths);
  }

  @Override
  public void update(RoleDto dto) {
    Role role = dto.getRole();
    roleService.update(role);
    Long roleId = role.getId();
    final List<Long> menuIds = dto.getMenuIds();
    List<Long> halfMenuIds = dto.getHalfMenuIds();
    List<Long> allMenuIds = StreamUtil.unionAll(menuIds, halfMenuIds);
    List<RoleMenu> allRoleMenus = allMenuIds.parallelStream().map(menuId->{
      RoleMenu roleMenu = new RoleMenu();
      roleMenu.setRoleId(roleId);
      roleMenu.setMenuId(menuId);
      roleMenu.setIsHalf(menuIds.contains(menuId)?Enabled.F:Enabled.T);
      return roleMenu;
    }).collect(Collectors.toList());
    Map<Long, RoleMenu> roleMenuMap = 
        allRoleMenus.parallelStream().collect(Collectors.toMap(RoleMenu::getMenuId, Function.identity()));
    
    List<RoleMenu> dbRoleMenus = roleMenuService.findByRoleId(roleId);
    List<Long> dbMenuIds = dbRoleMenus.parallelStream().map(RoleMenu::getMenuId).collect(Collectors.toList());
    Map<Long, RoleMenu> dbRoleMenuMap = dbRoleMenus.parallelStream().collect(Collectors.toMap(RoleMenu::getMenuId, Function.identity()));
    //删除
    List<Long> removeMenuIds = StreamUtil.subtract(dbMenuIds, allMenuIds);
    roleMenuService.delete(roleId,removeMenuIds);
    
    //新增
    List<Long> addMenuIds = StreamUtil.subtract(allMenuIds, dbMenuIds);
    List<RoleMenu> addRoleMenus = allRoleMenus.parallelStream()
        .filter(roleMenu ->addMenuIds.contains(roleMenu.getMenuId())).collect(Collectors.toList());
    roleMenuService.saveBatch(addRoleMenus);
    
    //修改 
    List<Long> modifyMenuIds = StreamUtil.subtract(allMenuIds, addMenuIds);
    List<RoleMenu> modifyRoleMenus = modifyMenuIds.parallelStream().filter(menuId->{
      RoleMenu roleMenu = roleMenuMap.get(menuId);
      RoleMenu dbRoleMenu = dbRoleMenuMap.get(menuId);
      return !roleMenu.getIsHalf().equals(dbRoleMenu.getIsHalf());
    }).map(menuId->{
      RoleMenu roleMenu = roleMenuMap.get(menuId);
      RoleMenu dbRoleMenu = dbRoleMenuMap.get(menuId);
      dbRoleMenu.setIsHalf(roleMenu.getIsHalf());
      return dbRoleMenu;
    }).collect(Collectors.toList());
    for(RoleMenu roleMenu :modifyRoleMenus) {
      roleMenuService.update(roleMenu);
    }
    //处理权限
    List<Auth> dbAuths = authService.findAll();
    Map<String, Long> authMap = dbAuths.parallelStream().collect(Collectors.toMap(Auth::getCode, Auth::getId));
    //权限处理
    //所有菜单
    List<Menu> menus = menuService.findAll();
    Map<Long, Menu> menuMap = menus.parallelStream().collect(Collectors.toMap(Menu::getId, Function.identity()));
    List<Long> authIds = menuIds.parallelStream().flatMap(menuId->{
      Menu menu = menuMap.get(menuId);
      String authCode =menu.getAuthCode();
      if(StringUtil.isEmpty(authCode)) {
        return Stream.empty();
      }else {
        return Stream.of(authCode.split(SymbolConstants.COMMA));
      }
    }).distinct().map(code->{
      return authMap.get(code);
    }).collect(Collectors.toList());
    
    // 删除权限
    List<RoleAuth> dbRoleAuths = roleAuthService.findByRoleId(roleId);
    List<Long> dbAuthIds = dbRoleAuths.parallelStream().map(RoleAuth::getAuthId).collect(Collectors.toList());
    List<Long> removeAuthIds = StreamUtil.subtract(dbAuthIds, authIds);
    roleAuthService.delete(roleId,removeAuthIds);
    //新增权限
    List<Long> addAuthIds = StreamUtil.subtract(authIds, dbAuthIds);
    List<RoleAuth> addAuths = addAuthIds.parallelStream().map(authId->{
      RoleAuth roleAuth = new RoleAuth();
      roleAuth.setAuthId(authId);
      roleAuth.setRoleId(roleId);
      return roleAuth;
    }).collect(Collectors.toList());
    roleAuthService.saveBatch(addAuths);

  }

  @Override
  public void deleteById(Long id) {
    //如果角色已被用户使用，那么就不可以删除
    long num = userRoleService.countByRoleId(id);
    if(num>0) {
      Assert.customException(message.getMessage("roleId.used"));
    }
    roleService.deleteById(id);
    roleMenuService.deleteByRoleId(id);
    roleAuthService.deleteByRoleId(id);

  }

  @Override
  public void deleteByIds(Long[] ids) {
    for(Long id :ids) {
      deleteById(id);
    }

  }

  @Override
  public List<String> findAuthByRoleCode(String roleCode) {
    List<Auth> auths = authService.findAll();
    if(SymbolConstants.ASTERISK.equals(roleCode)) {
      List<String> codes = auths.parallelStream().map(Auth::getCode).collect(Collectors.toList());
      return codes;
    }else {
      Role role = roleService.findByCode(roleCode);
      Long roleId = role.getId();
      List<RoleAuth> roleAuths = roleAuthService.findByRoleId(roleId);
      Set<Long> authIds = roleAuths.parallelStream().map(RoleAuth::getAuthId).collect(Collectors.toSet());
      List<String> codes = auths.parallelStream().filter(auth->authIds.contains(auth.getId())).map(Auth::getCode)
          .collect(Collectors.toList());
      return codes;
    }
  }

}
