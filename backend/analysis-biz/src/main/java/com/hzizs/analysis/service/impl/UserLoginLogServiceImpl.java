package com.hzizs.analysis.service.impl;
import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.mybatis.service.impl.MyBatisServiceImpl;


import com.hzizs.analysis.dao.UserLoginLogDao;
import com.hzizs.analysis.entity.UserLoginLog;
import com.hzizs.analysis.service.UserLoginLogService;
/**
 * 用户登录日志 服务实现类
 * @author crazy_cabbage
 *
 */
public class UserLoginLogServiceImpl extends MyBatisServiceImpl<UserLoginLog> implements UserLoginLogService {
	

	private UserLoginLogDao userLoginLogDao;
	public void setUserLoginLogDao(UserLoginLogDao userLoginLogDao) {
		this.userLoginLogDao = userLoginLogDao;
	}
	@Override
	protected MyBatisDao<UserLoginLog> getDao() {
		return userLoginLogDao;
	}
	//========================================业务方法========================================
}
