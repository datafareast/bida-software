package com.hzizs.analysis.dao;

import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.analysis.entity.SysUserLog;
/**
 * 系统用户日志 数据库操作类	 
 * @author crazy_cabbage
 *
 */
public interface SysUserLogDao extends MyBatisDao<SysUserLog> {

}
