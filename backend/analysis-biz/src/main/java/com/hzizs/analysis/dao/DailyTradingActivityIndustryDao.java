package com.hzizs.analysis.dao;

import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.analysis.entity.DailyTradingActivityIndustry;
/**
 * 行业日交易 数据库操作类	 
 * @author crazy_cabbage
 *
 */
public interface DailyTradingActivityIndustryDao extends MyBatisDao<DailyTradingActivityIndustry> {

}
