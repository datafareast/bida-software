package com.hzizs.analysis.dao.impl;
import java.util.List;

import com.fasterxml.jackson.core.type.TypeReference;
import com.hzizs.mybatis.dao.impl.MyBatisDaoImpl;
import com.hzizs.analysis.dao.RoleDao;
import com.hzizs.analysis.entity.Role;
/**
 * 角色 数据库操作类实现类	 
 * @author crazy_cabbage
 *
 */
public class RoleDaoImpl extends MyBatisDaoImpl<Role> implements RoleDao {



	@Override
	public TypeReference<List<Role>> getListEntityType() {
		return new TypeReference<List<Role>>() {
		};
	}
	//========================================业务方法========================================
}
