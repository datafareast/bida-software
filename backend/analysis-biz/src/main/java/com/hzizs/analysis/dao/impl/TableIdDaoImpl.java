package com.hzizs.analysis.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.core.type.TypeReference;
import com.hzizs.Assert;
import com.hzizs.analysis.dao.TableIdDao;
import com.hzizs.analysis.entity.TableId;
import com.hzizs.mybatis.dao.impl.MyBatisDaoImpl;

/**
 * 表主键 数据库操作类实现类
 * 
 * @author crazy_cabbage
 *
 */
public class TableIdDaoImpl extends MyBatisDaoImpl<TableId> implements TableIdDao {



  @Override
  public TypeReference<List<TableId>> getListEntityType() {
    return new TypeReference<List<TableId>>() {
    };
  }

  // ========================================业务方法========================================
  @Override
  public TableId findByCode(String code) {
    Assert.assertNotEmptyString(code, message.getMessage("code.isnull"));
    Map<String, Object> parameters = new HashMap<String, Object>(1);
    parameters.put("code", code);
    return findOne(parameters);
  }
}
