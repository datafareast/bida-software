package com.hzizs.analysis.dao;

import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.analysis.entity.RoleMenu;
/**
 * 角色菜单 数据库操作类	 
 * @author crazy_cabbage
 *
 */
public interface RoleMenuDao extends MyBatisDao<RoleMenu> {

}
