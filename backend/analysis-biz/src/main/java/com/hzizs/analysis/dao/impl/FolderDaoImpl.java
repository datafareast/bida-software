package com.hzizs.analysis.dao.impl;

import java.util.List;
import com.fasterxml.jackson.core.type.TypeReference;
import com.hzizs.analysis.dao.FolderDao;
import com.hzizs.analysis.entity.Folder;
import com.hzizs.mybatis.dao.impl.MyBatisShardingDaoImpl;

/**
 * 文件夹 数据库操作类实现类
 * 
 * @author crazy_cabbage
 *
 */
public class FolderDaoImpl extends MyBatisShardingDaoImpl<Folder> implements FolderDao {



  @Override
  public TypeReference<List<Folder>> getListEntityType() {
    return new TypeReference<List<Folder>>() {
    };
  }

  // ========================================业务方法========================================
  @Override
  public String shardingFieldName() {
    return "userId";
  }

}
