package com.hzizs.analysis.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.hzizs.Assert;
import com.hzizs.analysis.dao.SysRoleMenuDao;
import com.hzizs.analysis.entity.SysRoleMenu;
import com.hzizs.analysis.service.SysRoleMenuService;
import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.mybatis.service.impl.MyBatisServiceImpl;
import com.hzizs.util.CollectionUtil;

/**
 * 系统角色菜单 服务实现类
 * 
 * @author crazy_cabbage
 *
 */
public class SysRoleMenuServiceImpl extends MyBatisServiceImpl<SysRoleMenu> implements SysRoleMenuService {


  private SysRoleMenuDao sysRoleMenuDao;

  public void setSysRoleMenuDao(SysRoleMenuDao sysRoleMenuDao) {
    this.sysRoleMenuDao = sysRoleMenuDao;
  }

  @Override
  protected MyBatisDao<SysRoleMenu> getDao() {
    return sysRoleMenuDao;
  }

  // ========================================业务方法========================================
  @Override
  public List<SysRoleMenu> findByRoleId(Long roleId) {
    Assert.assertNotNull(roleId, message.getMessage("roleId.isnull"));
    Map<String, Object> parameters = new HashMap<String, Object>(1);
    parameters.put("roleId", roleId);
    return find(parameters);
  }

  @Override
  public void delete(Long roleId, List<Long> menuIds) {
    if (CollectionUtil.isEmpty(menuIds)) {
      return;
    }
    Assert.assertNotNull(roleId, message.getMessage("roleId.isnull"));
    Map<String, Object> parameters = new HashMap<String, Object>(2);
    parameters.put("roleId", roleId);
    parameters.put("menuIds", menuIds);
    delete(parameters);
  }

  @Override
  public void deleteByRoleId(Long roleId) {
    Assert.assertNotNull(roleId, message.getMessage("roleId.isnull"));
    Map<String, Object> parameters = new HashMap<String, Object>(1);
    parameters.put("roleId", roleId);
    delete(parameters);
  }

  @Override
  public long countByMenuId(Long menuId) {
    Assert.assertNotNull(menuId, message.getMessage("menuId.isnull"));
    Map<String, Object> parameters = new HashMap<String, Object>(1);
    parameters.put("menuId", menuId);
    return count(parameters);
  }
}
