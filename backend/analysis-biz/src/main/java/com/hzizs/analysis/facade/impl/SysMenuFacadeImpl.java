package com.hzizs.analysis.facade.impl;

import com.hzizs.Assert;
import com.hzizs.analysis.facade.SysMenuFacade;
import com.hzizs.analysis.service.SysMenuService;
import com.hzizs.analysis.service.SysRoleMenuService;
import com.hzizs.facade.impl.BaseFacadeImpl;

public class SysMenuFacadeImpl extends BaseFacadeImpl implements SysMenuFacade {
 private SysMenuService sysMenuService;
 private SysRoleMenuService sysRoleMenuService;
  
  public void setSysMenuService(SysMenuService sysMenuService) {
  this.sysMenuService = sysMenuService;
}

public void setSysRoleMenuService(SysRoleMenuService sysRoleMenuService) {
  this.sysRoleMenuService = sysRoleMenuService;
}

//==================================================================
  @Override
  public void deleteById(Long id) {
    long num = sysMenuService.countByParentId(id);
    if(num>0) {
      Assert.customException(message.getMessage("menu.hasChild"));
    }
    num = sysRoleMenuService.countByMenuId(id);
    if(num>0) {
      Assert.customException(message.getMessage("menu.used"));
    }
    sysMenuService.deleteById(id);

  }

  @Override
  public void deleteByIds(Long[] ids) {
       if(ids!=null) {
         for(Long id:ids) {
           deleteById(id);
         }
       }
  }

}
