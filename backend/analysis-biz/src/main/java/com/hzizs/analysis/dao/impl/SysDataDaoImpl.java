package com.hzizs.analysis.dao.impl;
import java.util.List;

import com.fasterxml.jackson.core.type.TypeReference;
import com.hzizs.mybatis.dao.impl.MyBatisDaoImpl;
import com.hzizs.analysis.dao.SysDataDao;
import com.hzizs.analysis.entity.SysData;
/**
 * 数据字典 数据库操作类实现类	 
 * @author crazy_cabbage
 *
 */
public class SysDataDaoImpl extends MyBatisDaoImpl<SysData> implements SysDataDao {



	@Override
	public TypeReference<List<SysData>> getListEntityType() {
		return new TypeReference<List<SysData>>() {
		};
	}
	//========================================业务方法========================================
}
