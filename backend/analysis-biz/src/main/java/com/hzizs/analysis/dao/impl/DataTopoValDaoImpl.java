package com.hzizs.analysis.dao.impl;
import java.util.List;

import com.fasterxml.jackson.core.type.TypeReference;
import com.hzizs.mybatis.dao.impl.MyBatisDaoImpl;
import com.hzizs.analysis.dao.DataTopoValDao;
import com.hzizs.analysis.entity.DataTopoVal;
/**
 * 数据拓普值 数据库操作类实现类	 
 * @author crazy_cabbage
 *
 */
public class DataTopoValDaoImpl extends MyBatisDaoImpl<DataTopoVal> implements DataTopoValDao {



	@Override
	public TypeReference<List<DataTopoVal>> getListEntityType() {
		return new TypeReference<List<DataTopoVal>>() {
		};
	}
	//========================================业务方法========================================
}
