package com.hzizs.analysis.dao.impl;
import java.util.List;

import com.fasterxml.jackson.core.type.TypeReference;
import com.hzizs.mybatis.dao.impl.MyBatisDaoImpl;
import com.hzizs.analysis.dao.AuthDao;
import com.hzizs.analysis.entity.Auth;
/**
 * 权限 数据库操作类实现类	 
 * @author crazy_cabbage
 *
 */
public class AuthDaoImpl extends MyBatisDaoImpl<Auth> implements AuthDao {



	@Override
	public TypeReference<List<Auth>> getListEntityType() {
		return new TypeReference<List<Auth>>() {
		};
	}
	//========================================业务方法========================================
}
