package com.hzizs.analysis.dao;

import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.analysis.entity.Industry;
/**
 * 行业 数据库操作类	 
 * @author crazy_cabbage
 *
 */
public interface IndustryDao extends MyBatisDao<Industry> {

}
