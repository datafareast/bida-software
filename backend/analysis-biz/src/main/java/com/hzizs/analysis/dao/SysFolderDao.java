package com.hzizs.analysis.dao;

import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.analysis.entity.SysFolder;
/**
 * 系统文件夹 数据库操作类	 
 * @author crazy_cabbage
 *
 */
public interface SysFolderDao extends MyBatisDao<SysFolder> {

}
