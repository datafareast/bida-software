package com.hzizs.analysis.service.impl;
import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.mybatis.service.impl.MyBatisServiceImpl;


import com.hzizs.analysis.dao.IndustryDao;
import com.hzizs.analysis.entity.Industry;
import com.hzizs.analysis.service.IndustryService;
/**
 * 行业 服务实现类
 * @author crazy_cabbage
 *
 */
public class IndustryServiceImpl extends MyBatisServiceImpl<Industry> implements IndustryService {
	

	private IndustryDao industryDao;
	public void setIndustryDao(IndustryDao industryDao) {
		this.industryDao = industryDao;
	}
	@Override
	protected MyBatisDao<Industry> getDao() {
		return industryDao;
	}
	//========================================业务方法========================================
}
