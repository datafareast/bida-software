package com.hzizs.analysis.dao.impl;
import java.util.List;

import com.fasterxml.jackson.core.type.TypeReference;
import com.hzizs.mybatis.dao.impl.MyBatisDaoImpl;
import com.hzizs.analysis.dao.SysRoleMenuDao;
import com.hzizs.analysis.entity.SysRoleMenu;
/**
 * 系统角色菜单 数据库操作类实现类	 
 * @author crazy_cabbage
 *
 */
public class SysRoleMenuDaoImpl extends MyBatisDaoImpl<SysRoleMenu> implements SysRoleMenuDao {



	@Override
	public TypeReference<List<SysRoleMenu>> getListEntityType() {
		return new TypeReference<List<SysRoleMenu>>() {
		};
	}
	//========================================业务方法========================================
}
