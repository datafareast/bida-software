package com.hzizs.analysis.service.impl;
import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.mybatis.service.impl.MyBatisServiceImpl;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.hzizs.analysis.dao.IndexTypeDao;
import com.hzizs.analysis.entity.IndexType;
import com.hzizs.analysis.service.IndexTypeService;
import com.hzizs.constants.Enabled;
/**
 * 指标类型 服务实现类
 * @author crazy_cabbage
 *
 */
public class IndexTypeServiceImpl extends MyBatisServiceImpl<IndexType> implements IndexTypeService {
	

	private IndexTypeDao indexTypeDao;
	public void setIndexTypeDao(IndexTypeDao indexTypeDao) {
		this.indexTypeDao = indexTypeDao;
	}
	@Override
	protected MyBatisDao<IndexType> getDao() {
		return indexTypeDao;
	}
	//========================================业务方法========================================
  @Override
  public List<IndexType> findEnabled() {
    Map<String, Object> parameters = new HashMap<String,Object>(1);
    parameters.put("enabled", Enabled.T);
    return find(parameters);
  }
}
