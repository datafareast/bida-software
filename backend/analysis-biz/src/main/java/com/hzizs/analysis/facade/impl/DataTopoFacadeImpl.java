package com.hzizs.analysis.facade.impl;

import java.util.List;
import com.hzizs.analysis.dto.DataTopoDto;
import com.hzizs.analysis.entity.DataTopo;
import com.hzizs.analysis.facade.DataTopoFacade;
import com.hzizs.analysis.service.DataTopoService;

public class DataTopoFacadeImpl implements DataTopoFacade{
  
 private DataTopoService dataTopoService;
  public void setDataTopoService(DataTopoService dataTopoService) {
  this.dataTopoService = dataTopoService;
}

  //====================================================================================

  @Override
  public DataTopoDto findDto(Long id) {
    DataTopoDto dto = new DataTopoDto();
    DataTopo dataTopo = dataTopoService.findById(id);
    dto.setDataTopo(dataTopo);
    List<DataTopo> parent = dataTopoService.findByParent(id);
    List<DataTopo> child = dataTopoService.findByChild(id);
    dto.setChild(child);
    dto.setParent(parent);
    return dto;
  }
  

}
