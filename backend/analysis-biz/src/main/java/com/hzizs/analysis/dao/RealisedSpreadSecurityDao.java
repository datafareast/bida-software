package com.hzizs.analysis.dao;

import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.analysis.entity.RealisedSpreadSecurity;
/**
 * 证券实现价差 数据库操作类	 
 * @author crazy_cabbage
 *
 */
public interface RealisedSpreadSecurityDao extends MyBatisDao<RealisedSpreadSecurity> {

}
