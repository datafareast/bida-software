package com.hzizs.analysis.dao.impl;
import java.util.List;

import com.fasterxml.jackson.core.type.TypeReference;
import com.hzizs.mybatis.dao.impl.MyBatisDaoImpl;
import com.hzizs.analysis.dao.I18nDao;
import com.hzizs.analysis.entity.I18n;
/**
 * 国际化 数据库操作类实现类	 
 * @author crazy_cabbage
 *
 */
public class I18nDaoImpl extends MyBatisDaoImpl<I18n> implements I18nDao {



	@Override
	public TypeReference<List<I18n>> getListEntityType() {
		return new TypeReference<List<I18n>>() {
		};
	}
	//========================================业务方法========================================
}
