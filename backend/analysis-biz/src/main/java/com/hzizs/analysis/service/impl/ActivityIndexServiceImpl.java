package com.hzizs.analysis.service.impl;
import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.mybatis.service.impl.MyBatisServiceImpl;


import com.hzizs.analysis.dao.ActivityIndexDao;
import com.hzizs.analysis.entity.ActivityIndex;
import com.hzizs.analysis.service.ActivityIndexService;
/**
 * 指数指标 服务实现类
 * @author crazy_cabbage
 *
 */
public class ActivityIndexServiceImpl extends MyBatisServiceImpl<ActivityIndex> implements ActivityIndexService {
	

	private ActivityIndexDao activityIndexDao;
	public void setActivityIndexDao(ActivityIndexDao activityIndexDao) {
		this.activityIndexDao = activityIndexDao;
	}
	@Override
	protected MyBatisDao<ActivityIndex> getDao() {
		return activityIndexDao;
	}
	//========================================业务方法========================================
}
