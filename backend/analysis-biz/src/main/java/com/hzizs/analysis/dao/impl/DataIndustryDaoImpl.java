package com.hzizs.analysis.dao.impl;
import java.util.List;

import com.fasterxml.jackson.core.type.TypeReference;
import com.hzizs.mybatis.dao.impl.MyBatisDaoImpl;
import com.hzizs.analysis.dao.DataIndustryDao;
import com.hzizs.analysis.entity.DataIndustry;
/**
 * 数据行业 数据库操作类实现类	 
 * @author crazy_cabbage
 *
 */
public class DataIndustryDaoImpl extends MyBatisDaoImpl<DataIndustry> implements DataIndustryDao {



	@Override
	public TypeReference<List<DataIndustry>> getListEntityType() {
		return new TypeReference<List<DataIndustry>>() {
		};
	}
	//========================================业务方法========================================
}
