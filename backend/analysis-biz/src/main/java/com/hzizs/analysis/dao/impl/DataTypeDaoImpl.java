package com.hzizs.analysis.dao.impl;
import java.util.List;

import com.fasterxml.jackson.core.type.TypeReference;
import com.hzizs.mybatis.dao.impl.MyBatisDaoImpl;
import com.hzizs.analysis.dao.DataTypeDao;
import com.hzizs.analysis.entity.DataType;
/**
 * 数据种类 数据库操作类实现类	 
 * @author crazy_cabbage
 *
 */
public class DataTypeDaoImpl extends MyBatisDaoImpl<DataType> implements DataTypeDao {



	@Override
	public TypeReference<List<DataType>> getListEntityType() {
		return new TypeReference<List<DataType>>() {
		};
	}
	//========================================业务方法========================================
}
