package com.hzizs.analysis.service.impl;

import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.mybatis.service.impl.MyBatisServiceImpl;
import java.util.HashMap;
import java.util.Map;
import com.hzizs.Assert;
import com.hzizs.analysis.dao.DataIndustryDao;
import com.hzizs.analysis.entity.DataIndustry;
import com.hzizs.analysis.service.DataIndustryService;

/**
 * 数据行业 服务实现类
 * 
 * @author crazy_cabbage
 *
 */
public class DataIndustryServiceImpl extends MyBatisServiceImpl<DataIndustry> implements DataIndustryService {


  private DataIndustryDao dataIndustryDao;

  public void setDataIndustryDao(DataIndustryDao dataIndustryDao) {
    this.dataIndustryDao = dataIndustryDao;
  }

  @Override
  protected MyBatisDao<DataIndustry> getDao() {
    return dataIndustryDao;
  }

  // ========================================业务方法========================================
  @Override
  public DataIndustry findByName(String name) {
    Assert.assertNotEmptyString(name, message.getMessage("name.isnull"));
    Map<String, Object> parameters = new HashMap<String, Object>(1);
    parameters.put("name", name);
    return findOne(parameters);
  }
}
