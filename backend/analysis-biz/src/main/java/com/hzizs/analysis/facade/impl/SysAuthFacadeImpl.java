package com.hzizs.analysis.facade.impl;

import com.hzizs.Assert;
import com.hzizs.analysis.entity.SysAuth;
import com.hzizs.analysis.facade.SysAuthFacade;
import com.hzizs.analysis.service.SysAuthService;
import com.hzizs.analysis.service.SysRoleAuthService;
import com.hzizs.facade.impl.BaseFacadeImpl;

public class SysAuthFacadeImpl extends BaseFacadeImpl implements SysAuthFacade {

  private SysRoleAuthService sysRoleAuthService;
  private SysAuthService sysAuthService;
  
  public void setSysRoleAuthService(SysRoleAuthService sysRoleAuthService) {
    this.sysRoleAuthService = sysRoleAuthService;
  }

  public void setSysAuthService(SysAuthService sysAuthService) {
    this.sysAuthService = sysAuthService;
  }
// =================================================================
  @Override
  public void deleteById(Long id) {
       long num = sysRoleAuthService.countByAuthId(id);
       if(num>0) {
         Assert.customException(message.getMessage("authId.used"));
       }
       sysAuthService.deleteById(id);
  }

  @Override
  public void deleteByIds(Long[] ids) {
    if(ids!=null) {
      for(Long id:ids) {
        deleteById(id);
      }
    }

  }

  @Override
  public void save(SysAuth sysAuth) {
    String code = sysAuth.getCode();
    SysAuth po = sysAuthService.findByCode(code);
    if(po!=null) {
      Assert.customException(message.getMessage("code.exists"));
    }
    sysAuthService.save(sysAuth);
    
  }

  @Override
  public void update(SysAuth sysAuth) {
    String code = sysAuth.getCode();
    SysAuth po = sysAuthService.findByCode(code);
    if(po!=null) {
      if(!po.getId().equals(sysAuth.getId())) {
        Assert.customException(message.getMessage("code.exists"));
      }
    }
    sysAuthService.update(sysAuth);
  }

}
