package com.hzizs.analysis.dao.impl;
import java.util.List;

import com.fasterxml.jackson.core.type.TypeReference;
import com.hzizs.mybatis.dao.impl.MyBatisDaoImpl;
import com.hzizs.analysis.dao.CmsDao;
import com.hzizs.analysis.entity.Cms;
/**
 * 内容管理系统 数据库操作类实现类	 
 * @author crazy_cabbage
 *
 */
public class CmsDaoImpl extends MyBatisDaoImpl<Cms> implements CmsDao {



	@Override
	public TypeReference<List<Cms>> getListEntityType() {
		return new TypeReference<List<Cms>>() {
		};
	}
	//========================================业务方法========================================
}
