package com.hzizs.analysis.dao.impl;
import java.util.List;

import com.fasterxml.jackson.core.type.TypeReference;
import com.hzizs.mybatis.dao.impl.MyBatisDaoImpl;
import com.hzizs.analysis.dao.IndustryIndexDao;
import com.hzizs.analysis.entity.IndustryIndex;
/**
 * 行业指标 数据库操作类实现类	 
 * @author crazy_cabbage
 *
 */
public class IndustryIndexDaoImpl extends MyBatisDaoImpl<IndustryIndex> implements IndustryIndexDao {



	@Override
	public TypeReference<List<IndustryIndex>> getListEntityType() {
		return new TypeReference<List<IndustryIndex>>() {
		};
	}
	//========================================业务方法========================================
}
