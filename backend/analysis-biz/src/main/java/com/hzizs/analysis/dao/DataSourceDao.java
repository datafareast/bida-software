package com.hzizs.analysis.dao;

import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.analysis.entity.DataSource;
/**
 * 数据源 数据库操作类	 
 * @author crazy_cabbage
 *
 */
public interface DataSourceDao extends MyBatisDao<DataSource> {

}
