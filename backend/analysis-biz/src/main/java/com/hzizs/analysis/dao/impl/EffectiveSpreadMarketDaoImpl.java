package com.hzizs.analysis.dao.impl;
import java.util.List;

import com.fasterxml.jackson.core.type.TypeReference;
import com.hzizs.mybatis.dao.impl.MyBatisDaoImpl;
import com.hzizs.analysis.dao.EffectiveSpreadMarketDao;
import com.hzizs.analysis.entity.EffectiveSpreadMarket;
/**
 * 市场有效价差 数据库操作类实现类	 
 * @author crazy_cabbage
 *
 */
public class EffectiveSpreadMarketDaoImpl extends MyBatisDaoImpl<EffectiveSpreadMarket> implements EffectiveSpreadMarketDao {



	@Override
	public TypeReference<List<EffectiveSpreadMarket>> getListEntityType() {
		return new TypeReference<List<EffectiveSpreadMarket>>() {
		};
	}
	//========================================业务方法========================================
}
