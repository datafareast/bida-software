package com.hzizs.analysis.dao;

import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.analysis.entity.Slide;
/**
 * 轮播图 数据库操作类	 
 * @author crazy_cabbage
 *
 */
public interface SlideDao extends MyBatisDao<Slide> {

}
