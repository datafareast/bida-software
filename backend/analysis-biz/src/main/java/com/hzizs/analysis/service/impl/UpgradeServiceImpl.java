package com.hzizs.analysis.service.impl;
import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.mybatis.service.impl.MyBatisServiceImpl;


import com.hzizs.analysis.dao.UpgradeDao;
import com.hzizs.analysis.entity.Upgrade;
import com.hzizs.analysis.service.UpgradeService;
/**
 * 升级信息 服务实现类
 * @author crazy_cabbage
 *
 */
public class UpgradeServiceImpl extends MyBatisServiceImpl<Upgrade> implements UpgradeService {
	

	private UpgradeDao upgradeDao;
	public void setUpgradeDao(UpgradeDao upgradeDao) {
		this.upgradeDao = upgradeDao;
	}
	@Override
	protected MyBatisDao<Upgrade> getDao() {
		return upgradeDao;
	}
	//========================================业务方法========================================
}
