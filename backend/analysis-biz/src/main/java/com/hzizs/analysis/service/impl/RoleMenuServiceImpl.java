package com.hzizs.analysis.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.hzizs.Assert;
import com.hzizs.analysis.dao.RoleMenuDao;
import com.hzizs.analysis.entity.RoleMenu;
import com.hzizs.analysis.service.RoleMenuService;
import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.mybatis.service.impl.MyBatisServiceImpl;
import com.hzizs.util.CollectionUtil;

/**
 * 角色菜单 服务实现类
 * 
 * @author crazy_cabbage
 *
 */
public class RoleMenuServiceImpl extends MyBatisServiceImpl<RoleMenu> implements RoleMenuService {


  private RoleMenuDao roleMenuDao;

  public void setRoleMenuDao(RoleMenuDao roleMenuDao) {
    this.roleMenuDao = roleMenuDao;
  }

  @Override
  protected MyBatisDao<RoleMenu> getDao() {
    return roleMenuDao;
  }

  // ========================================业务方法========================================
  @Override
  public List<RoleMenu> findByRoleId(Long roleId) {
    Assert.assertNotNull(roleId, message.getMessage("roleId.isnull"));
    Map<String, Object> parameters = new HashMap<String, Object>(1);
    parameters.put("roleId", roleId);
    return find(parameters);
  }

  @Override
  public void delete(Long roleId, List<Long> menuIds) {
    Assert.assertNotNull(roleId, message.getMessage("roleId.isnull"));
    if (CollectionUtil.isEmpty(menuIds)) {
      return;
    }
    Map<String, Object> parameters = new HashMap<String, Object>(2);
    parameters.put("roleId", roleId);
    parameters.put("menuIds", menuIds);
    delete(parameters);
  }

  @Override
  public void deleteByRoleId(Long roleId) {
    Assert.assertNotNull(roleId, message.getMessage("roleId.isnull"));
    Map<String, Object> parameters = new HashMap<String, Object>(1);
    parameters.put("roleId", roleId);
    delete(parameters);
  }

  @Override
  public long countByMenuId(Long menuId) {
    Assert.assertNotNull(menuId, message.getMessage("menuId.isnull"));
    Map<String, Object> parameters = new HashMap<String, Object>(1);
    parameters.put("menuId", menuId);
    return count(parameters);
  }
}
