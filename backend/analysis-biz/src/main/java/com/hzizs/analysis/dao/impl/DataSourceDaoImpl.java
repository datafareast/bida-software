package com.hzizs.analysis.dao.impl;
import java.util.List;

import com.fasterxml.jackson.core.type.TypeReference;
import com.hzizs.mybatis.dao.impl.MyBatisDaoImpl;
import com.hzizs.analysis.dao.DataSourceDao;
import com.hzizs.analysis.entity.DataSource;
/**
 * 数据源 数据库操作类实现类	 
 * @author crazy_cabbage
 *
 */
public class DataSourceDaoImpl extends MyBatisDaoImpl<DataSource> implements DataSourceDao {



	@Override
	public TypeReference<List<DataSource>> getListEntityType() {
		return new TypeReference<List<DataSource>>() {
		};
	}
	//========================================业务方法========================================
}
