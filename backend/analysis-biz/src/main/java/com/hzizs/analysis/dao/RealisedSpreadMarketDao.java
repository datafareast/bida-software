package com.hzizs.analysis.dao;

import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.analysis.entity.RealisedSpreadMarket;
/**
 * 市场实现价差 数据库操作类	 
 * @author crazy_cabbage
 *
 */
public interface RealisedSpreadMarketDao extends MyBatisDao<RealisedSpreadMarket> {

}
