package com.hzizs.analysis.facade.impl;

import com.hzizs.Assert;
import com.hzizs.analysis.entity.Ip;
import com.hzizs.analysis.facade.IpFacade;
import com.hzizs.analysis.service.IpService;
import com.hzizs.facade.impl.BaseFacadeImpl;
import com.hzizs.util.IPUtil;
import com.hzizs.util.RegexUtil;
import com.hzizs.util.StringUtil;

public class IpFacadeImpl extends BaseFacadeImpl implements IpFacade {
  private IpService ipService;

  public void setIpService(IpService ipService) {
    this.ipService = ipService;
  }

  // =============================================================================
  @Override
  public Ip save(Ip ip) {
    String startIpStr = ip.getStartIpStr();
    String endIpStr = ip.getEndIpStr();
    if (StringUtil.isEmpty(startIpStr)) {
      Assert.customException(message.getMessage("startIp.isnull"));
    }
    if (StringUtil.isEmpty(endIpStr)) {
      Assert.customException(message.getMessage("endIp.isnull"));
    }
    if (!RegexUtil.isIp(startIpStr)) {
      Assert.customException(message.getMessage("startIp.err"));
    }
    if (!RegexUtil.isIp(endIpStr)) {
      Assert.customException(message.getMessage("endIp.err"));
    }
    long startIp = IPUtil.str2num(startIpStr);
    long endIp = IPUtil.str2num(endIpStr);
    if (startIp > endIp) {
      Assert.customException(message.getMessage("startIp.big.endIp"));
    }
    long num = ipService.countByStartIpEndIp(startIp, endIp);
    if (num > 0L) {
      Assert.customException(message.getMessage("ip.exists"));
    }
    ip.setStartIp(startIp);
    ip.setEndIp(endIp);
    return ipService.save(ip);
  }

  @Override
  public Ip update(Ip ip) {
    String startIpStr = ip.getStartIpStr();
    String endIpStr = ip.getEndIpStr();
    if (StringUtil.isEmpty(startIpStr)) {
      Assert.customException(message.getMessage("startIp.isnull"));
    }
    if (StringUtil.isEmpty(endIpStr)) {
      Assert.customException(message.getMessage("endIp.isnull"));
    }
    if (!RegexUtil.isIp(startIpStr)) {
      Assert.customException(message.getMessage("startIp.err"));
    }
    if (!RegexUtil.isIp(endIpStr)) {
      Assert.customException(message.getMessage("endIp.err"));
    }
    long startIp = IPUtil.str2num(startIpStr);
    long endIp = IPUtil.str2num(endIpStr);
    if (startIp > endIp) {
      Assert.customException(message.getMessage("startIp.big.endIp"));
    }
    ip.setStartIp(startIp);
    ip.setEndIp(endIp);
    Ip po = ipService.findById(ip.getId());
    if (po.equals(ip)) {
      return ip;
    }
    long num = ipService.countByStartIpEndIp(startIp, endIp);
    if (num > 1L) {
      Assert.customException(message.getMessage("ip.exists"));
    }
    return ipService.update(ip);
  }

  @Override
  public void deleteByIds(Long[] ids) {
    if (ids != null) {
      for (Long id : ids) {
        deleteById(id);
      }
    }
  }

  @Override
  public void deleteById(Long id) {
    ipService.deleteById(id);
  }

}
