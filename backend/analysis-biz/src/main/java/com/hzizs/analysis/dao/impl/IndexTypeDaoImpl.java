package com.hzizs.analysis.dao.impl;
import java.util.List;

import com.fasterxml.jackson.core.type.TypeReference;
import com.hzizs.mybatis.dao.impl.MyBatisDaoImpl;
import com.hzizs.analysis.dao.IndexTypeDao;
import com.hzizs.analysis.entity.IndexType;
/**
 * 指标类型 数据库操作类实现类	 
 * @author crazy_cabbage
 *
 */
public class IndexTypeDaoImpl extends MyBatisDaoImpl<IndexType> implements IndexTypeDao {



	@Override
	public TypeReference<List<IndexType>> getListEntityType() {
		return new TypeReference<List<IndexType>>() {
		};
	}
	//========================================业务方法========================================
}
