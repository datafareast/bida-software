package com.hzizs.analysis.dao;

import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.analysis.entity.SysAuth;
/**
 * 系统权限 数据库操作类	 
 * @author crazy_cabbage
 *
 */
public interface SysAuthDao extends MyBatisDao<SysAuth> {

}
