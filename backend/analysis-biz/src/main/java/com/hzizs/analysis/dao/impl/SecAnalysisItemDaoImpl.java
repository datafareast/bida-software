package com.hzizs.analysis.dao.impl;

import java.util.List;

import com.fasterxml.jackson.core.type.TypeReference;
import com.hzizs.mybatis.dao.impl.MyBatisDaoImpl;
import com.hzizs.analysis.dao.SecAnalysisItemDao;
import com.hzizs.analysis.entity.SecAnalysisItem;

/**
 * 业绩分析条目 数据库操作类实现类
 * 
 * @author crazy_cabbage
 *
 */
public class SecAnalysisItemDaoImpl extends MyBatisDaoImpl<SecAnalysisItem> implements SecAnalysisItemDao {



  @Override
  public TypeReference<List<SecAnalysisItem>> getListEntityType() {
    return new TypeReference<List<SecAnalysisItem>>() {
    };
  }
  // ========================================业务方法========================================
}
