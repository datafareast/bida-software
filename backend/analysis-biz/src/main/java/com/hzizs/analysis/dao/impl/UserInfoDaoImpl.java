package com.hzizs.analysis.dao.impl;
import java.util.List;

import com.fasterxml.jackson.core.type.TypeReference;
import com.hzizs.mybatis.dao.impl.MyBatisDaoImpl;
import com.hzizs.analysis.dao.UserInfoDao;
import com.hzizs.analysis.entity.UserInfo;
/**
 * 用户信息 数据库操作类实现类	 
 * @author crazy_cabbage
 *
 */
public class UserInfoDaoImpl extends MyBatisDaoImpl<UserInfo> implements UserInfoDao {



	@Override
	public TypeReference<List<UserInfo>> getListEntityType() {
		return new TypeReference<List<UserInfo>>() {
		};
	}
	//========================================业务方法========================================
}
