package com.hzizs.analysis.service.impl;
import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.mybatis.service.impl.MyBatisServiceImpl;


import com.hzizs.analysis.dao.TableIdDao;
import com.hzizs.analysis.entity.TableId;
import com.hzizs.analysis.service.TableIdService;
/**
 * 表主键 服务实现类
 * @author crazy_cabbage
 *
 */
public class TableIdServiceImpl extends MyBatisServiceImpl<TableId> implements TableIdService {
	

	private TableIdDao tableIdDao;
	public void setTableIdDao(TableIdDao tableIdDao) {
		this.tableIdDao = tableIdDao;
	}
	@Override
	protected MyBatisDao<TableId> getDao() {
		return tableIdDao;
	}
	//========================================业务方法========================================

	  @Override
	  public TableId doSaveOrGet(String code) {
	    TableId tableId = tableIdDao.findByCode(code);
	    if(tableId==null) {
	      tableId = new TableId();
	      tableId.setName(code);
	      tableId.setCode(code);
	      tableId.setStep(300L);
	      tableId.setVal(0L);
	      tableIdDao.save(tableId);
	    }else {
	      tableId.setVal(tableId.getVal()+1);
	      tableIdDao.update(tableId);
	    }
	    return tableId;
	  }
}
