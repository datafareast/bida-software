package com.hzizs.analysis.dao.impl;
import java.util.List;

import com.fasterxml.jackson.core.type.TypeReference;
import com.hzizs.mybatis.dao.impl.MyBatisDaoImpl;
import com.hzizs.analysis.dao.UserDao;
import com.hzizs.analysis.entity.User;
/**
 * 用户 数据库操作类实现类	 
 * @author crazy_cabbage
 *
 */
public class UserDaoImpl extends MyBatisDaoImpl<User> implements UserDao {



	@Override
	public TypeReference<List<User>> getListEntityType() {
		return new TypeReference<List<User>>() {
		};
	}
	//========================================业务方法========================================
}
