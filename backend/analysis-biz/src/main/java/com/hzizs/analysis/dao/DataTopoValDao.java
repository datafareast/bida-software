package com.hzizs.analysis.dao;

import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.analysis.entity.DataTopoVal;
/**
 * 数据拓普值 数据库操作类	 
 * @author crazy_cabbage
 *
 */
public interface DataTopoValDao extends MyBatisDao<DataTopoVal> {

}
