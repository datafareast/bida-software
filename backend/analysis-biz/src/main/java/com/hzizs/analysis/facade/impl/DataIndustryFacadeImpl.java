package com.hzizs.analysis.facade.impl;

import com.hzizs.Assert;
import com.hzizs.analysis.entity.DataIndustry;
import com.hzizs.analysis.facade.DataIndustryFacade;
import com.hzizs.analysis.service.DataIndustryService;
import com.hzizs.analysis.service.DataTypeService;
import com.hzizs.facade.impl.BaseFacadeImpl;

public class DataIndustryFacadeImpl extends BaseFacadeImpl implements DataIndustryFacade {
  private DataIndustryService dataIndustryService;
  private DataTypeService dataTypeService;

  public void setDataIndustryService(DataIndustryService dataIndustryService) {
    this.dataIndustryService = dataIndustryService;
  }

  public void setDataTypeService(DataTypeService dataTypeService) {
    this.dataTypeService = dataTypeService;
  }


  // ========================================业务=========================================
  @Override
  public void deleteByIds(Long[] ids) {
    if (ids != null) {
      for (Long id : ids) {
        deleteById(id);
      }
    }
  }

  @Override
  public void deleteById(Long id) {
    long num = dataTypeService.countByDataIndustryId(id);
    if (num > 0L) {
      Assert.customException(message.getMessage("dataIndustryId.used"));
    }
    dataIndustryService.deleteById(id);
  }

  @Override
  public DataIndustry save(DataIndustry dataIndustry) {
    String name = dataIndustry.getName();
    DataIndustry po = dataIndustryService.findByName(name);
    if (po != null) {
      Assert.customException(message.getMessage("name.exists"));
    }
    dataIndustryService.save(dataIndustry);
    return dataIndustry;
  }

  @Override
  public DataIndustry update(DataIndustry dataIndustry) {
    Long id = dataIndustry.getId();
    String name = dataIndustry.getName();
    DataIndustry po = dataIndustryService.findByName(name);
    if (po != null && !po.getId().equals(id)) {
      Assert.customException(message.getMessage("name.exists"));
    }
    dataIndustryService.update(dataIndustry);
    return dataIndustry;
  }

}
