package com.hzizs.analysis.service.impl;
import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.mybatis.service.impl.MyBatisServiceImpl;


import com.hzizs.analysis.dao.DailyTradingActivitySecurityDao;
import com.hzizs.analysis.entity.DailyTradingActivitySecurity;
import com.hzizs.analysis.service.DailyTradingActivitySecurityService;
/**
 * 证券日交易 服务实现类
 * @author crazy_cabbage
 *
 */
public class DailyTradingActivitySecurityServiceImpl extends MyBatisServiceImpl<DailyTradingActivitySecurity> implements DailyTradingActivitySecurityService {
	

	private DailyTradingActivitySecurityDao dailyTradingActivitySecurityDao;
	public void setDailyTradingActivitySecurityDao(DailyTradingActivitySecurityDao dailyTradingActivitySecurityDao) {
		this.dailyTradingActivitySecurityDao = dailyTradingActivitySecurityDao;
	}
	@Override
	protected MyBatisDao<DailyTradingActivitySecurity> getDao() {
		return dailyTradingActivitySecurityDao;
	}
	//========================================业务方法========================================
}
