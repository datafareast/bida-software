package com.hzizs.analysis.facade.impl;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import com.hzizs.Assert;
import com.hzizs.analysis.dto.SysRoleDto;
import com.hzizs.analysis.entity.SysAuth;
import com.hzizs.analysis.entity.SysMenu;
import com.hzizs.analysis.entity.SysRole;
import com.hzizs.analysis.entity.SysRoleAuth;
import com.hzizs.analysis.entity.SysRoleMenu;
import com.hzizs.analysis.facade.SysRoleFacade;
import com.hzizs.analysis.service.SysAuthService;
import com.hzizs.analysis.service.SysMenuService;
import com.hzizs.analysis.service.SysRoleAuthService;
import com.hzizs.analysis.service.SysRoleMenuService;
import com.hzizs.analysis.service.SysRoleService;
import com.hzizs.analysis.service.SysUserRoleService;
import com.hzizs.constants.Enabled;
import com.hzizs.constants.SymbolConstants;
import com.hzizs.facade.impl.BaseFacadeImpl;
import com.hzizs.util.StreamUtil;
import com.hzizs.util.StringUtil;

public class SysRoleFacadeImpl  extends BaseFacadeImpl implements SysRoleFacade{
  private SysRoleService sysRoleService;
  private SysRoleMenuService sysRoleMenuService;
  private SysMenuService sysMenuService;
  private SysAuthService sysAuthService;
  private SysRoleAuthService sysRoleAuthService;
  private SysUserRoleService sysUserRoleService;
  public void setSysRoleService(SysRoleService sysRoleService) {
    this.sysRoleService = sysRoleService;
  }
  public void setSysRoleMenuService(SysRoleMenuService sysRoleMenuService) {
    this.sysRoleMenuService = sysRoleMenuService;
  }
  public void setSysMenuService(SysMenuService sysMenuService) {
    this.sysMenuService = sysMenuService;
  }
  public void setSysAuthService(SysAuthService sysAuthService) {
    this.sysAuthService = sysAuthService;
  }
  public void setSysRoleAuthService(SysRoleAuthService sysRoleAuthService) {
    this.sysRoleAuthService = sysRoleAuthService;
  }
  public void setSysUserRoleService(SysUserRoleService sysUserRoleService) {
    this.sysUserRoleService = sysUserRoleService;
  }
  //=============================================================================
  @Override
  public SysRoleDto findById(Long id) {
    SysRole role = sysRoleService.findById(id);
    SysRoleDto dto = new SysRoleDto();
    dto.setRole(role);
    List<SysRoleMenu> roleMenus = sysRoleMenuService.findByRoleId(id);
    List<Long> menuIds = roleMenus.parallelStream().filter(roleMenu->Enabled.F.equals(roleMenu.getIsHalf())).map(SysRoleMenu::getMenuId).collect(Collectors.toList());
    List<Long> halfMenuIds = roleMenus.parallelStream().filter(roleMenu->Enabled.T.equals(roleMenu.getIsHalf())).map(SysRoleMenu::getMenuId).collect(Collectors.toList());
    dto.setMenuIds(menuIds);
    dto.setHalfMenuIds(halfMenuIds);
    return dto;
  }
  @Override
  public void save(SysRoleDto dto) {
    SysRole role = dto.getRole();
    sysRoleService.save(role);
    Long roleId = role.getId();
    final List<Long> menuIds = dto.getMenuIds();
    List<Long> halfMenuIds = dto.getHalfMenuIds();
    List<Long> allMenuIds = StreamUtil.unionAll(menuIds, halfMenuIds);
    List<SysRoleMenu> allRoleMenus = allMenuIds.parallelStream().map(menuId->{
      SysRoleMenu roleMenu = new SysRoleMenu();
      roleMenu.setRoleId(roleId);
      roleMenu.setMenuId(menuId);
      roleMenu.setIsHalf(menuIds.contains(menuId)?Enabled.F:Enabled.T);
      return roleMenu;
    }).collect(Collectors.toList());
    sysRoleMenuService.saveBatch(allRoleMenus);
    List<SysMenu> menus = sysMenuService.findAll();
    //找到权限码
    List<String> authCodes = menus.parallelStream().filter(menu->allMenuIds.contains(menu.getId())).flatMap(menu->{
      String authCode = menu.getAuthCode();
      if(StringUtil.isEmpty(authCode)) {
        return Stream.empty();
      }else {
        return Stream.of(authCode.split(SymbolConstants.COMMA));
      }
    }).distinct().collect(Collectors.toList());
    List<SysAuth> auths = sysAuthService.findAll();
    Map<String, Long> authMap = auths.parallelStream().collect(Collectors.toMap(SysAuth::getCode, SysAuth::getId));
    List<SysRoleAuth> roleAuths = authCodes.parallelStream().map(code->{
      Long authId = authMap.get(code);
      SysRoleAuth roleAuth = new SysRoleAuth();
      roleAuth.setAuthId(authId);
      roleAuth.setRoleId(roleId);
      return roleAuth;
    }).collect(Collectors.toList());
    sysRoleAuthService.saveBatch(roleAuths);
    
  }
  @Override
  public void update(SysRoleDto dto) {
    SysRole role = dto.getRole();
    Long roleId = role.getId();
    SysRole po = sysRoleService.findById(roleId);
    if(!po.equals(role)) {
      sysRoleService.update(role);
    }
    final List<Long> menuIds = dto.getMenuIds();
    List<Long> halfMenuIds = dto.getHalfMenuIds();
    List<Long> allMenuIds = StreamUtil.unionAll(menuIds, halfMenuIds);
    List<SysRoleMenu> allRoleMenus = allMenuIds.parallelStream().map(menuId->{
      SysRoleMenu roleMenu = new SysRoleMenu();
      roleMenu.setRoleId(roleId);
      roleMenu.setMenuId(menuId);
      roleMenu.setIsHalf(menuIds.contains(menuId)?Enabled.F:Enabled.T);
      return roleMenu;
    }).collect(Collectors.toList());
    
    Map<Long, SysRoleMenu> roleMenuMap = allRoleMenus.parallelStream().collect(Collectors.toMap(SysRoleMenu::getMenuId , Function.identity()));
    List<SysRoleMenu> dbRoleMenus = sysRoleMenuService.findByRoleId(roleId);
    List<Long> dbMenuIds = dbRoleMenus.parallelStream().map(SysRoleMenu::getMenuId).collect(Collectors.toList());
    Map<Long, SysRoleMenu> dbRoleMenuMap = dbRoleMenus.parallelStream().collect(Collectors.toMap(SysRoleMenu::getMenuId, Function.identity()));
    //删除
    List<Long> removeMenuIds = StreamUtil.subtract(dbMenuIds, allMenuIds);
    sysRoleMenuService.delete(roleId,removeMenuIds);
    
    //新增
    List<Long> addMenuIds = StreamUtil.subtract(allMenuIds, dbMenuIds);
    List<SysRoleMenu> addRoleMenus = allRoleMenus.parallelStream().filter(roleMenu->addMenuIds.contains(roleMenu.getMenuId())).collect(Collectors.toList());
    sysRoleMenuService.saveBatch(addRoleMenus);
    
    //修改
    List<Long> modifyMenuIds = StreamUtil.subtract(allMenuIds, addMenuIds);
    List<SysRoleMenu> modifyRoleMenus = modifyMenuIds.parallelStream().filter(menuId->{
      SysRoleMenu roleMenu = roleMenuMap.get(menuId);
      SysRoleMenu dbRoleMenu = dbRoleMenuMap.get(menuId);
      return !roleMenu.getIsHalf().equals(dbRoleMenu.getIsHalf());
    }).map(menuId->{
      SysRoleMenu roleMenu = roleMenuMap.get(menuId);
      SysRoleMenu dbRoleMenu = dbRoleMenuMap.get(menuId);
      dbRoleMenu.setIsHalf(roleMenu.getIsHalf());
      return dbRoleMenu;
    }).collect(Collectors.toList());
    for(SysRoleMenu roleMenu:modifyRoleMenus) {
      sysRoleMenuService.update(roleMenu);
    }
    //处理权限
    List<SysAuth> dbAuths = sysAuthService.findAll();
    Map<String, Long> authMap = dbAuths.parallelStream().collect(Collectors.toMap(SysAuth::getCode, SysAuth::getId));
    
    //权限处理
    //所有菜单
    List<SysMenu> menus = sysMenuService.findAll();
    Map<Long, SysMenu> menuMap = menus.parallelStream().collect(Collectors.toMap(SysMenu::getId, Function.identity()));
    List<Long> authIds = menuIds.parallelStream().flatMap(menuId->{
      SysMenu menu = menuMap.get(menuId);
      String authCode = menu.getAuthCode();
      if(StringUtil.isEmpty(authCode)) {
        return Stream.empty();
      }else {
        return Stream.of(authCode.split(SymbolConstants.COMMA));
      }
    }).distinct().map(code->{
      return authMap.get(code);
    }).collect(Collectors.toList());
    
    //删除权限
    List<SysRoleAuth> dbRoleAuths = sysRoleAuthService.findByRoleId(roleId);
    List<Long> dbAuthIds = dbRoleAuths.parallelStream().map(SysRoleAuth::getAuthId).collect(Collectors.toList());
    List<Long> removeAuthIds = StreamUtil.subtract(dbAuthIds, authIds);
    sysRoleAuthService.delete(roleId,removeAuthIds);
    //新增权限
    List<Long> addAuthIds = StreamUtil.subtract(authIds, dbAuthIds);
    List<SysRoleAuth> addAuths = addAuthIds.parallelStream().map(authId->{
      SysRoleAuth roleAuth = new SysRoleAuth();
      roleAuth.setAuthId(authId);
      roleAuth.setRoleId(roleId);
      return roleAuth;
    }).collect(Collectors.toList());
    sysRoleAuthService.saveBatch(addAuths);
    
    
  }
  @Override
  public void deleteById(Long id) {
    long num = sysUserRoleService.countByRoleId(id);
    if(num>0) {
      Assert.customException(message.getMessage("roleId.used"));
    }
    sysRoleService.deleteById(id);
    sysRoleMenuService.deleteByRoleId(id);
    sysRoleAuthService.deleteByRoleId(id);
    
  }
  @Override
  public void deleteByIds(Long[] ids) {
    for(Long id :ids) {
      deleteById(id);
    }
  }
  @Override
  public List<String> findAuthByRoleCode(String roleCode) {
    List<SysAuth> auths = sysAuthService.findAll();
    if(SymbolConstants.ASTERISK.equals(roleCode)) {
      List<String> codes  = auths.parallelStream().map(SysAuth::getCode).collect(Collectors.toList());
      return codes;
    }else {
      SysRole role = sysRoleService.findByCode(roleCode);
      Long roleId = role.getId();
      List<SysRoleAuth> roleAuths = sysRoleAuthService.findByRoleId(roleId);
      Set<Long> authIds = roleAuths.parallelStream().map(SysRoleAuth::getAuthId).collect(Collectors.toSet());
      List<String> codes = auths.parallelStream().filter(auth->authIds.contains(auth.getId())).map(SysAuth::getCode).collect(Collectors.toList());
      return codes;
    }
  }


}
