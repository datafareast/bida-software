package com.hzizs.analysis.service.impl;
import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.mybatis.service.impl.MyBatisServiceImpl;


import com.hzizs.analysis.dao.DataExtractDao;
import com.hzizs.analysis.entity.DataExtract;
import com.hzizs.analysis.service.DataExtractService;
/**
 * 数据抽取 服务实现类
 * @author crazy_cabbage
 *
 */
public class DataExtractServiceImpl extends MyBatisServiceImpl<DataExtract> implements DataExtractService {
	

	private DataExtractDao dataExtractDao;
	public void setDataExtractDao(DataExtractDao dataExtractDao) {
		this.dataExtractDao = dataExtractDao;
	}
	@Override
	protected MyBatisDao<DataExtract> getDao() {
		return dataExtractDao;
	}
	//========================================业务方法========================================
}
