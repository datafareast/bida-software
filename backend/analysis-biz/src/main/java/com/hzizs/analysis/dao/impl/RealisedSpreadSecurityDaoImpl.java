package com.hzizs.analysis.dao.impl;
import java.util.List;

import com.fasterxml.jackson.core.type.TypeReference;
import com.hzizs.mybatis.dao.impl.MyBatisDaoImpl;
import com.hzizs.analysis.dao.RealisedSpreadSecurityDao;
import com.hzizs.analysis.entity.RealisedSpreadSecurity;
/**
 * 证券实现价差 数据库操作类实现类	 
 * @author crazy_cabbage
 *
 */
public class RealisedSpreadSecurityDaoImpl extends MyBatisDaoImpl<RealisedSpreadSecurity> implements RealisedSpreadSecurityDao {



	@Override
	public TypeReference<List<RealisedSpreadSecurity>> getListEntityType() {
		return new TypeReference<List<RealisedSpreadSecurity>>() {
		};
	}
	//========================================业务方法========================================
}
