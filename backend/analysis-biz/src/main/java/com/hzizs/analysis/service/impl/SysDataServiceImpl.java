package com.hzizs.analysis.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.hzizs.Assert;
import com.hzizs.analysis.dao.SysDataDao;
import com.hzizs.analysis.entity.SysData;
import com.hzizs.analysis.service.SysDataService;
import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.mybatis.service.impl.MyBatisServiceImpl;

/**
 * 数据字典 服务实现类
 * 
 * @author crazy_cabbage
 *
 */
public class SysDataServiceImpl extends MyBatisServiceImpl<SysData> implements SysDataService {


  private SysDataDao sysDataDao;

  public void setSysDataDao(SysDataDao sysDataDao) {
    this.sysDataDao = sysDataDao;
  }

  @Override
  protected MyBatisDao<SysData> getDao() {
    return sysDataDao;
  }

  // ========================================业务方法========================================
  @Override
  public List<SysData> findByParentId(Long parentId) {
    Assert.assertNotNull(parentId, message.getMessage("parentId.isnull"));
    Map<String, Object> parameters = new HashMap<String, Object>(1);
    parameters.put("parentId", parentId);
    return find(parameters);
  }

  @Override
  public List<SysData> findByParentCode(String parentCode) {
    SysData sysData = findByCode(parentCode, 0L);
    return findByParentId(sysData.getId());
  }

  @Override
  public SysData findByCode(String code, Long parentId) {
    Assert.assertNotEmptyString(code, message.getMessage("code.isnull"));
    Assert.assertNotNull(parentId, message.getMessage("parentId.isnull"));
    Map<String, Object> parameters = new HashMap<String, Object>(2);
    parameters.put("code", code);
    parameters.put("parentId", parentId);
    return findOne(parameters);
  }

  @Override
  public long countByParentId(Long parentId) {
    Assert.assertNotNull(parentId, message.getMessage("parentId.isnull"));
    Map<String, Object> parameters = new HashMap<String, Object>(1);
    parameters.put("parentId", parentId);
    return count(parameters);
  }
}
