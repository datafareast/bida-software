package com.hzizs.analysis.dao.impl;
import java.util.List;

import com.fasterxml.jackson.core.type.TypeReference;
import com.hzizs.mybatis.dao.impl.MyBatisDaoImpl;
import com.hzizs.analysis.dao.SysMenuDao;
import com.hzizs.analysis.entity.SysMenu;
/**
 * 系统菜单 数据库操作类实现类	 
 * @author crazy_cabbage
 *
 */
public class SysMenuDaoImpl extends MyBatisDaoImpl<SysMenu> implements SysMenuDao {



	@Override
	public TypeReference<List<SysMenu>> getListEntityType() {
		return new TypeReference<List<SysMenu>>() {
		};
	}
	//========================================业务方法========================================
}
