package com.hzizs.analysis.dao;

import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.analysis.entity.DataIndustry;
/**
 * 数据行业 数据库操作类	 
 * @author crazy_cabbage
 *
 */
public interface DataIndustryDao extends MyBatisDao<DataIndustry> {

}
