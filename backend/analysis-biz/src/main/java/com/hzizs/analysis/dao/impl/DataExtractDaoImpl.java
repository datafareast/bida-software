package com.hzizs.analysis.dao.impl;
import java.util.List;

import com.fasterxml.jackson.core.type.TypeReference;
import com.hzizs.mybatis.dao.impl.MyBatisDaoImpl;
import com.hzizs.analysis.dao.DataExtractDao;
import com.hzizs.analysis.entity.DataExtract;
/**
 * 数据抽取 数据库操作类实现类	 
 * @author crazy_cabbage
 *
 */
public class DataExtractDaoImpl extends MyBatisDaoImpl<DataExtract> implements DataExtractDao {



	@Override
	public TypeReference<List<DataExtract>> getListEntityType() {
		return new TypeReference<List<DataExtract>>() {
		};
	}
	//========================================业务方法========================================
}
