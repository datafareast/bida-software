package com.hzizs.analysis.service.impl;

import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.mybatis.service.impl.MyBatisServiceImpl;
import com.hzizs.util.DateUtil;
import com.hzizs.util.StringUtil;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.hzizs.Assert;
import com.hzizs.analysis.constants.Constants.SysOptionCode;
import com.hzizs.analysis.dao.SysOptionDao;
import com.hzizs.analysis.entity.SysOption;
import com.hzizs.analysis.service.SysOptionService;
import com.hzizs.analysis.vo.DataConfig;
import com.hzizs.constants.SymbolConstants;

/**
 * 数据选项 服务实现类
 * 
 * @author crazy_cabbage
 *
 */
public class SysOptionServiceImpl extends MyBatisServiceImpl<SysOption> implements SysOptionService {


  private SysOptionDao sysOptionDao;

  public void setSysOptionDao(SysOptionDao sysOptionDao) {
    this.sysOptionDao = sysOptionDao;
  }

  @Override
  protected MyBatisDao<SysOption> getDao() {
    return sysOptionDao;
  }

  // ========================================业务方法========================================
  @Override
  public int findLockTimes() {
    return findInt(SysOptionCode.LOCK_TIMES);
  }

  @Override
  public int findLockTime() {
    return findInt(SysOptionCode.LOCK_TIME);
  }

  @Override
  public SysOption findByCode(String code) {
    Assert.assertNotEmptyString(code, message.getMessage("code.isnull"));
    Map<String, Object> parameters = new HashMap<String, Object>(1);
    parameters.put("code", code);
    return findOne(parameters);
  }

  @Override
  public int findInt(String code) {
    SysOption sysOption = findByCode(code);
    return Integer.parseInt(sysOption.getVal());
  }

  @Override
  public Long findLong(String code) {
    SysOption sysOption = findByCode(code);
    return Long.parseLong(sysOption.getVal());
  }

  @Override
  public String findStr(String code) {
    SysOption sysOption = findByCode(code);
    return sysOption.getVal();
  }

  @Override
  public String findUploadServer() {
    return findStr(SysOptionCode.UPLOAD_SERVER);
  }

  @Override
  public String findBrowserServer() {
    return findStr(SysOptionCode.BROWSER_SERVER);
  }

  @Override
  public String findAdminSalt() {
    return findStr(SysOptionCode.ADMIN_SALT);
  }

  @Override
  public String findBossSalt() {
    return findStr(SysOptionCode.BOSS_SALT);
  }

  @Override
  public int findFolderNum() {
    return findInt(SysOptionCode.FOLDER_NUM);
  }

  @Override
  public DataConfig findKConfig() {
    DataConfig config = new DataConfig();
    String strStartDate = findStr(SysOptionCode.K_START_TIME);
    config.setStartTime(DateUtil.parse(strStartDate, DateUtil.yyyy_MM_dd));
    String strEndDate = findStr(SysOptionCode.K_END_TIME);
    config.setEndTime(DateUtil.parse(strEndDate, DateUtil.yyyy_MM_dd));
    String code = findStr(SysOptionCode.K_MARKET);
    config.setCode(code);
    return config;
  }

  @Override
  public DataConfig findHotmapConfig() {
    DataConfig config = new DataConfig();
    String strStartDate = findStr(SysOptionCode.HOTMAP_START_TIME);
    config.setStartTime(DateUtil.parse(strStartDate, DateUtil.yyyy_MM_dd));
    String strEndDate = findStr(SysOptionCode.HOTMAP_END_TIME);
    config.setEndTime(DateUtil.parse(strEndDate, DateUtil.yyyy_MM_dd));
    String strMarket = findStr(SysOptionCode.HOTMAP_MARKET);
    config.setMarket(Long.parseLong(strMarket));
    String marketIdx = findStr(SysOptionCode.HOTMAP_MARKET_IDX);
    config.setMarketIdx(marketIdx);
    return config;


  }

  @Override
  public DataConfig findLineConfig() {
    DataConfig config = new DataConfig();
    String strStartDate = findStr(SysOptionCode.LINE_START_TIME);
    config.setStartTime(DateUtil.parse(strStartDate, DateUtil.yyyy_MM_dd));
    String strEndDate = findStr(SysOptionCode.LINE_END_TIME);
    config.setEndTime(DateUtil.parse(strEndDate, DateUtil.yyyy_MM_dd));
    String strMarket = findStr(SysOptionCode.LINE_MARKET);
    List<Long> markets = new ArrayList<Long>();
    String[] str = strMarket.split(SymbolConstants.COMMA);
    for (String s : str) {
      if (StringUtil.isNotEmpty(s)) {
        markets.add(Long.parseLong(s));
      }
    }
    config.setMarkets(markets);
    String marketIdx = findStr(SysOptionCode.LINE_MARKET_IDX);
    config.setMarketIdxs(Arrays.asList(marketIdx.split(SymbolConstants.COMMA)));
    return config;
  }

  @Override
  public String findMxServer() {
    return findStr(SysOptionCode.MX_SERVER);
  }
}
