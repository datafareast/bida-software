package com.hzizs.analysis.dao;

import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.analysis.entity.SysRoleAuth;
/**
 * 系统角色权限 数据库操作类	 
 * @author crazy_cabbage
 *
 */
public interface SysRoleAuthDao extends MyBatisDao<SysRoleAuth> {

}
