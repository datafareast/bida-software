package com.hzizs.analysis.dao.impl;
import java.util.List;

import com.fasterxml.jackson.core.type.TypeReference;
import com.hzizs.mybatis.dao.impl.MyBatisDaoImpl;
import com.hzizs.analysis.dao.IndustryDao;
import com.hzizs.analysis.entity.Industry;
/**
 * 行业 数据库操作类实现类	 
 * @author crazy_cabbage
 *
 */
public class IndustryDaoImpl extends MyBatisDaoImpl<Industry> implements IndustryDao {



	@Override
	public TypeReference<List<Industry>> getListEntityType() {
		return new TypeReference<List<Industry>>() {
		};
	}
	//========================================业务方法========================================
}
