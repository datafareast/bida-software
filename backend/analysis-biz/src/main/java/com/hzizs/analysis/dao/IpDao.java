package com.hzizs.analysis.dao;

import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.analysis.entity.Ip;
/**
 * IP 数据库操作类	 
 * @author crazy_cabbage
 *
 */
public interface IpDao extends MyBatisDao<Ip> {

}
