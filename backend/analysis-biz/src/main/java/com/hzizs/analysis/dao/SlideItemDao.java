package com.hzizs.analysis.dao;

import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.analysis.entity.SlideItem;
/**
 * 轮播图条目 数据库操作类	 
 * @author crazy_cabbage
 *
 */
public interface SlideItemDao extends MyBatisDao<SlideItem> {

}
