package com.hzizs.analysis.service.impl;
import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.mybatis.service.impl.MyBatisServiceImpl;


import com.hzizs.analysis.dao.SysUserLoginLogDao;
import com.hzizs.analysis.entity.SysUserLoginLog;
import com.hzizs.analysis.service.SysUserLoginLogService;
/**
 * 系统用户登录日志 服务实现类
 * @author crazy_cabbage
 *
 */
public class SysUserLoginLogServiceImpl extends MyBatisServiceImpl<SysUserLoginLog> implements SysUserLoginLogService {
	

	private SysUserLoginLogDao sysUserLoginLogDao;
	public void setSysUserLoginLogDao(SysUserLoginLogDao sysUserLoginLogDao) {
		this.sysUserLoginLogDao = sysUserLoginLogDao;
	}
	@Override
	protected MyBatisDao<SysUserLoginLog> getDao() {
		return sysUserLoginLogDao;
	}
	//========================================业务方法========================================
}
