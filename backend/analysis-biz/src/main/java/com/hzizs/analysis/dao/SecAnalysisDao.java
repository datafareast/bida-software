package com.hzizs.analysis.dao;

import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.analysis.entity.SecAnalysis;

/**
 * 业绩分析 数据库操作类
 * 
 * @author crazy_cabbage
 *
 */
public interface SecAnalysisDao extends MyBatisDao<SecAnalysis> {

}
