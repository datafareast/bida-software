package com.hzizs.analysis.service.impl;

import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.mybatis.service.impl.MyBatisServiceImpl;


import com.hzizs.analysis.dao.SecExchangeDao;
import com.hzizs.analysis.entity.SecExchange;
import com.hzizs.analysis.service.SecExchangeService;

/**
 * 交易所 服务实现类
 * 
 * @author crazy_cabbage
 *
 */
public class SecExchangeServiceImpl extends MyBatisServiceImpl<SecExchange> implements SecExchangeService {


  private SecExchangeDao secExchangeDao;

  public void setSecExchangeDao(SecExchangeDao secExchangeDao) {
    this.secExchangeDao = secExchangeDao;
  }

  @Override
  protected MyBatisDao<SecExchange> getDao() {
    return secExchangeDao;
  }
  // ========================================业务方法========================================
}
