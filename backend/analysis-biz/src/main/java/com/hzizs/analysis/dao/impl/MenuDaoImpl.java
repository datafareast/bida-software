package com.hzizs.analysis.dao.impl;
import java.util.List;

import com.fasterxml.jackson.core.type.TypeReference;
import com.hzizs.mybatis.dao.impl.MyBatisDaoImpl;
import com.hzizs.analysis.dao.MenuDao;
import com.hzizs.analysis.entity.Menu;
/**
 * 菜单 数据库操作类实现类	 
 * @author crazy_cabbage
 *
 */
public class MenuDaoImpl extends MyBatisDaoImpl<Menu> implements MenuDao {



	@Override
	public TypeReference<List<Menu>> getListEntityType() {
		return new TypeReference<List<Menu>>() {
		};
	}
	//========================================业务方法========================================
}
