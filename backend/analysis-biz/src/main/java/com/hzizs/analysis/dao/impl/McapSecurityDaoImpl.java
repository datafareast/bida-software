package com.hzizs.analysis.dao.impl;
import java.util.List;

import com.fasterxml.jackson.core.type.TypeReference;
import com.hzizs.mybatis.dao.impl.MyBatisDaoImpl;
import com.hzizs.analysis.dao.McapSecurityDao;
import com.hzizs.analysis.entity.McapSecurity;
/**
 * 证券市值 数据库操作类实现类	 
 * @author crazy_cabbage
 *
 */
public class McapSecurityDaoImpl extends MyBatisDaoImpl<McapSecurity> implements McapSecurityDao {



	@Override
	public TypeReference<List<McapSecurity>> getListEntityType() {
		return new TypeReference<List<McapSecurity>>() {
		};
	}
	//========================================业务方法========================================
}
