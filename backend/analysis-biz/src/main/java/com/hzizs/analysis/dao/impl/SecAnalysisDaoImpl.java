package com.hzizs.analysis.dao.impl;

import java.util.List;

import com.fasterxml.jackson.core.type.TypeReference;
import com.hzizs.mybatis.dao.impl.MyBatisDaoImpl;
import com.hzizs.analysis.dao.SecAnalysisDao;
import com.hzizs.analysis.entity.SecAnalysis;

/**
 * 业绩分析 数据库操作类实现类
 * 
 * @author crazy_cabbage
 *
 */
public class SecAnalysisDaoImpl extends MyBatisDaoImpl<SecAnalysis> implements SecAnalysisDao {



  @Override
  public TypeReference<List<SecAnalysis>> getListEntityType() {
    return new TypeReference<List<SecAnalysis>>() {
    };
  }
  // ========================================业务方法========================================
}
