package com.hzizs.analysis.service.impl;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.hzizs.Assert;
import com.hzizs.analysis.constants.Constants.MenuCat;
import com.hzizs.analysis.dao.MenuDao;
import com.hzizs.analysis.entity.Menu;
import com.hzizs.analysis.service.MenuService;
import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.mybatis.service.impl.MyBatisServiceImpl;
import com.hzizs.util.CollectionUtil;
/**
 * 菜单 服务实现类
 * @author crazy_cabbage
 *
 */
public class MenuServiceImpl extends MyBatisServiceImpl<Menu> implements MenuService {
	

	private MenuDao menuDao;
	public void setMenuDao(MenuDao menuDao) {
		this.menuDao = menuDao;
	}
	@Override
	protected MyBatisDao<Menu> getDao() {
		return menuDao;
	}
	//========================================业务方法========================================

	  @Override
	  public List<Menu> findMenu() {
	    List<String> cats = Arrays.asList(MenuCat.MENU_GROUP, MenuCat.MENU);
	    return findByCats(cats);
	  }

	  @Override
	  public List<Menu> findBtn() {
	    List<String> cats = Arrays.asList(MenuCat.BTN);
	    return findByCats(cats);
	  }

	  @Override
	  public List<Menu> findByCats(List<String> cats) {
	    if (CollectionUtil.isEmpty(cats)) {
	      Assert.customException(message.getMessage("cats.empty"));
	    }
	    Map<String, Object> parameters = new HashMap<String, Object>(1);
	    parameters.put("cats", cats);
	    return find(parameters);
	  }

	  @Override
	  public long countByParentId(Long parentId) {
	    Assert.assertNotNull(parentId, message.getMessage("parentId.isnull"));
	    Map<String, Object> parameters = new HashMap<String, Object>(1);
	    parameters.put("parentId", parentId);
	    return count(parameters);
	  }
}
