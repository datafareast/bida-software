package com.hzizs.analysis.service.impl;

import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.mybatis.service.impl.MyBatisServiceImpl;
import com.hzizs.util.BeanUtil;
import com.hzizs.util.StringUtil;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.hzizs.Assert;
import com.hzizs.analysis.dao.DataTopoDao;
import com.hzizs.analysis.entity.DataTopo;
import com.hzizs.analysis.service.DataTopoService;
import com.hzizs.constants.Enabled;
import com.hzizs.constants.SymbolConstants;

/**
 * 数据拓普图 服务实现类
 * 
 * @author crazy_cabbage
 *
 */
public class DataTopoServiceImpl extends MyBatisServiceImpl<DataTopo> implements DataTopoService {


  private DataTopoDao dataTopoDao;

  public void setDataTopoDao(DataTopoDao dataTopoDao) {
    this.dataTopoDao = dataTopoDao;
  }

  @Override
  protected MyBatisDao<DataTopo> getDao() {
    return dataTopoDao;
  }

  // ========================================业务方法========================================
  @Override
  public List<DataTopo> findByParent(Long id) {
    DataTopo dataTopo = findById(id);
    String parentIds = dataTopo.getParentIds();
    List<DataTopo> dataTopos = new ArrayList<DataTopo>();
    if (StringUtil.isNotEmpty(parentIds)) {
      String[] pIds = parentIds.split(SymbolConstants.COMMA);
      for (String pid : pIds) {
        DataTopo item = findById(Long.parseLong(pid));
        dataTopos.add(item);
      }
    }
    return dataTopos;
  }

  @Override
  public List<DataTopo> findByChild(Long id) {
    Map<String, Object> parameters = new HashMap<String, Object>(2);
    parameters.put("pid", SymbolConstants.PERCENT + SymbolConstants.COMMA + id + SymbolConstants.COMMA + SymbolConstants.PERCENT);
    return find(parameters);
  }

  @Override
  public List<DataTopo> findShowByDataTypeId(Long dataTypeId) {
    Assert.assertNotNull(dataTypeId, message.getMessage("dataTypeId.isnull"));
    Map<String, Object> parameters = new HashMap<String, Object>(2);
    parameters.put("dataTypeId", dataTypeId);
    parameters.put("isShow", Enabled.T);
    return find(parameters);
  }

  @Override
  public DataTopo updateBound(DataTopo dataTopo) {
    Long id = dataTopo.getId();
    DataTopo po = findById(id);
    BeanUtil.copyInclude(dataTopo, po, "x","y","width","height");
    return update(po);
  }

  @Override
  public long countByDataTypeId(Long dataTypeId) {
    Assert.assertNotNull(dataTypeId, message.getMessage("dataTypeId.isnull"));
    Map<String, Object> parameters = new HashMap<String, Object>(1);
    parameters.put("dataTypeId", dataTypeId);
    return count(parameters);
  }

  @Override
  public List<DataTopo> findAllByDataTypeId(Long dataTypeId) {
    Assert.assertNotNull(dataTypeId, message.getMessage("dataTypeId.isnull"));
    Map<String, Object> parameters = new HashMap<String, Object>(1);
    parameters.put("dataTypeId", dataTypeId);
    return find(parameters);
  }
}
