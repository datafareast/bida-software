package com.hzizs.analysis.dao;

import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.analysis.entity.ImitatePositionItem;
/**
 * 模拟持仓条目 数据库操作类	 
 * @author crazy_cabbage
 *
 */
public interface ImitatePositionItemDao extends MyBatisDao<ImitatePositionItem> {

}
