package com.hzizs.analysis.dao;

import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.analysis.entity.Cms;
/**
 * 内容管理系统 数据库操作类	 
 * @author crazy_cabbage
 *
 */
public interface CmsDao extends MyBatisDao<Cms> {

}
