package com.hzizs.analysis.dao;

import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.analysis.entity.AutocorrelationSecurity;
/**
 * 证券自相关 数据库操作类	 
 * @author crazy_cabbage
 *
 */
public interface AutocorrelationSecurityDao extends MyBatisDao<AutocorrelationSecurity> {

}
