package com.hzizs.analysis.dao.impl;
import java.util.List;

import com.fasterxml.jackson.core.type.TypeReference;
import com.hzizs.mybatis.dao.impl.MyBatisDaoImpl;
import com.hzizs.analysis.dao.MarketDao;
import com.hzizs.analysis.entity.Market;
/**
 * 市场 数据库操作类实现类	 
 * @author crazy_cabbage
 *
 */
public class MarketDaoImpl extends MyBatisDaoImpl<Market> implements MarketDao {



	@Override
	public TypeReference<List<Market>> getListEntityType() {
		return new TypeReference<List<Market>>() {
		};
	}
	//========================================业务方法========================================
}
