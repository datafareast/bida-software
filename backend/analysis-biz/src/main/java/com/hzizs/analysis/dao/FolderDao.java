package com.hzizs.analysis.dao;

import com.hzizs.analysis.entity.Folder;
import com.hzizs.mybatis.dao.MyBatisShardingDao;
/**
 * 文件夹 数据库操作类	 
 * @author crazy_cabbage
 *
 */
public interface FolderDao extends MyBatisShardingDao<Folder> {

}
