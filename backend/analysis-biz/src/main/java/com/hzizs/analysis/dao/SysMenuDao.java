package com.hzizs.analysis.dao;

import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.analysis.entity.SysMenu;
/**
 * 系统菜单 数据库操作类	 
 * @author crazy_cabbage
 *
 */
public interface SysMenuDao extends MyBatisDao<SysMenu> {

}
