package com.hzizs.analysis.facade.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.jsoup.Jsoup;
import org.jsoup.nodes.DataNode;
import org.jsoup.nodes.Document;
import com.hzizs.Assert;
import com.hzizs.analysis.entity.ResearchReport;
import com.hzizs.analysis.entity.UserInfo;
import com.hzizs.analysis.entity.UserRole;
import com.hzizs.analysis.facade.ResearchReportFacade;
import com.hzizs.analysis.service.ResearchReportService;
import com.hzizs.analysis.service.UserInfoService;
import com.hzizs.analysis.service.UserRoleService;
import com.hzizs.constants.Enabled;
import com.hzizs.constants.SymbolConstants;
import com.hzizs.facade.impl.BaseFacadeImpl;
import com.hzizs.util.StringUtil;

public class ResearchReportFacadeImpl extends BaseFacadeImpl implements ResearchReportFacade {
  private ResearchReportService researchReportService;
  private UserInfoService userInfoService;
  private UserRoleService userRoleService;
  public void setResearchReportService(ResearchReportService researchReportService) {
    this.researchReportService = researchReportService;
  }

  public void setUserInfoService(UserInfoService userInfoService) {
    this.userInfoService = userInfoService;
  }
  

  public void setUserRoleService(UserRoleService userRoleService) {
    this.userRoleService = userRoleService;
  }

  // ===========================================业务方法================================
  private void processContent(ResearchReport researchReport) {
    String content = researchReport.getContent();
    if (StringUtil.isNotEmpty(content)) {
      Document document = Jsoup.parse(content, "UTF-8");
      int jquerySize = document.select("script[src*=jquery]").size();
      if (jquerySize == 0) {
        DataNode jquery = new DataNode("<script src=\"http://code.jquery.com/jquery-2.1.4.min.js\"></script>");
        document.head().insertChildren(document.head().childNodeSize(), jquery);
        String eventStr = "<script type=\"text/javascript\"> \n" + "$(document).ready(function(){ \n" + "   $(\"#eBtn\").click(function(){ \n"
            + " var _this=$(this); \n" + "if(_this.val()==='显示'){ \n " + "_this.val('隐藏');  \n" + "$('div.input,.jp-Cell-inputWrapper').show(500);" + "}else{\n"
            + "_this.val('显示');  \n" + "$('div.input,.jp-Cell-inputWrapper').hide(500);" + " }});\n" + "});\n" + "</script>";
        DataNode jqueryEvent = new DataNode(eventStr);
        document.head().insertChildren(document.head().childNodeSize(), jqueryEvent);

        DataNode btn = new DataNode("<input type='button' value='隐藏' id='eBtn' />");
        document.body().insertChildren(0, btn);
        content = document.toString();
        researchReport.setContent(content);
      }
      
    }
  }

  @Override
  public void save(ResearchReport researchReport) {
    String name = researchReport.getName();
    Assert.assertNotEmptyString(name,message.getMessage("name.isnull"));
    ResearchReport po = researchReportService.findByName(name);
    if(po!=null) {
      Assert.customException(message.getMessage("name.exists"));
    }
    processContent(researchReport);
    researchReportService.save(researchReport);
  }

  @Override
  public void update(ResearchReport researchReport) {
    String name = researchReport.getName();
    Assert.assertNotEmptyString(name,message.getMessage("name.isnull"));
    ResearchReport po = researchReportService.findByName(name);
    if(po!=null&& !po.getId().equals(researchReport.getId())) {
      Assert.customException(message.getMessage("name.exists"));
    }
    processContent(researchReport);
    researchReportService.update(researchReport);
    
  }

  @Override
  public List<ResearchReport> findByUserId(Long userId) {
    UserInfo userInfo = userInfoService.findById(userId);
    Map<String, Object> parameters = new HashMap<String,Object>();
    if(Enabled.F.equals(userInfo.getIsManager())) {
      UserRole userRole = userRoleService.findById(userId);
      parameters.put("role", SymbolConstants.COMMA+userRole.getRoleId()+SymbolConstants.COMMA);
    }
    return researchReportService.find(parameters);
  }

}
