package com.hzizs.analysis.dao;

import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.analysis.entity.SysI18n;
/**
 * 系统国际化 数据库操作类	 
 * @author crazy_cabbage
 *
 */
public interface SysI18nDao extends MyBatisDao<SysI18n> {

}
