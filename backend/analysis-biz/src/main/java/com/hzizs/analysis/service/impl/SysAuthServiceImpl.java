package com.hzizs.analysis.service.impl;

import java.util.HashMap;
import java.util.Map;
import com.hzizs.Assert;
import com.hzizs.analysis.dao.SysAuthDao;
import com.hzizs.analysis.entity.SysAuth;
import com.hzizs.analysis.service.SysAuthService;
import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.mybatis.service.impl.MyBatisServiceImpl;

/**
 * 系统权限 服务实现类
 * 
 * @author crazy_cabbage
 *
 */
public class SysAuthServiceImpl extends MyBatisServiceImpl<SysAuth> implements SysAuthService {


  private SysAuthDao sysAuthDao;

  public void setSysAuthDao(SysAuthDao sysAuthDao) {
    this.sysAuthDao = sysAuthDao;
  }

  @Override
  protected MyBatisDao<SysAuth> getDao() {
    return sysAuthDao;
  }

  // ========================================业务方法========================================
  @Override
  public SysAuth findByCode(String code) {
    Assert.assertNotEmptyString(code, message.getMessage("code.isnull"));
    Map<String, Object> parameters = new HashMap<String, Object>(1);
    parameters.put("code", code);
    return findOne(parameters);
  }
}
