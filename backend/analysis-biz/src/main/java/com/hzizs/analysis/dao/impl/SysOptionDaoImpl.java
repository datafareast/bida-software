package com.hzizs.analysis.dao.impl;
import java.util.List;

import com.fasterxml.jackson.core.type.TypeReference;
import com.hzizs.mybatis.dao.impl.MyBatisDaoImpl;
import com.hzizs.analysis.dao.SysOptionDao;
import com.hzizs.analysis.entity.SysOption;
/**
 * 数据选项 数据库操作类实现类	 
 * @author crazy_cabbage
 *
 */
public class SysOptionDaoImpl extends MyBatisDaoImpl<SysOption> implements SysOptionDao {



	@Override
	public TypeReference<List<SysOption>> getListEntityType() {
		return new TypeReference<List<SysOption>>() {
		};
	}
	//========================================业务方法========================================
}
