package com.hzizs.analysis.dao.impl;
import java.util.List;

import com.fasterxml.jackson.core.type.TypeReference;
import com.hzizs.mybatis.dao.impl.MyBatisDaoImpl;
import com.hzizs.analysis.dao.DailyTradingActivitySecurityDao;
import com.hzizs.analysis.entity.DailyTradingActivitySecurity;
/**
 * 证券日交易 数据库操作类实现类	 
 * @author crazy_cabbage
 *
 */
public class DailyTradingActivitySecurityDaoImpl extends MyBatisDaoImpl<DailyTradingActivitySecurity> implements DailyTradingActivitySecurityDao {



	@Override
	public TypeReference<List<DailyTradingActivitySecurity>> getListEntityType() {
		return new TypeReference<List<DailyTradingActivitySecurity>>() {
		};
	}
	//========================================业务方法========================================
}
