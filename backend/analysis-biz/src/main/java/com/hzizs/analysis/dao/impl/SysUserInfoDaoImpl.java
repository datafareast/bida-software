package com.hzizs.analysis.dao.impl;
import java.util.List;

import com.fasterxml.jackson.core.type.TypeReference;
import com.hzizs.mybatis.dao.impl.MyBatisDaoImpl;
import com.hzizs.analysis.dao.SysUserInfoDao;
import com.hzizs.analysis.entity.SysUserInfo;
/**
 * 系统用户信息 数据库操作类实现类	 
 * @author crazy_cabbage
 *
 */
public class SysUserInfoDaoImpl extends MyBatisDaoImpl<SysUserInfo> implements SysUserInfoDao {



	@Override
	public TypeReference<List<SysUserInfo>> getListEntityType() {
		return new TypeReference<List<SysUserInfo>>() {
		};
	}
	//========================================业务方法========================================
}
