package com.hzizs.analysis.dao;

import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.analysis.entity.Upgrade;
/**
 * 升级信息 数据库操作类	 
 * @author crazy_cabbage
 *
 */
public interface UpgradeDao extends MyBatisDao<Upgrade> {

}
