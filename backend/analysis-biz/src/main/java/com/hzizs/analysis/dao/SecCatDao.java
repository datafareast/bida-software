package com.hzizs.analysis.dao;

import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.analysis.entity.SecCat;

/**
 * 类别 数据库操作类
 * 
 * @author crazy_cabbage
 *
 */
public interface SecCatDao extends MyBatisDao<SecCat> {

}
