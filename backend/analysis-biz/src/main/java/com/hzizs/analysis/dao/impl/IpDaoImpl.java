package com.hzizs.analysis.dao.impl;
import java.util.List;

import com.fasterxml.jackson.core.type.TypeReference;
import com.hzizs.mybatis.dao.impl.MyBatisDaoImpl;
import com.hzizs.analysis.dao.IpDao;
import com.hzizs.analysis.entity.Ip;
/**
 * IP 数据库操作类实现类	 
 * @author crazy_cabbage
 *
 */
public class IpDaoImpl extends MyBatisDaoImpl<Ip> implements IpDao {



	@Override
	public TypeReference<List<Ip>> getListEntityType() {
		return new TypeReference<List<Ip>>() {
		};
	}
	//========================================业务方法========================================
}
