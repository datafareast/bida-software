package com.hzizs.analysis.dao;

import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.analysis.entity.IndexType;
/**
 * 指标类型 数据库操作类	 
 * @author crazy_cabbage
 *
 */
public interface IndexTypeDao extends MyBatisDao<IndexType> {

}
