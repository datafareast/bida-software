package com.hzizs.analysis.dao;

import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.analysis.entity.UserInfo;
/**
 * 用户信息 数据库操作类	 
 * @author crazy_cabbage
 *
 */
public interface UserInfoDao extends MyBatisDao<UserInfo> {

}
