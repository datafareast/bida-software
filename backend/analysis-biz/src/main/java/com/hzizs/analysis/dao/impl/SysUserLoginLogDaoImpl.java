package com.hzizs.analysis.dao.impl;
import java.util.List;

import com.fasterxml.jackson.core.type.TypeReference;
import com.hzizs.mybatis.dao.impl.MyBatisDaoImpl;
import com.hzizs.analysis.dao.SysUserLoginLogDao;
import com.hzizs.analysis.entity.SysUserLoginLog;
/**
 * 系统用户登录日志 数据库操作类实现类	 
 * @author crazy_cabbage
 *
 */
public class SysUserLoginLogDaoImpl extends MyBatisDaoImpl<SysUserLoginLog> implements SysUserLoginLogDao {



	@Override
	public TypeReference<List<SysUserLoginLog>> getListEntityType() {
		return new TypeReference<List<SysUserLoginLog>>() {
		};
	}
	//========================================业务方法========================================
}
