package com.hzizs.analysis.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.hzizs.Assert;
import com.hzizs.analysis.dao.SysRoleAuthDao;
import com.hzizs.analysis.entity.SysRoleAuth;
import com.hzizs.analysis.service.SysRoleAuthService;
import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.mybatis.service.impl.MyBatisServiceImpl;
import com.hzizs.util.CollectionUtil;

/**
 * 系统角色权限 服务实现类
 * 
 * @author crazy_cabbage
 *
 */
public class SysRoleAuthServiceImpl extends MyBatisServiceImpl<SysRoleAuth> implements SysRoleAuthService {


  private SysRoleAuthDao sysRoleAuthDao;

  public void setSysRoleAuthDao(SysRoleAuthDao sysRoleAuthDao) {
    this.sysRoleAuthDao = sysRoleAuthDao;
  }

  @Override
  protected MyBatisDao<SysRoleAuth> getDao() {
    return sysRoleAuthDao;
  }

  // ========================================业务方法========================================
  @Override
  public List<SysRoleAuth> findByRoleId(Long roleId) {
    Assert.assertNotNull(roleId, message.getMessage("roleId.isnull"));
    Map<String, Object> parameters = new HashMap<String, Object>(1);
    parameters.put("roleId", roleId);
    return find(parameters);
  }

  @Override
  public void delete(Long roleId, List<Long> authIds) {
    if (CollectionUtil.isEmpty(authIds)) {
      return;
    }
    Assert.assertNotNull(roleId, message.getMessage("roleId.isnull"));
    Map<String, Object> parameters = new HashMap<String, Object>(2);
    parameters.put("roleId", roleId);
    parameters.put("authIds", authIds);
    delete(parameters);
  }

  @Override
  public void deleteByRoleId(Long roleId) {
    Assert.assertNotNull(roleId, message.getMessage("roleId.isnull"));
    Map<String, Object> parameters = new HashMap<String, Object>(1);
    parameters.put("roleId", roleId);
    delete(parameters);
  }

  @Override
  public long countByAuthId(Long authId) {
    Assert.assertNotNull(authId, message.getMessage("authId.isnull"));
    Map<String, Object> parameters = new HashMap<String, Object>(1);
    parameters.put("authId", authId);
    return count(parameters);
  }
}
