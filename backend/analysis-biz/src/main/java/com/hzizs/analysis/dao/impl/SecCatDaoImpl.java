package com.hzizs.analysis.dao.impl;

import java.util.List;

import com.fasterxml.jackson.core.type.TypeReference;
import com.hzizs.mybatis.dao.impl.MyBatisDaoImpl;
import com.hzizs.analysis.dao.SecCatDao;
import com.hzizs.analysis.entity.SecCat;

/**
 * 类别 数据库操作类实现类
 * 
 * @author crazy_cabbage
 *
 */
public class SecCatDaoImpl extends MyBatisDaoImpl<SecCat> implements SecCatDao {



  @Override
  public TypeReference<List<SecCat>> getListEntityType() {
    return new TypeReference<List<SecCat>>() {
    };
  }
  // ========================================业务方法========================================
}
