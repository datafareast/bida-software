package com.hzizs.analysis.dao;

import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.analysis.entity.SecIndex;
/**
 * 常用指标 数据库操作类	 
 * @author crazy_cabbage
 *
 */
public interface SecIndexDao extends MyBatisDao<SecIndex> {

}
