package com.hzizs.analysis.service.impl;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.hzizs.Assert;
import com.hzizs.analysis.constants.Constants.MenuCat;
import com.hzizs.analysis.dao.SysMenuDao;
import com.hzizs.analysis.entity.SysMenu;
import com.hzizs.analysis.service.SysMenuService;
import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.mybatis.service.impl.MyBatisServiceImpl;
import com.hzizs.util.CollectionUtil;

/**
 * 系统菜单 服务实现类
 * 
 * @author crazy_cabbage
 *
 */
public class SysMenuServiceImpl extends MyBatisServiceImpl<SysMenu> implements SysMenuService {


  private SysMenuDao sysMenuDao;

  public void setSysMenuDao(SysMenuDao sysMenuDao) {
    this.sysMenuDao = sysMenuDao;
  }

  @Override
  protected MyBatisDao<SysMenu> getDao() {
    return sysMenuDao;
  }

  // ========================================业务方法========================================
  @Override
  public List<SysMenu> findMenu() {
    List<String> cats = Arrays.asList(MenuCat.MENU_GROUP, MenuCat.MENU);
    return findByCats(cats);
  }

  @Override
  public List<SysMenu> findBtn() {
    List<String> cats = Arrays.asList(MenuCat.BTN);
    return findByCats(cats);
  }

  @Override
  public List<SysMenu> findByCats(List<String> cats) {
    if (CollectionUtil.isEmpty(cats)) {
      Assert.customException(message.getMessage("cats.isnull"));
    }
    Map<String, Object> parameters = new HashMap<String, Object>(1);
    parameters.put("cats", cats);
    return find(parameters);
  }

  @Override
  public long countByParentId(Long parentId) {
    Assert.assertNotNull(parentId, message.getMessage("parentId.isnull"));
    Map<String, Object> parameters = new HashMap<String, Object>(1);
    parameters.put("parentId", parentId);
    return count(parameters);
  }
}
