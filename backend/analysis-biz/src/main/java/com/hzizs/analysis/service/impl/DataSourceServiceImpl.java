package com.hzizs.analysis.service.impl;
import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.mybatis.service.impl.MyBatisServiceImpl;


import com.hzizs.analysis.dao.DataSourceDao;
import com.hzizs.analysis.entity.DataSource;
import com.hzizs.analysis.service.DataSourceService;
/**
 * 数据源 服务实现类
 * @author crazy_cabbage
 *
 */
public class DataSourceServiceImpl extends MyBatisServiceImpl<DataSource> implements DataSourceService {
	

	private DataSourceDao dataSourceDao;
	public void setDataSourceDao(DataSourceDao dataSourceDao) {
		this.dataSourceDao = dataSourceDao;
	}
	@Override
	protected MyBatisDao<DataSource> getDao() {
		return dataSourceDao;
	}
	//========================================业务方法========================================
}
