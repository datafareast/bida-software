package com.hzizs.analysis.dao;

import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.analysis.entity.EffectiveSpreadSecurity;
/**
 * 证券有效价差 数据库操作类	 
 * @author crazy_cabbage
 *
 */
public interface EffectiveSpreadSecurityDao extends MyBatisDao<EffectiveSpreadSecurity> {

}
