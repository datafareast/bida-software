package com.hzizs.analysis.dao;

import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.analysis.entity.UserRole;
/**
 * 用户角色 数据库操作类	 
 * @author crazy_cabbage
 *
 */
public interface UserRoleDao extends MyBatisDao<UserRole> {

}
