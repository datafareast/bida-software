package com.hzizs.analysis.service.impl;
import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.mybatis.service.impl.MyBatisServiceImpl;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.hzizs.Assert;
import com.hzizs.analysis.dao.QuotedSpreadMarketDao;
import com.hzizs.analysis.entity.QuotedSpreadMarket;
import com.hzizs.analysis.service.QuotedSpreadMarketService;
/**
 * 市场报价价差 服务实现类
 * @author crazy_cabbage
 *
 */
public class QuotedSpreadMarketServiceImpl extends MyBatisServiceImpl<QuotedSpreadMarket> implements QuotedSpreadMarketService {
	

	private QuotedSpreadMarketDao quotedSpreadMarketDao;
	public void setQuotedSpreadMarketDao(QuotedSpreadMarketDao quotedSpreadMarketDao) {
		this.quotedSpreadMarketDao = quotedSpreadMarketDao;
	}
	@Override
	protected MyBatisDao<QuotedSpreadMarket> getDao() {
		return quotedSpreadMarketDao;
	}
	//========================================业务方法========================================
  @Override
  public List<QuotedSpreadMarket> find(Long marketId, Date startTime, Date endTime) {
    Assert.assertNotNull(marketId, message.getMessage("marketId.isnull"));
    Assert.assertNotNull(startTime, message.getMessage("startTime.isnull"));
    Assert.assertNotNull(endTime, message.getMessage("endTime.isnull"));
    Map<String, Object> parameters = new HashMap<String,Object>(3);
    parameters.put("marketId", marketId);
    parameters.put("startTime", startTime);
    parameters.put("endTime", endTime);
    return find(parameters);
  }
}
