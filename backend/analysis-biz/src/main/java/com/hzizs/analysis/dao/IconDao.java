package com.hzizs.analysis.dao;

import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.analysis.entity.Icon;
/**
 * 图标库 数据库操作类	 
 * @author crazy_cabbage
 *
 */
public interface IconDao extends MyBatisDao<Icon> {

}
