package com.hzizs.analysis.service.impl;
import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.mybatis.service.impl.MyBatisServiceImpl;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.hzizs.Assert;
import com.hzizs.analysis.dao.RealisedSpreadMarketDao;
import com.hzizs.analysis.entity.RealisedSpreadMarket;
import com.hzizs.analysis.service.RealisedSpreadMarketService;
/**
 * 市场实现价差 服务实现类
 * @author crazy_cabbage
 *
 */
public class RealisedSpreadMarketServiceImpl extends MyBatisServiceImpl<RealisedSpreadMarket> implements RealisedSpreadMarketService {
	

	private RealisedSpreadMarketDao realisedSpreadMarketDao;
	public void setRealisedSpreadMarketDao(RealisedSpreadMarketDao realisedSpreadMarketDao) {
		this.realisedSpreadMarketDao = realisedSpreadMarketDao;
	}
	@Override
	protected MyBatisDao<RealisedSpreadMarket> getDao() {
		return realisedSpreadMarketDao;
	}
	//========================================业务方法========================================
  @Override
  public List<RealisedSpreadMarket> find(Long marketId, Date startTime, Date endTime) {
    Assert.assertNotNull(marketId, message.getMessage("marketId.isnull"));
    Assert.assertNotNull(startTime, message.getMessage("startTime.isnull"));
    Assert.assertNotNull(endTime, message.getMessage("endTime.isnull"));
    Map<String, Object> parameters = new HashMap<String,Object>(3);
    parameters.put("marketId", marketId);
    parameters.put("startTime", startTime);
    parameters.put("endTime", endTime);
    return find(parameters);
  }
}
