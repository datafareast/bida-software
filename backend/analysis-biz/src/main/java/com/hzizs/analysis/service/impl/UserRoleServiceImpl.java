package com.hzizs.analysis.service.impl;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.hzizs.Assert;
import com.hzizs.analysis.dao.UserRoleDao;
import com.hzizs.analysis.entity.UserRole;
import com.hzizs.analysis.service.UserRoleService;
import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.mybatis.service.impl.MyBatisServiceImpl;
/**
 * 用户角色 服务实现类
 * @author crazy_cabbage
 *
 */
public class UserRoleServiceImpl extends MyBatisServiceImpl<UserRole> implements UserRoleService {
	

	private UserRoleDao userRoleDao;
	public void setUserRoleDao(UserRoleDao userRoleDao) {
		this.userRoleDao = userRoleDao;
	}
	@Override
	protected MyBatisDao<UserRole> getDao() {
		return userRoleDao;
	}
	//========================================业务方法========================================
	 @Override
	  public UserRole findByUserId(Long userId) {
	    Assert.assertNotNull(userId, message.getMessage("userId.isnull"));
	    Map<String, Object> parameters = new HashMap<String, Object>(2);
	    parameters.put("userId", userId);
	    return findOne(parameters);
	  }

	  @Override
	  public List<UserRole> findByRoleId(Long roleId) {
	    Assert.assertNotNull(roleId, message.getMessage("roleId.isnull"));
	    Map<String, Object> parameters = new HashMap<String, Object>(1);
	    parameters.put("roleId", roleId);
	    return find(parameters);
	  }

	  @Override
	  public long countByRoleId(Long roleId) {
	    Assert.assertNotNull(roleId, message.getMessage("roleId.isnull"));
	    Map<String, Object> parameters = new HashMap<String, Object>(1);
	    parameters.put("roleId", roleId);
	    return count(parameters);
	  }
}
