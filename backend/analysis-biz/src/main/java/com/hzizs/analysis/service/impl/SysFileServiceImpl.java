package com.hzizs.analysis.service.impl;
import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.mybatis.service.impl.MyBatisServiceImpl;


import com.hzizs.analysis.dao.SysFileDao;
import com.hzizs.analysis.entity.SysFile;
import com.hzizs.analysis.service.SysFileService;
/**
 * 系统文件 服务实现类
 * @author crazy_cabbage
 *
 */
public class SysFileServiceImpl extends MyBatisServiceImpl<SysFile> implements SysFileService {
	

	private SysFileDao sysFileDao;
	public void setSysFileDao(SysFileDao sysFileDao) {
		this.sysFileDao = sysFileDao;
	}
	@Override
	protected MyBatisDao<SysFile> getDao() {
		return sysFileDao;
	}
	//========================================业务方法========================================
}
