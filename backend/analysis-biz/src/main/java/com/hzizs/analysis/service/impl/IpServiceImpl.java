package com.hzizs.analysis.service.impl;
import java.util.HashMap;
import java.util.Map;
import com.hzizs.Assert;
import com.hzizs.analysis.dao.IpDao;
import com.hzizs.analysis.entity.Ip;
import com.hzizs.analysis.service.IpService;
import com.hzizs.constants.SymbolConstants;
import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.mybatis.service.impl.MyBatisServiceImpl;
import com.hzizs.util.IPUtil;
import com.hzizs.util.RegexUtil;
import com.hzizs.util.StringUtil;
/**
 * IP 服务实现类
 * @author crazy_cabbage
 *
 */
public class IpServiceImpl extends MyBatisServiceImpl<Ip> implements IpService {
	

	private IpDao ipDao;
	public void setIpDao(IpDao ipDao) {
		this.ipDao = ipDao;
	}
	@Override
	protected MyBatisDao<Ip> getDao() {
		return ipDao;
	}
	//========================================业务方法========================================
  @Override
  public Ip findByIp(String ip) {
    if(RegexUtil.isIp(ip)) {
      Map<String, Object> parameters = new HashMap<String,Object>(1);
      parameters.put("ip", IPUtil.str2num(ip));
     return findOne(parameters);
    }
    return null;
  }
  @Override
  public String findAddrByIp(String ip) {
    if(StringUtil.isEmpty(ip)) {
      return "unknow";
    }
    StringBuffer sb = new StringBuffer();
    String[] ips = ip.split(SymbolConstants.COMMA);
    for (String ipStr : ips) {
      Ip vo = findByIp(ipStr);
      if (vo == null) {
        sb.append("unknow");
      } else {
        sb.append(vo.getAddr());
        if (StringUtil.isNotEmpty(vo.getAddr2())) {
          sb.append(vo.getAddr2());
        }
        if (StringUtil.isNotEmpty(vo.getAddr3())) {
          sb.append(vo.getAddr3());
        }
      }
      sb.append(SymbolConstants.COMMA);
    }
    sb.deleteCharAt(sb.length() - 1);
    return sb.toString();
  }
  @Override
  public long countByStartIpEndIp(Long startIp, Long endIp) {
    Assert.assertNotNull(startIp, message.getMessage("startIp.isnull"));
    Assert.assertNotNull(endIp, message.getMessage("endIp.isnull"));
    if(startIp.compareTo(endIp)==1) {
      Assert.customException(message.getMessage("startIp.big.endIp"));
    }
    Map<String, Object> parameters = new HashMap<String, Object>(2);
    parameters.put("startIp", startIp);
    parameters.put("endIp", endIp);
    return count(parameters);
  }
}
