package com.hzizs.analysis.service.impl;

import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.mybatis.service.impl.MyBatisServiceImpl;
import java.util.HashMap;
import java.util.Map;
import com.hzizs.Assert;
import com.hzizs.analysis.dao.LinkItemDao;
import com.hzizs.analysis.entity.LinkItem;
import com.hzizs.analysis.service.LinkItemService;

/**
 * 友情链接条目 服务实现类
 * 
 * @author crazy_cabbage
 *
 */
public class LinkItemServiceImpl extends MyBatisServiceImpl<LinkItem> implements LinkItemService {


  private LinkItemDao linkItemDao;

  public void setLinkItemDao(LinkItemDao linkItemDao) {
    this.linkItemDao = linkItemDao;
  }

  @Override
  protected MyBatisDao<LinkItem> getDao() {
    return linkItemDao;
  }

  // ========================================业务方法========================================
  @Override
  public long countByLinkId(Long linkId) {
    Assert.assertNotNull(linkId, message.getMessage("linkId.isnull"));
    Map<String, Object> parameters = new HashMap<String, Object>(1);
    parameters.put("linkId", linkId);
    return count(parameters);
  }
}
