package com.hzizs.analysis.dao.impl;
import java.util.List;

import com.fasterxml.jackson.core.type.TypeReference;
import com.hzizs.mybatis.dao.impl.MyBatisDaoImpl;
import com.hzizs.analysis.dao.SysRoleDao;
import com.hzizs.analysis.entity.SysRole;
/**
 * 系统角色 数据库操作类实现类	 
 * @author crazy_cabbage
 *
 */
public class SysRoleDaoImpl extends MyBatisDaoImpl<SysRole> implements SysRoleDao {



	@Override
	public TypeReference<List<SysRole>> getListEntityType() {
		return new TypeReference<List<SysRole>>() {
		};
	}
	//========================================业务方法========================================
}
