package com.hzizs.analysis.service.impl;
import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.mybatis.service.impl.MyBatisServiceImpl;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.hzizs.Assert;
import com.hzizs.analysis.dao.AutocorrelationMarketDao;
import com.hzizs.analysis.entity.AutocorrelationMarket;
import com.hzizs.analysis.service.AutocorrelationMarketService;
/**
 * 市场自相关 服务实现类
 * @author crazy_cabbage
 *
 */
public class AutocorrelationMarketServiceImpl extends MyBatisServiceImpl<AutocorrelationMarket> implements AutocorrelationMarketService {
	

	private AutocorrelationMarketDao autocorrelationMarketDao;
	public void setAutocorrelationMarketDao(AutocorrelationMarketDao autocorrelationMarketDao) {
		this.autocorrelationMarketDao = autocorrelationMarketDao;
	}
	@Override
	protected MyBatisDao<AutocorrelationMarket> getDao() {
		return autocorrelationMarketDao;
	}
	//========================================业务方法========================================
  @Override
  public List<AutocorrelationMarket> find(Long marketId, Date startTime, Date endTime) {
    Assert.assertNotNull(marketId, message.getMessage("marketId.isnull"));
    Assert.assertNotNull(startTime, message.getMessage("startTime.isnull"));
    Assert.assertNotNull(endTime, message.getMessage("endTime.isnull"));
    Map<String, Object> parameters = new HashMap<String,Object>(3);
    parameters.put("marketId", marketId);
    parameters.put("startTime", startTime);
    parameters.put("endTime", endTime);
    return find(parameters);
  }
}
