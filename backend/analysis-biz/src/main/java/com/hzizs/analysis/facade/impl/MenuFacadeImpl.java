package com.hzizs.analysis.facade.impl;

import com.hzizs.Assert;
import com.hzizs.analysis.facade.MenuFacade;
import com.hzizs.analysis.service.MenuService;
import com.hzizs.analysis.service.RoleMenuService;
import com.hzizs.facade.impl.BaseFacadeImpl;

/**
 * @author weihu
 *
 */
public class MenuFacadeImpl extends BaseFacadeImpl implements MenuFacade {
private MenuService menuService;
private RoleMenuService roleMenuService;  

 

public void setMenuService(MenuService menuService) {
  this.menuService = menuService;
}

public void setRoleMenuService(RoleMenuService roleMenuService) {
  this.roleMenuService = roleMenuService;
}
//==================================================================================

  @Override
  public void deleteById(Long id) {
    long num = menuService.countByParentId(id);
    if(num>0) {
      Assert.customException(message.getMessage("menu.hasChild"));
    }
    num = roleMenuService.countByMenuId(id);
    if(num>0) {
      Assert.customException(message.getMessage("menu.used"));
    }
    menuService.deleteById(id);
    
  }

  @Override
  public void deleteByIds(Long[] ids) {
    if(ids!=null) {
      for(Long id:ids) {
        deleteById(id);
      }
    }
    
  }
  

}
