package com.hzizs.analysis.service.impl;
import java.util.HashMap;
import java.util.Map;
import com.hzizs.Assert;
import com.hzizs.analysis.dao.SlideItemDao;
import com.hzizs.analysis.entity.SlideItem;
import com.hzizs.analysis.service.SlideItemService;
import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.mybatis.service.impl.MyBatisServiceImpl;
/**
 * 轮播图条目 服务实现类
 * @author crazy_cabbage
 *
 */
public class SlideItemServiceImpl extends MyBatisServiceImpl<SlideItem> implements SlideItemService {
	

	private SlideItemDao slideItemDao;
	public void setSlideItemDao(SlideItemDao slideItemDao) {
		this.slideItemDao = slideItemDao;
	}
	@Override
	protected MyBatisDao<SlideItem> getDao() {
		return slideItemDao;
	}
	//========================================业务方法========================================
	  @Override
	  public long countBySlideId(Long slideId) {
	    Assert.assertNotNull(slideId, message.getMessage("slideId.isnull"));
	    Map<String, Object> parameters = new HashMap<String, Object>(1);
	    parameters.put("slideId", slideId);
	    return count(parameters);
	  }
}
