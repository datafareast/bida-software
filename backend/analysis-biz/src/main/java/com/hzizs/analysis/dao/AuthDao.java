package com.hzizs.analysis.dao;

import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.analysis.entity.Auth;
/**
 * 权限 数据库操作类	 
 * @author crazy_cabbage
 *
 */
public interface AuthDao extends MyBatisDao<Auth> {

}
