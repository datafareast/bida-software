package com.hzizs.analysis.dao;

import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.analysis.entity.SysRole;
/**
 * 系统角色 数据库操作类	 
 * @author crazy_cabbage
 *
 */
public interface SysRoleDao extends MyBatisDao<SysRole> {

}
