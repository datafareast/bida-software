package com.hzizs.analysis.service.impl;

import java.util.HashMap;
import java.util.Map;
import com.hzizs.Assert;
import com.hzizs.analysis.dao.SysRoleDao;
import com.hzizs.analysis.entity.SysRole;
import com.hzizs.analysis.service.SysRoleService;
import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.mybatis.service.impl.MyBatisServiceImpl;

/**
 * 系统角色 服务实现类
 * 
 * @author crazy_cabbage
 *
 */
public class SysRoleServiceImpl extends MyBatisServiceImpl<SysRole> implements SysRoleService {


  private SysRoleDao sysRoleDao;

  public void setSysRoleDao(SysRoleDao sysRoleDao) {
    this.sysRoleDao = sysRoleDao;
  }

  @Override
  protected MyBatisDao<SysRole> getDao() {
    return sysRoleDao;
  }

  // ========================================业务方法========================================
  @Override
  public SysRole findByCode(String code) {
    Assert.assertNotEmptyString(code, message.getMessage("code.isnull"));
    Map<String, Object> parameters = new HashMap<String, Object>(1);
    parameters.put("code", code);
    return findOne(parameters);
  }
}
