package com.hzizs.analysis.facade.impl;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import com.hzizs.Assert;
import com.hzizs.analysis.dto.UserInfoDto;
import com.hzizs.analysis.entity.Menu;
import com.hzizs.analysis.entity.Role;
import com.hzizs.analysis.entity.RoleMenu;
import com.hzizs.analysis.entity.User;
import com.hzizs.analysis.entity.UserInfo;
import com.hzizs.analysis.entity.UserLog;
import com.hzizs.analysis.entity.UserLoginLog;
import com.hzizs.analysis.entity.UserRole;
import com.hzizs.analysis.facade.UserFacade;
import com.hzizs.analysis.service.IpService;
import com.hzizs.analysis.service.MenuService;
import com.hzizs.analysis.service.RoleMenuService;
import com.hzizs.analysis.service.RoleService;
import com.hzizs.analysis.service.SysOptionService;
import com.hzizs.analysis.service.UserInfoService;
import com.hzizs.analysis.service.UserLogService;
import com.hzizs.analysis.service.UserLoginLogService;
import com.hzizs.analysis.service.UserRoleService;
import com.hzizs.analysis.service.UserService;
import com.hzizs.constants.Enabled;
import com.hzizs.constants.SymbolConstants;
import com.hzizs.dto.LoginLogDto;
import com.hzizs.dto.UserDto;
import com.hzizs.facade.impl.BaseFacadeImpl;
import com.hzizs.util.BeanUtil;
import com.hzizs.util.DateUtil;
import com.hzizs.util.prefs.Md5Util;

public class UserFacadeImpl extends BaseFacadeImpl implements UserFacade {
  
  private UserService userService;
  private UserRoleService userRoleService;
  private UserInfoService userInfoService;
  private UserLogService userLogService;
  private RoleService roleService;
  private UserLoginLogService userLoginLogService;
  private SysOptionService sysOptionService;
  private IpService ipService;
  private MenuService menuService;
  private RoleMenuService roleMenuService;
  

  public void setUserService(UserService userService) {
    this.userService = userService;
  }

  public void setUserRoleService(UserRoleService userRoleService) {
    this.userRoleService = userRoleService;
  }

  public void setUserInfoService(UserInfoService userInfoService) {
    this.userInfoService = userInfoService;
  }

  public void setUserLogService(UserLogService userLogService) {
    this.userLogService = userLogService;
  }

  public void setRoleService(RoleService roleService) {
    this.roleService = roleService;
  }

  public void setUserLoginLogService(UserLoginLogService userLoginLogService) {
    this.userLoginLogService = userLoginLogService;
  }

  public void setSysOptionService(SysOptionService sysOptionService) {
    this.sysOptionService = sysOptionService;
  }

  public void setIpService(IpService ipService) {
    this.ipService = ipService;
  }

  public void setMenuService(MenuService menuService) {
    this.menuService = menuService;
  }

  public void setRoleMenuService(RoleMenuService roleMenuService) {
    this.roleMenuService = roleMenuService;
  }

  //====================================================================
  @Override
  public void save(UserInfoDto dto) {
    User user = dto.getUser();
    User po = userService.findByUsername(user.getUsername());
    if(po!=null) {
      Assert.customException(message.getMessage("username.exists"));
    }
    String salt = sysOptionService.findBossSalt();
    user.setPassword(Md5Util.saltPassword(salt, user.getPassword()));
    userService.save(user);
    UserInfo userInfo = dto.getUserInfo();
    userInfo.setId(user.getId());
    userInfo.setIsManager(Enabled.F);
    userInfo.setIsDel(Enabled.F);
    userInfo.setUsername(user.getUsername());
    userInfo.setEnabled(Enabled.T);
    userInfoService.save(userInfo);
    UserRole userRole = dto.getUserRole();
    userRole.setUserId(user.getId());
    userRoleService.save(userRole);
    
    UserLog userLog = new UserLog();
    userLog.setId(user.getId());
    userLog.setErrTimes(0);
    userLogService.save(userLog);
  }

  @Override
  public UserInfoDto findById(Long id) {
    UserInfoDto dto = new UserInfoDto();
    UserInfo userInfo = userInfoService.findById(id);
    dto.setUserInfo(userInfo);
    UserRole userRole = userRoleService.findByUserId(id);
    dto.setUserRole(userRole);
    return dto;
  }

  @Override
  public void update(UserInfoDto dto) {
    UserInfo userInfo = dto.getUserInfo();
    UserInfo po = userInfoService.findById(userInfo.getId());
    BeanUtil.copyExclude(userInfo, po, "id","createTime","isDel","isManager","lockTime","username");
    userInfoService.update(po);
    
    UserRole userRole = dto.getUserRole();
    UserRole userRole2 = userRoleService.findByUserId(userInfo.getId());
    if(!userRole2.getRoleId().equals(userRole.getRoleId())) {
      userRole2.setRoleId(userRole.getRoleId());
      userRoleService.update(userRole2);
    }

  }

  @Override
  public UserDto findByUsername(String username) {
    User user = userService.findByUsername(username);
    if(user==null) {
      return null;
    }
    UserInfo userInfo = userInfoService.findById(user.getId());
    UserDto userDto = new UserDto();
    userDto.setUserId(user.getId());
    userDto.setUsername(user.getUsername());
    userDto.setPassword(user.getPassword());
    if(Enabled.T.equals(userInfo.getIsManager())) {
      userDto.setRole(SymbolConstants.ASTERISK);
    }else {
      UserRole userRole = userRoleService.findByUserId(user.getId());
      Role role = roleService.findById(userRole.getRoleId());
      userDto.setRole(role.getCode());
    }
    userDto.setEnabled(Enabled.T.equals(userInfo.getEnabled()));
    if(userInfo.getLockTime()==null) {
      userDto.setLocked(true);
    }else {
      userDto.setLocked(userInfo.getLockTime().after(new Date()));
    }
    return userDto;
  }

  @Override
  public void saveLoginSuccess(LoginLogDto dto) {
    Long userId = dto.getUserId();
    String ip = dto.getIp();
    String userAgent = dto.getUserAgent();
    String src = dto.getSrc();
    UserLog userLog = userLogService.findById(userId);
    userLog.setLastIp(userLog.getLoginIp());
    userLog.setLastTime(userLog.getLoginTime());
    userLog.setLockTime(null);
    userLog.setLoginIp(ip);
    userLog.setLockTime(new Date());
    userLogService.update(userLog);
    String ipAddr = ipService.findAddrByIp(ip);
    UserLoginLog userLoginLog = new UserLoginLog();
    userLoginLog.setUserId(userId);
    userLoginLog.setIp(ip);
    userLoginLog.setIpAddr(ipAddr);
    userLoginLog.setUserAgent(userAgent);
    userLoginLog.setRet(Enabled.T);
    userLoginLog.setSrc(src);
    userLoginLogService.save(userLoginLog);

  }

  @Override
  public void saveLoginFailure(LoginLogDto dto) {
    Long userId = dto.getUserId();
    String ip = dto.getIp();
    String userAgent = dto.getUserAgent();
    String src = dto.getSrc();
    UserLog userLog = userLogService.findById(userId);
    userLog.setErrTimes(userLog.getErrTimes()+1);
    int lockTimes = sysOptionService.findLockTimes();
    if(lockTimes>=userLog.getErrTimes()) {
      int lockTime = sysOptionService.findLockTime();
      userLog.setLockTime(DateUtil.add(new Date(), Calendar.HOUR_OF_DAY, lockTime));
      UserInfo userInfo = userInfoService.findById(userId);
      userInfo.setLockTime(userLog.getLockTime());
      userInfoService.update(userInfo);
    }
    userLogService.update(userLog);
    UserLoginLog userLoginLog = new UserLoginLog();
    String ipAddr = ipService.findAddrByIp(ip);
    userLoginLog.setUserId(userId);
    userLoginLog.setIp(ip);
    userLoginLog.setIpAddr(ipAddr);
    userLoginLog.setUserAgent(userAgent);
    userLoginLog.setRet(Enabled.F);
    userLoginLog.setSrc(src);
    userLoginLogService.save(userLoginLog);

  }

  @Override
  public Map<String, Object> findInfo(Long userId, String role) {
    Map<String, Object> root = new HashMap<String,Object>(4);
    root.put("role", role);
    UserInfo userInfo = userInfoService.findById(userId);
    root.put("headImg", userInfo.getHeadImg());
    List<Menu> menus = findMenu(role);
    root.put("menus", menus);
    List<String> btns = findBtn(role);
    root.put("btns", btns);
    return root;
  }

  @Override
  public void updatePassword(Long userId, String oldPassword, String password) {
    User user = userService.findById(userId);
    String salt = sysOptionService.findBossSalt();
    if(!user.getPassword().equals(Md5Util.saltPassword(salt, oldPassword))) {
      Assert.customException(message.getMessage("oldPassword.err"));
    }
    user.setPassword(Md5Util.saltPassword(salt, password));
    userService.update(user);

  }

  @Override
  public void delete(Long id) {
    UserInfo userInfo = userInfoService.findById(id);
    userInfo.setIsDel(Enabled.T);
    userInfoService.update(userInfo);

  }

  @Override
  public void updateResetPassword(Long id, String password) {
    User user = userService.findById(id);
    String salt = sysOptionService.findBossSalt();
    user.setPassword(Md5Util.saltPassword(salt, password));
    userService.update(user);

  }

  @Override
  public void updateUserInfo(UserInfo userInfo) {
    UserInfo po = userInfoService.findById(userInfo.getId());
    BeanUtil.copyExclude(userInfo, po, "id","createTime","isDel","isManager","lockTime","enabled","username");
    userInfoService.update(po);

  }

  @Override
  public List<Menu> findMenu(String roleCode) {
    List<Menu> menus = menuService.findMenu();
    Stream<Menu> stream = menus.parallelStream();
    if(!SymbolConstants.ASTERISK.equals(roleCode)) {
      Role role = roleService.findByCode(roleCode);
      List<RoleMenu> roleMenus=roleMenuService.findByRoleId(role.getId());
      Set<Long> menuIds = roleMenus.parallelStream().map(RoleMenu::getMenuId).collect(Collectors.toSet());
      stream = stream.filter(menu->menuIds.contains(menu.getId()));
    }
    return stream.collect(Collectors.toList());
  }

  @Override
  public List<String> findBtn(String roleCode) {
    List<Menu> menus = menuService.findBtn();
    Stream<Menu> stream = menus.parallelStream();
    if(!SymbolConstants.ASTERISK.equals(roleCode)) {
      Role role = roleService.findByCode(roleCode);
      List<RoleMenu> roleMenus = roleMenuService.findByRoleId(role.getId());
      Set<Long> menuIds = roleMenus.parallelStream().map(RoleMenu::getMenuId).collect(Collectors.toSet());
      stream = stream.filter(menu->menuIds.contains(menu.getId()));
    }
    
    return stream.map(Menu::getCode).collect(Collectors.toList());
  }

  @Override
  public void deleteByIds(Long[] ids) {
     if(ids!=null) {
       for(Long id:ids) {
         deleteById(id);
       }
     }
  }

  @Override
  public void deleteById(Long id) {
    UserInfo userInfo = userInfoService.findById(id);
    userInfo.setIsDel(Enabled.T);
    userInfoService.update(userInfo);

  }

}
