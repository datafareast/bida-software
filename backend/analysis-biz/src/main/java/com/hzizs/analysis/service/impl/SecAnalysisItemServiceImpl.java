package com.hzizs.analysis.service.impl;

import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.mybatis.service.impl.MyBatisServiceImpl;


import com.hzizs.analysis.dao.SecAnalysisItemDao;
import com.hzizs.analysis.entity.SecAnalysisItem;
import com.hzizs.analysis.service.SecAnalysisItemService;

/**
 * 业绩分析条目 服务实现类
 * 
 * @author crazy_cabbage
 *
 */
public class SecAnalysisItemServiceImpl extends MyBatisServiceImpl<SecAnalysisItem> implements SecAnalysisItemService {


  private SecAnalysisItemDao secAnalysisItemDao;

  public void setSecAnalysisItemDao(SecAnalysisItemDao secAnalysisItemDao) {
    this.secAnalysisItemDao = secAnalysisItemDao;
  }

  @Override
  protected MyBatisDao<SecAnalysisItem> getDao() {
    return secAnalysisItemDao;
  }
  // ========================================业务方法========================================
}
