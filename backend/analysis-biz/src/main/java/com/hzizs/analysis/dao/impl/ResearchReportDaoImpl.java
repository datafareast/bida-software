package com.hzizs.analysis.dao.impl;
import java.util.List;

import com.fasterxml.jackson.core.type.TypeReference;
import com.hzizs.mybatis.dao.impl.MyBatisDaoImpl;
import com.hzizs.analysis.dao.ResearchReportDao;
import com.hzizs.analysis.entity.ResearchReport;
/**
 * 研究报告 数据库操作类实现类	 
 * @author crazy_cabbage
 *
 */
public class ResearchReportDaoImpl extends MyBatisDaoImpl<ResearchReport> implements ResearchReportDao {



	@Override
	public TypeReference<List<ResearchReport>> getListEntityType() {
		return new TypeReference<List<ResearchReport>>() {
		};
	}
	//========================================业务方法========================================
}
