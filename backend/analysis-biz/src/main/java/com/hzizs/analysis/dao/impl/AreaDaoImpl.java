package com.hzizs.analysis.dao.impl;
import java.util.List;

import com.fasterxml.jackson.core.type.TypeReference;
import com.hzizs.mybatis.dao.impl.MyBatisDaoImpl;
import com.hzizs.analysis.dao.AreaDao;
import com.hzizs.analysis.entity.Area;
/**
 * 区域 数据库操作类实现类	 
 * @author crazy_cabbage
 *
 */
public class AreaDaoImpl extends MyBatisDaoImpl<Area> implements AreaDao {



	@Override
	public TypeReference<List<Area>> getListEntityType() {
		return new TypeReference<List<Area>>() {
		};
	}
	//========================================业务方法========================================
}
