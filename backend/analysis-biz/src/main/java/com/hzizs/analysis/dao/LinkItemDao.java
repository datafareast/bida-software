package com.hzizs.analysis.dao;

import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.analysis.entity.LinkItem;
/**
 * 友情链接条目 数据库操作类	 
 * @author crazy_cabbage
 *
 */
public interface LinkItemDao extends MyBatisDao<LinkItem> {

}
