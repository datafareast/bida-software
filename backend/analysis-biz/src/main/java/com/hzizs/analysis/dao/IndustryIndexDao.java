package com.hzizs.analysis.dao;

import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.analysis.entity.IndustryIndex;
/**
 * 行业指标 数据库操作类	 
 * @author crazy_cabbage
 *
 */
public interface IndustryIndexDao extends MyBatisDao<IndustryIndex> {

}
