package com.hzizs.analysis.service.impl;
import com.hzizs.mybatis.dao.MyBatisDao;
import com.hzizs.mybatis.service.impl.MyBatisServiceImpl;


import com.hzizs.analysis.dao.SysUserLogDao;
import com.hzizs.analysis.entity.SysUserLog;
import com.hzizs.analysis.service.SysUserLogService;
/**
 * 系统用户日志 服务实现类
 * @author crazy_cabbage
 *
 */
public class SysUserLogServiceImpl extends MyBatisServiceImpl<SysUserLog> implements SysUserLogService {
	

	private SysUserLogDao sysUserLogDao;
	public void setSysUserLogDao(SysUserLogDao sysUserLogDao) {
		this.sysUserLogDao = sysUserLogDao;
	}
	@Override
	protected MyBatisDao<SysUserLog> getDao() {
		return sysUserLogDao;
	}
	//========================================业务方法========================================
}
