package com.hzizs.analysis.facade;

import com.hzizs.analysis.entity.Folder;

public interface FolderFacade {
  /**
   * 根据主键集合， 用户主键 删除文件夹
   * 
   * @param ids    主键集合
   * @param userId 用户主键
   */
  void deleteByIds(Long[] ids, Long userId);

  /**
   * 根据主键，用户主键 删除文件夹
   * 
   * @param id     主键
   * @param userId 用户主键
   */
  void deleteById(Long id, Long userId);

  /**
   * 保存文件夹
   * 
   * @param folder 文件夹
   * @param userId 用户主键
   */
  void save(Folder folder, Long userId);

  /**
   * 更新文件夹
   * 
   * @param folder 文件夹
   * @param userId 用户主键
   */
  void update(Folder folder, Long userId);

}
