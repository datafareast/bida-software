package com.hzizs.analysis.service;

import java.util.List;
import com.hzizs.analysis.entity.UserRole;
import com.hzizs.service.IService;

/**
 * 用户角色 服务类
 * 
 * @author crazy_cabbage
 *
 */
public interface UserRoleService extends IService<UserRole> {
  /**
   * 根据用户主键 查找 用户角色
   * 
   * @param userId 用户主键
   * @return 找到用户角色
   */
  UserRole findByUserId(Long userId);

  /**
   * 根据角色主键 查找用户角色
   * 
   * @param roleId 角色主键
   * @return 用户角色
   */
  List<UserRole> findByRoleId(Long roleId);

  /**
   * 统计角色使用数量
   * 
   * @param roleId 角色主键
   * @return 角色使用数量
   */
  long countByRoleId(Long roleId);
}
