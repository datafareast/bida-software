package com.hzizs.analysis.entity;

import com.hzizs.entity.Entity;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * 数据种类		
 * @author crazy_cabbage
 *
 */
@XmlRootElement		
@SuppressWarnings("serial")
public class DataType extends Entity  {
   // 主键
	private Long id;
	// 行业
	@NotNull
	private Long dataIndustryId;
	// 名称
	private String name;
	// 排序
	private Integer sortOrder;
    		 		
    		
 

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
    		
	public Long getDataIndustryId() {
		return dataIndustryId;
	}

	public void setDataIndustryId(Long dataIndustryId) {
		this.dataIndustryId = dataIndustryId;
	}
    		
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
    		
	public Integer getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(Integer sortOrder) {
		this.sortOrder = sortOrder;
	}
	
}
