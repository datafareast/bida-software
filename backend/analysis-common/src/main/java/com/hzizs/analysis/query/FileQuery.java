package com.hzizs.analysis.query;

import java.util.HashMap;
import java.util.Map;

import com.hzizs.query.ListQuery;
import com.hzizs.util.StringUtil;

public class FileQuery extends ListQuery {

  /**
   * 
   */
  private static final long serialVersionUID = 2434741367573023480L;
  private String cat;
  private Long folderId;


  public String getCat() {
    return cat;
  }


  public void setCat(String cat) {
    this.cat = cat;
  }


  public Long getFolderId() {
    return folderId;
  }


  public void setFolderId(Long folderId) {
    this.folderId = folderId;
  }


  @Override
  protected Map<String, Object> queryParams(Map<String, Object> params) {
    if (params == null) {
      params = new HashMap<String, Object>();
    }
    super.queryParams(params);
    if (folderId != null) {
      params.put("folderId", folderId);
    }
    if (StringUtil.isNotEmpty(cat)) {
      params.put("cat", cat);
    }
    return params;
  }

}
