package com.hzizs.analysis.service;

import com.hzizs.service.IService;
import java.util.Date;
import java.util.List;
import com.hzizs.analysis.entity.DataTopoVal;

/**
 * 数据拓普值 服务类
 * 
 * @author crazy_cabbage
 *
 */
public interface DataTopoValService extends IService<DataTopoVal> {
  /**
   * 根据 类型 ，指标 ，父级指标 发生日期查找 指标数据
   * 
   * @param dataTypeId       类型
   * @param dataTopoId       指标
   * @param dataTopoParentId 父级指标
   * @param occTime          发生日期
   * @return 指标数据
   */
  DataTopoVal find(Long dataTypeId, Long dataTopoId, Long dataTopoParentId, Date occTime);
  /**
   * 根据指标 查找数据
   * @param dataTopoId 指标
   * @return 找到的数据
   */
  List<DataTopoVal> findByDataTopoId(Long dataTopoId);

}
