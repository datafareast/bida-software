package com.hzizs.analysis.service;

import com.hzizs.service.IService;
import java.util.List;
import com.hzizs.analysis.entity.RoleAuth;

/**
 * 角色权限 服务类
 * 
 * @author crazy_cabbage
 *
 */
public interface RoleAuthService extends IService<RoleAuth> {
  /**
   * 根据角色主键 查找 角色权限
   * 
   * @param roleId 角色主键
   * @return 角色权限
   */
  List<RoleAuth> findByRoleId(Long roleId);

  /**
   * 根据角色主键，权限集合，删除角色权限
   * 
   * @param roleId  角色主键
   * @param authIds 权限集合
   */
  void delete(Long roleId, List<Long> authIds);

  /**
   * 根据角色主键 删除角色权限
   * 
   * @param roleId 角色主键
   */
  void deleteByRoleId(Long roleId);

  /**
   * 根据权限主键统计
   * 
   * @param authId 权限
   * @return 统计权限的使用数量
   */
  long countByAuthId(Long authId);

}
