package com.hzizs.analysis.facade;

import java.util.List;
import java.util.Map;
import com.hzizs.analysis.entity.DataTopoVal;

public interface DataTopoValFacade {
  /**
   * 保存指标值
   * @param dataTopoVal 指标值
   */
  void save(DataTopoVal dataTopoVal);
 
  /**
   * 更新指标值
   * @param dataTopoVal 指标值
   */
  void update(DataTopoVal dataTopoVal);
  /**
   * 根据指标查找父级 数据
   * @param dataTopoId 指标
   * @return 找到的父级数据
   */
  List<Map<String, Object>> findData(Long dataTopoId);

}
