package com.hzizs.analysis.service;

import com.hzizs.service.IService;
import java.util.Map;
import com.hzizs.analysis.entity.SysI18n;

/**
 * 系统国际化 服务类
 * 
 * @author crazy_cabbage
 *
 */
public interface SysI18nService extends IService<SysI18n> {
  /**
   * 查找国际化对象
   * @return 国际化对象
   */
  Map<String, Object> findMap();
  /**
   * 根据
   * @param code
   * @return 找到的国际化
   */
  SysI18n findByCode(String code);
}
