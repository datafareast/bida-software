package com.hzizs.analysis.query;

import java.util.HashMap;
import java.util.Map;

import com.hzizs.query.ListQuery;
import com.hzizs.util.StringUtil;

public class IndustryIndexQuery extends ListQuery {

  /**
   * 
   */
  private static final long serialVersionUID = 2434741367573023480L;
  private String name;
  private String code;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }
  

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  @Override
  protected Map<String, Object> queryParams(Map<String, Object> params) {
    if (params == null) {
      params = new HashMap<String, Object>();
    }
    super.queryParams(params);
    if (StringUtil.isNotEmpty(name)) {
      params.put("name", name);
    }
    if (StringUtil.isNotEmpty(code)) {
      params.put("code", code);
    }
    return params;
  }

}
