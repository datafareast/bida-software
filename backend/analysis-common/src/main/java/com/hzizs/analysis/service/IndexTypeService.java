package com.hzizs.analysis.service;

import com.hzizs.service.IService;
import java.util.List;
import com.hzizs.analysis.entity.IndexType;
/**
 * 指标类型 服务类 
 * @author crazy_cabbage
 *
 */
public interface IndexTypeService extends IService<IndexType> {
  /**
   * 查找启用指标
   * @return 找到启用指标
   */
  List<IndexType> findEnabled();

}
