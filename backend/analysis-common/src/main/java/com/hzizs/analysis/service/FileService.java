package com.hzizs.analysis.service;

import com.hzizs.service.IShardingService;
import com.hzizs.analysis.entity.File;

/**
 * 文件 服务类
 * 
 * @author crazy_cabbage
 *
 */
public interface FileService extends IShardingService<File> {
  /**
   * 根据名称查找文件
   * 
   * @param name   名称
   * @param userId 用户
   * @return 找到的文件对象
   */
  File findByName(String name, Long userId);

}
