package com.hzizs.analysis.service;

import com.hzizs.service.IService;
import java.util.Map;
import com.hzizs.analysis.entity.I18n;

/**
 * 国际化 服务类
 * 
 * @author crazy_cabbage
 *
 */
public interface I18nService extends IService<I18n> {
  /**
   * 查找国际化对象
   * @return 国际化对象
   */
  Map<String, Object> findMap();
  /**
   * 根据编码查找国际化
   * @param code 编码
   * @return 找到的国际化
   */
  I18n findByCode(String code);

}
