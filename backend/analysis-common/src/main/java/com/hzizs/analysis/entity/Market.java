package com.hzizs.analysis.entity;

import com.hzizs.entity.Entity;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * 市场		
 * @author crazy_cabbage
 *
 */
@XmlRootElement		
@SuppressWarnings("serial")
public class Market extends Entity  {
   // 主键
	private Long id;
	// 名称
	private String name;
	// 简写
	private String shoartName;
    		 		
    		
 

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
    		
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
    		
	public String getShoartName() {
		return shoartName;
	}

	public void setShoartName(String shoartName) {
		this.shoartName = shoartName;
	}
	
}
