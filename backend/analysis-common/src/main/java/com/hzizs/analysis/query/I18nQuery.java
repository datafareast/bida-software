package com.hzizs.analysis.query;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import com.hzizs.constants.SymbolConstants;
import com.hzizs.query.ListQuery;
import com.hzizs.util.StringUtil;

public class I18nQuery extends ListQuery {

  /**
   * 
   */
  private static final long serialVersionUID = 2434741367573023480L;
  private String name;
  private String code;
  private Date startTime;
  private Date endTime;
  private Date startModifyTime;
  private Date endModifyTime;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public Date getStartTime() {
    return startTime;
  }

  public void setStartTime(Date startTime) {
    this.startTime = startTime;
  }

  public Date getEndTime() {
    return endTime;
  }

  public void setEndTime(Date endTime) {
    this.endTime = endTime;
  }

  public Date getStartModifyTime() {
    return startModifyTime;
  }

  public void setStartModifyTime(Date startModifyTime) {
    this.startModifyTime = startModifyTime;
  }

  public Date getEndModifyTime() {
    return endModifyTime;
  }

  public void setEndModifyTime(Date endModifyTime) {
    this.endModifyTime = endModifyTime;
  }

  @Override
  protected Map<String, Object> queryParams(Map<String, Object> params) {
    if (params == null) {
      params = new HashMap<String, Object>();
    }
    super.queryParams(params);
    if (StringUtil.isNotEmpty(name)) {
      params.put("likeName", name + SymbolConstants.PERCENT);
    }
    if (StringUtil.isNotEmpty(code)) {
      params.put("likeCode", code + SymbolConstants.PERCENT);
    }
    if (startTime != null) {
      params.put("startTime", startTime);
    }
    if (endTime != null) {
      params.put("endTime", endTime);
    }
    if (startModifyTime != null) {
      params.put("startModifyTime", startModifyTime);
    }
    if (endModifyTime != null) {
      params.put("endModifyTime", endModifyTime);
    }
    return params;
  }

}
