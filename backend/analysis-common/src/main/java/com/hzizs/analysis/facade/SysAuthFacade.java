package com.hzizs.analysis.facade;

import com.hzizs.analysis.entity.SysAuth;

public interface SysAuthFacade {
  /**
   * 删除系统权限
   * 
   * @param id 主键
   */
  void deleteById(Long id);

  /**
   * 批量删除系统权限
   * 
   * @param ids 主键集合
   */
  void deleteByIds(Long[] ids);
  /**
   *  保存系统权限
   * @param sysAuth 系统权限
   */
  void save(SysAuth sysAuth);
  /**
   * 更新系统权限
   * @param sysAuth 系统权限
   */
  void update(SysAuth sysAuth);

}
