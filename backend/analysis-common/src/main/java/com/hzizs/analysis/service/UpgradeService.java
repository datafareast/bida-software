package com.hzizs.analysis.service;

import com.hzizs.service.IService;
import com.hzizs.analysis.entity.Upgrade;

/**
 * 升级信息 服务类
 * 
 * @author crazy_cabbage
 *
 */
public interface UpgradeService extends IService<Upgrade> {

}
