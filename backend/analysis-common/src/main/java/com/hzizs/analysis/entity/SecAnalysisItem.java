package com.hzizs.analysis.entity;

import com.hzizs.entity.Entity;
import javax.xml.bind.annotation.XmlRootElement;


import java.math.BigDecimal;
import java.util.Date;

/**
 * 业绩分析条目
 * 
 * @author crazy_cabbage
 *
 */
@XmlRootElement
@SuppressWarnings("serial")
public class SecAnalysisItem extends Entity {
  // 主键
  private Long id;
  // 名称
  private String name;
  // 数值
  private BigDecimal num;
  // 数值2
  private BigDecimal num2;
  // 日期
  private Date occDate;
  // 业务分析
  private Long secAnalysisId;



  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public BigDecimal getNum() {
    return num;
  }

  public void setNum(BigDecimal num) {
    this.num = num;
  }

  public BigDecimal getNum2() {
    return num2;
  }

  public void setNum2(BigDecimal num2) {
    this.num2 = num2;
  }

  public Date getOccDate() {
    return occDate;
  }

  public void setOccDate(Date occDate) {
    this.occDate = occDate;
  }

  public Long getSecAnalysisId() {
    return secAnalysisId;
  }

  public void setSecAnalysisId(Long secAnalysisId) {
    this.secAnalysisId = secAnalysisId;
  }

}
