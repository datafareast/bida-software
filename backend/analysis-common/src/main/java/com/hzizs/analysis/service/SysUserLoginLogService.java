package com.hzizs.analysis.service;

import com.hzizs.service.IService;
import com.hzizs.analysis.entity.SysUserLoginLog;

/**
 * 系统用户登录日志 服务类
 * 
 * @author crazy_cabbage
 *
 */
public interface SysUserLoginLogService extends IService<SysUserLoginLog> {

}
