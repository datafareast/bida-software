package com.hzizs.analysis.query;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.hzizs.constants.SymbolConstants;
import com.hzizs.query.ListQuery;
import com.hzizs.util.CollectionUtil;
import com.hzizs.util.StringUtil;

public class DataTopoQuery extends ListQuery {

  /**
   * 
   */
  private static final long serialVersionUID = 2434741367573023480L;
  private String name;
  private String cat;
  private String isShow;
  private Long dataTypeId;
  private List<Long> pIds;
  public String getName() {
    return name;
  }

  public List<Long> getpIds() {
    return pIds;
  }

  public void setpIds(List<Long> pIds) {
    this.pIds = pIds;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getCat() {
    return cat;
  }

  public void setCat(String cat) {
    this.cat = cat;
  }

  public String getIsShow() {
    return isShow;
  }

  public void setIsShow(String isShow) {
    this.isShow = isShow;
  }

  public Long getDataTypeId() {
    return dataTypeId;
  }

  public void setDataTypeId(Long dataTypeId) {
    this.dataTypeId = dataTypeId;
  }

  @Override
  protected Map<String, Object> queryParams(Map<String, Object> params) {
    if (params == null) {
      params = new HashMap<String, Object>();
    }
    super.queryParams(params);
    if (StringUtil.isNotEmpty(name)) {
      params.put("likeName", SymbolConstants.PERCENT+name+SymbolConstants.PERCENT);
    }
    if(CollectionUtil.isNotEmpty(pIds)) {
      params.put("pIds", pIds);
    }
    if (StringUtil.isNotEmpty(cat)) {
      params.put("cat", cat);
    }
    if (StringUtil.isNotEmpty(isShow)) {
      params.put("isShow", isShow);
    }
    if (dataTypeId!=null) {
      params.put("dataTypeId", dataTypeId);
    }
    return params;
  }

}
