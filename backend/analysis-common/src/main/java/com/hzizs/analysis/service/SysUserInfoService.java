package com.hzizs.analysis.service;

import com.hzizs.service.IService;
import com.hzizs.analysis.entity.SysUserInfo;

/**
 * 系统用户信息 服务类
 * 
 * @author crazy_cabbage
 *
 */
public interface SysUserInfoService extends IService<SysUserInfo> {
  /**
   * 更新用户基本信息
   * 
   * @param userInfo 用户信息
   */
  void updateInfo(SysUserInfo userInfo);

  /**
   * 更新用户基本信息，去掉禁用等关键信息
   * 
   * @param userInfo 用户信息
   */
  void updateInfo2(SysUserInfo userInfo);

}
