package com.hzizs.analysis.entity;

import com.hzizs.entity.Entity;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * 数据抽取		
 * @author crazy_cabbage
 *
 */
@XmlRootElement		
@SuppressWarnings("serial")
public class DataExtract extends Entity  {
   // 主键
	private Long id;
	// 数据源
	private Long dataSourceId;
	// 语句
	private String query;
	// 目标
	private String target;
	// 标题
	private String title;
    		 		
    		
 

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
    		
	public Long getDataSourceId() {
		return dataSourceId;
	}

	public void setDataSourceId(Long dataSourceId) {
		this.dataSourceId = dataSourceId;
	}
    		
	public String getQuery() {
		return query;
	}

	public void setQuery(String query) {
		this.query = query;
	}
    		
	public String getTarget() {
		return target;
	}

	public void setTarget(String target) {
		this.target = target;
	}
    		
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
}
