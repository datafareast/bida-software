package com.hzizs.analysis.entity;

import com.hzizs.entity.Entity;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * 证券参考数据		
 * @author crazy_cabbage
 *
 */
@XmlRootElement		
@SuppressWarnings("serial")
public class RefSecurity extends Entity  {
   // 主键
	private Long id;
	// 货币
	private String currency;
	// 行业
	private String industry;
	// 行业细节
	private String industryDetail;
	// 上市市场
	private String listingMarket;
	// 上市市场主键
	private Long listingMarketId;
	// 编码
	private String securityCode;
	// 证券主键
	private Long securityId;
	// 名称
	private String securityName;
	// 交易市场
	private String tradingMarket;
	// 交易市场主键
	private Long tradingMarketId;
    		 		
    		
 

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
    		
	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}
    		
	public String getIndustry() {
		return industry;
	}

	public void setIndustry(String industry) {
		this.industry = industry;
	}
    		
	public String getIndustryDetail() {
		return industryDetail;
	}

	public void setIndustryDetail(String industryDetail) {
		this.industryDetail = industryDetail;
	}
    		
	public String getListingMarket() {
		return listingMarket;
	}

	public void setListingMarket(String listingMarket) {
		this.listingMarket = listingMarket;
	}
    		
	public Long getListingMarketId() {
		return listingMarketId;
	}

	public void setListingMarketId(Long listingMarketId) {
		this.listingMarketId = listingMarketId;
	}
    		
	public String getSecurityCode() {
		return securityCode;
	}

	public void setSecurityCode(String securityCode) {
		this.securityCode = securityCode;
	}
    		
	public Long getSecurityId() {
		return securityId;
	}

	public void setSecurityId(Long securityId) {
		this.securityId = securityId;
	}
    		
	public String getSecurityName() {
		return securityName;
	}

	public void setSecurityName(String securityName) {
		this.securityName = securityName;
	}
    		
	public String getTradingMarket() {
		return tradingMarket;
	}

	public void setTradingMarket(String tradingMarket) {
		this.tradingMarket = tradingMarket;
	}
    		
	public Long getTradingMarketId() {
		return tradingMarketId;
	}

	public void setTradingMarketId(Long tradingMarketId) {
		this.tradingMarketId = tradingMarketId;
	}
	
}
