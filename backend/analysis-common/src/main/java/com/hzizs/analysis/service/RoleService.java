package com.hzizs.analysis.service;

import com.hzizs.service.IService;
import com.hzizs.analysis.entity.Role;

/**
 * 角色 服务类
 * 
 * @author crazy_cabbage
 *
 */
public interface RoleService extends IService<Role> {
  /**
   * 根据角色编码 查找角色
   * 
   * @param code 角色编码
   * @return 角色
   */
  Role findByCode(String code);

}
