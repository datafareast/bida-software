package com.hzizs.analysis.entity;

import com.hzizs.entity.Entity;
import com.hzizs.entity.ICreate;
import com.hzizs.entity.IModify;
import javax.xml.bind.annotation.XmlRootElement;


import java.util.Date;

/**
 * 国际化
 * 
 * @author crazy_cabbage
 *
 */
@XmlRootElement
@SuppressWarnings("serial")
public class I18n extends Entity implements ICreate, IModify {
  // 主键
  private Long id;
  // 编码
  private String code;
  // 创建时间
  private Date createTime;
  // 修改时间
  private Date modifyTime;
  // 中文
  private String name;
  // 英文
  private String nameEn;
  // 排序
  private Integer sortOrder;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public Date getCreateTime() {
    return createTime;
  }

  public void setCreateTime(Date createTime) {
    this.createTime = createTime;
  }

  public Date getModifyTime() {
    return modifyTime;
  }

  public void setModifyTime(Date modifyTime) {
    this.modifyTime = modifyTime;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getNameEn() {
    return nameEn;
  }

  public void setNameEn(String nameEn) {
    this.nameEn = nameEn;
  }

  public Integer getSortOrder() {
    return sortOrder;
  }

  public void setSortOrder(Integer sortOrder) {
    this.sortOrder = sortOrder;
  }

  @Override
  public void setModifyBy(Long modifyBy) {
    
  }

  @Override
  public void setCreateBy(Long createBy) {
    
  }

}
