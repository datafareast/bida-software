package com.hzizs.analysis.service;

import com.hzizs.service.IService;
import com.hzizs.analysis.entity.DataExtract;

/**
 * 数据抽取 服务类
 * 
 * @author crazy_cabbage
 *
 */
public interface DataExtractService extends IService<DataExtract> {

}
