package com.hzizs.analysis.dto;

import com.hzizs.analysis.entity.User;
import com.hzizs.analysis.entity.UserInfo;
import com.hzizs.analysis.entity.UserLog;
import com.hzizs.analysis.entity.UserRole;
import com.hzizs.dto.Dto;

public class UserInfoDto extends Dto{

  /**
   * 
   */
  private static final long serialVersionUID = -1158589539940951695L;
  
  private User user;
  private UserInfo userInfo;
  private UserRole userRole;
  private UserLog userLog;
  public User getUser() {
    return user;
  }
  public void setUser(User user) {
    this.user = user;
  }
  public UserInfo getUserInfo() {
    return userInfo;
  }
  public void setUserInfo(UserInfo userInfo) {
    this.userInfo = userInfo;
  }
  public UserRole getUserRole() {
    return userRole;
  }
  public void setUserRole(UserRole userRole) {
    this.userRole = userRole;
  }
  public UserLog getUserLog() {
    return userLog;
  }
  public void setUserLog(UserLog userLog) {
    this.userLog = userLog;
  }
  

}
