package com.hzizs.analysis.query;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import com.hzizs.query.ListQuery;
import com.hzizs.util.StringUtil;

public class DataTopoValQuery extends ListQuery {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2434741367573023480L;
	private String code;
    private Long dataTypeId;
    private Long dataTopoId;
    private Long dataTopoParentId;
    private Date occTime;
 
    public String getCode() {
      return code;
    }

    public void setCode(String code) {
      this.code = code;
    }

    public Long getDataTypeId() {
      return dataTypeId;
    }

    public void setDataTypeId(Long dataTypeId) {
      this.dataTypeId = dataTypeId;
    }

    public Long getDataTopoId() {
      return dataTopoId;
    }

    public void setDataTopoId(Long dataTopoId) {
      this.dataTopoId = dataTopoId;
    }

    public Long getDataTopoParentId() {
      return dataTopoParentId;
    }

    public void setDataTopoParentId(Long dataTopoParentId) {
      this.dataTopoParentId = dataTopoParentId;
    }

    public Date getOccTime() {
      return occTime;
    }

    public void setOccTime(Date occTime) {
      this.occTime = occTime;
    }

    @Override
	protected Map<String, Object> queryParams(Map<String, Object> params) {
    	if (params == null) {
			params = new HashMap<String, Object>();
		}
		super.queryParams(params);
		if (StringUtil.isNotEmpty(code)) {
			params.put("code", code);
		}
		if (dataTypeId!=null) {
		  params.put("dataTypeId", dataTypeId);
		}
		if (dataTopoId!=null) {
		  params.put("dataTopoId", dataTopoId);
		}
		if (dataTopoParentId!=null) {
		  params.put("dataTopoParentId", dataTopoParentId);
		}
		if (occTime!=null) {
		  params.put("occTime", occTime);
		}
		return params;
	}

}
