package com.hzizs.analysis.service;

import com.hzizs.service.IService;
import com.hzizs.analysis.entity.ImitatePositionItem;

/**
 * 模拟持仓条目 服务类
 * 
 * @author crazy_cabbage
 *
 */
public interface ImitatePositionItemService extends IService<ImitatePositionItem> {

}
