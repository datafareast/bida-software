package com.hzizs.analysis.entity;

import com.hzizs.entity.Entity;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * 证券		
 * @author crazy_cabbage
 *
 */
@XmlRootElement		
@SuppressWarnings("serial")
public class Security extends Entity  {
   // 主键
	private Long id;
	// 上市
	private Long listingMarket;
	// 编码
	private String seurityCode;
	// 交易
	private Long tradingMarket;
    		 		
    		
 

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
    		
	public Long getListingMarket() {
		return listingMarket;
	}

	public void setListingMarket(Long listingMarket) {
		this.listingMarket = listingMarket;
	}
    		
	public String getSeurityCode() {
		return seurityCode;
	}

	public void setSeurityCode(String seurityCode) {
		this.seurityCode = seurityCode;
	}
    		
	public Long getTradingMarket() {
		return tradingMarket;
	}

	public void setTradingMarket(Long tradingMarket) {
		this.tradingMarket = tradingMarket;
	}
	
}
