package com.hzizs.analysis.entity;

import java.util.Date;
import javax.xml.bind.annotation.XmlRootElement;
import com.hzizs.entity.ICreate;
import com.hzizs.entity.ISortOrder;
import com.hzizs.entity.ShardingEntity;

/**
 * 文件
 * 
 * @author crazy_cabbage
 *
 */
@XmlRootElement
@SuppressWarnings("serial")
public class File extends ShardingEntity implements ICreate,ISortOrder {
  // 主键
  private Long id;
  // 类别
  private String cat;
  // 创建时间
  private Date createTime;
  // 文件夹
  private Long folderId;
  // 名称
  private String name;
  // 原始文件名
  private String oldName;
  // 路径
  private String path;
  // 排序
  private Integer sortOrder;
  // URL
  private String url;
  // 用户
  private Long userId;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getCat() {
    return cat;
  }

  public void setCat(String cat) {
    this.cat = cat;
  }

  public Date getCreateTime() {
    return createTime;
  }

  public void setCreateTime(Date createTime) {
    this.createTime = createTime;
  }

  public Long getFolderId() {
    return folderId;
  }

  public void setFolderId(Long folderId) {
    this.folderId = folderId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getOldName() {
    return oldName;
  }

  public void setOldName(String oldName) {
    this.oldName = oldName;
  }

  public String getPath() {
    return path;
  }

  public void setPath(String path) {
    this.path = path;
  }

  public Integer getSortOrder() {
    return sortOrder;
  }

  public void setSortOrder(Integer sortOrder) {
    this.sortOrder = sortOrder;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public Long getUserId() {
    return userId;
  }

  public void setUserId(Long userId) {
    this.userId = userId;
  }

  @Override
  public void setCreateBy(Long createBy) {
    
  }

}
