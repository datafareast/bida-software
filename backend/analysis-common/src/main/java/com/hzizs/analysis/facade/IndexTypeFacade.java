package com.hzizs.analysis.facade;

import com.hzizs.analysis.dto.IndexDto;
import com.hzizs.analysis.query.DataQuery;

public interface IndexTypeFacade {
   
   /**
    * 根据 条件查找 指标结果
    * @param query 查询条件
    * @return 统计指标
    */
  IndexDto findData(DataQuery query);

}
