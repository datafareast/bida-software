package com.hzizs.analysis.service;

import com.hzizs.service.IService;
import com.hzizs.analysis.entity.Industry;
/**
 * 行业 服务类 
 * @author crazy_cabbage
 *
 */
public interface IndustryService extends IService<Industry> {

}
