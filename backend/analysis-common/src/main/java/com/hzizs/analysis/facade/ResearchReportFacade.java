package com.hzizs.analysis.facade;

import java.util.List;
import com.hzizs.analysis.entity.ResearchReport;

public interface ResearchReportFacade {
  /**
   * 保存研究报告
   * @param researchReport 保存研究报告
   */
  void save(ResearchReport researchReport);
  /**
   * 更新研究报告
   * @param researchReport 更新研究报告
   */
  void update(ResearchReport researchReport);
  /**
   * 根据用户查找 研究报告
   * @param userId 用户主键
   * @return 找到研究报告
   */
  List<ResearchReport> findByUserId(Long userId);

}
