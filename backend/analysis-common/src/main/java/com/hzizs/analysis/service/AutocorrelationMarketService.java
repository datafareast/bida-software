package com.hzizs.analysis.service;

import com.hzizs.service.IService;
import java.util.Date;
import java.util.List;
import com.hzizs.analysis.entity.AutocorrelationMarket;
/**
 * 市场自相关 服务类 
 * @author crazy_cabbage
 *
 */
public interface AutocorrelationMarketService extends IService<AutocorrelationMarket> {
  /**
   * 查找市场自相关 
   * @param marketId 市场ID
   * @param startTime 开始时间
   * @param endTime 结束时间
   * @return 找到市场自相关
   */
  List<AutocorrelationMarket> find(Long marketId, Date startTime, Date endTime);

}
