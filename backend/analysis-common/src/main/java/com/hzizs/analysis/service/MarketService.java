package com.hzizs.analysis.service;

import com.hzizs.service.IService;
import com.hzizs.analysis.entity.Market;
/**
 * 市场 服务类 
 * @author crazy_cabbage
 *
 */
public interface MarketService extends IService<Market> {

}
