package com.hzizs.analysis.service;

import com.hzizs.service.IService;
import java.util.Date;
import java.util.List;
import com.hzizs.analysis.entity.EffectiveSpreadMarket;
/**
 * 市场有效价差 服务类 
 * @author crazy_cabbage
 *
 */
public interface EffectiveSpreadMarketService extends IService<EffectiveSpreadMarket> {
  /**
   * 查找有效价差
   * @param marketId 市场主键
   * @param startTime 开始时间
   * @param endTime 结束时间
   * @return 有效价差
   */
  List<EffectiveSpreadMarket> find(Long marketId, Date startTime, Date endTime);

}
