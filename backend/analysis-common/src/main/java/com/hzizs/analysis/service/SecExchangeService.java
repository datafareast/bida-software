package com.hzizs.analysis.service;

import com.hzizs.service.IService;
import com.hzizs.analysis.entity.SecExchange;

/**
 * 交易所 服务类
 * 
 * @author crazy_cabbage
 *
 */
public interface SecExchangeService extends IService<SecExchange> {

}
