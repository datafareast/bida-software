package com.hzizs.analysis.service;

import com.hzizs.service.IService;
import com.hzizs.analysis.entity.LinkItem;

/**
 * 友情链接条目 服务类
 * 
 * @author crazy_cabbage
 *
 */
public interface LinkItemService extends IService<LinkItem> {
  /**
   * 根据链接查找统计条目数量
   * 
   * @param linkId 链接主键
   * @return 找到的条目数量
   */
  long countByLinkId(Long linkId);

}
