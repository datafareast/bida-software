package com.hzizs.analysis.facade;

import com.hzizs.analysis.entity.Ip;

public interface IpFacade {
  /**
   * 保存ip对象
   * @param ip ip对象
   * @return ip对象
   */
  Ip save(Ip ip);
  
  /**
   * 更新IP对象
   * @param ip ip对象
   * @return ip对象
   */
  Ip update(Ip ip);
  /**
   * 批量删除
   * @param ids 主键
   */
  void deleteByIds(Long []ids);
  /**
   * 根据主键删除
   * @param id 主键
   */
  void deleteById(Long id);

}
