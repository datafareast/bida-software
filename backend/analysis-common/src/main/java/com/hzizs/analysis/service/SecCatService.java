package com.hzizs.analysis.service;

import com.hzizs.service.IService;
import com.hzizs.analysis.entity.SecCat;

/**
 * 类别 服务类
 * 
 * @author crazy_cabbage
 *
 */
public interface SecCatService extends IService<SecCat> {

}
