package com.hzizs.analysis.service;

import java.util.List;
import com.hzizs.analysis.entity.SysData;
import com.hzizs.service.IService;

/**
 * 数据字典 服务类
 * 
 * @author crazy_cabbage
 *
 */
public interface SysDataService extends IService<SysData> {
  /**
   * 根据父主键 查找 系统数据字典
   * 
   * @param parentId 父主键
   * @return 找到系统数据字典
   */
  List<SysData> findByParentId(Long parentId);

  /**
   * 根据父编码查找 子集合
   * 
   * @param parentCode 父编码
   * @return 找到子集合
   */
  List<SysData> findByParentCode(String parentCode);

  /**
   * 根据编码 父主键 查找 数据字典对象
   * 
   * @param code     编码
   * @param parentId 父主键
   * @return 找到字典对象
   */
  SysData findByCode(String code, Long parentId);

  /**
   * 根据父主键 统计子项数量
   * 
   * @param parentId 父主键
   * @return 找到的子项数量
   */
  long countByParentId(Long parentId);
}
