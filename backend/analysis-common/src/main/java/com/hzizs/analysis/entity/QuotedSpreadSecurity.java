package com.hzizs.analysis.entity;

import com.hzizs.entity.Entity;
import javax.xml.bind.annotation.XmlRootElement;


import java.util.Date;		
import java.math.BigDecimal;		
/**
 * 证券报价价差		
 * @author crazy_cabbage
 *
 */
@XmlRootElement		
@SuppressWarnings("serial")
public class QuotedSpreadSecurity extends Entity  {
   // 主键
	private Long id;
	// 日期
	private Date occDate;
	// 绝对报价价差
	private BigDecimal quotedSpreadAbs;
	// 基点报价价差
	private BigDecimal quotedSpreadBps;
	// 证券
	private Long securityId;
    		 		
    		
 

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
    		
	public Date getOccDate() {
		return occDate;
	}

	public void setOccDate(Date occDate) {
		this.occDate = occDate;
	}
    		
	public BigDecimal getQuotedSpreadAbs() {
		return quotedSpreadAbs;
	}

	public void setQuotedSpreadAbs(BigDecimal quotedSpreadAbs) {
		this.quotedSpreadAbs = quotedSpreadAbs;
	}
    		
	public BigDecimal getQuotedSpreadBps() {
		return quotedSpreadBps;
	}

	public void setQuotedSpreadBps(BigDecimal quotedSpreadBps) {
		this.quotedSpreadBps = quotedSpreadBps;
	}
    		
	public Long getSecurityId() {
		return securityId;
	}

	public void setSecurityId(Long securityId) {
		this.securityId = securityId;
	}
	
}
