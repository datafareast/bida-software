package com.hzizs.analysis.service;

import com.hzizs.service.IService;
import java.util.Date;
import java.util.List;
import com.hzizs.analysis.entity.AutocorrelationSecurity;
/**
 * 证券自相关 服务类 
 * @author crazy_cabbage
 *
 */
public interface AutocorrelationSecurityService extends IService<AutocorrelationSecurity> {
  /**
   * 查找自相关 
   * @param securityId 证券主键
   * @param startTime 开始时间
   * @param endTime 结束时间
   * @return 找到自相关
   */
  List<AutocorrelationSecurity> find(Long securityId, Date startTime, Date endTime);

}
