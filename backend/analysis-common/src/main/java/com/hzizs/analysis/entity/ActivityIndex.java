package com.hzizs.analysis.entity;

import com.hzizs.entity.Entity;
import javax.xml.bind.annotation.XmlRootElement;


import java.math.BigDecimal;		
import java.util.Date;		
/**
 * 指数指标		
 * @author crazy_cabbage
 *
 */
@XmlRootElement		
@SuppressWarnings("serial")
public class ActivityIndex extends Entity  {
   // 主键
	private Long id;
	// 收盘价
	private BigDecimal closePrice;
	// 编码
	private String code;
	// 名称
	private String name;
	// 日期
	private Date occDate;
	// 收益率
	private BigDecimal retPrt;
    		 		
    		
 

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
    		
	public BigDecimal getClosePrice() {
		return closePrice;
	}

	public void setClosePrice(BigDecimal closePrice) {
		this.closePrice = closePrice;
	}
    		
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
    		
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
    		
	public Date getOccDate() {
		return occDate;
	}

	public void setOccDate(Date occDate) {
		this.occDate = occDate;
	}
    		
	public BigDecimal getRetPrt() {
		return retPrt;
	}

	public void setRetPrt(BigDecimal retPrt) {
		this.retPrt = retPrt;
	}
	
}
