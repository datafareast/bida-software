package com.hzizs.analysis.service;

import com.hzizs.service.IService;
import com.hzizs.analysis.entity.SysUserRole;

/**
 * 系统用户角色 服务类
 * 
 * @author crazy_cabbage
 *
 */
public interface SysUserRoleService extends IService<SysUserRole> {
  /**
   * 根据用户主键 查找 用户角色
   * 
   * @param userId 用户主键
   * @return 用户角色
   */
  SysUserRole findByUserId(Long userId);

  /**
   * 根据角色主键统计数量
   * 
   * @param roleId 角色主键
   * @return 统计出的数量
   */
  long countByRoleId(Long roleId);


}
