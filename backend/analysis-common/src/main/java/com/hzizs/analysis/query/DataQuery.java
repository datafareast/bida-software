package com.hzizs.analysis.query;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.hzizs.query.Query;
import com.hzizs.util.StringUtil;

public class DataQuery extends Query {

  /**
   * 
   */
  private static final long serialVersionUID = 2434741367573023480L;
  private String code;
  private Date startTime;
  private Date endTime;
  private Long marketId;
  // 个股
  private List<Long> security;
  // 个股指标
  private List<String> securityIndexType;
  // 市场
  private List<Long> market;
  // 市场指标
  private List<String> marketIndexType;


  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public Date getStartTime() {
    return startTime;
  }

  public void setStartTime(Date startTime) {
    this.startTime = startTime;
  }

  public Date getEndTime() {
    return endTime;
  }

  public void setEndTime(Date endTime) {
    this.endTime = endTime;
  }


  public List<Long> getSecurity() {
    return security;
  }

  public void setSecurity(List<Long> security) {
    this.security = security;
  }

  public List<String> getSecurityIndexType() {
    return securityIndexType;
  }

  public void setSecurityIndexType(List<String> securityIndexType) {
    this.securityIndexType = securityIndexType;
  }

  public List<Long> getMarket() {
    return market;
  }

  public void setMarket(List<Long> market) {
    this.market = market;
  }

  public List<String> getMarketIndexType() {
    return marketIndexType;
  }

  public void setMarketIndexType(List<String> marketIndexType) {
    this.marketIndexType = marketIndexType;
  }

  public Long getMarketId() {
    return marketId;
  }

  public void setMarketId(Long marketId) {
    this.marketId = marketId;
  }

  @Override
  protected Map<String, Object> queryParams(Map<String, Object> params) {
    if (params == null) {
      params = new HashMap<String, Object>();
    }
    super.queryParams(params);
    if (StringUtil.isNotEmpty(code)) {
      params.put("code", code);
    }
    if (startTime != null) {
      params.put("startTime", startTime);
    }
    if (endTime != null) {
      params.put("endTime", endTime);
    }
    if (marketId != null) {
      params.put("marketId", marketId);
    }
    return params;
  }

}
