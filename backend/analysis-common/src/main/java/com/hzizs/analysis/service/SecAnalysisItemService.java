package com.hzizs.analysis.service;

import com.hzizs.service.IService;
import com.hzizs.analysis.entity.SecAnalysisItem;

/**
 * 业绩分析条目 服务类
 * 
 * @author crazy_cabbage
 *
 */
public interface SecAnalysisItemService extends IService<SecAnalysisItem> {

}
