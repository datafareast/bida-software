package com.hzizs.analysis.entity;

import com.hzizs.entity.Entity;
import javax.xml.bind.annotation.XmlRootElement;


import java.math.BigDecimal;		
import java.util.Date;		
/**
 * 证券日交易		
 * @author crazy_cabbage
 *
 */
@XmlRootElement		
@SuppressWarnings("serial")
public class DailyTradingActivitySecurity extends Entity  {
   // 主键
	private Long id;
	// 盘后额
	private BigDecimal afterValue;
	// 盘后量
	private BigDecimal afterVolume;
	// 收盘价
	private BigDecimal closePrice;
	// 发生日期
	private Date occDate;
	// 收益率
	private BigDecimal returnPct;
	// 证券
	private Long securityId;
	// 额
	private BigDecimal value;
	// 量
	private BigDecimal volume;
    		 		
    		
 

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
    		
	public BigDecimal getAfterValue() {
		return afterValue;
	}

	public void setAfterValue(BigDecimal afterValue) {
		this.afterValue = afterValue;
	}
    		
	public BigDecimal getAfterVolume() {
		return afterVolume;
	}

	public void setAfterVolume(BigDecimal afterVolume) {
		this.afterVolume = afterVolume;
	}
    		
	public BigDecimal getClosePrice() {
		return closePrice;
	}

	public void setClosePrice(BigDecimal closePrice) {
		this.closePrice = closePrice;
	}
    		
	public Date getOccDate() {
		return occDate;
	}

	public void setOccDate(Date occDate) {
		this.occDate = occDate;
	}
    		
	public BigDecimal getReturnPct() {
		return returnPct;
	}

	public void setReturnPct(BigDecimal returnPct) {
		this.returnPct = returnPct;
	}
    		
	public Long getSecurityId() {
		return securityId;
	}

	public void setSecurityId(Long securityId) {
		this.securityId = securityId;
	}
    		
	public BigDecimal getValue() {
		return value;
	}

	public void setValue(BigDecimal value) {
		this.value = value;
	}
    		
	public BigDecimal getVolume() {
		return volume;
	}

	public void setVolume(BigDecimal volume) {
		this.volume = volume;
	}
	
}
