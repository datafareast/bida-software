package com.hzizs.analysis.entity;

import com.hzizs.entity.Entity;
import javax.xml.bind.annotation.XmlRootElement;


import java.math.BigDecimal;
import java.util.Date;

/**
 * 持仓分析
 * 
 * @author crazy_cabbage
 *
 */
@XmlRootElement
@SuppressWarnings("serial")
public class SecPosition extends Entity {
  // 主键
  private Long id;
  // 类别
  private String cat;
  // 涨跌幅
  private BigDecimal changePercent;
  // 涨跌
  private BigDecimal changeValue;
  // 资产代码
  private String code;
  // 合约
  private String contract;
  // 币种
  private String currency;
  // 到期时间
  private Date endDate;
  // 交易所
  private String exchange;
  // 公允价格
  private BigDecimal fairValue;
  // 是否主力
  private String isMain;
  // 资产名称
  private String name;
  // 持仓市值
  private BigDecimal netValue;
  // 持仓数量
  private Long num;
  // 日期
  private Date occDate;
  // 行权价格
  private BigDecimal strikePrice;
  // 行权实价AT 0 大于0 in 看涨 小于 0 out 看跌
  private BigDecimal strikeRealPrice;
  // 看涨，看跌 CALL OPTION, PUT OPTION
  private String strikeType;



  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getCat() {
    return cat;
  }

  public void setCat(String cat) {
    this.cat = cat;
  }

  public BigDecimal getChangePercent() {
    return changePercent;
  }

  public void setChangePercent(BigDecimal changePercent) {
    this.changePercent = changePercent;
  }

  public BigDecimal getChangeValue() {
    return changeValue;
  }

  public void setChangeValue(BigDecimal changeValue) {
    this.changeValue = changeValue;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getContract() {
    return contract;
  }

  public void setContract(String contract) {
    this.contract = contract;
  }

  public String getCurrency() {
    return currency;
  }

  public void setCurrency(String currency) {
    this.currency = currency;
  }

  public Date getEndDate() {
    return endDate;
  }

  public void setEndDate(Date endDate) {
    this.endDate = endDate;
  }

  public String getExchange() {
    return exchange;
  }

  public void setExchange(String exchange) {
    this.exchange = exchange;
  }

  public BigDecimal getFairValue() {
    return fairValue;
  }

  public void setFairValue(BigDecimal fairValue) {
    this.fairValue = fairValue;
  }

  public String getIsMain() {
    return isMain;
  }

  public void setIsMain(String isMain) {
    this.isMain = isMain;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

 
  public BigDecimal getNetValue() {
    return netValue;
  }

  public void setNetValue(BigDecimal netValue) {
    this.netValue = netValue;
  }

  public Long getNum() {
    return num;
  }

  public void setNum(Long num) {
    this.num = num;
  }

  public Date getOccDate() {
    return occDate;
  }

  public void setOccDate(Date occDate) {
    this.occDate = occDate;
  }

  public BigDecimal getStrikePrice() {
    return strikePrice;
  }

  public void setStrikePrice(BigDecimal strikePrice) {
    this.strikePrice = strikePrice;
  }

  public BigDecimal getStrikeRealPrice() {
    return strikeRealPrice;
  }

  public void setStrikeRealPrice(BigDecimal strikeRealPrice) {
    this.strikeRealPrice = strikeRealPrice;
  }

  public String getStrikeType() {
    return strikeType;
  }

  public void setStrikeType(String strikeType) {
    this.strikeType = strikeType;
  }

}
