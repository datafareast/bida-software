package com.hzizs.analysis.entity;

import com.hzizs.entity.Entity;
import javax.xml.bind.annotation.XmlRootElement;


import java.util.Date;

/**
 * 业绩分析
 * 
 * @author crazy_cabbage
 *
 */
@XmlRootElement
@SuppressWarnings("serial")
public class SecAnalysis extends Entity {
  // 主键
  private Long id;
  // 创建时间
  private Date createTime;
  // 货币
  private String currency;
  // 完成时间
  private Date doneTime;
  // 结束时间
  private Date endTime;
  // 基准业绩
  private String idxCode;
  // 状态
  private String sta;
  // 开始时间
  private Date startTime;
  // 概要
  private String summary;
  // 指标概要
  private String summary2;



  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Date getCreateTime() {
    return createTime;
  }

  public void setCreateTime(Date createTime) {
    this.createTime = createTime;
  }

 
  public String getCurrency() {
    return currency;
  }

  public void setCurrency(String currency) {
    this.currency = currency;
  }

  public Date getDoneTime() {
    return doneTime;
  }

  public void setDoneTime(Date doneTime) {
    this.doneTime = doneTime;
  }

  public Date getEndTime() {
    return endTime;
  }

  public void setEndTime(Date endTime) {
    this.endTime = endTime;
  }

  public String getIdxCode() {
    return idxCode;
  }

  public void setIdxCode(String idxCode) {
    this.idxCode = idxCode;
  }

  public String getSta() {
    return sta;
  }

  public void setSta(String sta) {
    this.sta = sta;
  }

  public Date getStartTime() {
    return startTime;
  }

  public void setStartTime(Date startTime) {
    this.startTime = startTime;
  }

  public String getSummary() {
    return summary;
  }

  public void setSummary(String summary) {
    this.summary = summary;
  }

  public String getSummary2() {
    return summary2;
  }

  public void setSummary2(String summary2) {
    this.summary2 = summary2;
  }

}
