package com.hzizs.analysis.service;

import com.hzizs.service.IService;
import com.hzizs.analysis.entity.SecIndex;
/**
 * 常用指标 服务类 
 * @author crazy_cabbage
 *
 */
public interface SecIndexService extends IService<SecIndex> {

}
