package com.hzizs.analysis.facade;

import com.hzizs.analysis.dto.DataTopoDto;

public interface DataTopoFacade {
  /**
   * 查找对象
   * @param id  主键
   * @return 找到的对象
   */
  DataTopoDto findDto(Long id);

}
