package com.hzizs.analysis.query;
import java.util.HashMap;
import java.util.Map;

import com.hzizs.query.ListQuery;
import com.hzizs.util.StringUtil;

public class SecIndexQuery extends ListQuery {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2434741367573023480L;
	private String exchange;
	private String idx;

 
    public String getExchange() {
    return exchange;
  }


  public void setExchange(String exchange) {
    this.exchange = exchange;
  }


  public String getIdx() {
    return idx;
  }


  public void setIdx(String idx) {
    this.idx = idx;
  }


    @Override
	protected Map<String, Object> queryParams(Map<String, Object> params) {
    	if (params == null) {
			params = new HashMap<String, Object>();
		}
		super.queryParams(params);
		if (StringUtil.isNotEmpty(exchange)) {
			params.put("exchange", exchange);
		}
		if (StringUtil.isNotEmpty(idx)) {
		  params.put("idx", idx);
		}
		return params;
	}

}
