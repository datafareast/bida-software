package com.hzizs.analysis.facade;

import java.util.List;
import java.util.Map;

public interface DataExtractFacade {
  /**
   * 查询数量
   * @param dataSourceId 数据源
   * @param query 查询语句
   * @return 查询的结果
   */
  List<Map<String, Object>> preview(Long dataSourceId, String query);

}
