package com.hzizs.analysis.entity;

import com.hzizs.entity.Entity;
import javax.xml.bind.annotation.XmlRootElement;


import java.util.Date;		
import java.math.BigDecimal;		
/**
 * 市场报价价差		
 * @author crazy_cabbage
 *
 */
@XmlRootElement		
@SuppressWarnings("serial")
public class QuotedSpreadMarket extends Entity  {
   // 主键
	private Long id;
	// 市场
	private Long marketId;
	// 发生日期
	private Date occDate;
	// 绝对报价价差
	private BigDecimal quotedSpreadAbs;
	// 基点报价价差
	private BigDecimal quotedSpreadBps;
    		 		
    		
 

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
    		
	public Long getMarketId() {
		return marketId;
	}

	public void setMarketId(Long marketId) {
		this.marketId = marketId;
	}
    		
	public Date getOccDate() {
		return occDate;
	}

	public void setOccDate(Date occDate) {
		this.occDate = occDate;
	}
    		
	public BigDecimal getQuotedSpreadAbs() {
		return quotedSpreadAbs;
	}

	public void setQuotedSpreadAbs(BigDecimal quotedSpreadAbs) {
		this.quotedSpreadAbs = quotedSpreadAbs;
	}
    		
	public BigDecimal getQuotedSpreadBps() {
		return quotedSpreadBps;
	}

	public void setQuotedSpreadBps(BigDecimal quotedSpreadBps) {
		this.quotedSpreadBps = quotedSpreadBps;
	}
	
}
