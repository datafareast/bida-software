package com.hzizs.analysis.facade;

public interface MenuFacade {
  /**
   * 根据主键 删除
   * 
   * @param id 主键
   */
  void deleteById(Long id);

  /**
   * 批量删除 根据主键批量删除
   * 
   * @param ids 主键集合
   */
  void deleteByIds(Long[] ids);
}
