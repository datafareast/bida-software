package com.hzizs.analysis.service;

import com.hzizs.service.IService;
import java.util.Date;
import java.util.List;
import com.hzizs.analysis.entity.QuotedSpreadMarket;
/**
 * 市场报价价差 服务类 
 * @author crazy_cabbage
 *
 */
public interface QuotedSpreadMarketService extends IService<QuotedSpreadMarket> {
  /**
   * 查询报价价差
   * @param marketId 市场主键
   * @param startTime 开始时间
   * @param endTime 结束时间
   * @return 报价价差
   */
  List<QuotedSpreadMarket> find(Long marketId, Date startTime, Date endTime);

}
