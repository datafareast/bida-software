package com.hzizs.analysis.entity;

import com.hzizs.entity.Entity;
import javax.xml.bind.annotation.XmlRootElement;


import java.util.Date;

/**
 * 系统用户日志
 * 
 * @author crazy_cabbage
 *
 */
@XmlRootElement
@SuppressWarnings("serial")
public class SysUserLog extends Entity {
  // 主键
  private Long id;
  // 错误次数
  private Integer errTimes;
  // 首错时间
  private Date firstErrTime;
  // 上次登录IP
  private String lastIp;
  // 上次登录时间
  private Date lastTime;
  // 锁定时间
  private Date lockTime;
  // 登录IP
  private String loginIp;
  // 登录时间
  private Date loginTime;
  // 注册IP
  private String regIp;
  // 注册时间
  private Date regTime;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Integer getErrTimes() {
    return errTimes;
  }

  public void setErrTimes(Integer errTimes) {
    this.errTimes = errTimes;
  }

  public Date getFirstErrTime() {
    return firstErrTime;
  }

  public void setFirstErrTime(Date firstErrTime) {
    this.firstErrTime = firstErrTime;
  }

  public String getLastIp() {
    return lastIp;
  }

  public void setLastIp(String lastIp) {
    this.lastIp = lastIp;
  }

  public Date getLastTime() {
    return lastTime;
  }

  public void setLastTime(Date lastTime) {
    this.lastTime = lastTime;
  }

  public Date getLockTime() {
    return lockTime;
  }

  public void setLockTime(Date lockTime) {
    this.lockTime = lockTime;
  }

  public String getLoginIp() {
    return loginIp;
  }

  public void setLoginIp(String loginIp) {
    this.loginIp = loginIp;
  }

  public Date getLoginTime() {
    return loginTime;
  }

  public void setLoginTime(Date loginTime) {
    this.loginTime = loginTime;
  }

  public String getRegIp() {
    return regIp;
  }

  public void setRegIp(String regIp) {
    this.regIp = regIp;
  }

  public Date getRegTime() {
    return regTime;
  }

  public void setRegTime(Date regTime) {
    this.regTime = regTime;
  }

}
