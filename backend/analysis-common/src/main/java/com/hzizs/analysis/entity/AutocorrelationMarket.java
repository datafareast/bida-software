package com.hzizs.analysis.entity;

import com.hzizs.entity.Entity;
import javax.xml.bind.annotation.XmlRootElement;


import java.math.BigDecimal;		
import java.util.Date;		
/**
 * 市场自相关		
 * @author crazy_cabbage
 *
 */
@XmlRootElement		
@SuppressWarnings("serial")
public class AutocorrelationMarket extends Entity  {
   // 主键
	private Long id;
	// 自相关10秒
	private BigDecimal autocorrelation10sec;
	// 自相关60秒
	private BigDecimal autocorrelation60sec;
	// 交易市场
	private Long marketId;
	// 发生日期
	private Date occDate;
    		 		
    		
 

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
    		
	public BigDecimal getAutocorrelation10sec() {
		return autocorrelation10sec;
	}

	public void setAutocorrelation10sec(BigDecimal autocorrelation10sec) {
		this.autocorrelation10sec = autocorrelation10sec;
	}
    		
	public BigDecimal getAutocorrelation60sec() {
		return autocorrelation60sec;
	}

	public void setAutocorrelation60sec(BigDecimal autocorrelation60sec) {
		this.autocorrelation60sec = autocorrelation60sec;
	}
    		
	public Long getMarketId() {
		return marketId;
	}

	public void setMarketId(Long marketId) {
		this.marketId = marketId;
	}
    		
	public Date getOccDate() {
		return occDate;
	}

	public void setOccDate(Date occDate) {
		this.occDate = occDate;
	}
	
}
