package com.hzizs.analysis.service;

import com.hzizs.service.IService;
import com.hzizs.analysis.entity.ResearchReport;

/**
 * 研究报告 服务类
 * 
 * @author crazy_cabbage
 *
 */
public interface ResearchReportService extends IService<ResearchReport> {
  /**
   * 根据名称查找 研究报告
   * 
   * @param name 名称
   * @return 找到的研究报告
   */
  ResearchReport findByName(String name);

}
