package com.hzizs.analysis.service;

import com.hzizs.service.IService;
import java.util.List;
import com.hzizs.analysis.entity.RoleMenu;

/**
 * 角色菜单 服务类
 * 
 * @author crazy_cabbage
 *
 */
public interface RoleMenuService extends IService<RoleMenu> {
  /**
   * 根据角色主键 查找角色菜单
   * 
   * @param roleId 角色主键
   * @return 角色菜单
   */
  List<RoleMenu> findByRoleId(Long roleId);

  /**
   * 批量删除角色菜单
   * 
   * @param roleId  角色主键
   * @param menuIds 菜单主键
   */
  void delete(Long roleId, List<Long> menuIds);

  /**
   * 根据角色主键，删除角色菜单
   * 
   * @param roleId 角色主键
   */
  void deleteByRoleId(Long roleId);

  /**
   * 根据菜单主键 统计 数量
   * 
   * @param menuId 菜单主键
   * @return 统计出的数量
   */
  long countByMenuId(Long menuId);

}
