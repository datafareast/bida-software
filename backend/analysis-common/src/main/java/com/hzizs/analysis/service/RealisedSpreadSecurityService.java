package com.hzizs.analysis.service;

import com.hzizs.service.IService;
import java.util.Date;
import java.util.List;
import com.hzizs.analysis.entity.RealisedSpreadSecurity;
/**
 * 证券实现价差 服务类 
 * @author crazy_cabbage
 *
 */
public interface RealisedSpreadSecurityService extends IService<RealisedSpreadSecurity> {
  /**
   * 查找 实现价差
   * @param securityId 证券主键
   * @param startTime 开始时间
   * @param endTime 结束时间
   * @return 实现价差
   */
  List<RealisedSpreadSecurity> find(Long securityId, Date startTime, Date endTime);

}
