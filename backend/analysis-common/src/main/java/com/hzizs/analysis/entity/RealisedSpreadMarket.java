package com.hzizs.analysis.entity;

import com.hzizs.entity.Entity;
import javax.xml.bind.annotation.XmlRootElement;


import java.util.Date;		
import java.math.BigDecimal;		
/**
 * 市场实现价差		
 * @author crazy_cabbage
 *
 */
@XmlRootElement		
@SuppressWarnings("serial")
public class RealisedSpreadMarket extends Entity  {
   // 主键
	private Long id;
	// 市场
	private Long marketId;
	// 日期
	private Date occDate;
	// 基点10分
	private BigDecimal realisedSpread10minAbs;
	// 绝对10分
	private BigDecimal realisedSpread10minBps;
	// 基点10秒
	private BigDecimal realisedSpread10secAbs;
	// 绝对10秒
	private BigDecimal realisedSpread10secBps;
	// 基点1分
	private BigDecimal realisedSpread1minAbs;
	// 绝对1分
	private BigDecimal realisedSpread1minBps;
	// 基点1秒
	private BigDecimal realisedSpread1secAbs;
	// 绝对1秒
	private BigDecimal realisedSpread1secBps;
	// 基点30秒
	private BigDecimal realisedSpread30secAbs;
	// 绝对30秒
	private BigDecimal realisedSpread30secBps;
	// 基点5分
	private BigDecimal realisedSpread5minAbs;
	// 绝对5分
	private BigDecimal realisedSpread5minBps;
    		 		
    		
 

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
    		
	public Long getMarketId() {
		return marketId;
	}

	public void setMarketId(Long marketId) {
		this.marketId = marketId;
	}
    		
	public Date getOccDate() {
		return occDate;
	}

	public void setOccDate(Date occDate) {
		this.occDate = occDate;
	}
    		
	public BigDecimal getRealisedSpread10minAbs() {
		return realisedSpread10minAbs;
	}

	public void setRealisedSpread10minAbs(BigDecimal realisedSpread10minAbs) {
		this.realisedSpread10minAbs = realisedSpread10minAbs;
	}
    		
	public BigDecimal getRealisedSpread10minBps() {
		return realisedSpread10minBps;
	}

	public void setRealisedSpread10minBps(BigDecimal realisedSpread10minBps) {
		this.realisedSpread10minBps = realisedSpread10minBps;
	}
    		
	public BigDecimal getRealisedSpread10secAbs() {
		return realisedSpread10secAbs;
	}

	public void setRealisedSpread10secAbs(BigDecimal realisedSpread10secAbs) {
		this.realisedSpread10secAbs = realisedSpread10secAbs;
	}
    		
	public BigDecimal getRealisedSpread10secBps() {
		return realisedSpread10secBps;
	}

	public void setRealisedSpread10secBps(BigDecimal realisedSpread10secBps) {
		this.realisedSpread10secBps = realisedSpread10secBps;
	}
    		
	public BigDecimal getRealisedSpread1minAbs() {
		return realisedSpread1minAbs;
	}

	public void setRealisedSpread1minAbs(BigDecimal realisedSpread1minAbs) {
		this.realisedSpread1minAbs = realisedSpread1minAbs;
	}
    		
	public BigDecimal getRealisedSpread1minBps() {
		return realisedSpread1minBps;
	}

	public void setRealisedSpread1minBps(BigDecimal realisedSpread1minBps) {
		this.realisedSpread1minBps = realisedSpread1minBps;
	}
    		
	public BigDecimal getRealisedSpread1secAbs() {
		return realisedSpread1secAbs;
	}

	public void setRealisedSpread1secAbs(BigDecimal realisedSpread1secAbs) {
		this.realisedSpread1secAbs = realisedSpread1secAbs;
	}
    		
	public BigDecimal getRealisedSpread1secBps() {
		return realisedSpread1secBps;
	}

	public void setRealisedSpread1secBps(BigDecimal realisedSpread1secBps) {
		this.realisedSpread1secBps = realisedSpread1secBps;
	}
    		
	public BigDecimal getRealisedSpread30secAbs() {
		return realisedSpread30secAbs;
	}

	public void setRealisedSpread30secAbs(BigDecimal realisedSpread30secAbs) {
		this.realisedSpread30secAbs = realisedSpread30secAbs;
	}
    		
	public BigDecimal getRealisedSpread30secBps() {
		return realisedSpread30secBps;
	}

	public void setRealisedSpread30secBps(BigDecimal realisedSpread30secBps) {
		this.realisedSpread30secBps = realisedSpread30secBps;
	}
    		
	public BigDecimal getRealisedSpread5minAbs() {
		return realisedSpread5minAbs;
	}

	public void setRealisedSpread5minAbs(BigDecimal realisedSpread5minAbs) {
		this.realisedSpread5minAbs = realisedSpread5minAbs;
	}
    		
	public BigDecimal getRealisedSpread5minBps() {
		return realisedSpread5minBps;
	}

	public void setRealisedSpread5minBps(BigDecimal realisedSpread5minBps) {
		this.realisedSpread5minBps = realisedSpread5minBps;
	}
	
}
