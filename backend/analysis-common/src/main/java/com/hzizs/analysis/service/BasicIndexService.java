package com.hzizs.analysis.service;

import com.hzizs.service.IService;
import java.util.List;
import com.hzizs.analysis.entity.BasicIndex;
/**
 * 指数基指 服务类 
 * @author crazy_cabbage
 *
 */
public interface BasicIndexService extends IService<BasicIndex> {
  /**
   * 查找所有启用的
   * @return 找到所有启用的指标
   */
  List<BasicIndex> findEnabledAll();

}
