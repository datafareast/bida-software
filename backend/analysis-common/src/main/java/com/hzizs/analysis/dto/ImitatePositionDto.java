package com.hzizs.analysis.dto;

import java.util.List;
import com.hzizs.analysis.entity.ImitatePosition;
import com.hzizs.analysis.entity.ImitatePositionItem;
import com.hzizs.dto.Dto;

public class ImitatePositionDto extends Dto {
   /**
   * 
   */
  private static final long serialVersionUID = -7804097949659514076L;
  private ImitatePosition imitatePosition;
   private List<ImitatePositionItem> imitatePositionItems;
  public ImitatePosition getImitatePosition() {
    return imitatePosition;
  }
  public void setImitatePosition(ImitatePosition imitatePosition) {
    this.imitatePosition = imitatePosition;
  }
  public List<ImitatePositionItem> getImitatePositionItems() {
    return imitatePositionItems;
  }
  public void setImitatePositionItems(List<ImitatePositionItem> imitatePositionItems) {
    this.imitatePositionItems = imitatePositionItems;
  }
   
}
