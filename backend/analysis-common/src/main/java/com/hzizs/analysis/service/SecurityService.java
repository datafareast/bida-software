package com.hzizs.analysis.service;

import com.hzizs.service.IService;
import com.hzizs.analysis.entity.Security;
/**
 * 证券 服务类 
 * @author crazy_cabbage
 *
 */
public interface SecurityService extends IService<Security> {

}
