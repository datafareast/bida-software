package com.hzizs.analysis.service;

import com.hzizs.service.IService;
import com.hzizs.analysis.entity.Icon;

/**
 * 图标库 服务类
 * 
 * @author crazy_cabbage
 *
 */
public interface IconService extends IService<Icon> {

}
