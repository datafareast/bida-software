package com.hzizs.analysis.dto;

import java.util.ArrayList;
import java.util.List;
import com.hzizs.analysis.entity.SysRole;
import com.hzizs.dto.Dto;

public class SysRoleDto extends Dto {

  /**
   * 
   */
  private static final long serialVersionUID = -215116769726206084L;

  private SysRole role;
  private List<Long> menuIds;
  private List<Long> halfMenuIds;
  public SysRole getRole() {
    return role;
  }
  public void setRole(SysRole role) {
    this.role = role;
  }
  public List<Long> getMenuIds() {
    if(menuIds==null) {
      menuIds = new ArrayList<Long>();
    }
    return menuIds;
  }
  public void setMenuIds(List<Long> menuIds) {
    this.menuIds = menuIds;
  }
  public List<Long> getHalfMenuIds() {
    if(halfMenuIds==null) {
      halfMenuIds= new ArrayList<Long>();
    }
    return halfMenuIds;
  }
  public void setHalfMenuIds(List<Long> halfMenuIds) {
    this.halfMenuIds = halfMenuIds;
  }
  
}
