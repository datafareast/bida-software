package com.hzizs.analysis.entity;

import com.hzizs.entity.Entity;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;


import java.util.Date;		
import java.math.BigDecimal;		
/**
 * 数据拓普值		
 * @author crazy_cabbage
 *
 */
@XmlRootElement		
@SuppressWarnings("serial")
public class DataTopoVal extends Entity  {
   // 主键
	private Long id;
	// 编码
	private String code;
	// 类型
	@NotNull
	private Long dataTypeId;
	// 指标
	@NotNull
	private Long dataTopoId;
	// 指标父级
	@NotNull
	private Long dataTopoParentId;
	// 时间
	@NotNull
	private Date occTime;
	// 值
	@NotNull
	private BigDecimal val;
    		 		
    		
 

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
    		
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
 

  public Long getDataTypeId() {
    return dataTypeId;
  }

  public void setDataTypeId(Long dataTypeId) {
    this.dataTypeId = dataTypeId;
  }

  public Long getDataTopoId() {
		return dataTopoId;
	}

	public void setDataTopoId(Long dataTopoId) {
		this.dataTopoId = dataTopoId;
	}
    		
	public Long getDataTopoParentId() {
		return dataTopoParentId;
	}

	public void setDataTopoParentId(Long dataTopoParentId) {
		this.dataTopoParentId = dataTopoParentId;
	}
    		
	public Date getOccTime() {
		return occTime;
	}

	public void setOccTime(Date occTime) {
		this.occTime = occTime;
	}
    		
	public BigDecimal getVal() {
		return val;
	}

	public void setVal(BigDecimal val) {
		this.val = val;
	}
	
}
