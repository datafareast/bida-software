package com.hzizs.analysis.entity;

import com.hzizs.entity.Entity;
import com.hzizs.entity.IVersion;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * 表主键
 * 
 * @author crazy_cabbage
 *
 */
@XmlRootElement
@SuppressWarnings("serial")
public class TableId extends Entity implements IVersion<Long> {
  // 主键
  private Long id;
  // 编码
  private String code;
  // 表名
  private String name;
  // 步长
  private Long step;
  // 值
  private Long val;
  private Long version;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Long getStep() {
    return step;
  }

  public void setStep(Long step) {
    this.step = step;
  }

  public Long getVal() {
    return val;
  }

  public void setVal(Long val) {
    this.val = val;
  }

  @Override
  public Long getVersion() {
    return version;
  }

  @Override
  public void setVersion(Long version) {
    this.version = version;
  }

  @Override
  public void reset() {
    version = 0L;
  }

  @Override
  public void increase() {
    version++;
  }

}
