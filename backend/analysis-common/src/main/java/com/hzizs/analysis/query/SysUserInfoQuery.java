package com.hzizs.analysis.query;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import com.hzizs.constants.SymbolConstants;
import com.hzizs.query.ListQuery;
import com.hzizs.util.StringUtil;

public class SysUserInfoQuery extends ListQuery {
  
  /**
   * 
   */
  private static final long serialVersionUID = 1951914660225366703L;
  private String area;
  private String email;
  private String enabled;
  private String idNo;
  private String isDel;
  private String isManager;
  private String mobile;
  private String msn;
  private String nickname;
  private String qq;
  private String realName;
  private String username;
  private String wechat;
  private Date startTime;
  private Date endTime;
  private Date lockStartTime;
  private Date lockEndTime;
  private Date birthdayStartTime;
  private Date birthdayEndTime;

  public String getArea() {
    return area;
  }

  public void setArea(String area) {
    this.area = area;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getEnabled() {
    return enabled;
  }

  public void setEnabled(String enabled) {
    this.enabled = enabled;
  }

  public String getIdNo() {
    return idNo;
  }

  public void setIdNo(String idNo) {
    this.idNo = idNo;
  }

  public String getIsDel() {
    return isDel;
  }

  public void setIsDel(String isDel) {
    this.isDel = isDel;
  }

  public String getIsManager() {
    return isManager;
  }

  public void setIsManager(String isManager) {
    this.isManager = isManager;
  }

  public String getMobile() {
    return mobile;
  }

  public void setMobile(String mobile) {
    this.mobile = mobile;
  }

  public String getMsn() {
    return msn;
  }

  public void setMsn(String msn) {
    this.msn = msn;
  }

  public String getNickname() {
    return nickname;
  }

  public void setNickname(String nickname) {
    this.nickname = nickname;
  }

  public String getQq() {
    return qq;
  }

  public void setQq(String qq) {
    this.qq = qq;
  }

  public String getRealName() {
    return realName;
  }

  public void setRealName(String realName) {
    this.realName = realName;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getWechat() {
    return wechat;
  }

  public void setWechat(String wechat) {
    this.wechat = wechat;
  }

  public Date getStartTime() {
    return startTime;
  }

  public void setStartTime(Date startTime) {
    this.startTime = startTime;
  }

  public Date getEndTime() {
    return endTime;
  }

  public void setEndTime(Date endTime) {
    this.endTime = endTime;
  }

  public Date getLockStartTime() {
    return lockStartTime;
  }

  public void setLockStartTime(Date lockStartTime) {
    this.lockStartTime = lockStartTime;
  }

  public Date getLockEndTime() {
    return lockEndTime;
  }

  public void setLockEndTime(Date lockEndTime) {
    this.lockEndTime = lockEndTime;
  }

  public Date getBirthdayStartTime() {
    return birthdayStartTime;
  }

  public void setBirthdayStartTime(Date birthdayStartTime) {
    this.birthdayStartTime = birthdayStartTime;
  }

  public Date getBirthdayEndTime() {
    return birthdayEndTime;
  }

  public void setBirthdayEndTime(Date birthdayEndTime) {
    this.birthdayEndTime = birthdayEndTime;
  }

  @Override
  protected Map<String, Object> queryParams(Map<String, Object> params) {
    if (params == null) {
      params = new HashMap<String, Object>();
    }
    super.queryParams(params);

    if (StringUtil.isNotEmpty(area)) {
      params.put("likeArea", area + SymbolConstants.PERCENT);
    }
    if (StringUtil.isNotEmpty(email)) {
      params.put("email", email);
    }
    if (StringUtil.isNotEmpty(enabled)) {
      params.put("enabled", enabled);
    }
    if (StringUtil.isNotEmpty(idNo)) {
      params.put("idNo", idNo);
    }
    if (StringUtil.isNotEmpty(isDel)) {
      params.put("isDel", isDel);
    }
    if (StringUtil.isNotEmpty(isManager)) {
      params.put("isManager", isManager);
    }
    if (StringUtil.isNotEmpty(mobile)) {
      params.put("mobile", mobile);
    }
    if (StringUtil.isNotEmpty(msn)) {
      params.put("msn", msn);
    }
    if (StringUtil.isNotEmpty(nickname)) {
      params.put("nickname", nickname);
    }
    if (StringUtil.isNotEmpty(qq)) {
      params.put("qq", qq);
    }
    if (StringUtil.isNotEmpty(realName)) {
      params.put("realName", realName);
    }
    if (StringUtil.isNotEmpty(username)) {
      params.put("username", username);
    }
    if (StringUtil.isNotEmpty(wechat)) {
      params.put("wechat", wechat);
    }
    if (startTime != null) {
      params.put("startTime", startTime);
    }
    if (endTime != null) {
      params.put("endTime", endTime);
    }
    if (lockStartTime != null) {
      params.put("lockStartTime", lockStartTime);
    }
    if (lockEndTime != null) {
      params.put("lockEndTime", lockEndTime);
    }
    if (birthdayStartTime != null) {
      params.put("birthdayStartTime", birthdayStartTime);
    }
    if (birthdayEndTime != null) {
      params.put("birthdayEndTime", birthdayEndTime);
    }
    return params;
  }

}
