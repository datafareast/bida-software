package com.hzizs.analysis.facade;

import java.util.List;
import java.util.Map;
import com.hzizs.analysis.dto.UserInfoDto;
import com.hzizs.analysis.entity.Menu;
import com.hzizs.analysis.entity.UserInfo;
import com.hzizs.dto.LoginLogDto;
import com.hzizs.dto.UserDto;

public interface UserFacade {
  /**
   * 保存对象
   * 
   * @param dto 用户对象
   */
  void save(UserInfoDto dto);

  /**
   * 查找用户对象
   * 
   * @param id 用户主键
   * @return 找到用户对象
   */
  UserInfoDto findById(Long id);

  /**
   * 更新用户对象
   * 
   * @param dto 用户对象
   */
  void update(UserInfoDto dto);

  /**
   * 登录查询对象
   * 
   * @param username 用户名
   * @return 找到登录查询对象
   */
  UserDto findByUsername(String username);

  /**
   * 保存登录成功日志
   * 
   * @param dto 登录成功日志
   */
  void saveLoginSuccess(LoginLogDto dto);

  /**
   * 保存登录失败日志
   * 
   * @param dto 登录失败日志
   */
  void saveLoginFailure(LoginLogDto dto);

  /**
   * 查找用户信息
   * 
   * @param userId 用户主键
   * @param role   角色编码
   * @return 找到用户信息
   */
  Map<String, Object> findInfo(Long userId, String role);
  /**
   * 修改密码
   * @param userId  用户主键
   * @param oldPassword 旧密码
   * @param password 新密码
   */
  void updatePassword(Long userId, String oldPassword,String password);

   /**
    * 逻辑删除用户
    * @param id 用户主键
    */
  void delete(Long id);
  /**
   * 重置密码
   * @param id 用户主键
   * @param password 密码
   */
  void updateResetPassword(Long id ,String password);
  /**
   * 更改用户信息
   * @param userInfo 用户信息
   */
  void updateUserInfo(UserInfo userInfo);
  /**
   * 根据角色编码，查找菜单
   * @param roleCode 角色编码
   * @return 找到的菜单
   */
  List<Menu> findMenu(String roleCode);
   /**
    * 根据 角色编码 查找按钮
    * @param roleCode 角色编码 
    * @return 找到的按钮权限编码集合
    */
  List<String> findBtn(String roleCode);
  /**
   *  批量删除用户
   * @param ids 主键集合 
   */
  void deleteByIds(Long [] ids);
   /**
    * 删除用户
    * @param id 主键
    */
  void deleteById(Long id);
}
