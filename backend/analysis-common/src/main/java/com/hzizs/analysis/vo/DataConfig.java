package com.hzizs.analysis.vo;

import java.util.Date;
import java.util.List;

public class DataConfig {
  /**
   * 开始时间
   */
  private Date startTime;
  /**
   * 结束时间
   */
  private Date endTime;
  /**
   * 市场
   */
  private List<Long> markets;
  /**
   * 市场指标
   */
  private List<String> marketIdxs;
  /**
   *  市场
   */
  private Long market;
  /**
   * 市场指标
   */
  private String marketIdx;
  /**
   * 编码
   */
  private String code;

  public Date getStartTime() {
    return startTime;
  }

  public void setStartTime(Date startTime) {
    this.startTime = startTime;
  }

  public Date getEndTime() {
    return endTime;
  }

  public void setEndTime(Date endTime) {
    this.endTime = endTime;
  }



  public String getMarketIdx() {
    return marketIdx;
  }

  public void setMarketIdx(String marketIdx) {
    this.marketIdx = marketIdx;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public List<Long> getMarkets() {
    return markets;
  }

  public void setMarkets(List<Long> markets) {
    this.markets = markets;
  }

  public List<String> getMarketIdxs() {
    return marketIdxs;
  }

  public void setMarketIdxs(List<String> marketIdxs) {
    this.marketIdxs = marketIdxs;
  }

  public void setMarket(Long market) {
    this.market = market;
  }

  public Long getMarket() {
    return market;
  }



}
