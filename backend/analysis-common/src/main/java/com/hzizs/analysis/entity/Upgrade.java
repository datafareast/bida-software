package com.hzizs.analysis.entity;

import com.hzizs.entity.Entity;
import javax.xml.bind.annotation.XmlRootElement;


import java.util.Date;

/**
 * 升级信息
 * 
 * @author crazy_cabbage
 *
 */
@XmlRootElement
@SuppressWarnings("serial")
public class Upgrade extends Entity {
  // 主键
  private Long id;
  // 创建时间
  private Date createTime;
  // 平台
  private String platform;
  // 备注
  private String remark;
  // 升级时间
  private Date upgradeTime;
  // 版本
  private String ver;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Date getCreateTime() {
    return createTime;
  }

  public void setCreateTime(Date createTime) {
    this.createTime = createTime;
  }

  public String getPlatform() {
    return platform;
  }

  public void setPlatform(String platform) {
    this.platform = platform;
  }

  public String getRemark() {
    return remark;
  }

  public void setRemark(String remark) {
    this.remark = remark;
  }

  public Date getUpgradeTime() {
    return upgradeTime;
  }

  public void setUpgradeTime(Date upgradeTime) {
    this.upgradeTime = upgradeTime;
  }

  public String getVer() {
    return ver;
  }

  public void setVer(String ver) {
    this.ver = ver;
  }

}
