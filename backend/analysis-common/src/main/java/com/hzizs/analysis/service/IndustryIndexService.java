package com.hzizs.analysis.service;

import com.hzizs.service.IService;
import com.hzizs.analysis.entity.IndustryIndex;
/**
 * 行业指标 服务类 
 * @author crazy_cabbage
 *
 */
public interface IndustryIndexService extends IService<IndustryIndex> {

}
