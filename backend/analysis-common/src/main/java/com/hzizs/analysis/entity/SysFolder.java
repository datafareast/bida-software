package com.hzizs.analysis.entity;

import com.hzizs.entity.Entity;
import javax.xml.bind.annotation.XmlRootElement;


import java.util.Date;

/**
 * 系统文件夹
 * 
 * @author crazy_cabbage
 *
 */
@XmlRootElement
@SuppressWarnings("serial")
public class SysFolder extends Entity {
  // 主键
  private Long id;
  // 类别0全部 1默认 2自定义
  private String cat;
  // 创建时间
  private Date createTime;
  // 名称
  private String name;
  // 排序
  private Integer sortOrder;
  // 商家
  private Long sysUserId;
  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getCat() {
    return cat;
  }

  public void setCat(String cat) {
    this.cat = cat;
  }

  public Date getCreateTime() {
    return createTime;
  }

  public void setCreateTime(Date createTime) {
    this.createTime = createTime;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Integer getSortOrder() {
    return sortOrder;
  }

  public void setSortOrder(Integer sortOrder) {
    this.sortOrder = sortOrder;
  }

  public Long getSysUserId() {
    return sysUserId;
  }

  public void setSysUserId(Long sysUserId) {
    this.sysUserId = sysUserId;
  }

}
