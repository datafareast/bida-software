package com.hzizs.analysis.dto;

import java.util.ArrayList;
import java.util.List;
import com.hzizs.analysis.entity.Role;
import com.hzizs.dto.Dto;

public class RoleDto extends Dto {

  /**
   * 
   */
  private static final long serialVersionUID = 743773998695871125L;


  private Role role;
  private List<Long> menuIds;
  private List<Long> halfMenuIds;

  public Role getRole() {
    return role;
  }

  public void setRole(Role role) {
    this.role = role;
  }

  public List<Long> getMenuIds() {
    if(menuIds == null) {
      menuIds = new ArrayList<Long>();
    }
    return menuIds;
  }

  public void setMenuIds(List<Long> menuIds) {
    this.menuIds = menuIds;
  }

  public List<Long> getHalfMenuIds() {
    if(halfMenuIds == null) {
      halfMenuIds = new ArrayList<Long>();
    }
    return halfMenuIds;
  }

  public void setHalfMenuIds(List<Long> halfMenuIds) {
    this.halfMenuIds = halfMenuIds;
  }

}
