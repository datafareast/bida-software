package com.hzizs.analysis.dto;

import java.util.List;
import com.hzizs.analysis.entity.Area;
import com.hzizs.dto.Dto;

public class AreaDto  extends Dto{

  /**
   * 
   */
  private static final long serialVersionUID = 5508746972209903842L;
  
  private List<Long> parentIds;
  private Area area;
  public List<Long> getParentIds() {
    return parentIds;
  }
  public void setParentIds(List<Long> parentIds) {
    this.parentIds = parentIds;
  }
  public Area getArea() {
    return area;
  }
  public void setArea(Area area) {
    this.area = area;
  }
  
  

}
