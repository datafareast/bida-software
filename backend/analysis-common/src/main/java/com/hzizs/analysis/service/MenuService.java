package com.hzizs.analysis.service;

import java.util.List;
import com.hzizs.analysis.entity.Menu;
import com.hzizs.service.IService;

/**
 * 菜单 服务类
 * 
 * @author crazy_cabbage
 *
 */
public interface MenuService extends IService<Menu> {
  /**
   * 查找菜单
   * 
   * @return 找到的菜单
   */
  List<Menu> findMenu();

  /**
   * 查找功能按钮
   * 
   * @return 找到的功能按钮
   */
  List<Menu> findBtn();

  /**
   * 根据类别查找菜单
   * 
   * @param cats 类别
   * @return 找到菜单
   */
  List<Menu> findByCats(List<String> cats);

  /**
   * 根据父主键统计 子数量
   * 
   * @param parentId 父主键
   * @return 子数量
   */
  long countByParentId(Long parentId);
}
