package com.hzizs.analysis.entity;

import com.hzizs.entity.Entity;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * 类别
 * 
 * @author crazy_cabbage
 *
 */
@XmlRootElement
@SuppressWarnings("serial")
public class SecCat extends Entity {
  // 主键
  private Long id;
  // 背景色
  private String bgcolor;
  // 交联名称
  private String name;
  // 关联编码
  private String code;
  // 交易所
  private String exchange;
  // 前景色
  private String forecolor;
  // 排序
  private Integer sortOrder;



  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getBgcolor() {
    return bgcolor;
  }

  public void setBgcolor(String bgcolor) {
    this.bgcolor = bgcolor;
  }



  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getExchange() {
    return exchange;
  }

  public void setExchange(String exchange) {
    this.exchange = exchange;
  }

  public String getForecolor() {
    return forecolor;
  }

  public void setForecolor(String forecolor) {
    this.forecolor = forecolor;
  }

  public Integer getSortOrder() {
    return sortOrder;
  }

  public void setSortOrder(Integer sortOrder) {
    this.sortOrder = sortOrder;
  }

}
