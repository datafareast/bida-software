package com.hzizs.analysis.facade;

import com.hzizs.analysis.dto.AreaDto;
import com.hzizs.analysis.entity.Area;

public interface AreaFacade {
   /**
    * 根据主键 查找 地区
    * @param id 主键
    * @return 地区
    */
  AreaDto findById(Long id);
  /**
   * 保存地区
   * @param area 地区
   * @return 地区
   */
  Area save(Area area);
   /**
    * 更新地区
    * @param area 地区
    * @return 地区
    */
  Area update(Area area);
  /**
   * 删除 地区
   * @param ids 主键集合
   */
  void deleteByIds(Long[] ids );
  /**
   * 删除地区
   * @param id 主键
   */
  void deleteById(Long id);
}
