package com.hzizs.analysis.query;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.hzizs.query.ListQuery;
import com.hzizs.util.CollectionUtil;
import com.hzizs.util.StringUtil;

public class RoleQuery extends ListQuery {

  /**
   * 
   */
  private static final long serialVersionUID = 2434741367573023480L;
  private String name;
  private List<Long> roleIds;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public List<Long> getRoleIds() {
    return roleIds;
  }

  public void setRoleIds(List<Long> roleIds) {
    this.roleIds = roleIds;
  }

  @Override
  protected Map<String, Object> queryParams(Map<String, Object> params) {
    if (params == null) {
      params = new HashMap<String, Object>();
    }
    super.queryParams(params);
    if (StringUtil.isNotEmpty(name)) {
      params.put("name", name);
    }
    if(CollectionUtil.isNotEmpty(roleIds)) {
      params.put("roleIds", roleIds);
    }
    return params;
  }

}
