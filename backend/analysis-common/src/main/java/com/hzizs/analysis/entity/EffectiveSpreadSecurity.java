package com.hzizs.analysis.entity;

import com.hzizs.entity.Entity;
import javax.xml.bind.annotation.XmlRootElement;


import java.math.BigDecimal;		
import java.util.Date;		
/**
 * 证券有效价差		
 * @author crazy_cabbage
 *
 */
@XmlRootElement		
@SuppressWarnings("serial")
public class EffectiveSpreadSecurity extends Entity  {
   // 主键
	private Long id;
	// 绝对有效价差
	private BigDecimal effectiveSpreadAbs;
	// 基点有效价差
	private BigDecimal effectiveSpreadBps;
	// 发生日期
	private Date occDate;
	// 证券
	private Long securityId;
    		 		
    		
 

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
    		
	public BigDecimal getEffectiveSpreadAbs() {
		return effectiveSpreadAbs;
	}

	public void setEffectiveSpreadAbs(BigDecimal effectiveSpreadAbs) {
		this.effectiveSpreadAbs = effectiveSpreadAbs;
	}
    		
	public BigDecimal getEffectiveSpreadBps() {
		return effectiveSpreadBps;
	}

	public void setEffectiveSpreadBps(BigDecimal effectiveSpreadBps) {
		this.effectiveSpreadBps = effectiveSpreadBps;
	}
    		
	public Date getOccDate() {
		return occDate;
	}

	public void setOccDate(Date occDate) {
		this.occDate = occDate;
	}
    		
	public Long getSecurityId() {
		return securityId;
	}

	public void setSecurityId(Long securityId) {
		this.securityId = securityId;
	}
	
}
