package com.hzizs.analysis.facade;

import java.util.List;
import java.util.Map;
import com.hzizs.analysis.dto.SysUserInfoDto;
import com.hzizs.analysis.entity.SysMenu;
import com.hzizs.dto.LoginLogDto;
import com.hzizs.dto.UserDto;

public interface SysUserFacade {
  /**
   * 保存对象
   * 
   * @param dto 用户对象
   */
  void save(SysUserInfoDto dto);

  /**
   * 查找用户对象
   * 
   * @param id 用户主键
   * @return 用户对象
   */
  SysUserInfoDto findById(Long id);

  /**
   * 更新用户对象
   * 
   * @param dto 用户对象
   */
  void update(SysUserInfoDto dto);

  /**
   * 登录对象
   * 
   * @param username 用户名
   * @return 找到登录对象
   */
  UserDto findByUsername(String username);

  /**
   * 保存登录成功日志
   * 
   * @param dto 日志对象
   */
  void saveLoginSuccess(LoginLogDto dto);

  /**
   * 保存登录失败日志
   * 
   * @param dto 日志对象
   */
  void saveLoginFailure(LoginLogDto dto);

  /**
   * 查找用户 信息
   * 
   * @param userId 用户主键
   * @param role   角色编码
   * @return 找到用户信息
   */
  Map<String, Object> findInfo(Long userId, String role);

  /**
   * 批量删除用户
   * 
   * @param ids 主键集合
   */
  void deleteByIds(Long[] ids);

  /**
   * 删除用户
   * 
   * @param id 主键
   */
  void deleteById(Long id);

  /**
   * 修必密码
   * 
   * @param userId      用户主键
   * @param oldPassword 旧密码
   * @param password    新密码
   */
  void updatePassword(Long userId, String oldPassword, String password);

  /**
   * 根据角色编码 查找菜单
   * 
   * @param roleCode 角色编码
   * @return 找到的菜单
   */
  List<SysMenu> findMenu(String roleCode);

  /**
   * 根据角色编码查找按钮
   * 
   * @param roleCode 角色编码
   * @return 找到按钮权限编码集合
   */
  List<String> findBtn(String roleCode);

  /**
   * 重置密码
   * 
   * @param id       主键
   * @param password 密码
   */
  void updateResetPassword(Long id, String password);
}
