package com.hzizs.analysis.facade;

import com.hzizs.analysis.dto.ImitatePositionDto;

public interface ImitatePositionFacade {
   
  /**
   * 保存对象
   * @param dto 要保存的Dto对象
   */
  void save(ImitatePositionDto dto);

}
