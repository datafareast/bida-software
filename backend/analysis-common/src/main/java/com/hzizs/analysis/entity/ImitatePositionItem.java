package com.hzizs.analysis.entity;

import com.hzizs.entity.Entity;
import javax.xml.bind.annotation.XmlRootElement;


import java.math.BigDecimal;		
import java.util.Date;		
/**
 * 模拟持仓条目		
 * @author crazy_cabbage
 *
 */
@XmlRootElement		
@SuppressWarnings("serial")
public class ImitatePositionItem extends Entity  {
   // 主键
	private Long id;
	// 保证金
	private BigDecimal cashDeposit;
	// 类别
	private String cat;
	// 编码
	private String code;
	// 合约
	private String contract;
	// 币种
	private String currency;
	// 展期
	private String defer;
	// 期末净值
	private BigDecimal endNetValue;
	// 结束日期
	private Date endTime;
	// 交易所
	private String exchange;
	// 模拟持仓
	private Long imitatePositionId;
	// 指标
	private String kpi;
	// 名称
	private String name;
	// 净值
	private BigDecimal netValue;
	// 数量
	private Integer num;
    		 		
    		
 

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
    		
	public BigDecimal getCashDeposit() {
		return cashDeposit;
	}

	public void setCashDeposit(BigDecimal cashDeposit) {
		this.cashDeposit = cashDeposit;
	}
    		
	public String getCat() {
		return cat;
	}

	public void setCat(String cat) {
		this.cat = cat;
	}
    		
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
    		
	public String getContract() {
		return contract;
	}

	public void setContract(String contract) {
		this.contract = contract;
	}
    		
	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}
    		
	public String getDefer() {
		return defer;
	}

	public void setDefer(String defer) {
		this.defer = defer;
	}
    		
	public BigDecimal getEndNetValue() {
		return endNetValue;
	}

	public void setEndNetValue(BigDecimal endNetValue) {
		this.endNetValue = endNetValue;
	}
    		
	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}
    		
	public String getExchange() {
		return exchange;
	}

	public void setExchange(String exchange) {
		this.exchange = exchange;
	}
    		
	public Long getImitatePositionId() {
		return imitatePositionId;
	}

	public void setImitatePositionId(Long imitatePositionId) {
		this.imitatePositionId = imitatePositionId;
	}
    		
	public String getKpi() {
		return kpi;
	}

	public void setKpi(String kpi) {
		this.kpi = kpi;
	}
    		
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
    		
	public BigDecimal getNetValue() {
		return netValue;
	}

	public void setNetValue(BigDecimal netValue) {
		this.netValue = netValue;
	}
    		
	public Integer getNum() {
		return num;
	}

	public void setNum(Integer num) {
		this.num = num;
	}
	
}
