package com.hzizs.analysis.entity;

import com.hzizs.entity.Entity;
import javax.xml.bind.annotation.XmlRootElement;


import java.math.BigDecimal;		
import java.util.Date;		
/**
 * 行业日交易		
 * @author crazy_cabbage
 *
 */
@XmlRootElement		
@SuppressWarnings("serial")
public class DailyTradingActivityIndustry extends Entity  {
   // 主键
	private Long id;
	// 盘后额
	private BigDecimal afterValue;
	// 盘后量
	private BigDecimal afterVolume;
	// 行业
	private String industry;
	// 市场
	private Long marketId;
	// 日期
	private Date occDate;
	// 收益率
	private BigDecimal returnPct;
	// 额
	private BigDecimal value;
	// 量
	private BigDecimal volume;
    		 		
    		
 

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
    		
	public BigDecimal getAfterValue() {
		return afterValue;
	}

	public void setAfterValue(BigDecimal afterValue) {
		this.afterValue = afterValue;
	}
    		
	public BigDecimal getAfterVolume() {
		return afterVolume;
	}

	public void setAfterVolume(BigDecimal afterVolume) {
		this.afterVolume = afterVolume;
	}
    		
	public String getIndustry() {
		return industry;
	}

	public void setIndustry(String industry) {
		this.industry = industry;
	}
    		
	public Long getMarketId() {
		return marketId;
	}

	public void setMarketId(Long marketId) {
		this.marketId = marketId;
	}
    		
	public Date getOccDate() {
		return occDate;
	}

	public void setOccDate(Date occDate) {
		this.occDate = occDate;
	}
    		
	public BigDecimal getReturnPct() {
		return returnPct;
	}

	public void setReturnPct(BigDecimal returnPct) {
		this.returnPct = returnPct;
	}
    		
	public BigDecimal getValue() {
		return value;
	}

	public void setValue(BigDecimal value) {
		this.value = value;
	}
    		
	public BigDecimal getVolume() {
		return volume;
	}

	public void setVolume(BigDecimal volume) {
		this.volume = volume;
	}
	
}
