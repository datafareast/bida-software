package com.hzizs.analysis.facade;

import java.util.List;
import com.hzizs.analysis.dto.RoleDto;

public interface RoleFacade {

  /**
   * 查找对象
   * 
   * @param id 主键
   * @return 找到角色对象
   */
  RoleDto findById(Long id);

  /**
   * 保存角色
   * 
   * @param dto 角色
   */
  void save(RoleDto dto);

  /**
   * 更新角色
   * 
   * @param dto 角色对象
   */
  void update(RoleDto dto);

  /**
   * 删除角色
   * 
   * @param id 角色主键
   */
  void deleteById(Long id);

  /**
   * 批量删除角色
   * 
   * @param ids 角色主键集合
   */
  void deleteByIds(Long[] ids);

  /**
   * 根据角色查找权限编码集合
   * 
   * @param roleCode 角色编码
   * @return 找到的权限编码集合
   */
  List<String> findAuthByRoleCode(String roleCode);
}
