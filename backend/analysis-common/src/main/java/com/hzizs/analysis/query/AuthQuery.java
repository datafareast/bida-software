package com.hzizs.analysis.query;

import java.util.HashMap;
import java.util.Map;
import com.hzizs.constants.SymbolConstants;
import com.hzizs.query.ListQuery;
import com.hzizs.util.StringUtil;

public class AuthQuery extends ListQuery {

  /**
   * 
   */
  private static final long serialVersionUID = 2434741367573023480L;
  private String name;
  private String code;
  private String url;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  @Override
  protected Map<String, Object> queryParams(Map<String, Object> params) {
    if (params == null) {
      params = new HashMap<String, Object>();
    }
    super.queryParams(params);
    if (StringUtil.isNotEmpty(name)) {
      params.put("likeName", name + SymbolConstants.PERCENT);
    }
    if (StringUtil.isNotEmpty(code)) {
      params.put("likeCode", code + SymbolConstants.PERCENT);
    }
    if (StringUtil.isNotEmpty(url)) {
      params.put("url", url);
    }
    return params;
  }

}
