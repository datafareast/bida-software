package com.hzizs.analysis.service;

import java.util.List;
import com.hzizs.analysis.entity.DataTopo;
import com.hzizs.service.IService;

/**
 * 数据拓普图 服务类
 * 
 * @author crazy_cabbage
 *
 */
public interface DataTopoService extends IService<DataTopo> {
  /**
   * 根据主键 查找他们的父级
   * 
   * @param id 找到他们的父级
   * @return 拓普图
   */
  List<DataTopo> findByParent(Long id);

  /**
   * 根据主键 查找他的子级
   * 
   * @param id 找到他的子级
   * @return 找到子级
   */
  List<DataTopo> findByChild(Long id);

  /**
   * 根据数据类型 查找显示对象
   * 
   * @param dataTypeId 数据类型主键
   * @return 找到的显示对象
   */
  List<DataTopo> findShowByDataTypeId(Long dataTypeId);
  /**
   * 更改位置
   * @param dataTopo 数据对象
   * @return 更改后对象
   */
  DataTopo updateBound(DataTopo dataTopo);
 /**
  * 根据数据类型主键统计数量
  * @param dataTypeId  数据类型主键
  * @return 统计出的数量
  */
  long countByDataTypeId(Long dataTypeId);
  /**
   * 查找所有拓普指标
   * @param dataTypeId 数据类型
   * @return 找到拓普指标
   */
  List<DataTopo> findAllByDataTypeId(Long dataTypeId);
}
