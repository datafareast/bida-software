package com.hzizs.analysis.service;

import com.hzizs.service.IService;
import java.util.List;
import com.hzizs.analysis.entity.Area;

/**
 * 区域 服务类
 * 
 * @author crazy_cabbage
 *
 */
public interface AreaService extends IService<Area> {
  /**
   * 根据父主键 查找区域
   * 
   * @param parentId 父主键
   * @return 找到区域集合
   */
  List<Area> findByParentId(Long parentId);

  /**
   * 根据父主键 名称 查找区域
   * 
   * @param parentId 父主键
   * @param name     名称
   * @return 找到区域
   */
  Area findByParentIdName(Long parentId, String name);

  /**
   * 根据编码查找区域
   * 
   * @param code 编码
   * @return 找到的区域
   */
  Area findByCode(String code);

  /**
   * 根据父主键 统计子数量
   * 
   * @param parentId 父主键
   * @return 统计出的子数量
   */
  long countByParentId(Long parentId);

  /**
   * 查找省份城市数据
   * 
   * @return 省份城市数据
   */
  List<Area> findProvinceCityData();

}
