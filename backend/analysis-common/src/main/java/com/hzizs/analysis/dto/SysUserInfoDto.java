package com.hzizs.analysis.dto;

import com.hzizs.analysis.entity.SysUser;
import com.hzizs.analysis.entity.SysUserInfo;
import com.hzizs.analysis.entity.SysUserLog;
import com.hzizs.analysis.entity.SysUserRole;
import com.hzizs.dto.Dto;

public class SysUserInfoDto extends Dto {

  /**
   * 
   */
  private static final long serialVersionUID = 5322073023472550081L;

  private SysUser user;
  private SysUserInfo userInfo;
  private SysUserRole userRole;
  private SysUserLog userLog;

  public SysUser getUser() {
    return user;
  }

  public void setUser(SysUser user) {
    this.user = user;
  }

  public SysUserInfo getUserInfo() {
    return userInfo;
  }

  public void setUserInfo(SysUserInfo userInfo) {
    this.userInfo = userInfo;
  }

  public SysUserRole getUserRole() {
    return userRole;
  }

  public void setUserRole(SysUserRole userRole) {
    this.userRole = userRole;
  }

  public SysUserLog getUserLog() {
    return userLog;
  }

  public void setUserLog(SysUserLog userLog) {
    this.userLog = userLog;
  }

}
