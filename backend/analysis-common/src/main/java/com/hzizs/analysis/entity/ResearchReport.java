package com.hzizs.analysis.entity;

import com.hzizs.entity.Entity;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * 研究报告		
 * @author crazy_cabbage
 *
 */
@XmlRootElement		
@SuppressWarnings("serial")
public class ResearchReport extends Entity  {
   // 主键
	private Long id;
	// 内容
	private String content;
	// 标题
	private String name;
	// 角色
	private String roles;
	// 排序
	private Integer sortOrder;
    		 		
    		
 

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
    		
	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
    		
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
    		
	public String getRoles() {
		return roles;
	}

	public void setRoles(String roles) {
		this.roles = roles;
	}
    		
	public Integer getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(Integer sortOrder) {
		this.sortOrder = sortOrder;
	}
	
}
