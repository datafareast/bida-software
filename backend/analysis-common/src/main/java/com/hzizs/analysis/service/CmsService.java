package com.hzizs.analysis.service;

import com.hzizs.service.IService;
import java.util.List;
import com.hzizs.analysis.entity.Cms;

/**
 * 内容管理系统 服务类
 * 
 * @author crazy_cabbage
 *
 */
public interface CmsService extends IService<Cms> {
  /**
   * 查找导航
   * @return 找到导航
   */
  List<Cms> findNav();

}
