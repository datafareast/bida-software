package com.hzizs.analysis.service;

import com.hzizs.service.IService;
import com.hzizs.analysis.entity.SysAuth;

/**
 * 系统权限 服务类
 * 
 * @author crazy_cabbage
 *
 */
public interface SysAuthService extends IService<SysAuth> {
  /**
   * 根据编码查找 系统权限
   * 
   * @param code 编码
   * @return 找到的系统权限
   */
  SysAuth findByCode(String code);


}
