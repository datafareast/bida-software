package com.hzizs.analysis.service;

import com.hzizs.service.IService;
import com.hzizs.analysis.entity.SysRole;

/**
 * 系统角色 服务类
 * 
 * @author crazy_cabbage
 *
 */
public interface SysRoleService extends IService<SysRole> {
  /**
   * 根据编码查找系统角色
   * 
   * @param code 编码
   * @return 找到系统角色
   */
  SysRole findByCode(String code);
}
