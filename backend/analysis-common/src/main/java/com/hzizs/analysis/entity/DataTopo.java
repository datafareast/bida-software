package com.hzizs.analysis.entity;

import com.hzizs.entity.Entity;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * 数据拓普图
 * 
 * @author crazy_cabbage
 *
 */
@XmlRootElement
@SuppressWarnings("serial")
public class DataTopo extends Entity {
  // 主键
  private Long id;
  // 类别
  private String cat;
  //编码
  private String code;
  // 显示
  private String isShow;
  // 级别
  private String lv;
  // 名称
  private String name;
  // 父级
  private String parentIds;
  // 说明
  private String remark;
  // 排序
  private Integer sortOrder;
  //类型
  private Long dataTypeId;
  // X 坐标
  private Integer x;
  // Y 坐标
  private Integer y;
  // 宽
  private Integer width;
  // 高
  private Integer height;
  //颜色
  private String color;
  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getCat() {
    return cat;
  }

  public void setCat(String cat) {
    this.cat = cat;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getIsShow() {
    return isShow;
  }

  public void setIsShow(String isShow) {
    this.isShow = isShow;
  }

  public String getLv() {
    return lv;
  }

  public void setLv(String lv) {
    this.lv = lv;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getParentIds() {
    return parentIds;
  }

  public void setParentIds(String parentIds) {
    this.parentIds = parentIds;
  }

  public String getRemark() {
    return remark;
  }

  public void setRemark(String remark) {
    this.remark = remark;
  }

  public Integer getSortOrder() {
    return sortOrder;
  }

  public void setSortOrder(Integer sortOrder) {
    this.sortOrder = sortOrder;
  }

  public Long getDataTypeId() {
    return dataTypeId;
  }

  public void setDataTypeId(Long dataTypeId) {
    this.dataTypeId = dataTypeId;
  }

  public Integer getX() {
    return x;
  }

  public void setX(Integer x) {
    this.x = x;
  }

  public Integer getY() {
    return y;
  }

  public void setY(Integer y) {
    this.y = y;
  }

  public Integer getWidth() {
    return width;
  }

  public void setWidth(Integer width) {
    this.width = width;
  }

  public Integer getHeight() {
    return height;
  }

  public void setHeight(Integer height) {
    this.height = height;
  }

  public String getColor() {
    return color;
  }

  public void setColor(String color) {
    this.color = color;
  }

}
