package com.hzizs.analysis.service;

import com.hzizs.service.IService;
import com.hzizs.analysis.entity.UserLog;

/**
 * 用户日志 服务类
 * 
 * @author crazy_cabbage
 *
 */
public interface UserLogService extends IService<UserLog> {

}
