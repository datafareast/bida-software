package com.hzizs.analysis.entity;

import com.hzizs.entity.Entity;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * 常用指标
 * 
 * @author crazy_cabbage
 *
 */
@XmlRootElement
@SuppressWarnings("serial")
public class SecIndex extends Entity {
  // 主键
  private Long id;
  // 交易所
  private String exchange;
  // 指标
  private String idx;
  // 排序
  private Integer sortOrder;



  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getExchange() {
    return exchange;
  }

  public void setExchange(String exchange) {
    this.exchange = exchange;
  }

  public String getIdx() {
    return idx;
  }

  public void setIdx(String idx) {
    this.idx = idx;
  }

  public Integer getSortOrder() {
    return sortOrder;
  }

  public void setSortOrder(Integer sortOrder) {
    this.sortOrder = sortOrder;
  }

}
