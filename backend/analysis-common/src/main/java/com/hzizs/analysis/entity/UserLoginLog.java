package com.hzizs.analysis.entity;

import com.hzizs.entity.Entity;
import com.hzizs.entity.ICreate;
import javax.xml.bind.annotation.XmlRootElement;


import java.util.Date;

/**
 * 用户登录日志
 * 
 * @author crazy_cabbage
 *
 */
@XmlRootElement
@SuppressWarnings("serial")
public class UserLoginLog extends Entity implements ICreate {
  // 主键
  private Long id;
  // 创建时间
  private Date createTime;
  // IP
  private String ip;
  // IP地址
  private String ipAddr;
  // 结果
  private String ret;
  // 来源
  private String src;
  // 游览器
  private String userAgent;
  // 用户
  private Long userId;


  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Date getCreateTime() {
    return createTime;
  }

  public void setCreateTime(Date createTime) {
    this.createTime = createTime;
  }

  public String getIp() {
    return ip;
  }

  public void setIp(String ip) {
    this.ip = ip;
  }

  public String getIpAddr() {
    return ipAddr;
  }

  public void setIpAddr(String ipAddr) {
    this.ipAddr = ipAddr;
  }

  public String getRet() {
    return ret;
  }

  public void setRet(String ret) {
    this.ret = ret;
  }

  public String getSrc() {
    return src;
  }

  public void setSrc(String src) {
    this.src = src;
  }

  public String getUserAgent() {
    return userAgent;
  }

  public void setUserAgent(String userAgent) {
    this.userAgent = userAgent;
  }

  public Long getUserId() {
    return userId;
  }

  public void setUserId(Long userId) {
    this.userId = userId;
  }

  @Override
  public void setCreateBy(Long createBy) {
    
  }

}
