package com.hzizs.analysis.query;
import java.util.HashMap;
import java.util.Map;
import com.hzizs.constants.SymbolConstants;
import com.hzizs.query.ListQuery;
import com.hzizs.util.StringUtil;

public class DataTypeQuery extends ListQuery {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2434741367573023480L;
	private String name;
	private Long dataIndustryId;
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
    public Long getDataIndustryId() {
    return dataIndustryId;
  }

  public void setDataIndustryId(Long dataIndustryId) {
    this.dataIndustryId = dataIndustryId;
  }

    @Override
	protected Map<String, Object> queryParams(Map<String, Object> params) {
    	if (params == null) {
			params = new HashMap<String, Object>();
		}
		super.queryParams(params);
		if (StringUtil.isNotEmpty(name)) {
			params.put("likeName", SymbolConstants.PERCENT+name+SymbolConstants.PERCENT);
		}
		if(dataIndustryId!=null) {
		  params.put("dataIndustryId", dataIndustryId);
		}
		return params;
	}

}
