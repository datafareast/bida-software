package com.hzizs.analysis.service;

import com.hzizs.service.IService;
import com.hzizs.analysis.entity.ImitatePosition;

/**
 * 模拟持仓 服务类
 * 
 * @author crazy_cabbage
 *
 */
public interface ImitatePositionService extends IService<ImitatePosition> {

}
