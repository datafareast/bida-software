package com.hzizs.analysis.facade;

import com.hzizs.analysis.entity.Auth;

public interface AuthFacade {
  /**
   * 删除权限
   * 
   * @param id 主键
   */
  void deleteById(Long id);

  /**
   * 批量删除权限
   * 
   * @param ids 主键集合
   */
  void deleteByIds(Long[] ids);

  /**
   * 保存权限
   * 
   * @param auth 权限
   */
  void save(Auth auth);

  /**
   * 更新权限
   * 
   * @param auth 权限
   */
  void update(Auth auth);
}
