package com.hzizs.analysis.service;

import com.hzizs.service.IService;
import com.hzizs.analysis.entity.SysOption;
import com.hzizs.analysis.vo.DataConfig;

/**
 * 数据选项 服务类
 * 
 * @author crazy_cabbage
 *
 */
public interface SysOptionService extends IService<SysOption> {
  /**
   * 查找锁定次数
   * 
   * @return 锁定次数
   */
  int findLockTimes();

  /**
   * 查找 锁定时间
   * 
   * @return 锁定时间
   */
  int findLockTime();

  /**
   * 根据编码查找选项
   * 
   * @param code 编码
   * @return 选项
   */
  SysOption findByCode(String code);

  /**
   * 查找数值
   * 
   * @param code 编码
   * @return 找到数值
   */
  int findInt(String code);

  /**
   * 查找数值
   * 
   * @param code 编码
   * @return 找到数值
   */
  Long findLong(String code);

  /**
   * 查找字符串
   * 
   * @param code 编码
   * @return 找到字符串
   */
  String findStr(String code);

  /**
   * 查找上传服务器
   * 
   * @return 上传服务器
   */
  String findUploadServer();

  /**
   * 查找游览服器
   * 
   * @return 游览服务器
   */
  String findBrowserServer();

  /**
   * 查找管理盐值
   * 
   * @return 管理盐值
   */
  String findAdminSalt();

  /**
   * 查找管理盐值
   * 
   * @return 管理盐值
   */
  String findBossSalt();

  /**
   * 查找文件夹数量
   * 
   * @return 文件夹数量
   */
  int findFolderNum();

  /**
   * 查找K线配置
   * 
   * @return K线配置
   */
  DataConfig findKConfig();

  /**
   * 查找热点配置
   * 
   * @return 热点配置
   */
  DataConfig findHotmapConfig();

  /**
   * 查找Line线配置
   * 
   * @return Line 配置
   */
  DataConfig findLineConfig();

  /**
   * 查找迈析服务器
   * 
   * @return 查找迈析服务器
   */
  String findMxServer();
}
