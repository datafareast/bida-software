package com.hzizs.analysis.service;

import com.hzizs.service.IService;
import com.hzizs.analysis.entity.User;

/**
 * 用户 服务类
 * 
 * @author crazy_cabbage
 *
 */
public interface UserService extends IService<User> {
  /**
   * 根据用户名，查找用户
   * 
   * @param username 用户名
   * @return 找到的用户
   */
  User findByUsername(String username);

}
