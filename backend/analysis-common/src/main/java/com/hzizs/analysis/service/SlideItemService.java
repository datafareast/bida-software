package com.hzizs.analysis.service;

import com.hzizs.service.IService;
import com.hzizs.analysis.entity.SlideItem;

/**
 * 轮播图条目 服务类
 * 
 * @author crazy_cabbage
 *
 */
public interface SlideItemService extends IService<SlideItem> {
  /**
   * 根据轮播图主键 统计数量
   * 
   * @param slideId 轮播图主键
   * @return 数量
   */
  long countBySlideId(Long slideId);

}
