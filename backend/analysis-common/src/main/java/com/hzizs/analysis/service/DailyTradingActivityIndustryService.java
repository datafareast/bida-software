package com.hzizs.analysis.service;

import com.hzizs.service.IService;
import java.util.Date;
import java.util.List;
import com.hzizs.analysis.entity.DailyTradingActivityIndustry;
/**
 * 行业日交易 服务类 
 * @author crazy_cabbage
 *
 */
public interface DailyTradingActivityIndustryService extends IService<DailyTradingActivityIndustry> {
  /**
   * 查找行业日交易
   * @param marketId 市场 
   * @param startTime 开始时间
   * @param endTime 结束时间
   * @return 行业日交易
   */
  List<DailyTradingActivityIndustry> find(Long marketId, Date startTime, Date endTime);

}
