package com.hzizs.analysis.facade;

import com.hzizs.analysis.entity.DataType;

public interface DataTypeFacade {
  /**
   * 批量删除数据
   * @param ids 主键集合
   */
  void deleteByIds(Long[] ids);
   /**
    * 删除数据
    * @param id 主键
    */
  void deleteById(Long id);
  /**
   * 保存数据类型
   * @param dataType 数据类型
   * @return 数据类型
   */
  DataType save(DataType dataType);
  /**
   * 更数数据类型
   * @param dataType 数据类型
   * @return 数据类型
   */
  DataType update(DataType dataType);

}
