package com.hzizs.analysis.service;

import com.hzizs.service.IService;
import com.hzizs.analysis.entity.SysFile;

/**
 * 系统文件 服务类
 * 
 * @author crazy_cabbage
 *
 */
public interface SysFileService extends IService<SysFile> {

}
