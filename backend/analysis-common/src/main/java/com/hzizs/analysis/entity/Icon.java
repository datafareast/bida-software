package com.hzizs.analysis.entity;

import com.hzizs.entity.Entity;
import com.hzizs.entity.ICreate;
import javax.xml.bind.annotation.XmlRootElement;


import java.util.Date;

/**
 * 图标库
 * 
 * @author crazy_cabbage
 *
 */
@XmlRootElement
@SuppressWarnings("serial")
public class Icon extends Entity implements ICreate{
  // 主键
  private Long id;
  // 类别
  private String cat;
  // 编码
  private String code;
  // 创建时间
  private Date createTime;
  // 名称
  private String name;
  // 排序
  private Integer sortOrder;
  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getCat() {
    return cat;
  }

  public void setCat(String cat) {
    this.cat = cat;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public Date getCreateTime() {
    return createTime;
  }

  public void setCreateTime(Date createTime) {
    this.createTime = createTime;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Integer getSortOrder() {
    return sortOrder;
  }

  public void setSortOrder(Integer sortOrder) {
    this.sortOrder = sortOrder;
  }

  @Override
  public void setCreateBy(Long createBy) {
    
  }

}
