package com.hzizs.analysis.service;

import com.hzizs.service.IService;
import com.hzizs.analysis.entity.Slide;

/**
 * 轮播图 服务类
 * 
 * @author crazy_cabbage
 *
 */
public interface SlideService extends IService<Slide> {

}
