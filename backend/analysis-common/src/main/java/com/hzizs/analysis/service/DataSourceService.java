package com.hzizs.analysis.service;

import com.hzizs.service.IService;
import com.hzizs.analysis.entity.DataSource;

/**
 * 数据源 服务类
 * 
 * @author crazy_cabbage
 *
 */
public interface DataSourceService extends IService<DataSource> {

}
