package com.hzizs.analysis.entity;

import java.util.Date;
import javax.xml.bind.annotation.XmlRootElement;
import com.hzizs.entity.ICreate;
import com.hzizs.entity.ISortOrder;
import com.hzizs.entity.ShardingEntity;

/**
 * 文件夹
 * 
 * @author crazy_cabbage
 *
 */
@XmlRootElement
@SuppressWarnings("serial")
public class Folder extends ShardingEntity implements ICreate ,ISortOrder {
  // 主键
  private Long id;
  // 类别
  private String cat;
  // 创建时间
  private Date createTime;
  // 名称
  private String name;
  // 排序
  private Integer sortOrder;
  // 用户
  private Long userId;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getCat() {
    return cat;
  }

  public void setCat(String cat) {
    this.cat = cat;
  }

  public Date getCreateTime() {
    return createTime;
  }

  public void setCreateTime(Date createTime) {
    this.createTime = createTime;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Integer getSortOrder() {
    return sortOrder;
  }

  public void setSortOrder(Integer sortOrder) {
    this.sortOrder = sortOrder;
  }

  public Long getUserId() {
    return userId;
  }

  public void setUserId(Long userId) {
    this.userId = userId;
  }

  @Override
  public void setCreateBy(Long createBy) {
    
  }

}
