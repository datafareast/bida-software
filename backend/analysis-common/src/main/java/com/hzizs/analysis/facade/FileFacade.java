package com.hzizs.analysis.facade;

public interface FileFacade {
  /**
   * 根据主键集合，用户主键 删除文件
   * 
   * @param ids    主键集合
   * @param userId 商户主键
   */
  void deleteByIds(Long[] ids, Long userId);
  /**
   * 根据主键 ，用户主键  删除文件
   * @param id 主键
   * @param userId 用户主键
   */
  void deleteById(Long id,Long userId);

  /**
   * 移动文件到新文件夹
   * @param ids 主键集合
   * @param folderId 文件夹
   * @param userId 用户
   */
  void updateMove(Long [] ids ,Long folderId, Long userId  );
}
