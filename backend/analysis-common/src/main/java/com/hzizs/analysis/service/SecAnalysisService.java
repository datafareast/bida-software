package com.hzizs.analysis.service;

import com.hzizs.service.IService;
import com.hzizs.analysis.entity.SecAnalysis;

/**
 * 业绩分析 服务类
 * 
 * @author crazy_cabbage
 *
 */
public interface SecAnalysisService extends IService<SecAnalysis> {

}
