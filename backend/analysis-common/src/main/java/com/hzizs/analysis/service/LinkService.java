package com.hzizs.analysis.service;

import com.hzizs.service.IService;
import com.hzizs.analysis.entity.Link;

/**
 * 友情链接 服务类
 * 
 * @author crazy_cabbage
 *
 */
public interface LinkService extends IService<Link> {

}
