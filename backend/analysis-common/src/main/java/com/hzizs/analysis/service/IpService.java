package com.hzizs.analysis.service;

import com.hzizs.service.IService;
import com.hzizs.analysis.entity.Ip;

/**
 * IP 服务类
 * 
 * @author crazy_cabbage
 *
 */
public interface IpService extends IService<Ip> {
   /**
    * 根据ip地址 查找ip对象
    * @param ip ip地址
    * @return 找到的IP对象
    */
  Ip findByIp(String ip);
  /**
   * 根据ip查找ip地址
   * @param ip ip地址
   * @return 找到的ip地址
   */
  String findAddrByIp(String ip);
   /**
    * 根据开始IP 结束IP统计数量
    * @param startIp 开始IP 
    * @param endIp 结束IP
    * @return 统计出的数量
    */
  long countByStartIpEndIp(Long startIp,Long endIp);

}
