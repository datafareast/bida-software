package com.hzizs.analysis.dto;

import java.util.List;
import com.hzizs.analysis.entity.DataTopo;
import com.hzizs.dto.Dto;

public class DataTopoDto extends Dto {
  /**
   * 
   */
  private static final long serialVersionUID = -9024863700170336693L;
  // 父级
  private List<DataTopo> parent;
  // 自己
  private DataTopo dataTopo;
  // 孩子
  private List<DataTopo> child;

  public List<DataTopo> getParent() {
    return parent;
  }

  public void setParent(List<DataTopo> parent) {
    this.parent = parent;
  }

  public DataTopo getDataTopo() {
    return dataTopo;
  }

  public void setDataTopo(DataTopo dataTopo) {
    this.dataTopo = dataTopo;
  }

  public List<DataTopo> getChild() {
    return child;
  }

  public void setChild(List<DataTopo> child) {
    this.child = child;
  }



}
