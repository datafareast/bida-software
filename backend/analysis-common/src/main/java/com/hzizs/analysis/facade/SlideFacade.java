package com.hzizs.analysis.facade;

public interface SlideFacade {
   /**
    * 批量删除
    * @param ids 主键集合
    */
  void deleteByIds(Long [] ids);
  /**
   * 删除
   * @param id 主键
   */
  void deleteById(Long id);

}
