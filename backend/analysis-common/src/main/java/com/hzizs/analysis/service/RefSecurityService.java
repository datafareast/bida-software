package com.hzizs.analysis.service;

import com.hzizs.service.IService;
import com.hzizs.analysis.entity.RefSecurity;
/**
 * 证券参考数据 服务类 
 * @author crazy_cabbage
 *
 */
public interface RefSecurityService extends IService<RefSecurity> {
  /**
   * 根据证券主键，查找证券信息
   * @param securityId 证券主键
   * @return 找到证券信息
   */
  RefSecurity findBySecurityId(Long securityId);

}
