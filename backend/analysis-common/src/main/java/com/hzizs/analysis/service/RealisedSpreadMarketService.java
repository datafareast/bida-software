package com.hzizs.analysis.service;

import com.hzizs.service.IService;
import java.util.Date;
import java.util.List;
import com.hzizs.analysis.entity.RealisedSpreadMarket;
/**
 * 市场实现价差 服务类 
 * @author crazy_cabbage
 *
 */
public interface RealisedSpreadMarketService extends IService<RealisedSpreadMarket> {
   /**
    * 根据市场主键， 开始时间，结束时间查找 市场实现价差
    * @param marketId 市场主键
    * @param startTime 开始时间
    * @param endTime 结束时间
    * @return 找到的实现价差
    */
  List<RealisedSpreadMarket> find(Long marketId, Date startTime, Date endTime);

}
