package com.hzizs.analysis.service;

import com.hzizs.service.IService;
import com.hzizs.analysis.entity.DataIndustry;

/**
 * 数据行业 服务类
 * 
 * @author crazy_cabbage
 *
 */
public interface DataIndustryService extends IService<DataIndustry> {
  /**
   * 根据名称查找数据行业
   * 
   * @param name 名称
   * @return 找到的数据行业
   */
  DataIndustry findByName(String name);

}
