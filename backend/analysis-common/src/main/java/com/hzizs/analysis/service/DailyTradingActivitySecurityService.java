package com.hzizs.analysis.service;

import com.hzizs.service.IService;
import com.hzizs.analysis.entity.DailyTradingActivitySecurity;
/**
 * 证券日交易 服务类 
 * @author crazy_cabbage
 *
 */
public interface DailyTradingActivitySecurityService extends IService<DailyTradingActivitySecurity> {

}
