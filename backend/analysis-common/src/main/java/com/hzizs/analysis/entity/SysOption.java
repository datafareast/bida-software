package com.hzizs.analysis.entity;

import com.hzizs.entity.Entity;
import com.hzizs.entity.ICreate;
import com.hzizs.entity.IModify;
import javax.xml.bind.annotation.XmlRootElement;


import java.util.Date;

/**
 * 数据选项
 * 
 * @author crazy_cabbage
 *
 */
@XmlRootElement
@SuppressWarnings("serial")
public class SysOption extends Entity implements ICreate ,IModify{
  // 主键
  private Long id;
  // 类别
  private String cat;
  // 编码
  private String code;
  // 创建时间
  private Date createTime;
  // 类型
  private String inputType;
  // 修改时间
  private Date modifyTime;
  // 名称
  private String name;
  // 排序
  private Integer sortOrder;
  // 类型值
  private String typeVal;
  // 值
  private String val;
  // 值2
  private String val2;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getCat() {
    return cat;
  }

  public void setCat(String cat) {
    this.cat = cat;
  }

  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public Date getCreateTime() {
    return createTime;
  }

  public void setCreateTime(Date createTime) {
    this.createTime = createTime;
  }

  public String getInputType() {
    return inputType;
  }

  public void setInputType(String inputType) {
    this.inputType = inputType;
  }

  public Date getModifyTime() {
    return modifyTime;
  }

  public void setModifyTime(Date modifyTime) {
    this.modifyTime = modifyTime;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Integer getSortOrder() {
    return sortOrder;
  }

  public void setSortOrder(Integer sortOrder) {
    this.sortOrder = sortOrder;
  }

  public String getTypeVal() {
    return typeVal;
  }

  public void setTypeVal(String typeVal) {
    this.typeVal = typeVal;
  }

  public String getVal() {
    return val;
  }

  public void setVal(String val) {
    this.val = val;
  }

  public String getVal2() {
    return val2;
  }

  public void setVal2(String val2) {
    this.val2 = val2;
  }

  @Override
  public void setCreateBy(Long createBy) {
    
  }

  @Override
  public void setModifyBy(Long modifyBy) {
    
  }

}
