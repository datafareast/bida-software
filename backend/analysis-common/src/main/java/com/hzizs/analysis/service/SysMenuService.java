package com.hzizs.analysis.service;

import java.util.List;
import com.hzizs.analysis.entity.SysMenu;
import com.hzizs.service.IService;

/**
 * 系统菜单 服务类
 * 
 * @author crazy_cabbage
 *
 */
public interface SysMenuService extends IService<SysMenu> {
  /**
   * 查找菜单
   * 
   * @return 菜单
   */
  List<SysMenu> findMenu();

  /**
   * 查找功能按钮
   * 
   * @return 功能按钮
   */
  List<SysMenu> findBtn();

  /**
   * 根据类别查找菜单
   * 
   * @param cats 类别
   * @return 找到菜单
   */
  List<SysMenu> findByCats(List<String> cats);

  /**
   * 根据父主键 统计 子数量
   * 
   * @param parentId 父主键
   * @return 子数量
   */
  long countByParentId(Long parentId);
}
