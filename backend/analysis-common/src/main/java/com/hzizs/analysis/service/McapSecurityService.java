package com.hzizs.analysis.service;

import com.hzizs.service.IService;
import com.hzizs.analysis.entity.McapSecurity;
/**
 * 证券市值 服务类 
 * @author crazy_cabbage
 *
 */
public interface McapSecurityService extends IService<McapSecurity> {

}
