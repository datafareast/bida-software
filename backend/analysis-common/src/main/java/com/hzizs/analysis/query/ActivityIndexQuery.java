package com.hzizs.analysis.query;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import com.hzizs.query.ListQuery;
import com.hzizs.util.StringUtil;

public class ActivityIndexQuery extends ListQuery {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2434741367573023480L;
	private String code;
	private Date startTime;
	private Date endTime;

    public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public Date getStartTime() {
    return startTime;
  }

  public void setStartTime(Date startTime) {
    this.startTime = startTime;
  }

  public Date getEndTime() {
    return endTime;
  }

  public void setEndTime(Date endTime) {
    this.endTime = endTime;
  }


    @Override
	protected Map<String, Object> queryParams(Map<String, Object> params) {
    	if (params == null) {
			params = new HashMap<String, Object>();
		}
		super.queryParams(params);
		if (StringUtil.isNotEmpty(code)) {
			params.put("code", code);
		}
		if(startTime!=null) {
		  params.put("startTime", startTime);
		}
		if(endTime!=null) {
		  params.put("endTime", endTime);
		}
		return params;
	}

}
