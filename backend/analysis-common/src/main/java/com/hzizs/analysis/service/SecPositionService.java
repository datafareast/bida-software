package com.hzizs.analysis.service;

import com.hzizs.service.IService;
import java.util.Date;
import java.util.List;
import com.hzizs.analysis.entity.SecPosition;

/**
 * 持仓分析 服务类
 * 
 * @author crazy_cabbage
 *
 */
public interface SecPositionService extends IService<SecPosition> {
  /**
   * 根据发生日期查找产品持仓
   * 
   * @param occDate 发生日期
   * @return 找到产品持仓
   */
  List<SecPosition> findByOccDate(Date occDate);

}
