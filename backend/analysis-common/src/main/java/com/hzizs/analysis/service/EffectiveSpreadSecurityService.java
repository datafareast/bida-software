package com.hzizs.analysis.service;

import com.hzizs.service.IService;
import java.util.Date;
import java.util.List;
import com.hzizs.analysis.entity.EffectiveSpreadSecurity;
/**
 * 证券有效价差 服务类 
 * @author crazy_cabbage
 *
 */
public interface EffectiveSpreadSecurityService extends IService<EffectiveSpreadSecurity> {
/**
 * 查找证券有效价差
 * @param securityId 证券主键
 * @param startTime 开始时间
 * @param endTime 结束时间
 * @return 有效价差
 */
  List<EffectiveSpreadSecurity> find(Long securityId, Date startTime, Date endTime);

}
