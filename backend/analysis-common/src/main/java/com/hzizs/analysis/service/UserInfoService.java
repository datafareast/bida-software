package com.hzizs.analysis.service;

import com.hzizs.service.IService;
import com.hzizs.analysis.entity.UserInfo;

/**
 * 用户信息 服务类
 * 
 * @author crazy_cabbage
 *
 */
public interface UserInfoService extends IService<UserInfo> {
  /**
   * 更新用户基本信息
   * @param userInfo 用户信息
   */
  void updateInfo(UserInfo userInfo);
  /**
   * 更新用户基本信息， 去掉禁用等关键信息
   * @param userInfo 用户信息
   */
  void updateInfo2(UserInfo userInfo);
}
