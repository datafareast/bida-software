package com.hzizs.analysis.service;

import com.hzizs.service.IService;
import java.util.Date;
import java.util.List;
import com.hzizs.analysis.entity.QuotedSpreadSecurity;
/**
 * 证券报价价差 服务类 
 * @author crazy_cabbage
 *
 */
public interface QuotedSpreadSecurityService extends IService<QuotedSpreadSecurity> {
  /**
   * 查找报价价差
   * @param securityId 
   * @param startTime
   * @param endTime
   * @return
   */
  List<QuotedSpreadSecurity> find(Long securityId, Date startTime, Date endTime);

}
