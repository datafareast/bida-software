package com.hzizs.analysis.query;

import java.util.HashMap;
import java.util.Map;
import com.hzizs.constants.SymbolConstants;
import com.hzizs.query.ListQuery;
import com.hzizs.util.StringUtil;

public class AreaQuery extends ListQuery {

  /**
   * 
   */
  private static final long serialVersionUID = 2434741367573023480L;
  private String name;
  private String code;
  private String enabled;
  private String areaCode;
  private Long parentId;
  private String zipcode;
  private String cat;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  
  public String getCode() {
    return code;
  }

  public void setCode(String code) {
    this.code = code;
  }

  public String getEnabled() {
    return enabled;
  }

  public void setEnabled(String enabled) {
    this.enabled = enabled;
  }

  public String getAreaCode() {
    return areaCode;
  }

  public void setAreaCode(String areaCode) {
    this.areaCode = areaCode;
  }

  public Long getParentId() {
    return parentId;
  }

  public void setParentId(Long parentId) {
    this.parentId = parentId;
  }

  public String getZipcode() {
    return zipcode;
  }

  public void setZipcode(String zipcode) {
    this.zipcode = zipcode;
  }

  public String getCat() {
    return cat;
  }

  public void setCat(String cat) {
    this.cat = cat;
  }

  @Override
  protected Map<String, Object> queryParams(Map<String, Object> params) {
    if (params == null) {
      params = new HashMap<String, Object>();
    }
    super.queryParams(params);
    if (StringUtil.isNotEmpty(name)) {
      params.put("likeName", SymbolConstants.PERCENT + name + SymbolConstants.PERCENT);
    }
    if (StringUtil.isNotEmpty(code)) {
      params.put("code", code);
    }

    if (StringUtil.isNotEmpty(enabled)) {
      params.put("enabled", enabled);
    }
    if (StringUtil.isNotEmpty(areaCode)) {
      params.put("areaCode", areaCode);
    }
    if (parentId != null) {
      params.put("parentId", parentId);
    }
    if (StringUtil.isNotEmpty(zipcode)) {
      params.put("zipcode", zipcode);
    }
    if (StringUtil.isNotEmpty(cat)) {
      params.put("cat", cat);
    }
    return params;
  }

}
