package com.hzizs.analysis.dto;

import java.util.Date;
import java.util.List;

public class IndexDto {
  private List<String> legend;
  private List<Date> xdate;
  private List<IndexItem> items;

 

  public List<String> getLegend() {
    return legend;
  }

  public void setLegend(List<String> legend) {
    this.legend = legend;
  }

 
  public List<Date> getXdate() {
    return xdate;
  }

  public void setXdate(List<Date> xdate) {
    this.xdate = xdate;
  }

  public List<IndexItem> getItems() {
    return items;
  }

  public void setItems(List<IndexItem> items) {
    this.items = items;
  }

}
