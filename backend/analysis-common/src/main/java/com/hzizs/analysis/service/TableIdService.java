package com.hzizs.analysis.service;

import com.hzizs.service.IService;
import com.hzizs.analysis.entity.TableId;

/**
 * 表主键 服务类
 * 
 * @author crazy_cabbage
 *
 */
public interface TableIdService extends IService<TableId> {
  /**
   * 保存或查找 主键生成器
   * 
   * @param code 编码
   * @return 主键生成器
   */
  TableId doSaveOrGet(String code);
}
