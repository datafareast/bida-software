package com.hzizs.analysis.dto;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class IndexItem {
  private String name;
  private List<BigDecimal> data;
  private List<Item> item;
  public String getName() {
    return name;
  }
  public void setName(String name) {
    this.name = name;
  }
  
  public List<BigDecimal> getData() {
    return data;
  }
  public void setData(List<BigDecimal> data) {
    this.data = data;
  }
  public List<Item> getItem() {
    if(item==null) {
      item = new ArrayList<Item>();
    }
    return item;
  }
  public void setItem(List<Item> item) {
    this.item = item;
  }
  

}
