package com.hzizs.analysis.service;

import java.util.List;
import com.hzizs.analysis.entity.SysRoleMenu;
import com.hzizs.service.IService;

/**
 * 系统角色菜单 服务类
 * 
 * @author crazy_cabbage
 *
 */
public interface SysRoleMenuService extends IService<SysRoleMenu> {
  /**
   * 根据角色主键 查找 角色菜单
   * 
   * @param roleId 角色主键
   * @return 系统角色菜单
   */
  List<SysRoleMenu> findByRoleId(Long roleId);

  /**
   * 根据角色主键，菜单集合，删除角色菜单
   * 
   * @param roleId  角色主键
   * @param menuIds 菜单集合
   */
  void delete(Long roleId, List<Long> menuIds);

  /**
   * 根据角色主键 删除角色菜单
   * 
   * @param roleId 角色主键
   */
  void deleteByRoleId(Long roleId);

  /**
   * 根据菜单主键 统计 数量
   * 
   * @param menuId 菜单主键
   * @return 统计出的数量
   */
  long countByMenuId(Long menuId);
}
