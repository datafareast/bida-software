package com.hzizs.analysis.service;

import com.hzizs.service.IService;
import com.hzizs.analysis.entity.SysUser;

/**
 * 系统用户 服务类
 * 
 * @author crazy_cabbage
 *
 */
public interface SysUserService extends IService<SysUser> {
  /**
   * 根据用户名 查找用户
   * 
   * @param username 用户名
   * @return 找到用户
   */
  SysUser findByUsername(String username);

}
