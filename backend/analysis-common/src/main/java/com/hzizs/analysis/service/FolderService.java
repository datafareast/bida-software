package com.hzizs.analysis.service;

import com.hzizs.service.IShardingService;
import com.hzizs.analysis.entity.Folder;

/**
 * 文件夹 服务类
 * 
 * @author crazy_cabbage
 *
 */
public interface FolderService extends IShardingService<Folder> {
  /**
   * 保存或获得默认文件夹
   * 
   * @param userId 用户主键
   * @return 获得默认文件夹的主键
   */
  Long saveOrFindDefault(Long userId);

  /**
   * 查找默认文件夹
   * 
   * @param userId 用户主键
   * @return 默认文件夹
   */
  Long findDefault(Long userId);

  /**
   * 保存默认配置
   * 
   * @param userId 用户主键
   */
  void saveDefault(Long userId);

}
