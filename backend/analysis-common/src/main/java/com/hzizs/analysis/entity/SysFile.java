package com.hzizs.analysis.entity;

import com.hzizs.entity.Entity;
import javax.xml.bind.annotation.XmlRootElement;


import java.util.Date;

/**
 * 系统文件
 * 
 * @author crazy_cabbage
 *
 */
@XmlRootElement
@SuppressWarnings("serial")
public class SysFile extends Entity {
  // 主键
  private Long id;
  // 类别
  private String cat;
  // 创建时间
  private Date createTime;
  // 名称
  private String name;
  // 原始文件名
  private String oldName;
  // 路径
  private String path;
  // 排序
  private Integer sortOrder;
  // 文件夹
  private Long sysFolderId;
  // 系统用户
  private Long sysUserId;
  // URL
  private String url;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getCat() {
    return cat;
  }

  public void setCat(String cat) {
    this.cat = cat;
  }

  public Date getCreateTime() {
    return createTime;
  }

  public void setCreateTime(Date createTime) {
    this.createTime = createTime;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getOldName() {
    return oldName;
  }

  public void setOldName(String oldName) {
    this.oldName = oldName;
  }

  public String getPath() {
    return path;
  }

  public void setPath(String path) {
    this.path = path;
  }

  public Integer getSortOrder() {
    return sortOrder;
  }

  public void setSortOrder(Integer sortOrder) {
    this.sortOrder = sortOrder;
  }

  public Long getSysFolderId() {
    return sysFolderId;
  }

  public void setSysFolderId(Long sysFolderId) {
    this.sysFolderId = sysFolderId;
  }

  public Long getSysUserId() {
    return sysUserId;
  }

  public void setSysUserId(Long sysUserId) {
    this.sysUserId = sysUserId;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

}
