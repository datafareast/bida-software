package com.hzizs.analysis.query;
import java.util.HashMap;
import java.util.Map;
import com.hzizs.constants.SymbolConstants;
import com.hzizs.query.ListQuery;
import com.hzizs.util.StringUtil;

public class RefSecurityQuery extends ListQuery {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2434741367573023480L;
	private String q;

 
    public String getQ() {
    return q;
  }


  public void setQ(String q) {
    this.q = q;
  }


    @Override
	protected Map<String, Object> queryParams(Map<String, Object> params) {
    	if (params == null) {
			params = new HashMap<String, Object>();
		}
		super.queryParams(params);
		if (StringUtil.isNotEmpty(q)) {
			params.put("likeQ", q+SymbolConstants.PERCENT);
		}
		return params;
	}

}
