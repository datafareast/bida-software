package com.hzizs.analysis.service;

import com.hzizs.service.IService;
import com.hzizs.analysis.entity.SysUserLog;

/**
 * 系统用户日志 服务类
 * 
 * @author crazy_cabbage
 *
 */
public interface SysUserLogService extends IService<SysUserLog> {

}
