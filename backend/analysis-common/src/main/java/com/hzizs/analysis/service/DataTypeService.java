package com.hzizs.analysis.service;

import com.hzizs.service.IService;
import com.hzizs.analysis.entity.DataType;

/**
 * 数据种类 服务类
 * 
 * @author crazy_cabbage
 *
 */
public interface DataTypeService extends IService<DataType> {
  /**
   * 数据行业主键
   * 
   * @param dataIndustryId 数据行业主键
   * @return 统计出的数量
   */
  long countByDataIndustryId(Long dataIndustryId);

  /**
   * 根据名称 数据行业主键 查找 数据行业
   * 
   * @param name           名称
   * @param dataIndustryId 数据行业主键
   * @return 找到数据行业
   */
  DataType findByNameDataIndustryId(String name, Long dataIndustryId);

}
