package com.hzizs.analysis.entity;

import com.hzizs.entity.Entity;
import javax.xml.bind.annotation.XmlRootElement;


/**
 * IP
 * 
 * @author crazy_cabbage
 *
 */
@XmlRootElement
@SuppressWarnings("serial")
public class Ip extends Entity {
  // 主键
  private Long id;
  // 参考地址
  private String addr;
  // 参考地址2
  private String addr2;
  // 参考地址3
  private String addr3;
  // 结束IP
  private Long endIp;
  // 结束IP字符串
  private String endIpStr;
  // 开始IP
  private Long startIp;
  // 开始IP字符串
  private String startIpStr;
  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getAddr() {
    return addr;
  }

  public void setAddr(String addr) {
    this.addr = addr;
  }

  public String getAddr2() {
    return addr2;
  }

  public void setAddr2(String addr2) {
    this.addr2 = addr2;
  }

  public String getAddr3() {
    return addr3;
  }

  public void setAddr3(String addr3) {
    this.addr3 = addr3;
  }

  public Long getEndIp() {
    return endIp;
  }

  public void setEndIp(Long endIp) {
    this.endIp = endIp;
  }

  public String getEndIpStr() {
    return endIpStr;
  }

  public void setEndIpStr(String endIpStr) {
    this.endIpStr = endIpStr;
  }

  public Long getStartIp() {
    return startIp;
  }

  public void setStartIp(Long startIp) {
    this.startIp = startIp;
  }

  public String getStartIpStr() {
    return startIpStr;
  }

  public void setStartIpStr(String startIpStr) {
    this.startIpStr = startIpStr;
  }

}
