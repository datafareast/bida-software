package com.hzizs.analysis.query;

import java.util.HashMap;
import java.util.Map;

import com.hzizs.query.ListQuery;
import com.hzizs.util.StringUtil;

public class LinkItemQuery extends ListQuery {

  /**
   * 
   */
  private static final long serialVersionUID = 2434741367573023480L;
  private String name;
  private Long linkId;
  private String enabled;
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }
  

  public Long getLinkId() {
    return linkId;
  }

  public void setLinkId(Long linkId) {
    this.linkId = linkId;
  }

  public String getEnabled() {
    return enabled;
  }

  public void setEnabled(String enabled) {
    this.enabled = enabled;
  }

  @Override
  protected Map<String, Object> queryParams(Map<String, Object> params) {
    if (params == null) {
      params = new HashMap<String, Object>();
    }
    super.queryParams(params);
    if (StringUtil.isNotEmpty(name)) {
      params.put("name", name);
    }
    if (linkId != null) {
      params.put("linkId", linkId);
    }
    if (StringUtil.isNotEmpty(enabled)) {
      params.put("enabled", enabled);
    }
    return params;
  }

}
