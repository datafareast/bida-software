package com.hzizs.analysis.service;

import com.hzizs.service.IService;
import com.hzizs.analysis.entity.SysFolder;

/**
 * 系统文件夹 服务类
 * 
 * @author crazy_cabbage
 *
 */
public interface SysFolderService extends IService<SysFolder> {

}
