package com.hzizs.analysis.entity;

import com.hzizs.entity.Entity;
import javax.xml.bind.annotation.XmlRootElement;


import java.math.BigDecimal;		
import java.util.Date;		
/**
 * 模拟持仓		
 * @author crazy_cabbage
 *
 */
@XmlRootElement		
@SuppressWarnings("serial")
public class ImitatePosition extends Entity  {
   // 主键
	private Long id;
	// 余额
	private BigDecimal balance;
	// 结束日期
	private Date endDate;
	// 结束持仓资金
	private BigDecimal endPositionValue;
	// 指标
	private String kpi;
	// 名称
	private String name;
	// 日期
	private Date occDate;
	// 持仓资金
	private BigDecimal positionValue;
	// 状态
	private String sta;
    		 		
    		
 

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
    		
	public BigDecimal getBalance() {
		return balance;
	}

	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}
    		
	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
    		
	public BigDecimal getEndPositionValue() {
		return endPositionValue;
	}

	public void setEndPositionValue(BigDecimal endPositionValue) {
		this.endPositionValue = endPositionValue;
	}
    		
 	
	public String getKpi() {
		return kpi;
	}

	public void setKpi(String kpi) {
		this.kpi = kpi;
	}
    		
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
    		
	public Date getOccDate() {
		return occDate;
	}

	public void setOccDate(Date occDate) {
		this.occDate = occDate;
	}
    		
	public BigDecimal getPositionValue() {
		return positionValue;
	}

	public void setPositionValue(BigDecimal positionValue) {
		this.positionValue = positionValue;
	}
    		
	public String getSta() {
		return sta;
	}

	public void setSta(String sta) {
		this.sta = sta;
	}
	
}
