package com.hzizs.analysis.constants;

public interface Constants {
  /**
   * 文件夹类别
   * @author weihu
   *
   */
  public interface FolderCat{
    /**
     * 0 全部
     */
    String ALL="0";
    /**
     * 1 默认
     */
    String DEFAULT ="1";
    /**
     * 2 自定义
     */
    String DIY="2";
  }
  /**
   * 地区类别
   * @author weihu
   *
   */
  public interface AreaCat{
    /**
     * 省份
     */
    String PROVINCE ="province";
    /**
     * 城市
     */
    String CITY="city";
    /**
     * 郡、县、区
     */
    String COUNTY="county";
    /**
     * 乡、镇
     */
    String TOWN="town";
    /**
     * 社区、村
     */
    String VILLAGE="village";
  }
  /**
   * 系统选项编码
   * @author weihu
   *
   */
  public interface SysOptionCode{
    /**
     * 锁定次数
     */
    String LOCK_TIMES="LOCK_TIMES";
    /**
     * 锁定时间
     */
    String LOCK_TIME="LOCK_TIME";
    /**
     * 上传服务器
     */
    String UPLOAD_SERVER ="UPLOAD_SERVER";
    /**
     * 游览服务器
     */
    String BROWSER_SERVER="BROWSER_SERVER";
    /**
     * 文件夹数量
     */
    String FOLDER_NUM="FOLDER_NUM";
    /**
     * admin盐值
     */
    String ADMIN_SALT="ADMIN_SALT";
    /**
     * boss盐值
     */
    String BOSS_SALT="BOSS_SALT";
    /**
     * 热点开始时间
     */
    String HOTMAP_START_TIME="HOTMAP_START_TIME";
    /**
     * 热点结束时间
     */
    String HOTMAP_END_TIME="HOTMAP_END_TIME";
    /**
     * 热点市场
     */
    String HOTMAP_MARKET="HOTMAP_MARKET";
    /**
     * 热点市场指标
     */
    String HOTMAP_MARKET_IDX="HOTMAP_MARKET_IDX";
    /**
     * LINE开始时间
     */
    String LINE_START_TIME="LINE_START_TIME";
    /**
     * LINE 结束时间
     */
    String LINE_END_TIME="LINE_END_TIME";
    
    /**
     * LINE 市场
     */
    String LINE_MARKET="LINE_MARKET";
    /**
     *  LINE 市场指标
     */
    String LINE_MARKET_IDX="LINE_MARKET_IDX";
    /**
     * K线开始时间
     */
    String  K_START_TIME ="K_START_TIME";
    /**
     * K线结束时间
     */
    String  K_END_TIME ="K_END_TIME";
    /**
     * K 市场
     */
    String K_MARKET="K_MARKET";
    /**
     * 迈析服务地址
     */
    String  MX_SERVER ="MX_SERVER";
    
  }
  public interface MenuCat {
    /**
     * 菜单组
     */
    String MENU_GROUP = "0";
    /**
     * 菜单
     */
    String MENU = "1";
    /**
     * 按钮
     */
    String BTN = "2";
  }
  /**
   * 图标类型
   * 
   * @author weihu
   *
   */
  public interface IconCat {
    /**
     * 线
     */
    String LINE = "0";
    /**
     * 面
     */
    String SIDE = "1";
  }
  public interface ImitateSta{
    /**
     * 初始化
     */
    String STA_0="0";
    /**
     * 进行中
     */
    String STA_1="1";
   /**
    * 已完成
    */
    String STA_2="2";
  }
  public interface DbType{
    /**
     * ORACLE
     */
    String ORACLE="ORACLE";
    /**
     * MYSQL
     */
    String MYSQL="MYSQL";
  }
  public interface IndexTypeCode{
    /**
     * 自相关10秒
     */
    String AA10s="AA10s";
    /**
     * 自相关10秒
     */
    String AA60s="AA60s";
    /**
     * 基点实现价差1秒
     */
    String RA1s="RA1s";
    /**
     * 基点实现价差10秒
     */
    String RA10s ="RA10s";
    /**
     * 基点实现价差30秒
     */
    String RA30s="RA30s";
    /**
     * 基点实现价差1分钟
     */
    String RA1m="RA1m";
    /**
     * 基点实现价差5分钟
     */
    String RA5m="RA5m";
    /**
     * 基点实现价差10分钟
     */
    String RA10m="RA10m";
    /**
     * 绝对实现价差1秒
     */
    String RB1s = "RB1s";
    /**
     * 绝对实现价差10秒
     */
    String RB10s="RB10s";
    /**
     * 绝对实现价差30s
     */
    String RB30s = "RB30s";
     /**
      * 绝对实现价差1分钟
      */
    String RB1m ="RB1m";
    /**
     * 绝对实现价差5分钟
     */
    String RB5m = "RB5m";
    /**
     * 绝对实现价差10分钟
     */
    String RB10m ="RB10m";
    /**
     * 基点报价价差
     */
    String QA="QA"; 
    /**
     * 绝对报价价差
     */
    String QB="QB";
    /**
     * 基点有效价差
     */
    String EA="EA";
    /**
     * 绝对有效价差
     */
    String EB="EB";
  }
}
