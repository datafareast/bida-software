package com.hzizs.analysis.facade;

import com.hzizs.analysis.entity.DataIndustry;

public interface DataIndustryFacade {
  /**
   * 批量删除数据
   * @param ids 主键集合
   */
  void deleteByIds(Long[] ids);
  /**
   * 删除数据
   * @param id 主键
   */
  void deleteById(Long id);
   /**
    * 保存数据行业
    * @param dataIndustry 数据行业
    * @return 保存的数据行业
    */
  DataIndustry save(DataIndustry dataIndustry);
  /**
   * 更新数据行业
   * @param dataIndustry 数据行业
   * @return 更新的数据行业
   */
  DataIndustry update(DataIndustry dataIndustry);

}
