package com.hzizs.analysis.service;

import com.hzizs.service.IService;
import com.hzizs.analysis.entity.Auth;

/**
 * 权限 服务类
 * 
 * @author crazy_cabbage
 *
 */
public interface AuthService extends IService<Auth> {
  /**
   * 根据编码查找 权限
   * 
   * @param code 编码
   * @return 找到的权限
   */
  Auth findByCode(String code);

}
