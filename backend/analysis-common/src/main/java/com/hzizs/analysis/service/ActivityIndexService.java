package com.hzizs.analysis.service;

import com.hzizs.service.IService;
import com.hzizs.analysis.entity.ActivityIndex;
/**
 * 指数指标 服务类 
 * @author crazy_cabbage
 *
 */
public interface ActivityIndexService extends IService<ActivityIndex> {

}
