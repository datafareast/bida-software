package com.hzizs.analysis.facade;

public interface SysDataFacade {
  /**
   * 根据主键集合 批量删除
   * 
   * @param ids 主键集合
   */
  void deleteByIds(Long[] ids);

  /**
   * 根据主键 删除
   * 
   * @param id 主键
   */
  void deleteById(Long id);

}
