package com.hzizs.analysis.query;

import java.util.HashMap;
import java.util.Map;
import com.hzizs.constants.SymbolConstants;
import com.hzizs.query.ListQuery;
import com.hzizs.util.IPUtil;
import com.hzizs.util.RegexUtil;
import com.hzizs.util.StringUtil;

public class IpQuery extends ListQuery {

  /**
   * 
   */
  private static final long serialVersionUID = 2434741367573023480L;
  private String ip;
  private String addr;

  public String getIp() {
    return ip;
  }

  public void setIp(String ip) {
    this.ip = ip;
  }

  public String getAddr() {
    return addr;
  }

  public void setAddr(String addr) {
    this.addr = addr;
  }



  @Override
  protected Map<String, Object> queryParams(Map<String, Object> params) {
    if (params == null) {
      params = new HashMap<String, Object>();
    }
    super.queryParams(params);
    if (StringUtil.isNotEmpty(ip)) {
      if (RegexUtil.isIp(ip)) {
        params.put("ip", IPUtil.str2num(ip));
      } else {
        params.put("ip", -1);
      }
    }
    if (StringUtil.isNotEmpty(addr)) {
      params.put("likeAddr", SymbolConstants.PERCENT + addr + SymbolConstants.PERCENT);
    }
    return params;
  }

}
