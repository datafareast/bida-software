package com.hzizs.analysis.action;


import javax.annotation.Resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hzizs.analysis.entity.RealisedSpreadSecurity;
import com.hzizs.analysis.query.RealisedSpreadSecurityQuery;
import com.hzizs.analysis.service.RealisedSpreadSecurityService;
import com.hzizs.pojo.ErrorInfo;
import com.hzizs.vo.Container;
/**
 * 证券实现价差控制器
 * @author crazy_cabbage
 *
 */
@RestController
@RequestMapping("/realisedSpreadSecurity")
public class RealisedSpreadSecurityAction {
	@Resource
	private RealisedSpreadSecurityService realisedSpreadSecurityService;

	@GetMapping("/list")
	public ErrorInfo list(RealisedSpreadSecurityQuery query ) {
		Container<RealisedSpreadSecurity> container = realisedSpreadSecurityService.findContainer(query);
		return ErrorInfo.ok(container);
	}

	@GetMapping("/find")
	public ErrorInfo find(Long id) {
		RealisedSpreadSecurity realisedSpreadSecurity=realisedSpreadSecurityService.findById(id);
		return ErrorInfo.ok(realisedSpreadSecurity);
	}

 

	@PostMapping("/del")
	public ErrorInfo del(Long id) {
		realisedSpreadSecurityService.deleteById(id);
		return ErrorInfo.ok();
	}

	@PostMapping("/save")
	public ErrorInfo save(RealisedSpreadSecurity realisedSpreadSecurity) {
		realisedSpreadSecurityService.save(realisedSpreadSecurity);
		return ErrorInfo.ok();
	}

	@PostMapping("/update")
	public ErrorInfo update(RealisedSpreadSecurity realisedSpreadSecurity) {
		realisedSpreadSecurityService.update(realisedSpreadSecurity);
		return ErrorInfo.ok();
	}
}
