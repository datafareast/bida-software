package com.hzizs.analysis.action;


import javax.annotation.Resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hzizs.analysis.entity.Slide;
import com.hzizs.analysis.facade.SlideFacade;
import com.hzizs.analysis.query.SlideQuery;
import com.hzizs.analysis.service.SlideService;
import com.hzizs.pojo.ErrorInfo;
import com.hzizs.vo.Container;
/**
 * 轮播图控制器
 * @author crazy_cabbage
 *
 */
@RestController
@RequestMapping("/slide")
public class SlideAction {
	@Resource
	private SlideService slideService;
	@Resource
	private SlideFacade slideFacade;

	@GetMapping("/list")
	public ErrorInfo list(SlideQuery query ) {
		Container<Slide> container = slideService.findContainer(query);
		return ErrorInfo.ok(container);
	}

	@GetMapping("/find")
	public ErrorInfo find(Long id) {
		Slide slide=slideService.findById(id);
		return ErrorInfo.ok(slide);
	}

 

	@PostMapping("/del")
	public ErrorInfo del(Long[] id) {
		slideFacade.deleteByIds(id);
		return ErrorInfo.ok();
	}

	@PostMapping("/save")
	public ErrorInfo save(Slide slide) {
		slideService.save(slide);
		return ErrorInfo.ok();
	}

	@PostMapping("/update")
	public ErrorInfo update(Slide slide) {
		slideService.update(slide);
		return ErrorInfo.ok();
	}
}
