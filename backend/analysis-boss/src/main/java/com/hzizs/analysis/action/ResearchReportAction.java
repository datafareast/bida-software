package com.hzizs.analysis.action;


import java.util.List;
import javax.annotation.Resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.hzizs.ActionContext;
import com.hzizs.analysis.entity.ResearchReport;
import com.hzizs.analysis.facade.ResearchReportFacade;
import com.hzizs.analysis.query.ResearchReportQuery;
import com.hzizs.analysis.service.ResearchReportService;
import com.hzizs.pojo.ErrorInfo;
import com.hzizs.vo.Container;
/**
 * 研究报告控制器
 * @author crazy_cabbage
 *
 */
@RestController
@RequestMapping("/researchReport")
public class ResearchReportAction {
	@Resource
	private ResearchReportService researchReportService;
	
	@Resource
	private ResearchReportFacade researchReportFacade;
	@GetMapping("/list")
	public ErrorInfo list(ResearchReportQuery query ) {
		Container<ResearchReport> container = researchReportService.findContainer(query);
		return ErrorInfo.ok(container);
	}
	@GetMapping("/find")
	public ErrorInfo find(Long id) {
		ResearchReport researchReport=researchReportService.findById(id);
		return ErrorInfo.ok(researchReport);
	}
	@GetMapping("/findNav")
	public ErrorInfo findNav() {
	  Long userId = ActionContext.getUserId();
	  List<ResearchReport> researchReports=researchReportFacade.findByUserId(userId);
	  return ErrorInfo.ok(researchReports);
	}
 
 

	@PostMapping("/del")
	public ErrorInfo del(Long id) {
		researchReportService.deleteById(id);
		return ErrorInfo.ok();
	}

	@PostMapping("/save")
	public ErrorInfo save(ResearchReport researchReport) {
		researchReportService.save(researchReport);
		return ErrorInfo.ok();
	}

	@PostMapping("/update")
	public ErrorInfo update(ResearchReport researchReport) {
		researchReportService.update(researchReport);
		return ErrorInfo.ok();
	}
}
