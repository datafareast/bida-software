package com.hzizs.analysis.action;


import java.util.List;
import javax.annotation.Resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hzizs.analysis.entity.SecCat;
import com.hzizs.analysis.query.SecCatQuery;
import com.hzizs.analysis.service.SecCatService;
import com.hzizs.pojo.ErrorInfo;
import com.hzizs.vo.Container;

/**
 * 类别控制器
 * 
 * @author crazy_cabbage
 *
 */
@RestController
@RequestMapping("/secCat")
public class SecCatAction {
  @Resource
  private SecCatService secCatService;

  @GetMapping("/list")
  public ErrorInfo list(SecCatQuery query) {
    Container<SecCat> container = secCatService.findContainer(query);
    return ErrorInfo.ok(container);
  }

  @GetMapping("/find")
  public ErrorInfo find(Long id) {
    SecCat secCat = secCatService.findById(id);
    return ErrorInfo.ok(secCat);
  }
  @GetMapping("/findAll")
  public ErrorInfo findAll() {
    List<SecCat> secCats = secCatService.findAll();
    return ErrorInfo.ok(secCats);
  }



  @PostMapping("/del")
  public ErrorInfo del(Long id) {
    secCatService.deleteById(id);
    return ErrorInfo.ok();
  }

  @PostMapping("/save")
  public ErrorInfo save(SecCat secCat) {
    secCatService.save(secCat);
    return ErrorInfo.ok();
  }

  @PostMapping("/update")
  public ErrorInfo update(SecCat secCat) {
    secCatService.update(secCat);
    return ErrorInfo.ok();
  }
}
