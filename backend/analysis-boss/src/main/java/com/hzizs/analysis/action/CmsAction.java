package com.hzizs.analysis.action;


import java.util.List;
import javax.annotation.Resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hzizs.analysis.entity.Cms;
import com.hzizs.analysis.query.CmsQuery;
import com.hzizs.analysis.service.CmsService;
import com.hzizs.pojo.ErrorInfo;
import com.hzizs.vo.Container;
/**
 * 内容管理系统控制器
 * @author crazy_cabbage
 *
 */
@RestController
@RequestMapping("/cms")
public class CmsAction {
	@Resource
	private CmsService cmsService;

	@GetMapping("/list")
	public ErrorInfo list(CmsQuery query ) {
		Container<Cms> container = cmsService.findContainer(query);
		return ErrorInfo.ok(container);
	}
	@GetMapping("/findNav")
	public ErrorInfo findNav() {
	  List<Cms> cmses = cmsService.findNav();
	  return ErrorInfo.ok(cmses);
	}

	@GetMapping("/find")
	public ErrorInfo find(Long id) {
		Cms cms=cmsService.findById(id);
		return ErrorInfo.ok(cms);
	}

 

	@PostMapping("/del")
	public ErrorInfo del(Long[] id) {
		cmsService.deleteByIds(id);
		return ErrorInfo.ok();
	}

	@PostMapping("/save")
	public ErrorInfo save(Cms cms) {
		cmsService.save(cms);
		return ErrorInfo.ok();
	}

	@PostMapping("/update")
	public ErrorInfo update(Cms cms) {
		cmsService.update(cms);
		return ErrorInfo.ok();
	}
}
