package com.hzizs.analysis.action.common;

import java.io.IOException;
import java.io.OutputStream;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import com.hzizs.constants.CommonConstants;
import com.hzizs.constants.ImageConstants;
import com.hzizs.kaptcha.KaptchaHelper;
import com.hzizs.util.IOUtil;
import com.hzizs.util.ImageUtil;
import com.hzizs.util.ResponseUtil;

@Controller
public class IndexAction {
  @Resource
  private KaptchaHelper kaptchaHelper;

  @GetMapping("/captcha")
  public String captcha(HttpServletRequest req, HttpServletResponse res) throws IOException {
    ResponseUtil.addImageHeader(res);
    ResponseUtil.addNoCacheHeader(res);
    HttpSession session = req.getSession();
    String captcha = kaptchaHelper.createText();
    session.setAttribute(CommonConstants.CAPTCHA, captcha);
    OutputStream os = res.getOutputStream();
    ImageUtil.write(kaptchaHelper.createImage(captcha), ImageConstants.JPEG, os);
    IOUtil.closeQuietly(os);
    return null;

  }

}
