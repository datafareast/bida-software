package com.hzizs.analysis.action;


import javax.annotation.Resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hzizs.analysis.entity.SecAnalysisItem;
import com.hzizs.analysis.query.SecAnalysisItemQuery;
import com.hzizs.analysis.service.SecAnalysisItemService;
import com.hzizs.pojo.ErrorInfo;
import com.hzizs.vo.Container;

/**
 * 业绩分析条目控制器
 * 
 * @author crazy_cabbage
 *
 */
@RestController
@RequestMapping("/secAnalysisItem")
public class SecAnalysisItemAction {
  @Resource
  private SecAnalysisItemService secAnalysisItemService;

  @GetMapping("/list")
  public ErrorInfo list(SecAnalysisItemQuery query) {
    Container<SecAnalysisItem> container = secAnalysisItemService.findContainer(query);
    return ErrorInfo.ok(container);
  }

  @GetMapping("/find")
  public ErrorInfo find(Long id) {
    SecAnalysisItem secAnalysisItem = secAnalysisItemService.findById(id);
    return ErrorInfo.ok(secAnalysisItem);
  }



  @PostMapping("/del")
  public ErrorInfo del(Long id) {
    secAnalysisItemService.deleteById(id);
    return ErrorInfo.ok();
  }

  @PostMapping("/save")
  public ErrorInfo save(SecAnalysisItem secAnalysisItem) {
    secAnalysisItemService.save(secAnalysisItem);
    return ErrorInfo.ok();
  }

  @PostMapping("/update")
  public ErrorInfo update(SecAnalysisItem secAnalysisItem) {
    secAnalysisItemService.update(secAnalysisItem);
    return ErrorInfo.ok();
  }
}
