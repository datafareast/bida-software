package com.hzizs.analysis.action;


import javax.annotation.Resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hzizs.analysis.entity.SysUserLoginLog;
import com.hzizs.analysis.query.SysUserLoginLogQuery;
import com.hzizs.analysis.service.SysUserLoginLogService;
import com.hzizs.pojo.ErrorInfo;
import com.hzizs.vo.Container;
/**
 * 系统用户登录日志控制器
 * @author crazy_cabbage
 *
 */
@RestController
@RequestMapping("/sysUserLoginLog")
public class SysUserLoginLogAction {
	@Resource
	private SysUserLoginLogService sysUserLoginLogService;

	@GetMapping("/list")
	public ErrorInfo list(SysUserLoginLogQuery query ) {
		Container<SysUserLoginLog> container = sysUserLoginLogService.findContainer(query);
		return ErrorInfo.ok(container);
	}

	@GetMapping("/find")
	public ErrorInfo find(Long id) {
		SysUserLoginLog sysUserLoginLog=sysUserLoginLogService.findById(id);
		return ErrorInfo.ok(sysUserLoginLog);
	}

 

	@PostMapping("/del")
	public ErrorInfo del(Long id) {
		sysUserLoginLogService.deleteById(id);
		return ErrorInfo.ok();
	}

	@PostMapping("/save")
	public ErrorInfo save(SysUserLoginLog sysUserLoginLog) {
		sysUserLoginLogService.save(sysUserLoginLog);
		return ErrorInfo.ok();
	}

	@PostMapping("/update")
	public ErrorInfo update(SysUserLoginLog sysUserLoginLog) {
		sysUserLoginLogService.update(sysUserLoginLog);
		return ErrorInfo.ok();
	}
}
