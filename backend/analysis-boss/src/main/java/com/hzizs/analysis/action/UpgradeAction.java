package com.hzizs.analysis.action;


import javax.annotation.Resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hzizs.analysis.entity.Upgrade;
import com.hzizs.analysis.query.UpgradeQuery;
import com.hzizs.analysis.service.UpgradeService;
import com.hzizs.pojo.ErrorInfo;
import com.hzizs.vo.Container;
/**
 * 升级信息控制器
 * @author crazy_cabbage
 *
 */
@RestController
@RequestMapping("/upgrade")
public class UpgradeAction {
	@Resource
	private UpgradeService upgradeService;

	@GetMapping("/list")
	public ErrorInfo list(UpgradeQuery query ) {
		Container<Upgrade> container = upgradeService.findContainer(query);
		return ErrorInfo.ok(container);
	}

	@GetMapping("/find")
	public ErrorInfo find(Long id) {
		Upgrade upgrade=upgradeService.findById(id);
		return ErrorInfo.ok(upgrade);
	}

 

	@PostMapping("/del")
	public ErrorInfo del(Long id) {
		upgradeService.deleteById(id);
		return ErrorInfo.ok();
	}

	@PostMapping("/save")
	public ErrorInfo save(Upgrade upgrade) {
		upgradeService.save(upgrade);
		return ErrorInfo.ok();
	}

	@PostMapping("/update")
	public ErrorInfo update(Upgrade upgrade) {
		upgradeService.update(upgrade);
		return ErrorInfo.ok();
	}
}
