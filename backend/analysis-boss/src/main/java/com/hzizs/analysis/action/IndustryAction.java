package com.hzizs.analysis.action;


import java.util.List;
import javax.annotation.Resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hzizs.analysis.entity.Industry;
import com.hzizs.analysis.query.IndustryQuery;
import com.hzizs.analysis.service.IndustryService;
import com.hzizs.pojo.ErrorInfo;
import com.hzizs.vo.Container;
/**
 * 行业控制器
 * @author crazy_cabbage
 *
 */
@RestController
@RequestMapping("/industry")
public class IndustryAction {
	@Resource
	private IndustryService industryService;

	@GetMapping("/list")
	public ErrorInfo list(IndustryQuery query ) {
		Container<Industry> container = industryService.findContainer(query);
		return ErrorInfo.ok(container);
	}

	@GetMapping("/find")
	public ErrorInfo find(Long id) {
		Industry industry=industryService.findById(id);
		return ErrorInfo.ok(industry);
	}
	
	@GetMapping("/findAll")
	public ErrorInfo findAll() {
	  List<Industry> industries = industryService.findAll();
	  return ErrorInfo.ok(industries);
	}

 

	@PostMapping("/del")
	public ErrorInfo del(Long id) {
		industryService.deleteById(id);
		return ErrorInfo.ok();
	}

	@PostMapping("/save")
	public ErrorInfo save(Industry industry) {
		industryService.save(industry);
		return ErrorInfo.ok();
	}

	@PostMapping("/update")
	public ErrorInfo update(Industry industry) {
		industryService.update(industry);
		return ErrorInfo.ok();
	}
}
