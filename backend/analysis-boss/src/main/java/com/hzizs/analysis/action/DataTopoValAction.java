package com.hzizs.analysis.action;


import java.util.List;
import java.util.Map;
import javax.annotation.Resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hzizs.analysis.entity.DataTopoVal;
import com.hzizs.analysis.facade.DataTopoValFacade;
import com.hzizs.analysis.query.DataTopoValQuery;
import com.hzizs.analysis.service.DataTopoValService;
import com.hzizs.pojo.ErrorInfo;
import com.hzizs.vo.Container;
/**
 * 数据拓普值控制器
 * @author crazy_cabbage
 *
 */
@RestController
@RequestMapping("/dataTopoVal")
public class DataTopoValAction {
	@Resource
	private DataTopoValService dataTopoValService;
	@Resource
	private DataTopoValFacade dataTopoValFacade;

	@GetMapping("/list")
	public ErrorInfo list(DataTopoValQuery query ) {
		Container<DataTopoVal> container = dataTopoValService.findContainer(query);
		return ErrorInfo.ok(container);
	}
	@GetMapping("/findData")
    public ErrorInfo findData(Long id) {
      List<Map<String, Object>> data = dataTopoValFacade.findData(id);
      return ErrorInfo.ok(data);
	}
	@GetMapping("/find")
	public ErrorInfo find(Long id) {
		DataTopoVal dataTopoVal=dataTopoValService.findById(id);
		return ErrorInfo.ok(dataTopoVal);
	}

 

	@PostMapping("/del")
	public ErrorInfo del(Long id) {
		dataTopoValService.deleteById(id);
		return ErrorInfo.ok();
	}

	@PostMapping("/save")
	public ErrorInfo save(DataTopoVal dataTopoVal) {
		dataTopoValService.save(dataTopoVal);
		return ErrorInfo.ok();
	}

	@PostMapping("/update")
	public ErrorInfo update(DataTopoVal dataTopoVal) {
		dataTopoValService.update(dataTopoVal);
		return ErrorInfo.ok();
	}
}
