package com.hzizs.analysis.action;


import javax.annotation.Resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hzizs.analysis.entity.AutocorrelationMarket;
import com.hzizs.analysis.query.AutocorrelationMarketQuery;
import com.hzizs.analysis.service.AutocorrelationMarketService;
import com.hzizs.pojo.ErrorInfo;
import com.hzizs.vo.Container;
/**
 * 市场自相关控制器
 * @author crazy_cabbage
 *
 */
@RestController
@RequestMapping("/autocorrelationMarket")
public class AutocorrelationMarketAction {
	@Resource
	private AutocorrelationMarketService autocorrelationMarketService;

	@GetMapping("/list")
	public ErrorInfo list(AutocorrelationMarketQuery query ) {
		Container<AutocorrelationMarket> container = autocorrelationMarketService.findContainer(query);
		return ErrorInfo.ok(container);
	}

	@GetMapping("/find")
	public ErrorInfo find(Long id) {
		AutocorrelationMarket autocorrelationMarket=autocorrelationMarketService.findById(id);
		return ErrorInfo.ok(autocorrelationMarket);
	}

 

	@PostMapping("/del")
	public ErrorInfo del(Long id) {
		autocorrelationMarketService.deleteById(id);
		return ErrorInfo.ok();
	}

	@PostMapping("/save")
	public ErrorInfo save(AutocorrelationMarket autocorrelationMarket) {
		autocorrelationMarketService.save(autocorrelationMarket);
		return ErrorInfo.ok();
	}

	@PostMapping("/update")
	public ErrorInfo update(AutocorrelationMarket autocorrelationMarket) {
		autocorrelationMarketService.update(autocorrelationMarket);
		return ErrorInfo.ok();
	}
}
