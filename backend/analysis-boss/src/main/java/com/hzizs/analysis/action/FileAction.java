package com.hzizs.analysis.action;


import java.io.IOException;
import javax.annotation.Resource;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.http.Consts;
import org.apache.http.HttpEntity;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.InputStreamBody;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import com.hzizs.ActionContext;
import com.hzizs.analysis.entity.File;
import com.hzizs.analysis.facade.FileFacade;
import com.hzizs.analysis.query.FileQuery;
import com.hzizs.analysis.service.FileService;
import com.hzizs.analysis.service.FolderService;
import com.hzizs.analysis.service.SysOptionService;
import com.hzizs.json.JsonUtil;
import com.hzizs.net.pojo.NetRet;
import com.hzizs.net.util.HttpClientUtil;
import com.hzizs.pojo.ErrorInfo;
import com.hzizs.util.FileUtil;
import com.hzizs.vo.Container;

/**
 * 文件控制器
 * 
 * @author crazy_cabbage
 *
 */
@RestController
@RequestMapping("/file")
public class FileAction {
  @Resource
  private FileService fileService;
  @Resource
  private FolderService folderService;
  @Resource
  private FileFacade fileFacade;
  @Resource
  private SysOptionService sysOptionService;

  @GetMapping("/list")
  public ErrorInfo list(FileQuery query) {
    Long userId = ActionContext.getUserId();
    Container<File> container = fileService.findContainer(query, userId);
    return ErrorInfo.ok(container);
  }

  @GetMapping("/find")
  public ErrorInfo find(Long id) {
    Long userId = ActionContext.getUserId();
    File file = fileService.findById(id, userId);
    return ErrorInfo.ok(file);
  }


  @PostMapping("/upload")
  public ErrorInfo upload(MultipartFile file, Long folderId) throws IOException {
    Long userId = ActionContext.getUserId();
    if (file.isEmpty()) {
      return ErrorInfo.failure("文件不可以为空~");
    }
    String md5 = DigestUtils.md5Hex(file.getInputStream());
    if (folderId == null) {
      // 查找默认
      folderId = folderService.saveOrFindDefault(userId);
    }
    File file2 = fileService.findByName(md5, userId);
    if (file2 != null) {
      return ErrorInfo.ok(file2);
    }

    // 获得文件的原始文件名
    String originalFilename = file.getOriginalFilename();
    String project = "mall-mch";
    // 各个商家存不同文件夹
    String urlPath = sysOptionService.findBrowserServer();
    String datePath = FileUtil.datePath();
    String filename = project + "/" + userId + "/" + datePath + "/" + FileUtil.fileName(originalFilename);
    String cat = "3";
    if (FileUtil.isImage(originalFilename)) {
      cat = "0";
      if (file.getSize() >= 1024 * 1024) {
        return ErrorInfo.failure("图片文件大于1M");
      }
    }
    if (FileUtil.isVideo(originalFilename)) {
      cat = "1";
      if (file.getSize() >= 1024 * 1024 * 15) {
        return ErrorInfo.failure("视频文件大于15M");
      }
    }
    if (FileUtil.isAudio(originalFilename)) {
      cat = "2";
      if (file.getSize() >= 1024 * 1024 * 6) {
        return ErrorInfo.failure("音频文件大于6M");
      }
    }

    if (originalFilename.length() > 150) {
      originalFilename = originalFilename.substring(0, 150);
    }
    InputStreamBody isb = new InputStreamBody(file.getInputStream(), file.getOriginalFilename());
    HttpEntity httpEntity = MultipartEntityBuilder.create().addPart("file", isb).addTextBody("filename", filename).build();
    String uploadServer = sysOptionService.findUploadServer();
    NetRet netRet = HttpClientUtil.post(uploadServer + "/upload", httpEntity, Consts.UTF_8);
    ErrorInfo errorInfo = JsonUtil.read(netRet.getVal(), ErrorInfo.class);
    if (!errorInfo.isFlag()) {
      return ErrorInfo.failure("上传异常");
    }
    file2 = new File();
    file2.setName(md5);
    file2.setCat(cat);
    file2.setOldName(originalFilename);
    file2.setFolderId(folderId);
    file2.setUserId(userId);
    file2.setPath(filename);
    file2.setUrl(urlPath + "/" + filename);
    fileService.save(file2, userId);
    return ErrorInfo.ok(file2);
  }

  @PostMapping("/del")
  public ErrorInfo del(Long[] id) {
    Long userId = ActionContext.getUserId();
    fileFacade.deleteByIds(id, userId);
    return ErrorInfo.ok();
  }

  @PostMapping("/move")
  public ErrorInfo move(Long[] id, Long folderId) {
    Long userId = ActionContext.getUserId();
    fileFacade.updateMove(id, folderId, userId);
    return ErrorInfo.ok();
  }

}
