package com.hzizs.analysis.action;


import javax.annotation.Resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hzizs.analysis.entity.DataSource;
import com.hzizs.analysis.query.DataSourceQuery;
import com.hzizs.analysis.service.DataSourceService;
import com.hzizs.pojo.ErrorInfo;
import com.hzizs.vo.Container;
/**
 * 数据源控制器
 * @author crazy_cabbage
 *
 */
@RestController
@RequestMapping("/dataSource")
public class DataSourceAction {
	@Resource
	private DataSourceService dataSourceService;

	@GetMapping("/list")
	public ErrorInfo list(DataSourceQuery query ) {
		Container<DataSource> container = dataSourceService.findContainer(query);
		return ErrorInfo.ok(container);
	}

	@GetMapping("/find")
	public ErrorInfo find(Long id) {
		DataSource dataSource=dataSourceService.findById(id);
		return ErrorInfo.ok(dataSource);
	}

 

	@PostMapping("/del")
	public ErrorInfo del(Long id) {
		dataSourceService.deleteById(id);
		return ErrorInfo.ok();
	}

	@PostMapping("/save")
	public ErrorInfo save(DataSource dataSource) {
		dataSourceService.save(dataSource);
		return ErrorInfo.ok();
	}

	@PostMapping("/update")
	public ErrorInfo update(DataSource dataSource) {
		dataSourceService.update(dataSource);
		return ErrorInfo.ok();
	}
}
