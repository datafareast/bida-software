package com.hzizs.analysis.action;


import javax.annotation.Resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hzizs.analysis.entity.RealisedSpreadMarket;
import com.hzizs.analysis.query.RealisedSpreadMarketQuery;
import com.hzizs.analysis.service.RealisedSpreadMarketService;
import com.hzizs.pojo.ErrorInfo;
import com.hzizs.vo.Container;
/**
 * 市场实现价差控制器
 * @author crazy_cabbage
 *
 */
@RestController
@RequestMapping("/realisedSpreadMarket")
public class RealisedSpreadMarketAction {
	@Resource
	private RealisedSpreadMarketService realisedSpreadMarketService;

	@GetMapping("/list")
	public ErrorInfo list(RealisedSpreadMarketQuery query ) {
		Container<RealisedSpreadMarket> container = realisedSpreadMarketService.findContainer(query);
		return ErrorInfo.ok(container);
	}

	@GetMapping("/find")
	public ErrorInfo find(Long id) {
		RealisedSpreadMarket realisedSpreadMarket=realisedSpreadMarketService.findById(id);
		return ErrorInfo.ok(realisedSpreadMarket);
	}

 

	@PostMapping("/del")
	public ErrorInfo del(Long id) {
		realisedSpreadMarketService.deleteById(id);
		return ErrorInfo.ok();
	}

	@PostMapping("/save")
	public ErrorInfo save(RealisedSpreadMarket realisedSpreadMarket) {
		realisedSpreadMarketService.save(realisedSpreadMarket);
		return ErrorInfo.ok();
	}

	@PostMapping("/update")
	public ErrorInfo update(RealisedSpreadMarket realisedSpreadMarket) {
		realisedSpreadMarketService.update(realisedSpreadMarket);
		return ErrorInfo.ok();
	}
}
