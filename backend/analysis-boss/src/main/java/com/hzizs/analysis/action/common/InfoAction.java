package com.hzizs.analysis.action.common;

import java.util.Map;
import javax.annotation.Resource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import com.hzizs.ActionContext;
import com.hzizs.analysis.facade.UserFacade;
import com.hzizs.pojo.ErrorInfo;

@RestController
public class InfoAction {
  @Resource
  private UserFacade userFacade;

  @GetMapping("/userInfo")
  public ErrorInfo userInfo() {
    // 变正规
    String role = ActionContext.getRole();
    Long userId = ActionContext.getUserId();
    Map<String, Object> root = userFacade.findInfo(userId, role);
    return ErrorInfo.ok(root);
  }

  @PostMapping("/modifyPassword")
  public ErrorInfo modifyPassword(String oldPassword, String password) {
    Long userId = ActionContext.getUserId();
    userFacade.updatePassword(userId, oldPassword, password);
    return ErrorInfo.ok();
  }
}
