package com.hzizs.analysis.action;


import javax.annotation.Resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hzizs.analysis.entity.ImitatePositionItem;
import com.hzizs.analysis.query.ImitatePositionItemQuery;
import com.hzizs.analysis.service.ImitatePositionItemService;
import com.hzizs.pojo.ErrorInfo;
import com.hzizs.vo.Container;
/**
 * 模拟持仓条目控制器
 * @author crazy_cabbage
 *
 */
@RestController
@RequestMapping("/imitatePositionItem")
public class ImitatePositionItemAction {
	@Resource
	private ImitatePositionItemService imitatePositionItemService;

	@GetMapping("/list")
	public ErrorInfo list(ImitatePositionItemQuery query ) {
		Container<ImitatePositionItem> container = imitatePositionItemService.findContainer(query);
		return ErrorInfo.ok(container);
	}

	@GetMapping("/find")
	public ErrorInfo find(Long id) {
		ImitatePositionItem imitatePositionItem=imitatePositionItemService.findById(id);
		return ErrorInfo.ok(imitatePositionItem);
	}

 

	@PostMapping("/del")
	public ErrorInfo del(Long id) {
		imitatePositionItemService.deleteById(id);
		return ErrorInfo.ok();
	}

	@PostMapping("/save")
	public ErrorInfo save(ImitatePositionItem imitatePositionItem) {
		imitatePositionItemService.save(imitatePositionItem);
		return ErrorInfo.ok();
	}

	@PostMapping("/update")
	public ErrorInfo update(ImitatePositionItem imitatePositionItem) {
		imitatePositionItemService.update(imitatePositionItem);
		return ErrorInfo.ok();
	}
}
