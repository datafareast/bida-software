package com.hzizs.analysis.action;


import javax.annotation.Resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.hzizs.analysis.dto.ImitatePositionDto;
import com.hzizs.analysis.entity.ImitatePosition;
import com.hzizs.analysis.facade.ImitatePositionFacade;
import com.hzizs.analysis.query.ImitatePositionQuery;
import com.hzizs.analysis.service.ImitatePositionService;
import com.hzizs.pojo.ErrorInfo;
import com.hzizs.vo.Container;
/**
 * 模拟持仓控制器
 * @author crazy_cabbage
 *
 */
@RestController
@RequestMapping("/imitatePosition")
public class ImitatePositionAction {
	@Resource
	private ImitatePositionService imitatePositionService;
	@Resource
     private 	ImitatePositionFacade imitatePositionFacade;
	@GetMapping("/list")
	public ErrorInfo list(ImitatePositionQuery query ) {
		Container<ImitatePosition> container = imitatePositionService.findContainer(query);
		return ErrorInfo.ok(container);
	}

	@GetMapping("/find")
	public ErrorInfo find(Long id) {
		ImitatePosition imitatePosition=imitatePositionService.findById(id);
		return ErrorInfo.ok(imitatePosition);
	}

 

	@PostMapping("/del")
	public ErrorInfo del(Long id) {
		imitatePositionService.deleteById(id);
		return ErrorInfo.ok();
	}

	@PostMapping("/save")
	public ErrorInfo save(ImitatePositionDto dto) {
	 imitatePositionFacade.save(dto);
		return ErrorInfo.ok();
	}

	@PostMapping("/update")
	public ErrorInfo update(ImitatePosition imitatePosition) {
		imitatePositionService.update(imitatePosition);
		return ErrorInfo.ok();
	}
}
