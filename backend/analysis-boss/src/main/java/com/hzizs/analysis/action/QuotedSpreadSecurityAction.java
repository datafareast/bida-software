package com.hzizs.analysis.action;


import javax.annotation.Resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hzizs.analysis.entity.QuotedSpreadSecurity;
import com.hzizs.analysis.query.QuotedSpreadSecurityQuery;
import com.hzizs.analysis.service.QuotedSpreadSecurityService;
import com.hzizs.pojo.ErrorInfo;
import com.hzizs.vo.Container;
/**
 * 证券报价价差控制器
 * @author crazy_cabbage
 *
 */
@RestController
@RequestMapping("/quotedSpreadSecurity")
public class QuotedSpreadSecurityAction {
	@Resource
	private QuotedSpreadSecurityService quotedSpreadSecurityService;

	@GetMapping("/list")
	public ErrorInfo list(QuotedSpreadSecurityQuery query ) {
		Container<QuotedSpreadSecurity> container = quotedSpreadSecurityService.findContainer(query);
		return ErrorInfo.ok(container);
	}

	@GetMapping("/find")
	public ErrorInfo find(Long id) {
		QuotedSpreadSecurity quotedSpreadSecurity=quotedSpreadSecurityService.findById(id);
		return ErrorInfo.ok(quotedSpreadSecurity);
	}

 

	@PostMapping("/del")
	public ErrorInfo del(Long id) {
		quotedSpreadSecurityService.deleteById(id);
		return ErrorInfo.ok();
	}

	@PostMapping("/save")
	public ErrorInfo save(QuotedSpreadSecurity quotedSpreadSecurity) {
		quotedSpreadSecurityService.save(quotedSpreadSecurity);
		return ErrorInfo.ok();
	}

	@PostMapping("/update")
	public ErrorInfo update(QuotedSpreadSecurity quotedSpreadSecurity) {
		quotedSpreadSecurityService.update(quotedSpreadSecurity);
		return ErrorInfo.ok();
	}
}
