package com.hzizs.analysis.action;


import javax.annotation.Resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hzizs.analysis.entity.AutocorrelationSecurity;
import com.hzizs.analysis.query.AutocorrelationSecurityQuery;
import com.hzizs.analysis.service.AutocorrelationSecurityService;
import com.hzizs.pojo.ErrorInfo;
import com.hzizs.vo.Container;
/**
 * 证券自相关控制器
 * @author crazy_cabbage
 *
 */
@RestController
@RequestMapping("/autocorrelationSecurity")
public class AutocorrelationSecurityAction {
	@Resource
	private AutocorrelationSecurityService autocorrelationSecurityService;

	@GetMapping("/list")
	public ErrorInfo list(AutocorrelationSecurityQuery query ) {
		Container<AutocorrelationSecurity> container = autocorrelationSecurityService.findContainer(query);
		return ErrorInfo.ok(container);
	}

	@GetMapping("/find")
	public ErrorInfo find(Long id) {
		AutocorrelationSecurity autocorrelationSecurity=autocorrelationSecurityService.findById(id);
		return ErrorInfo.ok(autocorrelationSecurity);
	}

 

	@PostMapping("/del")
	public ErrorInfo del(Long id) {
		autocorrelationSecurityService.deleteById(id);
		return ErrorInfo.ok();
	}

	@PostMapping("/save")
	public ErrorInfo save(AutocorrelationSecurity autocorrelationSecurity) {
		autocorrelationSecurityService.save(autocorrelationSecurity);
		return ErrorInfo.ok();
	}

	@PostMapping("/update")
	public ErrorInfo update(AutocorrelationSecurity autocorrelationSecurity) {
		autocorrelationSecurityService.update(autocorrelationSecurity);
		return ErrorInfo.ok();
	}
}
