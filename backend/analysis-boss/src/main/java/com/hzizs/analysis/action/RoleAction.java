package com.hzizs.analysis.action;


import java.util.List;
import javax.annotation.Resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.hzizs.analysis.dto.RoleDto;
import com.hzizs.analysis.entity.Role;
import com.hzizs.analysis.facade.RoleFacade;
import com.hzizs.analysis.query.RoleQuery;
import com.hzizs.analysis.service.RoleService;
import com.hzizs.pojo.ErrorInfo;
import com.hzizs.vo.Container;
/**
 * 角色控制器
 * @author crazy_cabbage
 *
 */
@RestController
@RequestMapping("/role")
public class RoleAction {
	@Resource
	private RoleService roleService;
	
	@Resource
	private RoleFacade roleFacade;

	@GetMapping("/list")
	public ErrorInfo list(RoleQuery query ) {
		Container<Role> container = roleService.findContainer(query);
		return ErrorInfo.ok(container);
	}

	@GetMapping("/find")
	public ErrorInfo find(Long id) {
		RoleDto dto=roleFacade.findById(id);
		return ErrorInfo.ok(dto);
	}

	@GetMapping("/findAll")
    public ErrorInfo findAll() {
      List<Role> roles = roleService.findAll();
      return ErrorInfo.ok(roles);
    }

	@PostMapping("/del")
	public ErrorInfo del(Long[] id) {
		roleFacade.deleteByIds(id);
		return ErrorInfo.ok();
	}

	@PostMapping("/save")
	public ErrorInfo save(RoleDto dto) {
		roleFacade.save(dto);
		return ErrorInfo.ok();
	}

	@PostMapping("/update")
	public ErrorInfo update(RoleDto dto) {
		roleFacade.update(dto);
		return ErrorInfo.ok();
	}
}
