package com.hzizs.analysis.action;


import java.util.List;
import javax.annotation.Resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hzizs.analysis.entity.Market;
import com.hzizs.analysis.query.MarketQuery;
import com.hzizs.analysis.service.MarketService;
import com.hzizs.pojo.ErrorInfo;
import com.hzizs.vo.Container;
/**
 * 市场控制器
 * @author crazy_cabbage
 *
 */
@RestController
@RequestMapping("/market")
public class MarketAction {
	@Resource
	private MarketService marketService;

	@GetMapping("/list")
	public ErrorInfo list(MarketQuery query ) {
		Container<Market> container = marketService.findContainer(query);
		return ErrorInfo.ok(container);
	}

	@GetMapping("/find")
	public ErrorInfo find(Long id) {
		Market market=marketService.findById(id);
		return ErrorInfo.ok(market);
	}

	@GetMapping("/findAll")
   public ErrorInfo findAll() {
     List<Market> markets = marketService.findAll();
     return ErrorInfo.ok(markets);
   }

	@PostMapping("/del")
	public ErrorInfo del(Long id) {
		marketService.deleteById(id);
		return ErrorInfo.ok();
	}

	@PostMapping("/save")
	public ErrorInfo save(Market market) {
		marketService.save(market);
		return ErrorInfo.ok();
	}

	@PostMapping("/update")
	public ErrorInfo update(Market market) {
		marketService.update(market);
		return ErrorInfo.ok();
	}
}
