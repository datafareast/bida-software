package com.hzizs.analysis.action;


import java.util.List;
import javax.annotation.Resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hzizs.analysis.entity.DataIndustry;
import com.hzizs.analysis.query.DataIndustryQuery;
import com.hzizs.analysis.service.DataIndustryService;
import com.hzizs.pojo.ErrorInfo;
import com.hzizs.vo.Container;
/**
 * 数据行业控制器
 * @author crazy_cabbage
 *
 */
@RestController
@RequestMapping("/dataIndustry")
public class DataIndustryAction {
	@Resource
	private DataIndustryService dataIndustryService;

	@GetMapping("/list")
	public ErrorInfo list(DataIndustryQuery query ) {
		Container<DataIndustry> container = dataIndustryService.findContainer(query);
		return ErrorInfo.ok(container);
	}
	  @GetMapping("/findAll")
	  public ErrorInfo findAll() {
	    List<DataIndustry> dataIndustries = dataIndustryService.findAll();
	    return ErrorInfo.ok(dataIndustries);
	  }

	@GetMapping("/find")
	public ErrorInfo find(Long id) {
		DataIndustry dataIndustry=dataIndustryService.findById(id);
		return ErrorInfo.ok(dataIndustry);
	}

 

	@PostMapping("/del")
	public ErrorInfo del(Long id) {
		dataIndustryService.deleteById(id);
		return ErrorInfo.ok();
	}

	@PostMapping("/save")
	public ErrorInfo save(DataIndustry dataIndustry) {
		dataIndustryService.save(dataIndustry);
		return ErrorInfo.ok();
	}

	@PostMapping("/update")
	public ErrorInfo update(DataIndustry dataIndustry) {
		dataIndustryService.update(dataIndustry);
		return ErrorInfo.ok();
	}
}
