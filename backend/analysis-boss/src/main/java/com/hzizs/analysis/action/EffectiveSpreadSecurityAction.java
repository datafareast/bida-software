package com.hzizs.analysis.action;


import javax.annotation.Resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hzizs.analysis.entity.EffectiveSpreadSecurity;
import com.hzizs.analysis.query.EffectiveSpreadSecurityQuery;
import com.hzizs.analysis.service.EffectiveSpreadSecurityService;
import com.hzizs.pojo.ErrorInfo;
import com.hzizs.vo.Container;
/**
 * 证券有效价差控制器
 * @author crazy_cabbage
 *
 */
@RestController
@RequestMapping("/effectiveSpreadSecurity")
public class EffectiveSpreadSecurityAction {
	@Resource
	private EffectiveSpreadSecurityService effectiveSpreadSecurityService;

	@GetMapping("/list")
	public ErrorInfo list(EffectiveSpreadSecurityQuery query ) {
		Container<EffectiveSpreadSecurity> container = effectiveSpreadSecurityService.findContainer(query);
		return ErrorInfo.ok(container);
	}

	@GetMapping("/find")
	public ErrorInfo find(Long id) {
		EffectiveSpreadSecurity effectiveSpreadSecurity=effectiveSpreadSecurityService.findById(id);
		return ErrorInfo.ok(effectiveSpreadSecurity);
	}

 

	@PostMapping("/del")
	public ErrorInfo del(Long id) {
		effectiveSpreadSecurityService.deleteById(id);
		return ErrorInfo.ok();
	}

	@PostMapping("/save")
	public ErrorInfo save(EffectiveSpreadSecurity effectiveSpreadSecurity) {
		effectiveSpreadSecurityService.save(effectiveSpreadSecurity);
		return ErrorInfo.ok();
	}

	@PostMapping("/update")
	public ErrorInfo update(EffectiveSpreadSecurity effectiveSpreadSecurity) {
		effectiveSpreadSecurityService.update(effectiveSpreadSecurity);
		return ErrorInfo.ok();
	}
}
