package com.hzizs.analysis.action;


import javax.annotation.Resource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.hzizs.ActionContext;
import com.hzizs.analysis.dto.UserInfoDto;
import com.hzizs.analysis.entity.UserInfo;
import com.hzizs.analysis.facade.UserFacade;
import com.hzizs.analysis.query.UserQuery;
import com.hzizs.analysis.service.UserInfoService;
import com.hzizs.constants.Enabled;
import com.hzizs.pojo.ErrorInfo;
import com.hzizs.vo.Container;
/**
 * 用户控制器
 * @author crazy_cabbage
 *
 */
@RestController
@RequestMapping("/user")
public class UserAction {
	@Resource
	private UserFacade userFacade;
	@Resource
	private UserInfoService userInfoService;

	@GetMapping("/list")
	public ErrorInfo list(UserQuery query ) {
	  if(query==null) {
	    query = new UserQuery();
	  }
	  query.setIsDel(Enabled.F);
	  query.setIsManager(Enabled.F);
		Container<UserInfo> container = userInfoService.findContainer(query);
		return ErrorInfo.ok(container);
	}

	@GetMapping("/find")
	public ErrorInfo find(Long id) {
		UserInfoDto dto=userFacade.findById(id);
		return ErrorInfo.ok(dto);
	}
	 @GetMapping("/info")
	  public ErrorInfo info() {
	    Long userId = ActionContext.getUserId();
	    UserInfo userInfo = userInfoService.findById(userId);
	    return ErrorInfo.ok(userInfo);
	  }

	  @PostMapping("/updateOwner")
	  public ErrorInfo updateOwner(UserInfo userInfo) {
	    Long userId = ActionContext.getUserId();
	    userInfo.setId(userId);
	    userInfoService.updateInfo2(userInfo);
	    return ErrorInfo.ok();
	  }

	  @PostMapping("/resetPassword")
	  public ErrorInfo resetPassword(Long id, String password) {
	    userFacade.updateResetPassword(id, password);
	    return ErrorInfo.ok();
	  }

	  @PostMapping("/del")
	  public ErrorInfo del(Long[] id) {
	    userFacade.deleteByIds(id);
	    return ErrorInfo.ok();
	  }

	  @PostMapping("/save")
	  public ErrorInfo save(UserInfoDto dto) {
	    userFacade.save(dto);
	    return ErrorInfo.ok();
	  }

	  @PostMapping("/update")
	  public ErrorInfo update(UserInfoDto dto) {
	    userFacade.update(dto);
	    return ErrorInfo.ok();
	  }

 

}
