package com.hzizs.analysis.action;


import java.util.List;
import javax.annotation.Resource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.hzizs.analysis.entity.ActivityIndex;
import com.hzizs.analysis.query.ActivityIndexQuery;
import com.hzizs.analysis.query.DataQuery;
import com.hzizs.analysis.service.ActivityIndexService;
import com.hzizs.pojo.ErrorInfo;
import com.hzizs.vo.Container;
/**
 * 指数指标控制器
 * @author crazy_cabbage
 *
 */
@RestController
@RequestMapping("/activityIndex")
public class ActivityIndexAction {
	@Resource
	private ActivityIndexService activityIndexService;

	@GetMapping("/list")
	public ErrorInfo list(ActivityIndexQuery query ) {
		Container<ActivityIndex> container = activityIndexService.findContainer(query);
		return ErrorInfo.ok(container);
	}
	@GetMapping("/findData")
	public ErrorInfo findData(DataQuery query) {
	  List<ActivityIndex> activityIndexes = activityIndexService.find(query.queryParams());
	  return ErrorInfo.ok(activityIndexes);
	}
	@GetMapping("/find")
	public ErrorInfo find(Long id) {
		ActivityIndex activityIndex=activityIndexService.findById(id);
		return ErrorInfo.ok(activityIndex);
	}

 

	@PostMapping("/del")
	public ErrorInfo del(Long id) {
		activityIndexService.deleteById(id);
		return ErrorInfo.ok();
	}

	@PostMapping("/save")
	public ErrorInfo save(ActivityIndex activityIndex) {
		activityIndexService.save(activityIndex);
		return ErrorInfo.ok();
	}

	@PostMapping("/update")
	public ErrorInfo update(ActivityIndex activityIndex) {
		activityIndexService.update(activityIndex);
		return ErrorInfo.ok();
	}
}
