package com.hzizs.analysis.action;


import java.util.List;
import javax.annotation.Resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.hzizs.analysis.dto.DataTopoDto;
import com.hzizs.analysis.entity.DataTopo;
import com.hzizs.analysis.facade.DataTopoFacade;
import com.hzizs.analysis.query.DataTopoQuery;
import com.hzizs.analysis.service.DataTopoService;
import com.hzizs.pojo.ErrorInfo;
import com.hzizs.vo.Container;

/**
 * 数据拓普图控制器
 * 
 * @author crazy_cabbage
 *
 */
@RestController
@RequestMapping("/dataTopo")
public class DataTopoAction {
  @Resource
  private DataTopoService dataTopoService;
  
  @Resource
  private DataTopoFacade dataTopoFacade;

  @GetMapping("/list")
  public ErrorInfo list(DataTopoQuery query) {
    Container<DataTopo> container = dataTopoService.findContainer(query);
    return ErrorInfo.ok(container);
  }

  @GetMapping("/find")
  public ErrorInfo find(Long id) {
    DataTopo dataTop = dataTopoService.findById(id);
    return ErrorInfo.ok(dataTop);
  }
  @GetMapping("/findShow")
  public ErrorInfo findShow(Long dataTypeId) {
    List<DataTopo> dataTopos = dataTopoService.findShowByDataTypeId(dataTypeId);
    return ErrorInfo.ok(dataTopos);
    
  }
  @GetMapping("/findAll")
  public ErrorInfo findAll() {
    List<DataTopo> dataTops = dataTopoService.findAll();
    return ErrorInfo.ok(dataTops);
  }

  @GetMapping("/findDto")
  public ErrorInfo findDto(Long id) {
     DataTopoDto dto = dataTopoFacade.findDto(id);
     return ErrorInfo.ok(dto);
         
  }
  @PostMapping("/del")
  public ErrorInfo del(Long id) {
    dataTopoService.deleteById(id);
    return ErrorInfo.ok();
  }

  @PostMapping("/save")
  public ErrorInfo save(DataTopo dataTop) {
    dataTopoService.save(dataTop);
    return ErrorInfo.ok();
  }

  @PostMapping("/update")
  public ErrorInfo update(DataTopo dataTop) {
    dataTopoService.update(dataTop);
    return ErrorInfo.ok();
  }
}
