package com.hzizs.analysis.action;


import java.util.List;
import javax.annotation.Resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hzizs.analysis.entity.Menu;
import com.hzizs.analysis.facade.MenuFacade;
import com.hzizs.analysis.query.MenuQuery;
import com.hzizs.analysis.service.MenuService;
import com.hzizs.pojo.ErrorInfo;
import com.hzizs.vo.Container;
/**
 * 菜单控制器
 * @author crazy_cabbage
 *
 */
@RestController
@RequestMapping("/menu")
public class MenuAction {
	@Resource
	private MenuService menuService;
	@Resource
	private MenuFacade menuFacade;

	@GetMapping("/list")
	public ErrorInfo list(MenuQuery query ) {
		Container<Menu> container = menuService.findContainer(query);
		return ErrorInfo.ok(container);
	}

	@GetMapping("/find")
	public ErrorInfo find(Long id) {
		Menu menu=menuService.findById(id);
		return ErrorInfo.ok(menu);
	}

	@GetMapping("/findAll")
    public ErrorInfo findAll() {
      List<Menu> menus = menuService.findAll();
      return ErrorInfo.ok(menus);
    }

	@PostMapping("/del")
	public ErrorInfo del(Long[] id) {
		menuFacade.deleteByIds(id);
		return ErrorInfo.ok();
	}

	@PostMapping("/save")
	public ErrorInfo save(Menu menu) {
		menuService.save(menu);
		return ErrorInfo.ok();
	}

	@PostMapping("/update")
	public ErrorInfo update(Menu menu) {
		menuService.update(menu);
		return ErrorInfo.ok();
	}
}
