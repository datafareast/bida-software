package com.hzizs.analysis.action;


import java.util.List;
import javax.annotation.Resource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.hzizs.ActionContext;
import com.hzizs.analysis.constants.Constants.FolderCat;
import com.hzizs.analysis.entity.Folder;
import com.hzizs.analysis.facade.FolderFacade;
import com.hzizs.analysis.query.FolderQuery;
import com.hzizs.analysis.service.FolderService;
import com.hzizs.pojo.ErrorInfo;
import com.hzizs.vo.Container;

/**
 * 文件夹控制器
 * 
 * @author crazy_cabbage
 *
 */
@RestController
@RequestMapping("/folder")
public class FolderAction {
  @Resource
  private FolderService folderService;
  @Resource
  private FolderFacade folderFacade;

  @GetMapping("/list")
  public ErrorInfo list(FolderQuery query) {
    Long userId = ActionContext.getUserId();
    Container<Folder> container = folderService.findContainer(query, userId);
    return ErrorInfo.ok(container);
  }

  @GetMapping("/find")
  public ErrorInfo find(Long id) {
    Long userId = ActionContext.getUserId();
    Folder folder = folderService.findById(id, userId);
    return ErrorInfo.ok(folder);
  }

  @GetMapping("/findAll")
  public ErrorInfo findAll() {
    Long userId = ActionContext.getUserId();
    List<Folder> folders = folderService.findAll(userId);
    if (folders.isEmpty()) {
      folderService.saveDefault(userId);
      folders = folderService.findAll(userId);
    }
    return ErrorInfo.ok(folders);
  }



  @PostMapping("/del")
  public ErrorInfo del(Long[] id) {
    Long userId = ActionContext.getUserId();
    folderFacade.deleteByIds(id, userId);
    return ErrorInfo.ok();
  }

  @PostMapping("/save")
  public ErrorInfo save(Folder folder) {
    Long userId = ActionContext.getUserId();
    folder.setUserId(userId);
    folder.setCat(FolderCat.DIY);
    folderService.save(folder, userId);
    return ErrorInfo.ok();
  }

  @PostMapping("/update")
  public ErrorInfo update(Folder folder) {
    Long userId = ActionContext.getUserId();
    folderService.update(folder, userId);
    return ErrorInfo.ok();
  }
}
