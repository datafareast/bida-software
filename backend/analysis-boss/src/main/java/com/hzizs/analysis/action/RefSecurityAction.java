package com.hzizs.analysis.action;


import javax.annotation.Resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hzizs.analysis.entity.RefSecurity;
import com.hzizs.analysis.query.RefSecurityQuery;
import com.hzizs.analysis.service.RefSecurityService;
import com.hzizs.pojo.ErrorInfo;
import com.hzizs.vo.Container;
/**
 * 证券参考数据控制器
 * @author crazy_cabbage
 *
 */
@RestController
@RequestMapping("/refSecurity")
public class RefSecurityAction {
	@Resource
	private RefSecurityService refSecurityService;

	@GetMapping("/list")
	public ErrorInfo list(RefSecurityQuery query ) {
		Container<RefSecurity> container = refSecurityService.findContainer(query);
		return ErrorInfo.ok(container);
	}

	@GetMapping("/find")
	public ErrorInfo find(Long id) {
		RefSecurity refSecurity=refSecurityService.findById(id);
		return ErrorInfo.ok(refSecurity);
	}

 

	@PostMapping("/del")
	public ErrorInfo del(Long id) {
		refSecurityService.deleteById(id);
		return ErrorInfo.ok();
	}

	@PostMapping("/save")
	public ErrorInfo save(RefSecurity refSecurity) {
		refSecurityService.save(refSecurity);
		return ErrorInfo.ok();
	}

	@PostMapping("/update")
	public ErrorInfo update(RefSecurity refSecurity) {
		refSecurityService.update(refSecurity);
		return ErrorInfo.ok();
	}
}
