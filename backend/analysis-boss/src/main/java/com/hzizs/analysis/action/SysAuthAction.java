package com.hzizs.analysis.action;


import javax.annotation.Resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hzizs.analysis.entity.SysAuth;
import com.hzizs.analysis.query.SysAuthQuery;
import com.hzizs.analysis.service.SysAuthService;
import com.hzizs.pojo.ErrorInfo;
import com.hzizs.vo.Container;
/**
 * 系统权限控制器
 * @author crazy_cabbage
 *
 */
@RestController
@RequestMapping("/sysAuth")
public class SysAuthAction {
	@Resource
	private SysAuthService sysAuthService;

	@GetMapping("/list")
	public ErrorInfo list(SysAuthQuery query ) {
		Container<SysAuth> container = sysAuthService.findContainer(query);
		return ErrorInfo.ok(container);
	}

	@GetMapping("/find")
	public ErrorInfo find(Long id) {
		SysAuth sysAuth=sysAuthService.findById(id);
		return ErrorInfo.ok(sysAuth);
	}

 

	@PostMapping("/del")
	public ErrorInfo del(Long id) {
		sysAuthService.deleteById(id);
		return ErrorInfo.ok();
	}

	@PostMapping("/save")
	public ErrorInfo save(SysAuth sysAuth) {
		sysAuthService.save(sysAuth);
		return ErrorInfo.ok();
	}

	@PostMapping("/update")
	public ErrorInfo update(SysAuth sysAuth) {
		sysAuthService.update(sysAuth);
		return ErrorInfo.ok();
	}
}
