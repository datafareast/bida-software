package com.hzizs.analysis.action;


import javax.annotation.Resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hzizs.analysis.entity.SysUser;
import com.hzizs.analysis.query.SysUserQuery;
import com.hzizs.analysis.service.SysUserService;
import com.hzizs.pojo.ErrorInfo;
import com.hzizs.vo.Container;
/**
 * 系统用户控制器
 * @author crazy_cabbage
 *
 */
@RestController
@RequestMapping("/sysUser")
public class SysUserAction {
	@Resource
	private SysUserService sysUserService;

	@GetMapping("/list")
	public ErrorInfo list(SysUserQuery query ) {
		Container<SysUser> container = sysUserService.findContainer(query);
		return ErrorInfo.ok(container);
	}

	@GetMapping("/find")
	public ErrorInfo find(Long id) {
		SysUser sysUser=sysUserService.findById(id);
		return ErrorInfo.ok(sysUser);
	}

 

	@PostMapping("/del")
	public ErrorInfo del(Long id) {
		sysUserService.deleteById(id);
		return ErrorInfo.ok();
	}

	@PostMapping("/save")
	public ErrorInfo save(SysUser sysUser) {
		sysUserService.save(sysUser);
		return ErrorInfo.ok();
	}

	@PostMapping("/update")
	public ErrorInfo update(SysUser sysUser) {
		sysUserService.update(sysUser);
		return ErrorInfo.ok();
	}
}
