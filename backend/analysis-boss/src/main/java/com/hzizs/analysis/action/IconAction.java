package com.hzizs.analysis.action;


import javax.annotation.Resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hzizs.analysis.entity.Icon;
import com.hzizs.analysis.query.IconQuery;
import com.hzizs.analysis.service.IconService;
import com.hzizs.pojo.ErrorInfo;
import com.hzizs.vo.Container;
/**
 * 图标库控制器
 * @author crazy_cabbage
 *
 */
@RestController
@RequestMapping("/icon")
public class IconAction {
	@Resource
	private IconService iconService;

	@GetMapping("/list")
	public ErrorInfo list(IconQuery query ) {
		Container<Icon> container = iconService.findContainer(query);
		return ErrorInfo.ok(container);
	}

	@GetMapping("/find")
	public ErrorInfo find(Long id) {
		Icon icon=iconService.findById(id);
		return ErrorInfo.ok(icon);
	}

 

	@PostMapping("/del")
	public ErrorInfo del(Long id) {
		iconService.deleteById(id);
		return ErrorInfo.ok();
	}

	@PostMapping("/save")
	public ErrorInfo save(Icon icon) {
		iconService.save(icon);
		return ErrorInfo.ok();
	}

	@PostMapping("/update")
	public ErrorInfo update(Icon icon) {
		iconService.update(icon);
		return ErrorInfo.ok();
	}
}
