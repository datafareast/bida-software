package com.hzizs.analysis.action;


import javax.annotation.Resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hzizs.analysis.entity.EffectiveSpreadMarket;
import com.hzizs.analysis.query.EffectiveSpreadMarketQuery;
import com.hzizs.analysis.service.EffectiveSpreadMarketService;
import com.hzizs.pojo.ErrorInfo;
import com.hzizs.vo.Container;
/**
 * 市场有效价差控制器
 * @author crazy_cabbage
 *
 */
@RestController
@RequestMapping("/effectiveSpreadMarket")
public class EffectiveSpreadMarketAction {
	@Resource
	private EffectiveSpreadMarketService effectiveSpreadMarketService;

	@GetMapping("/list")
	public ErrorInfo list(EffectiveSpreadMarketQuery query ) {
		Container<EffectiveSpreadMarket> container = effectiveSpreadMarketService.findContainer(query);
		return ErrorInfo.ok(container);
	}

	@GetMapping("/find")
	public ErrorInfo find(Long id) {
		EffectiveSpreadMarket effectiveSpreadMarket=effectiveSpreadMarketService.findById(id);
		return ErrorInfo.ok(effectiveSpreadMarket);
	}

 

	@PostMapping("/del")
	public ErrorInfo del(Long id) {
		effectiveSpreadMarketService.deleteById(id);
		return ErrorInfo.ok();
	}

	@PostMapping("/save")
	public ErrorInfo save(EffectiveSpreadMarket effectiveSpreadMarket) {
		effectiveSpreadMarketService.save(effectiveSpreadMarket);
		return ErrorInfo.ok();
	}

	@PostMapping("/update")
	public ErrorInfo update(EffectiveSpreadMarket effectiveSpreadMarket) {
		effectiveSpreadMarketService.update(effectiveSpreadMarket);
		return ErrorInfo.ok();
	}
}
