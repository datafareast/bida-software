package com.hzizs.analysis.action;


import javax.annotation.Resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hzizs.analysis.entity.Security;
import com.hzizs.analysis.query.SecurityQuery;
import com.hzizs.analysis.service.SecurityService;
import com.hzizs.pojo.ErrorInfo;
import com.hzizs.vo.Container;
/**
 * 证券控制器
 * @author crazy_cabbage
 *
 */
@RestController
@RequestMapping("/security")
public class SecurityAction {
	@Resource
	private SecurityService securityService;

	@GetMapping("/list")
	public ErrorInfo list(SecurityQuery query ) {
		Container<Security> container = securityService.findContainer(query);
		return ErrorInfo.ok(container);
	}

	@GetMapping("/find")
	public ErrorInfo find(Long id) {
		Security security=securityService.findById(id);
		return ErrorInfo.ok(security);
	}

 

	@PostMapping("/del")
	public ErrorInfo del(Long id) {
		securityService.deleteById(id);
		return ErrorInfo.ok();
	}

	@PostMapping("/save")
	public ErrorInfo save(Security security) {
		securityService.save(security);
		return ErrorInfo.ok();
	}

	@PostMapping("/update")
	public ErrorInfo update(Security security) {
		securityService.update(security);
		return ErrorInfo.ok();
	}
}
