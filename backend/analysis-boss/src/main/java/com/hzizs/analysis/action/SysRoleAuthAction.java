package com.hzizs.analysis.action;


import javax.annotation.Resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hzizs.analysis.entity.SysRoleAuth;
import com.hzizs.analysis.query.SysRoleAuthQuery;
import com.hzizs.analysis.service.SysRoleAuthService;
import com.hzizs.pojo.ErrorInfo;
import com.hzizs.vo.Container;
/**
 * 系统角色权限控制器
 * @author crazy_cabbage
 *
 */
@RestController
@RequestMapping("/sysRoleAuth")
public class SysRoleAuthAction {
	@Resource
	private SysRoleAuthService sysRoleAuthService;

	@GetMapping("/list")
	public ErrorInfo list(SysRoleAuthQuery query ) {
		Container<SysRoleAuth> container = sysRoleAuthService.findContainer(query);
		return ErrorInfo.ok(container);
	}

	@GetMapping("/find")
	public ErrorInfo find(Long id) {
		SysRoleAuth sysRoleAuth=sysRoleAuthService.findById(id);
		return ErrorInfo.ok(sysRoleAuth);
	}

 

	@PostMapping("/del")
	public ErrorInfo del(Long id) {
		sysRoleAuthService.deleteById(id);
		return ErrorInfo.ok();
	}

	@PostMapping("/save")
	public ErrorInfo save(SysRoleAuth sysRoleAuth) {
		sysRoleAuthService.save(sysRoleAuth);
		return ErrorInfo.ok();
	}

	@PostMapping("/update")
	public ErrorInfo update(SysRoleAuth sysRoleAuth) {
		sysRoleAuthService.update(sysRoleAuth);
		return ErrorInfo.ok();
	}
}
