package com.hzizs.analysis.action;


import javax.annotation.Resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hzizs.analysis.entity.SecIndex;
import com.hzizs.analysis.query.SecIndexQuery;
import com.hzizs.analysis.service.SecIndexService;
import com.hzizs.pojo.ErrorInfo;
import com.hzizs.vo.Container;

/**
 * 常用指标控制器
 * 
 * @author crazy_cabbage
 *
 */
@RestController
@RequestMapping("/secIndex")
public class SecIndexAction {
  @Resource
  private SecIndexService secIndexService;

  @GetMapping("/list")
  public ErrorInfo list(SecIndexQuery query) {
    Container<SecIndex> container = secIndexService.findContainer(query);
    return ErrorInfo.ok(container);
  }

  @GetMapping("/find")
  public ErrorInfo find(Long id) {
    SecIndex secIndex = secIndexService.findById(id);
    return ErrorInfo.ok(secIndex);
  }



  @PostMapping("/del")
  public ErrorInfo del(Long id) {
    secIndexService.deleteById(id);
    return ErrorInfo.ok();
  }

  @PostMapping("/save")
  public ErrorInfo save(SecIndex secIndex) {
    secIndexService.save(secIndex);
    return ErrorInfo.ok();
  }

  @PostMapping("/update")
  public ErrorInfo update(SecIndex secIndex) {
    secIndexService.update(secIndex);
    return ErrorInfo.ok();
  }
}
