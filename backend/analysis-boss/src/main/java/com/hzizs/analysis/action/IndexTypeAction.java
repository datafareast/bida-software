package com.hzizs.analysis.action;


import java.util.List;
import javax.annotation.Resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.hzizs.analysis.dto.IndexDto;
import com.hzizs.analysis.entity.IndexType;
import com.hzizs.analysis.facade.IndexTypeFacade;
import com.hzizs.analysis.query.DataQuery;
import com.hzizs.analysis.query.IndexTypeQuery;
import com.hzizs.analysis.service.IndexTypeService;
import com.hzizs.pojo.ErrorInfo;
import com.hzizs.vo.Container;

/**
 * 指标类型控制器
 * 
 * @author crazy_cabbage
 *
 */
@RestController
@RequestMapping("/indexType")
public class IndexTypeAction {
  @Resource
  private IndexTypeService indexTypeService;
  @Resource
  private IndexTypeFacade indexTypeFacade;

  @GetMapping("/list")
  public ErrorInfo list(IndexTypeQuery query) {
    Container<IndexType> container = indexTypeService.findContainer(query);
    return ErrorInfo.ok(container);
  }

  @GetMapping("/find")
  public ErrorInfo find(Long id) {
    IndexType indexType = indexTypeService.findById(id);
    return ErrorInfo.ok(indexType);
  }

  @GetMapping("/findEnabled")
  public ErrorInfo findEnabled() {
    List<IndexType> indexTypes = indexTypeService.findEnabled();
    return ErrorInfo.ok(indexTypes);
  }

  @GetMapping("/findData")
  public ErrorInfo findData(DataQuery query) {
    IndexDto dto = indexTypeFacade.findData(query);
    return ErrorInfo.ok(dto);
  }

  @PostMapping("/del")
  public ErrorInfo del(Long id) {
    indexTypeService.deleteById(id);
    return ErrorInfo.ok();
  }

  @PostMapping("/save")
  public ErrorInfo save(IndexType indexType) {
    indexTypeService.save(indexType);
    return ErrorInfo.ok();
  }

  @PostMapping("/update")
  public ErrorInfo update(IndexType indexType) {
    indexTypeService.update(indexType);
    return ErrorInfo.ok();
  }
}
