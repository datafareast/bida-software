package com.hzizs.analysis.action;


import javax.annotation.Resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hzizs.analysis.entity.SysRole;
import com.hzizs.analysis.query.SysRoleQuery;
import com.hzizs.analysis.service.SysRoleService;
import com.hzizs.pojo.ErrorInfo;
import com.hzizs.vo.Container;
/**
 * 系统角色控制器
 * @author crazy_cabbage
 *
 */
@RestController
@RequestMapping("/sysRole")
public class SysRoleAction {
	@Resource
	private SysRoleService sysRoleService;

	@GetMapping("/list")
	public ErrorInfo list(SysRoleQuery query ) {
		Container<SysRole> container = sysRoleService.findContainer(query);
		return ErrorInfo.ok(container);
	}

	@GetMapping("/find")
	public ErrorInfo find(Long id) {
		SysRole sysRole=sysRoleService.findById(id);
		return ErrorInfo.ok(sysRole);
	}

 

	@PostMapping("/del")
	public ErrorInfo del(Long id) {
		sysRoleService.deleteById(id);
		return ErrorInfo.ok();
	}

	@PostMapping("/save")
	public ErrorInfo save(SysRole sysRole) {
		sysRoleService.save(sysRole);
		return ErrorInfo.ok();
	}

	@PostMapping("/update")
	public ErrorInfo update(SysRole sysRole) {
		sysRoleService.update(sysRole);
		return ErrorInfo.ok();
	}
}
