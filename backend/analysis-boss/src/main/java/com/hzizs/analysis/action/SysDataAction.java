package com.hzizs.analysis.action;


import java.util.List;
import javax.annotation.Resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hzizs.analysis.entity.SysData;
import com.hzizs.analysis.service.SysDataService;
import com.hzizs.pojo.ErrorInfo;
/**
 * 数据字典控制器
 * @author crazy_cabbage
 *
 */
@RestController
@RequestMapping("/sysData")
public class SysDataAction {
	@Resource
	private SysDataService sysDataService;
    
    @GetMapping("/findByParentCode")
    public ErrorInfo findByParentCode(String parentCode) {
      List<SysData> sysDatas = sysDataService.findByParentCode(parentCode);
      return ErrorInfo.ok(sysDatas);
    }

}
