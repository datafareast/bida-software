package com.hzizs.analysis.action;


import java.util.List;
import javax.annotation.Resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hzizs.analysis.entity.IndustryIndex;
import com.hzizs.analysis.query.IndustryIndexQuery;
import com.hzizs.analysis.service.IndustryIndexService;
import com.hzizs.pojo.ErrorInfo;
import com.hzizs.vo.Container;
/**
 * 行业指标控制器
 * @author crazy_cabbage
 *
 */
@RestController
@RequestMapping("/industryIndex")
public class IndustryIndexAction {
	@Resource
	private IndustryIndexService industryIndexService;

	@GetMapping("/list")
	public ErrorInfo list(IndustryIndexQuery query ) {
		Container<IndustryIndex> container = industryIndexService.findContainer(query);
		return ErrorInfo.ok(container);
	}

	@GetMapping("/find")
	public ErrorInfo find(Long id) {
		IndustryIndex industryIndex=industryIndexService.findById(id);
		return ErrorInfo.ok(industryIndex);
	}

	@GetMapping("/findAll")
	public ErrorInfo findAll() {
	  List<IndustryIndex> industryIndexes = industryIndexService.findAll();
	  return ErrorInfo.ok(industryIndexes);
	}

	@PostMapping("/del")
	public ErrorInfo del(Long id) {
		industryIndexService.deleteById(id);
		return ErrorInfo.ok();
	}

	@PostMapping("/save")
	public ErrorInfo save(IndustryIndex industryIndex) {
		industryIndexService.save(industryIndex);
		return ErrorInfo.ok();
	}

	@PostMapping("/update")
	public ErrorInfo update(IndustryIndex industryIndex) {
		industryIndexService.update(industryIndex);
		return ErrorInfo.ok();
	}
}
