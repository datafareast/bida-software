package com.hzizs.analysis.action;


import java.util.List;
import javax.annotation.Resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hzizs.analysis.entity.SecExchange;
import com.hzizs.analysis.query.SecExchangeQuery;
import com.hzizs.analysis.service.SecExchangeService;
import com.hzizs.pojo.ErrorInfo;
import com.hzizs.vo.Container;

/**
 * 交易所控制器
 * 
 * @author crazy_cabbage
 *
 */
@RestController
@RequestMapping("/secExchange")
public class SecExchangeAction {
  @Resource
  private SecExchangeService secExchangeService;

  @GetMapping("/list")
  public ErrorInfo list(SecExchangeQuery query) {
    Container<SecExchange> container = secExchangeService.findContainer(query);
    return ErrorInfo.ok(container);
  }

  @GetMapping("/find")
  public ErrorInfo find(Long id) {
    SecExchange secExchange = secExchangeService.findById(id);
    return ErrorInfo.ok(secExchange);
  }

  @GetMapping("/findAll")
  public ErrorInfo findAll() {
    List<SecExchange> secExchanges = secExchangeService.findAll();
    return ErrorInfo.ok(secExchanges);
  }


  @PostMapping("/del")
  public ErrorInfo del(Long id) {
    secExchangeService.deleteById(id);
    return ErrorInfo.ok();
  }

  @PostMapping("/save")
  public ErrorInfo save(SecExchange secExchange) {
    secExchangeService.save(secExchange);
    return ErrorInfo.ok();
  }

  @PostMapping("/update")
  public ErrorInfo update(SecExchange secExchange) {
    secExchangeService.update(secExchange);
    return ErrorInfo.ok();
  }
}
