package com.hzizs.analysis.action;


import javax.annotation.Resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hzizs.analysis.entity.SysUserInfo;
import com.hzizs.analysis.query.SysUserInfoQuery;
import com.hzizs.analysis.service.SysUserInfoService;
import com.hzizs.pojo.ErrorInfo;
import com.hzizs.vo.Container;
/**
 * 系统用户信息控制器
 * @author crazy_cabbage
 *
 */
@RestController
@RequestMapping("/sysUserInfo")
public class SysUserInfoAction {
	@Resource
	private SysUserInfoService sysUserInfoService;

	@GetMapping("/list")
	public ErrorInfo list(SysUserInfoQuery query ) {
		Container<SysUserInfo> container = sysUserInfoService.findContainer(query);
		return ErrorInfo.ok(container);
	}

	@GetMapping("/find")
	public ErrorInfo find(Long id) {
		SysUserInfo sysUserInfo=sysUserInfoService.findById(id);
		return ErrorInfo.ok(sysUserInfo);
	}

 

	@PostMapping("/del")
	public ErrorInfo del(Long id) {
		sysUserInfoService.deleteById(id);
		return ErrorInfo.ok();
	}

	@PostMapping("/save")
	public ErrorInfo save(SysUserInfo sysUserInfo) {
		sysUserInfoService.save(sysUserInfo);
		return ErrorInfo.ok();
	}

	@PostMapping("/update")
	public ErrorInfo update(SysUserInfo sysUserInfo) {
		sysUserInfoService.update(sysUserInfo);
		return ErrorInfo.ok();
	}
}
