package com.hzizs.analysis.action;


import javax.annotation.Resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hzizs.analysis.entity.UserRole;
import com.hzizs.analysis.query.UserRoleQuery;
import com.hzizs.analysis.service.UserRoleService;
import com.hzizs.pojo.ErrorInfo;
import com.hzizs.vo.Container;
/**
 * 用户角色控制器
 * @author crazy_cabbage
 *
 */
@RestController
@RequestMapping("/userRole")
public class UserRoleAction {
	@Resource
	private UserRoleService userRoleService;

	@GetMapping("/list")
	public ErrorInfo list(UserRoleQuery query ) {
		Container<UserRole> container = userRoleService.findContainer(query);
		return ErrorInfo.ok(container);
	}

	@GetMapping("/find")
	public ErrorInfo find(Long id) {
		UserRole userRole=userRoleService.findById(id);
		return ErrorInfo.ok(userRole);
	}

 

	@PostMapping("/del")
	public ErrorInfo del(Long id) {
		userRoleService.deleteById(id);
		return ErrorInfo.ok();
	}

	@PostMapping("/save")
	public ErrorInfo save(UserRole userRole) {
		userRoleService.save(userRole);
		return ErrorInfo.ok();
	}

	@PostMapping("/update")
	public ErrorInfo update(UserRole userRole) {
		userRoleService.update(userRole);
		return ErrorInfo.ok();
	}
}
