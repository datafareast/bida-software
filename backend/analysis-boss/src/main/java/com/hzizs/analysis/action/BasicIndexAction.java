package com.hzizs.analysis.action;


import java.util.List;
import javax.annotation.Resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hzizs.analysis.entity.BasicIndex;
import com.hzizs.analysis.query.BasicIndexQuery;
import com.hzizs.analysis.service.BasicIndexService;
import com.hzizs.pojo.ErrorInfo;
import com.hzizs.vo.Container;

/**
 * 指数基指控制器
 * 
 * @author crazy_cabbage
 *
 */
@RestController
@RequestMapping("/basicIndex")
public class BasicIndexAction {
  @Resource
  private BasicIndexService basicIndexService;

  @GetMapping("/list")
  public ErrorInfo list(BasicIndexQuery query) {
    Container<BasicIndex> container = basicIndexService.findContainer(query);
    return ErrorInfo.ok(container);
  }

  @GetMapping("/find")
  public ErrorInfo find(Long id) {
    BasicIndex basicIndex = basicIndexService.findById(id);
    return ErrorInfo.ok(basicIndex);
  }

  @GetMapping("/findEnabled")
  public ErrorInfo findEnabled() {
    List<BasicIndex> basicIndexes = basicIndexService.findEnabledAll();
    return ErrorInfo.ok(basicIndexes);
  }



  @PostMapping("/del")
  public ErrorInfo del(Long id) {
    basicIndexService.deleteById(id);
    return ErrorInfo.ok();
  }

  @PostMapping("/save")
  public ErrorInfo save(BasicIndex basicIndex) {
    basicIndexService.save(basicIndex);
    return ErrorInfo.ok();
  }

  @PostMapping("/update")
  public ErrorInfo update(BasicIndex basicIndex) {
    basicIndexService.update(basicIndex);
    return ErrorInfo.ok();
  }
}
