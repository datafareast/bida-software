package com.hzizs.analysis.action;


import javax.annotation.Resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hzizs.analysis.entity.McapSecurity;
import com.hzizs.analysis.query.McapSecurityQuery;
import com.hzizs.analysis.service.McapSecurityService;
import com.hzizs.pojo.ErrorInfo;
import com.hzizs.vo.Container;
/**
 * 证券市值控制器
 * @author crazy_cabbage
 *
 */
@RestController
@RequestMapping("/mcapSecurity")
public class McapSecurityAction {
	@Resource
	private McapSecurityService mcapSecurityService;

	@GetMapping("/list")
	public ErrorInfo list(McapSecurityQuery query ) {
		Container<McapSecurity> container = mcapSecurityService.findContainer(query);
		return ErrorInfo.ok(container);
	}

	@GetMapping("/find")
	public ErrorInfo find(Long id) {
		McapSecurity mcapSecurity=mcapSecurityService.findById(id);
		return ErrorInfo.ok(mcapSecurity);
	}

 

	@PostMapping("/del")
	public ErrorInfo del(Long id) {
		mcapSecurityService.deleteById(id);
		return ErrorInfo.ok();
	}

	@PostMapping("/save")
	public ErrorInfo save(McapSecurity mcapSecurity) {
		mcapSecurityService.save(mcapSecurity);
		return ErrorInfo.ok();
	}

	@PostMapping("/update")
	public ErrorInfo update(McapSecurity mcapSecurity) {
		mcapSecurityService.update(mcapSecurity);
		return ErrorInfo.ok();
	}
}
