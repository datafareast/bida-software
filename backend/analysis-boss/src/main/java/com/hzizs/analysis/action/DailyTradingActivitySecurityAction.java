package com.hzizs.analysis.action;


import javax.annotation.Resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hzizs.analysis.entity.DailyTradingActivitySecurity;
import com.hzizs.analysis.query.DailyTradingActivitySecurityQuery;
import com.hzizs.analysis.service.DailyTradingActivitySecurityService;
import com.hzizs.pojo.ErrorInfo;
import com.hzizs.vo.Container;
/**
 * 证券日交易控制器
 * @author crazy_cabbage
 *
 */
@RestController
@RequestMapping("/dailyTradingActivitySecurity")
public class DailyTradingActivitySecurityAction {
	@Resource
	private DailyTradingActivitySecurityService dailyTradingActivitySecurityService;

	@GetMapping("/list")
	public ErrorInfo list(DailyTradingActivitySecurityQuery query ) {
		Container<DailyTradingActivitySecurity> container = dailyTradingActivitySecurityService.findContainer(query);
		return ErrorInfo.ok(container);
	}

	@GetMapping("/find")
	public ErrorInfo find(Long id) {
		DailyTradingActivitySecurity dailyTradingActivitySecurity=dailyTradingActivitySecurityService.findById(id);
		return ErrorInfo.ok(dailyTradingActivitySecurity);
	}

 

	@PostMapping("/del")
	public ErrorInfo del(Long id) {
		dailyTradingActivitySecurityService.deleteById(id);
		return ErrorInfo.ok();
	}

	@PostMapping("/save")
	public ErrorInfo save(DailyTradingActivitySecurity dailyTradingActivitySecurity) {
		dailyTradingActivitySecurityService.save(dailyTradingActivitySecurity);
		return ErrorInfo.ok();
	}

	@PostMapping("/update")
	public ErrorInfo update(DailyTradingActivitySecurity dailyTradingActivitySecurity) {
		dailyTradingActivitySecurityService.update(dailyTradingActivitySecurity);
		return ErrorInfo.ok();
	}
}
