package com.hzizs.analysis.action;


import javax.annotation.Resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hzizs.analysis.entity.Link;
import com.hzizs.analysis.facade.LinkFacade;
import com.hzizs.analysis.query.LinkQuery;
import com.hzizs.analysis.service.LinkService;
import com.hzizs.pojo.ErrorInfo;
import com.hzizs.vo.Container;

/**
 * 友情链接控制器
 * 
 * @author crazy_cabbage
 *
 */
@RestController
@RequestMapping("/link")
public class LinkAction {
  @Resource
  private LinkService linkService;
  @Resource
  private LinkFacade linkFacade;

  @GetMapping("/list")
  public ErrorInfo list(LinkQuery query) {
    Container<Link> container = linkService.findContainer(query);
    return ErrorInfo.ok(container);
  }

  @GetMapping("/find")
  public ErrorInfo find(Long id) {
    Link link = linkService.findById(id);
    return ErrorInfo.ok(link);
  }



  @PostMapping("/del")
  public ErrorInfo del(Long[] id) {
    linkFacade.deleteByIds(id);
    return ErrorInfo.ok();
  }

  @PostMapping("/save")
  public ErrorInfo save(Link link) {
    linkService.save(link);
    return ErrorInfo.ok();
  }

  @PostMapping("/update")
  public ErrorInfo update(Link link) {
    linkService.update(link);
    return ErrorInfo.ok();
  }
}
