package com.hzizs.analysis.action;


import java.util.List;
import javax.annotation.Resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hzizs.analysis.entity.DailyTradingActivityIndustry;
import com.hzizs.analysis.query.DailyTradingActivityIndustryQuery;
import com.hzizs.analysis.query.DataQuery;
import com.hzizs.analysis.service.DailyTradingActivityIndustryService;
import com.hzizs.pojo.ErrorInfo;
import com.hzizs.vo.Container;

/**
 * 行业日交易控制器
 * 
 * @author crazy_cabbage
 *
 */
@RestController
@RequestMapping("/dailyTradingActivityIndustry")
public class DailyTradingActivityIndustryAction {
  @Resource
  private DailyTradingActivityIndustryService dailyTradingActivityIndustryService;

  @GetMapping("/list")
  public ErrorInfo list(DailyTradingActivityIndustryQuery query) {
    Container<DailyTradingActivityIndustry> container = dailyTradingActivityIndustryService.findContainer(query);
    return ErrorInfo.ok(container);
  }

  @GetMapping("/find")
  public ErrorInfo find(Long id) {
    DailyTradingActivityIndustry dailyTradingActivityIndustry = dailyTradingActivityIndustryService.findById(id);
    return ErrorInfo.ok(dailyTradingActivityIndustry);
  }

  @GetMapping("/findData")
  public ErrorInfo findData(DataQuery query) {
    List<DailyTradingActivityIndustry> dailyTradingActivityIndustries = dailyTradingActivityIndustryService.find(query.getMarketId(),query.getStartTime(),query.getEndTime());
    return ErrorInfo.ok(dailyTradingActivityIndustries);
  }



  @PostMapping("/del")
  public ErrorInfo del(Long id) {
    dailyTradingActivityIndustryService.deleteById(id);
    return ErrorInfo.ok();
  }

  @PostMapping("/save")
  public ErrorInfo save(DailyTradingActivityIndustry dailyTradingActivityIndustry) {
    dailyTradingActivityIndustryService.save(dailyTradingActivityIndustry);
    return ErrorInfo.ok();
  }

  @PostMapping("/update")
  public ErrorInfo update(DailyTradingActivityIndustry dailyTradingActivityIndustry) {
    dailyTradingActivityIndustryService.update(dailyTradingActivityIndustry);
    return ErrorInfo.ok();
  }
}
