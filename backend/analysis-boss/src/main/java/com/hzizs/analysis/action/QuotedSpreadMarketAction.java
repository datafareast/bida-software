package com.hzizs.analysis.action;


import javax.annotation.Resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hzizs.analysis.entity.QuotedSpreadMarket;
import com.hzizs.analysis.query.QuotedSpreadMarketQuery;
import com.hzizs.analysis.service.QuotedSpreadMarketService;
import com.hzizs.pojo.ErrorInfo;
import com.hzizs.vo.Container;
/**
 * 市场报价价差控制器
 * @author crazy_cabbage
 *
 */
@RestController
@RequestMapping("/quotedSpreadMarket")
public class QuotedSpreadMarketAction {
	@Resource
	private QuotedSpreadMarketService quotedSpreadMarketService;

	@GetMapping("/list")
	public ErrorInfo list(QuotedSpreadMarketQuery query ) {
		Container<QuotedSpreadMarket> container = quotedSpreadMarketService.findContainer(query);
		return ErrorInfo.ok(container);
	}

	@GetMapping("/find")
	public ErrorInfo find(Long id) {
		QuotedSpreadMarket quotedSpreadMarket=quotedSpreadMarketService.findById(id);
		return ErrorInfo.ok(quotedSpreadMarket);
	}

 

	@PostMapping("/del")
	public ErrorInfo del(Long id) {
		quotedSpreadMarketService.deleteById(id);
		return ErrorInfo.ok();
	}

	@PostMapping("/save")
	public ErrorInfo save(QuotedSpreadMarket quotedSpreadMarket) {
		quotedSpreadMarketService.save(quotedSpreadMarket);
		return ErrorInfo.ok();
	}

	@PostMapping("/update")
	public ErrorInfo update(QuotedSpreadMarket quotedSpreadMarket) {
		quotedSpreadMarketService.update(quotedSpreadMarket);
		return ErrorInfo.ok();
	}
}
