package com.hzizs.analysis.action;


import javax.annotation.Resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hzizs.analysis.entity.RoleAuth;
import com.hzizs.analysis.query.RoleAuthQuery;
import com.hzizs.analysis.service.RoleAuthService;
import com.hzizs.pojo.ErrorInfo;
import com.hzizs.vo.Container;
/**
 * 角色权限控制器
 * @author crazy_cabbage
 *
 */
@RestController
@RequestMapping("/roleAuth")
public class RoleAuthAction {
	@Resource
	private RoleAuthService roleAuthService;

	@GetMapping("/list")
	public ErrorInfo list(RoleAuthQuery query ) {
		Container<RoleAuth> container = roleAuthService.findContainer(query);
		return ErrorInfo.ok(container);
	}

	@GetMapping("/find")
	public ErrorInfo find(Long id) {
		RoleAuth roleAuth=roleAuthService.findById(id);
		return ErrorInfo.ok(roleAuth);
	}

 

	@PostMapping("/del")
	public ErrorInfo del(Long id) {
		roleAuthService.deleteById(id);
		return ErrorInfo.ok();
	}

	@PostMapping("/save")
	public ErrorInfo save(RoleAuth roleAuth) {
		roleAuthService.save(roleAuth);
		return ErrorInfo.ok();
	}

	@PostMapping("/update")
	public ErrorInfo update(RoleAuth roleAuth) {
		roleAuthService.update(roleAuth);
		return ErrorInfo.ok();
	}
}
