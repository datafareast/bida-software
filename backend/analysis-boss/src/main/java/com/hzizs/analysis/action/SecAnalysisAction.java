package com.hzizs.analysis.action;


import javax.annotation.Resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hzizs.analysis.entity.SecAnalysis;
import com.hzizs.analysis.query.SecAnalysisQuery;
import com.hzizs.analysis.service.SecAnalysisService;
import com.hzizs.pojo.ErrorInfo;
import com.hzizs.vo.Container;

/**
 * 业绩分析控制器
 * 
 * @author crazy_cabbage
 *
 */
@RestController
@RequestMapping("/secAnalysis")
public class SecAnalysisAction {
  @Resource
  private SecAnalysisService secAnalysisService;

  @GetMapping("/list")
  public ErrorInfo list(SecAnalysisQuery query) {
    Container<SecAnalysis> container = secAnalysisService.findContainer(query);
    return ErrorInfo.ok(container);
  }

  @GetMapping("/find")
  public ErrorInfo find(Long id) {
    SecAnalysis secAnalysis = secAnalysisService.findById(id);
    return ErrorInfo.ok(secAnalysis);
  }



  @PostMapping("/del")
  public ErrorInfo del(Long id) {
    secAnalysisService.deleteById(id);
    return ErrorInfo.ok();
  }

  @PostMapping("/save")
  public ErrorInfo save(SecAnalysis secAnalysis) {
    secAnalysisService.save(secAnalysis);
    return ErrorInfo.ok();
  }

  @PostMapping("/update")
  public ErrorInfo update(SecAnalysis secAnalysis) {
    secAnalysisService.update(secAnalysis);
    return ErrorInfo.ok();
  }
}
