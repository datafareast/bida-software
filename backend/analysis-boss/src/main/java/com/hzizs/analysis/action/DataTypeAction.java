package com.hzizs.analysis.action;


import javax.annotation.Resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hzizs.analysis.entity.DataType;
import com.hzizs.analysis.query.DataTypeQuery;
import com.hzizs.analysis.service.DataTypeService;
import com.hzizs.pojo.ErrorInfo;
import com.hzizs.vo.Container;
/**
 * 数据种类控制器
 * @author crazy_cabbage
 *
 */
@RestController
@RequestMapping("/dataType")
public class DataTypeAction {
	@Resource
	private DataTypeService dataTypeService;

	@GetMapping("/list")
	public ErrorInfo list(DataTypeQuery query ) {
		Container<DataType> container = dataTypeService.findContainer(query);
		return ErrorInfo.ok(container);
	}

	@GetMapping("/find")
	public ErrorInfo find(Long id) {
		DataType dataType=dataTypeService.findById(id);
		return ErrorInfo.ok(dataType);
	}

 

	@PostMapping("/del")
	public ErrorInfo del(Long id) {
		dataTypeService.deleteById(id);
		return ErrorInfo.ok();
	}

	@PostMapping("/save")
	public ErrorInfo save(DataType dataType) {
		dataTypeService.save(dataType);
		return ErrorInfo.ok();
	}

	@PostMapping("/update")
	public ErrorInfo update(DataType dataType) {
		dataTypeService.update(dataType);
		return ErrorInfo.ok();
	}
}
