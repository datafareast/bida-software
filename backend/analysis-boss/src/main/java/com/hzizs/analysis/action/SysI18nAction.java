package com.hzizs.analysis.action;


import javax.annotation.Resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hzizs.analysis.entity.SysI18n;
import com.hzizs.analysis.query.SysI18nQuery;
import com.hzizs.analysis.service.SysI18nService;
import com.hzizs.pojo.ErrorInfo;
import com.hzizs.vo.Container;
/**
 * 系统国际化控制器
 * @author crazy_cabbage
 *
 */
@RestController
@RequestMapping("/sysI18n")
public class SysI18nAction {
	@Resource
	private SysI18nService sysI18nService;

	@GetMapping("/list")
	public ErrorInfo list(SysI18nQuery query ) {
		Container<SysI18n> container = sysI18nService.findContainer(query);
		return ErrorInfo.ok(container);
	}

	@GetMapping("/find")
	public ErrorInfo find(Long id) {
		SysI18n sysI18n=sysI18nService.findById(id);
		return ErrorInfo.ok(sysI18n);
	}

 

	@PostMapping("/del")
	public ErrorInfo del(Long id) {
		sysI18nService.deleteById(id);
		return ErrorInfo.ok();
	}

	@PostMapping("/save")
	public ErrorInfo save(SysI18n sysI18n) {
		sysI18nService.save(sysI18n);
		return ErrorInfo.ok();
	}

	@PostMapping("/update")
	public ErrorInfo update(SysI18n sysI18n) {
		sysI18nService.update(sysI18n);
		return ErrorInfo.ok();
	}
}
