package com.hzizs.analysis.action;


import javax.annotation.Resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hzizs.analysis.entity.SysFolder;
import com.hzizs.analysis.query.SysFolderQuery;
import com.hzizs.analysis.service.SysFolderService;
import com.hzizs.pojo.ErrorInfo;
import com.hzizs.vo.Container;
/**
 * 系统文件夹控制器
 * @author crazy_cabbage
 *
 */
@RestController
@RequestMapping("/sysFolder")
public class SysFolderAction {
	@Resource
	private SysFolderService sysFolderService;

	@GetMapping("/list")
	public ErrorInfo list(SysFolderQuery query ) {
		Container<SysFolder> container = sysFolderService.findContainer(query);
		return ErrorInfo.ok(container);
	}

	@GetMapping("/find")
	public ErrorInfo find(Long id) {
		SysFolder sysFolder=sysFolderService.findById(id);
		return ErrorInfo.ok(sysFolder);
	}

 

	@PostMapping("/del")
	public ErrorInfo del(Long id) {
		sysFolderService.deleteById(id);
		return ErrorInfo.ok();
	}

	@PostMapping("/save")
	public ErrorInfo save(SysFolder sysFolder) {
		sysFolderService.save(sysFolder);
		return ErrorInfo.ok();
	}

	@PostMapping("/update")
	public ErrorInfo update(SysFolder sysFolder) {
		sysFolderService.update(sysFolder);
		return ErrorInfo.ok();
	}
}
