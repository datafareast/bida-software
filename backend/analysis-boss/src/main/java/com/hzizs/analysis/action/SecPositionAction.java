package com.hzizs.analysis.action;


import java.util.Date;
import java.util.List;
import javax.annotation.Resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hzizs.analysis.entity.SecPosition;
import com.hzizs.analysis.query.SecPositionQuery;
import com.hzizs.analysis.service.SecPositionService;
import com.hzizs.pojo.ErrorInfo;
import com.hzizs.vo.Container;

/**
 * 持仓分析控制器
 * 
 * @author crazy_cabbage
 *
 */
@RestController
@RequestMapping("/secPosition")
public class SecPositionAction {
  @Resource
  private SecPositionService secPositionService;

  @GetMapping("/list")
  public ErrorInfo list(SecPositionQuery query) {
    Container<SecPosition> container = secPositionService.findContainer(query);
    return ErrorInfo.ok(container);
  }

  @GetMapping("/find")
  public ErrorInfo find(Long id) {
    SecPosition secPosition = secPositionService.findById(id);
    return ErrorInfo.ok(secPosition);
  }

  @GetMapping("/findPosition")
  public ErrorInfo findPosition(Date occDate) {
    List<SecPosition> secPositions = secPositionService.findByOccDate(occDate);
    return ErrorInfo.ok(secPositions);
  }

  @PostMapping("/del")
  public ErrorInfo del(Long id) {
    secPositionService.deleteById(id);
    return ErrorInfo.ok();
  }

  @PostMapping("/save")
  public ErrorInfo save(SecPosition secPosition) {
    secPositionService.save(secPosition);
    return ErrorInfo.ok();
  }

  @PostMapping("/update")
  public ErrorInfo update(SecPosition secPosition) {
    secPositionService.update(secPosition);
    return ErrorInfo.ok();
  }
}
