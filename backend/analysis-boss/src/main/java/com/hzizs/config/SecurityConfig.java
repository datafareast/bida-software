package com.hzizs.config;

import java.io.IOException;
import java.io.PrintWriter;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.ObjectPostProcessor;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.access.intercept.FilterSecurityInterceptor;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;

import com.hzizs.config.security.JwtAuthenticationFilter;
import com.hzizs.config.security.JwtLoginFilter;
import com.hzizs.config.security.SecurityAccessDecisionManager;
import com.hzizs.config.security.SecurityAccessDeniedHandler;
import com.hzizs.config.security.SecurityAuthenticationEntryPoint;
import com.hzizs.config.security.SecurityMetadataSource;
import com.hzizs.json.JsonUtil;
import com.hzizs.pojo.ErrorInfo;

@SuppressWarnings("deprecation")
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

  @Resource
  private UserDetailsService userDetailsService;

  @Resource
  private DataSource dataSource;
  @Resource
  private SecurityMetadataSource securityMetadataSource;
  @Resource
  private SecurityAccessDecisionManager securityAccessDecisionManager;
  @Resource
  private SecurityAccessDeniedHandler securityAccessDeniedHandler;
  @Resource
  private SecurityAuthenticationEntryPoint securityAuthenticationEntryPoint;

  @Bean
  public JwtAuthenticationFilter jwtAuthenticationFilter() throws Exception {
    return new JwtAuthenticationFilter(authenticationManager());

  }

  @Bean
  public JwtLoginFilter jwtLoginFilter() {
    JwtLoginFilter jwtLoginFilter = new JwtLoginFilter("/logon");
    return jwtLoginFilter;
  }

  @Bean
  public PasswordEncoder passwordEncoder() {
    return PasswordEncoderFactories.createDelegatingPasswordEncoder();
  }
  // 这些用不到了
  // @Bean
  // public RememberMeAuthenticationProvider rememberMeAuthenticationProvider() {
  // return new RememberMeAuthenticationProvider(SECRET);
  // }
  // @Bean
  // public PersistentTokenRepository persistentTokenRepository(){
  // JdbcTokenRepositoryImpl tokenRepository = new JdbcTokenRepositoryImpl();
  // // tokenRepository.setCreateTableOnStartup(true);
  // tokenRepository.setDataSource(dataSource);
  // return tokenRepository;
  // }
  //
  // @Bean("tokenBasedRememberMeServices")
  // public TokenBasedRememberMeServices tokenBasedRememberMeServices() {
  // TokenBasedRememberMeServices rememberMeServices = new TokenBasedRememberMeServices(SECRET,
  // userDetailsService);
  // rememberMeServices.setAlwaysRemember(false);
  // rememberMeServices.setCookieName("remember-me");
  // rememberMeServices.setParameter("rememberMe");
  // rememberMeServices.setTokenValiditySeconds(AbstractRememberMeServices.TWO_WEEKS_S);
  // return rememberMeServices;
  // }

  @Override
  protected void configure(AuthenticationManagerBuilder auth) throws Exception {
    auth.userDetailsService(userDetailsService).addObjectPostProcessor(new ObjectPostProcessor<DaoAuthenticationProvider>() {
      @Override
      public <Processor extends DaoAuthenticationProvider> Processor postProcess(Processor processor) {
        processor.setHideUserNotFoundExceptions(false);
        // Permette alla UsernameNotFoundException di arrivare al FailureHandler
        processor.setPasswordEncoder(NoOpPasswordEncoder.getInstance());
        return processor;
      }
    });
  }

  @Override
  protected void configure(HttpSecurity http) throws Exception {
    http.cors().and().csrf().disable();
    http.headers().frameOptions().sameOrigin().httpStrictTransportSecurity().disable();
    http.httpBasic().disable();
    http.formLogin(login -> login.loginPage("/login"));
    http.authorizeRequests(authorize -> authorize
        // 跨域预检请求
        // .mvcMatchers(HttpMethod.OPTIONS, "/**").permitAll().mvcMatchers(HttpMethod.GET,
        // "/**").permitAll()
        // 登录URL
        .mvcMatchers("/static/**", "/captcha", "/login", "/logon", "/error/**", "/i18n/findAll").permitAll()
        // 其他所有请求需要身份认证
        .anyRequest().authenticated());

    // 这个加了，不停创建新的SESSION 虽然没有和SecurityContext 关联，但是会产生很多垃圾
    // http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
    // JWT 不需要记住密码
    // http.rememberMe()
    // .rememberMeServices(tokenBasedRememberMeServices())
    // .tokenRepository(persistentTokenRepository())
    // .and()
    // .authenticationProvider(rememberMeAuthenticationProvider());
    // 退出登录处理器

    http.logout(logout -> logout.logoutUrl("/logout").logoutSuccessHandler(new LogoutSuccessHandler() {
      @Override
      public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication)
          throws IOException, ServletException {
        ErrorInfo errorInfo = new ErrorInfo();
        response.setCharacterEncoding("UTF-8");
        response.setContentType("application/json;charset=utf-8");
        response.setStatus(HttpServletResponse.SC_OK);
        PrintWriter pw = response.getWriter();
        pw.write(JsonUtil.toString(errorInfo));
        pw.flush();
      }

    }).invalidateHttpSession(true).deleteCookies("JSESSIONID", JwtAuthenticationFilter.COOKIE_NAME));
    // 访问控制时登录状态检查过滤器
    http.addFilterBefore(jwtLoginFilter(), UsernamePasswordAuthenticationFilter.class);
    http.addFilterBefore(jwtAuthenticationFilter(), UsernamePasswordAuthenticationFilter.class);
    FilterSecurityInterceptor filterSecurityInterceptor = new FilterSecurityInterceptor();
    filterSecurityInterceptor.setAccessDecisionManager(securityAccessDecisionManager);
    filterSecurityInterceptor.setSecurityMetadataSource(securityMetadataSource);
    filterSecurityInterceptor.setAuthenticationManager(authenticationManager());
    http.addFilterAfter(filterSecurityInterceptor, FilterSecurityInterceptor.class);
    http.exceptionHandling().authenticationEntryPoint(securityAuthenticationEntryPoint).accessDeniedHandler(securityAccessDeniedHandler);

  }

  @Bean
  @Override
  public AuthenticationManager authenticationManager() throws Exception {
    return super.authenticationManager();
  }

}
