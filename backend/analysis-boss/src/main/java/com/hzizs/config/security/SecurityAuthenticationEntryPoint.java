package com.hzizs.config.security;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import com.hzizs.json.JsonUtil;
import com.hzizs.pojo.ErrorInfo;

@Component("securityAuthenticationEntryPoint")
public class SecurityAuthenticationEntryPoint implements AuthenticationEntryPoint {

  @Override
  public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) throws IOException, ServletException {
    response.setCharacterEncoding("UTF-8");
    response.setContentType("application/json;charset=utf-8");
    response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
    PrintWriter pw = response.getWriter();
    ErrorInfo errorInfo = new ErrorInfo();
    // 401 没有权限
    errorInfo.setCode(HttpServletResponse.SC_UNAUTHORIZED);
    errorInfo.setMsg(authException.getMessage());
    pw.write(JsonUtil.toString(errorInfo));
    pw.flush();

  }

}
