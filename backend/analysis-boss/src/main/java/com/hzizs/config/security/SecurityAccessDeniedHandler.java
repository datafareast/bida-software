package com.hzizs.config.security;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.stereotype.Component;

import com.hzizs.json.JsonUtil;
import com.hzizs.pojo.ErrorInfo;

@Component("securityAccessDeniedHandler")
public class SecurityAccessDeniedHandler implements AccessDeniedHandler {

  @Override
  public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException accessDeniedException)
      throws IOException, ServletException {
    response.setHeader("Content-Type", "application/json;charset=utf-8");
    response.setStatus(HttpServletResponse.SC_FORBIDDEN);
    response.setCharacterEncoding("UTF-8");
    ErrorInfo errorInfo = new ErrorInfo();
    errorInfo.setCode(HttpServletResponse.SC_FORBIDDEN);
    PrintWriter pw = response.getWriter();
    pw.write(JsonUtil.toString(errorInfo));
    pw.flush();
  }

}
